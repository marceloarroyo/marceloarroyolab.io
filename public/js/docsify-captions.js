
// Simple docsify plugin to format captions for images, tables and listings
// You can write captions below of any media, table or code block elements
// by using the following syntax: <keyword> n: caption text.
// Example:
// Table 1: Docsify plugins list.
// Default keywords are given below
// Keywords are user configurable. This plugin adds to docsify object the
// following object by default.
//
//      window.$docsify = {
//          ...
//          captions: {
//              language: 'en',
//              'en': 'Table|Figure|Listing|Demo|Audio|Video',
//              'es': 'Tabla|Figura|Listado|Demo|Audio|Video'
//          }
//      }
//
// Captions are formatted: left part in bold, text in italics and the class
// "caption" is added.
// Identifiers (<keyword-n>) are added to previous elements for referencing
// 
// Author: Marcelo Arroyo

(function() {
     function docsifyCaptions(hook, vm) {
        // Called by docsify on each page load after new HTML has been appended
        // to the DOM
        hook.doneEach(function () {
            let captions = document.querySelectorAll('.caption');
            for (const caption of captions) {
                let element = caption.previousElementSibling;
                if (element && !element.id) {
                    const id = caption.id;
                    element.id = id.substring(0, caption.id.lastIndexOf('-'));
                }
            }
        });
    }

    // Add plugin to docsify's plugin array
    $docsify = $docsify || {};
    $docsify.plugins = [].concat(docsifyCaptions, $docsify.plugins || []);

    // captions default options
    const options = {
        language: 'es',
        'en': 'Table|Figure|Listing|Demo|Audio|Video',
        'es': 'Tabla|Figura|Listado|Demo|Audio|Video',
        style: 'text-align:center; margin-top:0; padding-top:0;'
    }
    const userOptions = window.$docsify.captions || {}
    // merge user options with default object
    window.$docsify.captions = Object.assign({}, options, userOptions)

    // build regular expression from keywords
    const lang = window.$docsify.captions.language
    const keywords = window.$docsify.captions[lang]
    const pattern = new RegExp(`^ *(${keywords}) ([^:]+): *(.*)$`, 'i')

    // extend docsify markdown (marked) paragraph renderer
    if (!window.$docsify.markdown) {
        window.$docsify.markdown = { renderer: {} }
    } else if (!window.$docsify.markdown.renderer) {
        window.$docsify.markdown.renderer = {}
    }
    window.$docsify.markdown.renderer.paragraph = function(text) {
        const match = text.match(pattern)
        if (match) {
            const id = match[1].toLowerCase() + '-' + match[2] + '-caption'
            const style = window.$docsify.captions.style
            return `<p id="${id}" class="caption" style="${style}">` +
                   `<b>${match[1]} ${match[2]}:</b> <em>${match[3]}</em></p>`
        }
        return this.origin.paragraph.apply(this, arguments)
    }
})();