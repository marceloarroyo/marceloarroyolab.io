if (!window.$docsify.markdown) {
    window.$docsify.markdown = { renderer: {} }
} else if (!window.$docsify.markdown.renderer) {
    window.$docsify.markdown.renderer = {}
}
window.$docsify.markdown.renderer.code = function(code, lang) {
    if (lang == "td2svg")
        return '<figure class="divcenter">\n' + 
                diagram2svg(code) + 
                "\n</figure>\n";
    return this.origin.code.apply(this, arguments);
}