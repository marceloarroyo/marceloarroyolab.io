(function () {
    var asciinemaPlugin = function (hook, vm) {
      
      // Invoked on each page load after new HTML has been appended to the DOM
      hook.doneEach(function () {
        const asciinemaElements = document.getElementsByClassName('asciinema');
        for (let e of asciinemaElements) {
            AsciinemaPlayer.create(e.dataset.value, e);
        }
      });

    };
  
    // Add plugin to docsify's plugin array
    $docsify = $docsify || {};
    $docsify.plugins = [].concat(asciinemaPlugin, $docsify.plugins || []);

    // add docsify markdown image renderer to generate an asciinema element
    $docsify.markdown.renderer.image = function(href, title, text) {
      if (href.endsWith('.cast')) {
          // generate div for asciinema player
          console.log(`rendering asciinema ${href}`);
          const attrs = `id=${text} data-value="${href}"`;
          let html = `<div ${attrs} class="asciinema"></div>\n\n`;
          return html + `<p>${title}</p>\n`;
      }
      return this.origin.image.apply(this, arguments);
    }
  })();