// dalta: 1=next, -1=previous 
function goToSlide(delta) {
    let current = document.querySelector('ul.app-sub-sidebar > li.active')
    let next = delta > 0 ? current.nextSibling : current.previousSibling
    if (next) {
        let anchor = next.firstElementChild
        anchor.click()
    }
}