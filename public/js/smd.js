function htmlRenderer(counters, refLinks) {
  return {
    newScope: () => htmlRenderer(this.links),

    html: '',

    links: refLinks || {},

    addLink: function (key, url, title) {
        this.links[key] = {url: url, title: title}
    },

    beginCodeBlock: function (lang, code) {
        this.html += `<pre><code class="language-${lang}">\n${code}\n`
    },

    appendCode: function(text) {
        this.html += '\n\n' + text
    },

    endCodeBlock: function(code) {
        this.html += '\n' + code + '\n</code></pre>\n'
    },

    header: function(level, text, id) {
        const idAttr = id ? `id="${id}"` : ''
        const txt = this.inline(text)
        this.html += `<h${level} ${idAttr}>${txt}</h${level}>\n`
    },

    openItems: function(type, startN) {
        const start = (type == 'ol' && startN) ? ` start="${startN}"` : ''
        this.html += `<${type}${start}>\n`
    },

    closeItems: function(type) {
        this.html += `</li>\n</${type}>\n`
    },

    item: function(text, cls, closePrevious) {
        const closer = closePrevious ? '</li>\n' : ''
        const txt = this.inline(text)
        if (cls.length > 1) cls = ` class="${cls}"`
        this.html += `${closer}<li${cls}>${txt}\n`
    },

    blockquote: function(text, cls, cite) {
        const txt = this.inline(text)
        cls = cls ? ` class="${cls}"` : ''
        this.html += `<blockquote ${id}${cls}>\n${txt}\n`
        if (cite)
            this.html += `<cite>${cite}</cite>\n`
        this.html += '</blockquote>\n'
    },

    // image as block element
    figure: function(alt, url, attrs) {
        const match = alt.match(/^([^:]+): /)
        const id = match ? (' id="' + match[1].replace(' ', '-') + '"') : ''
        this.html += `<figure${id}>\n`
        this.html += `<img alt="${alt}"src="${url}"${attrs}>${alt}</img>\n`
        this.html += `<figcaption>${alt}</figcaption>\n`
        this.html += `</figure>\n`
    },

    table: function(rows) {
        const style = 'style="text-align: '
        const align = []
        this.html += `<table>\n  <tr>\n`
        for (let j=0; j < rows[0].length; j++) {
            const h = this.inline(rows[0][j])
            let a = 'left'
            if (rows[1][j].endsWith(':')) a = 'right'
            if (a == 'right' && rows[1][j].startsWith(':')) a = 'center'
            align.push(style + a + ';"')
            this.html += `    <th ${align[j]}>${h}</th>\n`
        }
        this.html += '  </tr>\n'
        for (let i = 2; i < rows.length; i++) {
            this.html += '  <tr>\n'
            for (let j=0; j<rows[i].length; j++) {
                const d = this.inline(rows[i][j].trim())
                this.html += `    <td ${align[j]}>${d}</td>\n`
            }
            this.html += '  </tr>\n'
        }
        this.html += '</table>\n'
    },

    hr: function(text) {
        let cls = 'hr-minus'
        if (text[0] == '=') cls = 'hr-eq'
        else if (text[0] == '*') cls = 'hr-star'
        this.html += `<hr class="${cls}">\n`
    },

    paragraph: function(text) {
        this.html += '<p>\n' + this.inline(text) + '\n</p>\n'
    },
    
    inlineCode: [],

    inline: function(text) {
        let inLineCodeRE = /[`~]([^`~]+)[`~]/gm
        let imgRE = /\!\[([^\]]*)\]\(([^\) ]+) ?([^\)]*)\)/gm
        let boldRE = /\*\*([^\*]+)\*\*/gm
        let emRE = /[\*_]([^\*_]+)[\*_]/gm
        let linkRE = /\[([^\]]*)\]\(([^\) ]+) ?"?([^"\)]*)"?\)/gm
        let refLinkRE = /\[([^\]]*)\]\[([^\]]+)\]/gm
        let urlRE = /<((?:mailto|https?|ftp):(?:[^\s>]+))>/gm
        let that = this

        return text.
            // protect inline code in this.inlineCode[] array
            replaceAll(inLineCodeRE, (_,c) => {
                const count = that.inlineCode.length
                that.inlineCode.push(c)
                return `{code-${count}}`
            })
            .replaceAll(imgRE, (_, alt, url, attrs) => {
                const id = alt.replace(' ', '-')
                return `<img id="${id}" alt="${alt}" src="${url}" ${attrs}>`
            })
            .replaceAll(boldRE, '<b>$1</b>')
            .replaceAll(emRE, '<em>$1</em>')
            .replaceAll(linkRE, (_, text, url, title) => {
                title = title ? ` title="${title}"` : ''
                return `<a href="${url}"${title}>${text}</a>`
            })
            .replaceAll(refLinkRE, (_, text, key) => {
                const ref = that.links[key]
                const url = ref ? ref.url : '#'
                const title = ref.title ? ` title="${ref.title}"` : ''
                return `<a href="${url}"${title}>${text}</a>`
            })
            .replaceAll(urlRE, '<a href="$1">$1</a>')
    },

    result: function () {
        // replace protected inline code by saved content in this.inlineCode[]
        for (let i=0; i < this.inlineCode.length; i++) {
            const code = this.inlineCode[i]
            this.html = this.html.replace(`{code-${i}}`, `<code>${code}</code>`)
        }
        // render footnotes
        for (const key of Object.keys(this.links)) {
            let n = 1
            if (key.startsWith('^')) {
                this.html += `<p>${n++}. ${this.links[key].text}</p>`
            }
        }
        return this.html
    }
  }
}

// Markdown parser
function parseMarkdown(text, renderer) {
    // block elements
    let refLinkRE = /^\[(.+)\]\: ([\S]+) ?["(']?([^"')]*)["')]?/gm
    let imgRE = /^\!\[([^\]]*)\]\(([^\) ]+) ?([^\)]*)\)$/
    let headerRE = /^(#{1,6}) /
    let itemRE = /^ *([-*+]|\d+[\.\)]) /
    let bqRE = /^> /
    let codeRE = /^\s*[`~]{3,} *(.*)\n/
    let endCodeRE = /\n\s*[`~]{3,} *$/
    let hrRE = /^[-=*]{3,}\s*$/

    // state
    let inCode = false
    let itemsCtx = [{indent: 0}]
    let indent = 0

    // replace tabs for four spaces
    text = text.replaceAll(/\t/gm, '    ')

    // separate items into different paragraphs
    text = text.replaceAll(/^\n\r?(\s*)([-*+]|\d+[\.\)]) /gm, '\n\n$1$2 ')

    // split paragraphs
    const blocks = text.trim().split(/(\n\r?\s*\n\r?)+/);

    if (!renderer) renderer = htmlRenderer()

    // collect and delete reflinks (and footnotes) from text
    text = text.replace(refLinkRE, (_, key, url, title) => {
        renderer.addLink(key, url, title)
        return ''
    })

    for (let block of blocks) {
        // skip blank blocks
        if (block.match(/^ *$/m)) continue

        console.log(block)

        // if we are processing a codeblock, collect it
        if (inCode) {
            let end = block.match(endCodeRE)
            const lsp = ' '.repeat(indent)
            if (end) {
                const code = block.substring(0, end.index).replace(lsp, '')
                renderer.endCodeBlock('\n\n' + code)
                inCode = false;
            } else
                renderer.appendCode(block.replace(lsp, ''));
            continue
        }

        // close items
        indent = block.match(/^ *./)[0].length - 1
        let ctx = itemsCtx[itemsCtx.length - 1]
        const isItem = block.match(itemRE)
        if (isItem) indent = isItem[0].length
        while (indent < ctx.indent) {
             renderer.closeItems(ctx.item)
             itemsCtx.pop()
             ctx = itemsCtx[itemsCtx.length-1]
        }

        // item
        if (isItem) {
            let closePrevious = false
            let cls = ''
            if (indent > ctx.indent) {
                const type = isItem[1] == '-' ? 'ul' : 'ol'
                let n = null
                if (isItem[1][0] == '*') cls = 'item-star'
                else if (isItem[1][0] == '+') cls = 'item-plus'
                if (type == 'ol') {
                    n = isItem[1].substring(0, isItem[1].length-1)
                }
                itemsCtx.push({indent: indent, item: type})
                renderer.openItems(type, n)
            } else
                closePrevious = true
            renderer.item(block.substring(isItem[0].length), cls, closePrevious)
            continue
        }

        // we can trim block from here (we already got indentation)
        block = block.trim()

        // code block
        let match = block.match(codeRE)
        if (match) {
            const lang = match[2]
            const  end = block.match(endCodeRE)
            const  lsp = ' '.repeat(indent)
            const  endIndex = end ? end.index : block.length
            const code = block.substring(match[0].length, endIndex)
                              .replace(lsp, '')
            renderer.beginCodeBlock(lang, code)
            if (!end) inCode = true
            if (end) renderer.endCodeBlock(code) 
            continue
        }

        // headers
        match = block.match(headerRE)
        if (match) {
            const level = match[1].length
            const id = block.match(/\{(.*)\} *$/)
            const end = id ? id.index : block.length
            const text = block.substring(match[0].length, end).trim()
            renderer.header(level, text, id ? id[1] : null)
            continue
        }

        // blockquote
        match = block.match(bqRE)
        if (match) {
            let txt = block.replaceAll(/^ *> */gm, '')
            const cls = txt.match(/^\*\*([^\:]+)\:\*\*/)
            const c = cls ? cls[1].toLowerCase() : null
            const citation = txt.match(/-- .*$/)
            let cite = null
            let r = renderer.newScope(renderer.counters, renderer.refLinks)
            if (citation) {
                cite = citation[0]
                txt = smd(txt.substring(0, citation.index), r)
            } else {
                txt = smd(txt, r)
            }
            renderer.blockquote(txt, c, cite)
            continue
        }

        // image (alone in block)
        match = block.match(imgRE)
        if (match) {
            renderer.figure(match[1], match[2], match[3] || '')
            continue
        }
        
        // html tag
        if (block.startsWith('<') && block.endsWith('>')) {
            renderer.append(block)
            continue
        }

        // hr
        if (match = block.match(hrRE)) {
            renderer.hr(match[0])
            continue
        }

        // table
        const lines = block.split('\n')
        if (lines.length > 2 && lines[1].match(/^\s*\:?-{3,}/)) {
            let rows = []
            let isTable = true
            for (let i=0; i<lines.length; i++) {
                let row = lines[i]
                let cols = row.split(' | ')
                for (let c = 0; c < cols.length; c++)
                    cols[c] = cols[c].trim()
                if (i == 1 && cols.length != rows[0].length) {
                    isTable = false
                    break
                }
                rows.push(cols)
            }
            if (isTable) {
                renderer.table(rows)
                continue
            }
        }

        // paragraph
        renderer.paragraph(block)
    }
    // close items
    while (itemsCtx.length > 1) {
        const ctx = itemsCtx.pop()
        renderer.closeItems(ctx.type)
    }

    return renderer.result()
}

// load markdown text from files into elements with given class (default
// 'markdown') and generate html inside. The tag must to define the
// data-markdown-file="filepath" attribute with the path to markdown file
function renderMarkdownInTags(className) {
    if (!className) className = 'markdown';
    let containers = document.getElementsByClassName(className);
    for (let elem of containers) {
        const file = elem.dataset.markdownFile
        if (file) {
            fetch(file)
            .then(response => response.text())
            .then(text => elem.innerHTML = parseMarkdown(text))
        }
    }
}

// load markdown content and store it in localStore (for offline working)
function loadAndRenderMarkdown(file, htmlElement) {
    if (location.href.startsWith('file:')) {
        const reader = new FileReader()
        reader.addEventListener('load', (event) => {
            htmlElement.innerHTML = parseMarkdown(event.target.result)
        })
        reader.readAsText(file)
    } else {
        try {
            fetch(file)
            .then(response => response.text())
            .then(text => {
                const html = parseMarkdown(text)
                htmlElement.innerHTML = html
            })
        } catch(error) {
            htmlElement.innerHTML = html || '<p>Error</p>'
        }
    }
}