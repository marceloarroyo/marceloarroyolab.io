

# Criptografía

La criptografía se remonta a tiempos de antes de Cristo. Su empleo siempre ha
sido para ocultar mensajes en otros, tal que quienes conocen los secretos de
descifrado o interpretación sólo pueden comprenderlos.

> [!NOTE|label:Definición]
> ***Criptografía***: *Escritura secreta*. El **criptoanálisis** es el estudio
> de las *debilidades* de un código criptográfico.

A lo largo de la historia se han desarrollado numerosos dispositivos y técnicas
para cifrar y descifrar mensajes.

Las guerras han motivado a que los militares impulsaran investigaciones para el
desarrollo de esta disciplina.

A modo de ejemplo, la famosa máquina
[Enigma](https://en.wikipedia.org/wiki/Enigma_machine) fue desarrolladas y
utilizada en la segunda guerra mundial por los alemanes y sus aliados para el
cifrado de las comunicaciones transmitidas telegráficamente.

Con la aparición de las computadoras se profundizó el estudio del desarrollo y
análisis de sistemas criptográficos para ser implementados en hardware y software.

Un ***Sistema criptográfico*** tiene los siguientes componentes:

- $P$: Alfabeto de los ***textos planos***
- $C$: Alfabeto de los ***textos cifrados***
- $K$ es un conjunto de ***claves***
- Funciones de cifrado: $E: P \times K \rightarrow C$
- Funciones de descifrado: $D: C \times K \rightarrow P$

## Ejemplo: El código César

El código César se debe al emperador Julio César que supuestamente usaba el
siguiente esquema criptográfico. La idea es reemplazar en un mensaje de texto
plano cada caracter por otro que le sigue *k* lugares en un alfabeto ordenado de
la forma $a=0, b=1, ...$.

El secreto o *clave* es *k*: el *desplazamiento* que determina el caracter usado
en la sustitución.

En este sistema, se podría establecer que

- $P=C$: Letras del alfabeto. *n=#P*
- $K=\{k=3\}$
- $E=(p_0.p_1...p_m,k) = c_0.c_1...c_m$ con $c_i=(p_i+k) \: mod \: n$

    $E("hola",k) = "krod"$

El algoritmo de descifrado es obvio y se deja como ejercicio.

## Diseño de sistemas criptográficos

En los comienzos de la criptografía no se conocía el algoritmo. En el caso del
código César podría decirse que se desconocía no sólo $k$ sino también el
proceso de cifrado y descifrado.

Otros intentos en hacer sistemas criptográficos con algoritmos secretos no
funcionaron. El problema es que cuando los algoritmos se revelan por alguna
razón, todo el sistema deja de tener sentido.

Uno de los ejemplos más paradigmáticos fue el sistema de cifrado de DVDs,
conocido como el *Content Scramble System (CSS)* que utilizaba un algoritmo
secreto de cifrado. Un grupo de programadores fue capaz de desarrollar el
programa [DeCSS](https://en.wikipedia.org/wiki/DeCSS) a partir del desensamble
de un programa de cifrado de un reproductor de un DVD comercial.

> Un sistema criptográfico debería basarse en que el ***único secreto*** es un >
> subconjunto de claves.
> Lo demás debería ser ***público*** (principio de Kerckhoffs)

El hecho que un sistema criptográfico sea público es que puede someterse al
análisis público de espcialistas en análisis criptográficos.

Existen varios tipos de análisis de ataques a los sistemas criptográficos:

1. **Ataque por fuerza bruta**: Probar con *todas* las claves posibles.
2. ***Cibertext***: El atacante conoce sólo los textos cifrados. Objetivo:
   obtener los textos planos correspondientes.
3. ***Plaintext known***: Tiene pares $(p,c)$, desconoce $k$
4. ***Choosen plaintext***: El atacante puede cifrar textos con claves elegidas

## Criptografía simétrica

Estos sistemas se basan en secuencias de operaciones de *sustitución* y
*transposición* de símbolos (bits).

- *Sustitución*: Reemplazar símbolos por otro (del mismo u otro alfabeto).
- *Transposición*: Intercambio de posiciones de símbolos.

Estos sistemas tienen las siguientes características

- Usan la misma clave $k$ para cifrar y descifrar: $c=E(p,k), p=D(c,k)$
- $k$ secreta:  Sólo conocida por los participantes
- Propiedades:
  1. Algoritmos eficientes, implementables en hardware
  2. Problema: Distribución de claves (secretos)

Previo a las computadoras se desarrollaron máquina electro-mecánicas de
cifrado/descrifrado como las máquinas de rotores como la Enigma. Usada en la
segunda guerra mundial. Estas máquinas se basan en estos conceptos.

### Cifrado XOR (aditivo)

Un operador comúnmente usado para sustitución de símbolos es el *o-exclusivo*
definido por los siguientes casos:

$0 \oplus 0 = 0$, $0 \oplus 1 = 1$, $1 \oplus 0 = 1$, $1 \oplus 1 = 0$

Este operador sobre bits tiene las siguientes propiedades.

1. $A \oplus 0 = A$
2. $A \oplus A = 0$
3. $A \oplus B = B \oplus A$
4. $(A \oplus B) \oplus C = A \oplus (B \oplus C)$
5. $(B \oplus A) \oplus A = B \oplus 0 = B$

Un uso común de este operador como algoritmo de cifrado/descifrado es del 
***One-time pad*** que se basa en que para cada mensaje $M$ se cifra con una
clave $K$ de la misma longitud de bits: $M \oplus K$.

Es teóricamente _inquebrable_ si se cumple:

1. La clave $K$ debe ser de longitud $\ge$ que el mensaje
2. $K$ debe ser ***aleatoria*** (*independiente* del mensaje y *uniformemente 
   distribuida*)
3. $K$ nunca debe ser re-usada
4. $K$ debe mantenerse *secreta* entre las partes

Esto hace que este esquema sea difícil de usar en la práctica tanto por la
longitud de la clave a usar como su verdadera aleatoriedad y además no deberá
usarse nunca más en el futuro.

Si se reusara una clave, podrían descifrarse mensajes basados en la recopilación
de mensajes anteriores y aplicando la propiedad 4 se podría obtener un texto
plano.

Esta técnica es la base de varios algoritmos para generar \emph{one time pads}.

### Data Encription Standard (DES)

Este estándar, hoy obsoleto y su uso está desaconsejado, fue desarrollado en la
década de 1970. Tiene las siguientes características.

- Clave ($k$) de 64 bits. Opera sobre bloques de 64 bits.
- Aplica 16 rondas (iteraciones) donde en cada ronda se realiza como se muestra
  en la siguiente figura

![Figura 1](img/DES.png)

Figura 1: Esquema de DES.

Los bloques *PI* y *PF* realizan la permutación inicial y final,
respectivamente. Estos son inversos entre sí.

En cada etapa el bloque *F* (*Feistel block*) toma como entrada el bloque de
salida de la etapa anterior de 64 bits. Los 32 bits menos significativos se usan
para generar una *subclave* de 48 bits (replicando algunos de sus bits) y se
hace un *o-exclusivo* con los 32 bits más significativos. Luego se aplican
sustituciones en base a 8 *S-boxes* operando en paralelo que toman 6 bits y
generan 4 bits generando una salida de 32 bits a la que se aplica una
permutación *P*. Estos 32 bits pasan a ser los menos significativos y los 32 de
la subclave pasan a ser los más significativos de la etapa siguiente.

En 1990 se encuentra un ataque efectivo, por lo que se considera actualmente *quebrado*.

DES siguió utilizándose, aunque se considera vulnerable y obsoleto por el National Institute of Standards and Technology (*NIST*) en 2017, usando una
*composición* de tres etapas, conocido como ***riple DES (3-DES)***.

Se aconseja usar 3-DES con tres claves diferentes.

- $c = E_{k3}(D_{k2}(E_{k1}(p)))$
- $p = D_{k1}(E_{k2}(D_{k3}(c)))$

### American Escription Standard (AES)

Como respuesta a los ataques criptográficos de DES, el NIST lanzó una nueva
convocatoria para desarrollar un nuevo estándar. El ganador fue el algoritmo
conocido como Rijndael o AES y tiene las siguientes características.

- Adoptado como estándar por el NIST en 2001
- Claves (bloques) de 128, 192 y 256 bits
- 10, 12 y 14 rondas (pasadas) a cada bloque, respectivamente
- Cada ronda realiza siguientes pasos (entre otros):
  1. *Key expansion*
  2. *Subbytes*: Sustitución no lineal
  3. *Shiftrows*: Transposición
  4. *MixColumns*: Mezclado lineal

### Modos de operación (cifrados por bloques)

Estos sistemas criptográficos operan sobre bloques de la longitud de la clave. 

Cuando hay que cifrar datos con longitud mayor que la clave se aplican
diferentes métodos conocidos como *modos de operación*.

- ***Electronic Codeblock (ECB)***: Se aplica a cada bloque de manera 
  independiente. ***No usar!!!***

- ***Cipher Block Chaining (CBC)***: Cada bloque se cifra en base a la salida 
  del bloque anterior: $c_i=E(b_{i_1} \oplus c_{i-1})$. $c_0$ se llama
  *initialization vector (iv)*
  
- ***Counter mode***: Para cada bloque $i$ se genera su *iv_i* en base a un *iv*
  inicial y el número de bloque $i$. El bloque de salida es $c_i=b_i \oplus
  iv_i$.
  
Cabe aclarar que estos algoritmos pueden ejecutarse de manera muy eficiente.
También existe hardware especial o cpus con instrucciones específicas para
algunas de las operaciones utilizadas en los algoritmos de criptografía simétrica.

### HASH criptográfico

Una función de ***hash criptográfico***, ***crypographic checksum*** o ***huella
digital*** es una función de la forma $h: A \rightarrow B$ con las siguientes propiedades:

1. $B$ es *finito*: $B=\{m \mid length(m)=k\}$
2. $h(x)$ es eficiente (para cualquier $x \in A$)
3. $\forall y \in B$, es *muy difícil* encontrar $x \in A$ tal que $h(x)=y$
4. Dado $h(x)=y$, es *muy difícil* encontrar $x' \neq x$ tal que $h(x')=y$

Algunos de estos algoritmos mas usados son MD5, SHA-1 o SHA-2.

Se debe notar que estas funciones no tiene inversas por lo que no pueden
considerarse funciones de cifrado ya que no hay posibilidad de descifrar.

Su uso principal es para obtener una *huella* de un documento o mensaje para
luego poder probar su integridad, es decir verificar si fue modificado o no.

Es común que los sitios que permiten descargar documentos o paquetes de software
provean el *hash* correspondiente para que los usuarios puedan verificar la
*integridad* del archivo descargado generando el hash y comparándolo con el
provisto en el sitio.

### Message Authentication Code

Su uso es para proveer *autenticación de origen* comúnmente en sistemas de
comunicaciones. Se basa en el uso de la generación de un *tag* o valor que
depende del mensaje (o documento) y una clave (secreta) $k$ compartida entre las partes.

- Un ***MAC*** es un *tag* que puede _anexarse_ a un mensaje para su
  *autenticación*
- $mac(k,m) \rightarrow tag$

Un receptor de un mensaje de la forma $(m,t)$ puede *verificar* el origen de *m*
si $mac(k,m) = t$.

Una función ***HMAC*** es una función ***MAC*** que usa un *hash criptográfico* para generar el *tag*.

### Gestión de claves

COn estos sistemas criptográficos hay que *distribuir secretos (claves)* en
forma segura. Comúnmente se usan diferentes tipos de claves.

- Una *clave de intercambio* se asocia a un *sujeto* en una comunicación
- Una *clave de sesión* es una clave compartida entre las partes intervinientes

Surge la necesidad de usar protocolos de intercambio de claves y autenticación.
Uno de los principales enfoques de estos protocolos es usar ***un tercero de confianza***.

### Intercambio de claves

Los sujetos $A$ y $B$ tiene sus propias claves secretas $k_A$ y $k_B$
respectivamente. Las claves también son conocidas también por $C$ (el *llavero* de confianza).

El llavero podría generar una *clave de sesión* y compartirla con $A$ y $B$:

1. $A \rightarrow C$: $E("I want a session key with B", k_A)$
   
   A envía a C un mensaje cifrado con su clave secreta solicitando una clave de
   sesión para comunicarse con B.

2. $C \rightarrow A$: $(E(<k_s, E(k_s, K_B)>, k_A)$

   C responde con la clave de sesión generada y la clave de sesión cifrada con
   la clave de B. Todo este contenido va cifrado con la clave a A.

3. $A \rightarrow B$: $E(k_s, k_B)$

   A envía a B la segunda parte del mensaje recibido de C: La clave de sesión
   cifrada con la clave de B.

4. $A$ y $B$ intercambian mensajes cifrados usando $k_s$ (clave de sesión)

Este protocolo tiene un problema: Reply attack (man-in-the-middle)
    
Un atacante (sea $E$)

1. Intercepta el mensaje 3 obteniendo $E(k_s, k_B)$
2. Luego, intercepta mensajes $E(m, k_s)$ y *reenvía* $(m', k_s)$

$A$ (o $B$) no puede asegurar que los mensajes vienen de $B$ (o $A$).

El protocolo de *Needham-Schroeder* resuelve el problema anterior.

1. $A \rightarrow C$: $A,B,N_A$
   $N_A$: *nounce* generado por $A$
2. $C \rightarrow A$: $\{N_A,\{k_{AB},A\}_{k_B}\}_{k_A}$
   ($k_{AB}$: *clave de sesión para* $A$ y *B* generada por $C$
3. $A \rightarrow B$: $\{k_{AB},A\}_{k_B}$
4. $B \rightarrow A$: $\{N_B\}_{k_{AB}}$
   $N_B$: *nounce* generado por $B$
6. $A \rightarrow B$: $\{N_B - 1\}_{k_{AB}}$

El sistema *Kerberos* (usado en MS-Windows y otros sistemas) se basa en el
protocolo Needham-Schroeder y usa los siguientes componentes:


1. *Servidor de autenticación*
2. *Servicios (service providers)*

Se basa en que los sujetos que desean acceder a un *servicio* $S$, se deben
autenticar en el servidor de autenticación el cual otorga *tickets*
(credenciales) de acceso para cada servicio

## Intercambio de claves sin un tercero de confianza 

En 1976 W. Diffie y M. Hellman publicaron un artículo que describe un protocolo
de intercambio de claves en forma segura. Se basa en un método matemático y
consiste en que dados dos valores $g$ y $p$, donde $p$ es un número primo y $g$
es una *raíz primitiva módulo p* es posible computar un valor secreto entre dos
sujetos $A$ y $B$ sin transmitirlo.

> [!NOTE|label:nota]
> En aritmética modular, un número $g$ es una *raíz primitiva módulo n* si para
> todo número *a*, *coprimo de n* existe *k* tal que $g^k = n$.

A continuación se analiza el algoritmo con un ejemplo:

1. *A* y *B* acuerdan usar $g=5$ y $p=23$
2. *A* elige (un random) $1 \leq a \leq p$. Sea $a=4$ y envía a $B$ $A = g^a \:
   mod \: p$. $A = 4$.
3. *B* hace lo mismo, elige $b=3$ y envía a $A$ $B = g^b \: mod \: p$
4. *A* calcula $s = B^a \: mod \: p$. $s = 18$
5. *B* calcula $s = A^b \: mod \: p$. $s = 18$

Los sujetos *A* y *B* computaron el mismo valor ($s$) ya que

$$A^b \: mod \: p = g^{ab} \: mod \: p = g^{ba} \: mod \: p = B^a \: mod \: p$$

Los valores $g$ y $p$ no necesitan ser secretos. Así un atacante conoce $g$,
$p$, $A$ y $B$. Para computar $s$ requiere $a$ o $b$ lo cual es un $x$ tal que
$g^x \: mod \: p$. Esto requiere que debe computar $log_g (g^x \: mod \: p)$.

Este problema se conoce como el cálculo de *logaritmos discretos* y no se
conocen algoritmos eficientes para computarlos. Los mejores algoritmos conocidos
son *NP-Hard*.

En este esquema es posible determinar que el par $(g,p)$ conforman la *clave
pública* y *a* y *b* son las *claves secretas* de *A* y *B* respectivamente,


### Criptografía asimétrica

El trabajo de Diffie-Hellman sentó las bases de la *criptografía de clave
pública-privada* o *criptografía asimétrica*.

Estos sistemas criptográficos se basan en métodos matemáticos y en la dificular
de computar algún valor para que un atacante pueda lograr quebrar el sistema.

Estos sistemas contienen los siguientes componentes:

1. Una función de generación de claves $keygen: \rightarrow (pk,sk)$, donde $pk$
   es la *clave pública* y $sk$ es la *clave privada*
2. Cifrado/descifrado:
   - $E(pk,m)=c$ entonces $D(sk,c)=m$
   - $E(sk,m)=c$ entonces $D(pk,c)=m$

Existen muchos algoritmos ampliamente usados en la actualidad como
***ElGamal***, basado en Diffie-Hellman y ***RSA***, ropuesto en 1977 por
Rivest, Shamir y Adleman y otros.

Las ventajas de estos métodos numéricos es que ante los avances del poder de
cómputo de la tecnología sólo es necesario aumentar el tamaño de las claves. Un
atacante al tener que usar algoritmos de quiebre que tienen complejidad
exponencial por lo que ante un pequeño aumento del tamaño de las claves, crece
exponencialmente la complejidad de un ataque.

### RSA

Este algoritmo se basa en el problema de *factorización de valores enteros
grandes*. Dado un valor $n=p \times q$ no se conocen algoritmos eficientes para
computar $p$ y $q$.

El algoritmo RSA se basa en los siguientes componentes.

***Generación de claves***:

- Sean $p,q$ dos números *primos* y $n=p \times q$ (el *módulo*)
- $\Phi(n)=(p-1)\times(q-1)$
- $gekgen(n)$:
  1. Elegir $e<n$ tal que $e$ es *primo relativo a* $n$
  2. Elegir $d$ tal que $(e \times d) \: mod \: \Phi(n) = 0$
  3. *Clave pública*: $pk=(e,n)$
  4. *Clave privada*: $sk=d$

***Función de cifrado/descifrado***

- Cifrado: $E(m,k_1) = m^{k_1} \: mod \: n = c$
- Descifrado $D(c,k_2) = c^{k_2} \: mod \: n = m$

donde $k_1=e$ (la clave pública) y $k_2=d$ (la clave privada), o $k_1=d$ y
$k_2=e$. Es decir que si se cifra un mensaje con la clave pública se puede
descifrar con la privada y viceversa.

> La seguridad de RSA se basa en que el mejor ataque es computar la inversa de
> $E$ factorizando $n$ para obtener $p$ y $q$ y no se conoce un algoritmo
> eficiente de factorización.

## Aplicaciones de la criptografía asimétrica

Los algoritmos de criptografía de clave pública-privada pueden usarse para
implementar las siguientes políticas de seguridad.

- *Confidencialidad*: *A* envía un mensaje *m* cifrado con la *clave pública de
  B*. Sólo *B* lo puede descifrar.

- *Autenticación*: 

  1. *A* envía a *B* un valor *m* cifrado con la clave pública de *B*
  2. *B* debe responder con un mensaje *f(m)*, donde *f* es acordada por las 
     partes.

- *Intercambio seguro (confidencial) de claves*:

  - Por Diffie-Hellman, o
  - con criptografía asimétrica:

    1. *A* genera una *pre-master-key pmk*.
    2. *A* envía a *B* el mensaje $E(B_{pk}, pmk)$.
    3. Ambos generan la *clave de sesión* usando algún algoritmo acordado desde
       la *pmk*. 

Cabe notar que el uso de estos sistemas criptográficos para cifrar grandes
volúmenes de datos es muy costoso desde el punto de vista computacional, por lo
que su uso comúnmente se reduce a autenticación o intercambiar claves de sesión
de manera segura para luego usar criptografía simétrica para cifrar la
comunicación o los datos.

## Firma digital (digital signature)

Usando hashes criptográficos y criptografía de clave pública/privada es posible
garantizar *integridad* y *no repudio de origen* en mensajes o documentos.

Un sujeto *A* puede *firmar* un mensaje o documento *m* haciendo

1. $A$: genera la *firma* un mensaje *m*: $sign_m=E(hash(m),A_{sk})$.
2. Un *documento firmado* tiene la forma $(m,A_{pk},sign_m)$, es decir, el
   documento o mensaje *m* junto con la clave pública de *A* y la *firma*.

   Se dice que la firma está *desvinculada* del documento o mensaje cuando se
   envían por separado.
   
Un sujeto *B* al recibir un documento $(m,A_{pk},sign_m)$ puede *verificar de
firma* haciendo:

1. Obtener el *hash* descifrando la firma con la clave pública de *A*: 
   $h = (sign_m, A_{pk})$
2. Verificar que $h = hash(m)$

Esto prueba dos cosas:

1. Que $m$ no fue alterado luego de ser firmado. Esta es una prueba de
   *integridad de m*.
2. Que el firmante de $m$ es *A*. Esto es *no repudio*: El sujeto *A* no puede
   *negar* que fué él quien lo firmó.

## Cadena de bloques

Una *cadena de bloques* se usa en escenarios donde se debe garantizar la
*integridad* de un *log*, esto es, una secuencia (archivo) de *transacciones* en
la que la única operación aceptada es *append* (agregar nuevos datos).

Sean $m_1, ..., m_n$ mensajes/documentos/transacciones, cada bloque $b_i=(m_i,
ts_i, hash(b_{i-1}), A_{pk}, \sigma_i)$, donde:

- $ts_i$ es el ***timestamp*** (*nounce*) del bloque.
- $hash(b_{i-1})$ es el *hash* del bloque anterior.
- $A_{pk}$ es la clave pública del sujeto firmante del bloque.
- $\sigma_i$ es la *firma digital* del bloque generada por *A*.

Esto hace que cada bloque *dependa* de todos los bloques previos. En un sistema
distribuido en que varios nodos de la red son *verificadores* o *aceptadores* de
nuevos bloques, éstos pueden llegar a un *consenso* de aceptación de otro nodo
que quiere *agregar* un nuevo bloque en el log.

Esta técnica se puede aplicar en sistemas de cripto-monedas donde el log (libro
contable de transacciones) está replicado y sólo se aceptan nuevos bloques en
base a algún algoritmo de consenso distribuido.

La seguridad de este mecanismo es que un atacante que quiere modificar el log
debe tener un gran poder de cómputo ya que para modificar el bloque *i* deberá
modificar (y lograr que lo acepten los verificadores) todos los bloques subsiguientes.

## Distribución de claves públicas

La gran ventaja de los sistemas de criptografía de clave pública/privada es que
no hace falta distribuir secretos. Sólo es necesario que los sujetos conozcan
las claves públicas de los demás.

De todos modos, un atacante *E* podría *impersonalizar* a (hacerse pasar por)
otro sujeto *A* y *engañar* a otro sujeto *B* diciéndole que es *A*.

Así, *B* podría enviarle mensajes confidenciales a *A* (cifrados con la clave
pública de *A*) cuando en realidad está cifrando con la clave pública de *E*. 

Si *E* intercepta los mensajes los puede descifrar (con su clave privada).

Esto muestra que es necesario asegurar la asociación de la *identidad de un
sujeto* con su *clave pública*.

Hay dos formas de abordar este problema:

1. ***Infraestructura de claves públicas (PKI)***: Uso de un *tercero de
   confianza* o *Autoridad de Certificación (CA)* que emite un *certificado* por
   cada sujeto que contiene la clave pública del sujeto y su identidad. El
   certificado está firmado por la CA.
   
2. ***Web of trust***: Sistema descentralizado de difusión de claves públicas,
   como en [Pretty Good Privacy
   (PGP)](https://en.wikipedia.org/wiki/Pretty_Good_Privacy).

## PGP

*Pretty Good Privacy* se usa comúnmente para *firmar* mensajes o documentos y el
envío de correos electrónicos en forma confidencial. Un usuario típicamente
distribuye su clave pública (certificado) como parte de su firma en mensajes
públicos o la publica en su sitio web. El software PGP soporta que se distribuya
un *fingerprint*, que consiste en un *hash* del certificado.

Los certificados comúnmente se almacenan en *servidores de certificados PGP*.
Existen muchos servicios online gratuitos.

Un usuario *A* generalmente envía un mensaje a un destinatario *B* cifrando el
mensaje con la clave pública de *B* y firmado con su clave privada (de *A*). El
receptor *B* podrá descifrar el mensaje con su clave pública y *verificar* la
*integridad del mensaje* y que el emisor fue *A* (no repudio de origen).

PGP inicialmente no se basó en autoridades de certificación sino en base a un
sistema de confianza conocido como *web of trust* que consiste en que un
certificado conteniendo la identidad (nombre y e-mail) de un sujeto puede
*firmarse* por un conjunto de sujetos (*introducers*) que aprueban o confían en
ese certificado.

Actualmente PGP también soporta el uso de CAs. Su uso es opcional.

Existen varias implementaciones de PGP. La implementación estándar es
[OpenPGP](https://www.openpgp.org/). Otra alternativa libre es [GPG](https://gnupg.org).

## SMIME

El estándar *Secure/Multipurpose Internet Mail Extensions* es una alternativa a
PGP para cifrar y firmar mensajes. SMIME usa autoridades de certificación (CAs)
para la distribución y su verificación.

Las firmas digitales de SMIME generalmente se usan en forma separada
(*detached*) de los mensajes.

El uso de email con PGP o SMIME generalmente requiere que el cliente de correo
soporte ésto. Una aplicación libre de correo electrónico muy recomendable con
soporte para PGP y SMIME es [Mozilla Thunderbird](https://www.thunderbird.net/en-US/thunderbird/all/).

## Certificados: Contenido (X.509)

Un certificado es un documento que básicamente contiene

- La *identidad* (*Common Name o CN*) del sujeto y otros datos como 
- Tiempo de validez
- Identidad del firmante (*issuer*)
- Los algoritmos criptográficos usados (ej: SHA-265, RSA 2048, ...)
- La *clave pública* del sujeto
- La *firma digital* de la CA

En una infraestructura de claves públicas un sujeto para obtener un certificado
debe generar un ***Certificate Signing Request (CSR)***. Este CSR debe ser
enviado a la CA para que ésta verifique la identidad del sujeto y le retorne el *certificado firmado*.

## Aplicaciones de PKI

Protocolos seguros como SSL/TLS hacen uso de certificados para

1. Autenticar a las partes y
2. Intercambiar una clave de sesión, usando criptografía asimétrica, para cifrar
   los mensajes entre los extremos usando criptografía simétrica.

La autenticación de la identidad de un sujeto se puede hacer:

1. Verificando la validez del certificado verificando la firma y su integridad.
   Notar que esto requiere disponer del certificado de la CA para poder
   descifrar la firma con su clave pública.
2. Verificar que no ha vencido y que no se ha revocado.
3. Autenticarlo mediante un *reto* como el descripto antes.
4. Verificar la identidad del sujeto que aparece en el certificado. 

### Cadenas de certificados

La clave pública de una CA, requerida para validar un certificado de un sujeto,
también se encuentra en un certificado. 

Una CA que emite certificados firmados a los sujetos comúnmente es una *CA
intermedia*. Su certificado está firmado por una CA superior.

El certificado de una CA *raíz (root)* está *auto-firmado*.

Esto significa que es posible verificar toda la cadena de certificados solamente
confiando en las CA raíces o *top-levels*. 
Es posible verificar la integridad del certificado de una CA intermedia usando
el certificado de su firmante (*issuer*) superior.

## Referencias

1. *Introduction to Computer Security*. Matt Bishop. Capítulos 8 y 9.
2. *The Joy of Criptography*: Libro *libre* descargable desde https://joyofcryptography.com/ 
3. *OpenSSL Cookbook*: https://www.feistyduck.com/library/openssl-cookbook/online/
