# Análisis de segurodad y herramientas

La práctica de la seguridad requiere realizar análisis de la seguridad en
sistemas y organizaciones.

Las [OWASP top 10](https://owasp.org/Top10/) define las principales categorías
de vulnerabilidades en software y protocolos de comunicación.

El informe OWASP 2021 reporta las siguientes categorías en orden de número de
vulnerabilidades relevadas:

1. *Control de acceso*
2. *Fallas en criptografía*
3. *Inyección de código/comandos*
4. *Problemas de diseño/arquitectura*
5. *Problemas de configuración*
6. *Uso de componentes vulnerables desactualizados*
7. *Fallas en identificación y autenticación*
8. *Errores en sofware e integridad de datos*
9. *Monitoreo y logging insuficiente*
10. *Falsificación de requerimientos en server-side (SSRF)*

## Actividades de seguridad

Un analista o ingeniero en seguridad puede comúnmente estar en los siguientes
escenarios:

1. *Equipo de desarrollo de software*: La principal actividad es usar
   herramientas de vulnerabilidades en software, configuración o sistemas de
   entrega o integración continua.
2. *Infraestructura de red*: Uso de herramientas de análisis de vulnerabilidades
   en servicios y/o protocolos de red.

## Seguridad en software

Las herramientas para el análisis de seguridad de aplicaciones pueden
clasificarse en las siguientes categorías:

- Análisis estático o *static application security testing (SAST)*
- Análisis dinámico o *dynamic application security testing (DAST)*
- Análisis (testing) interactivo (principalmente para aplicaciones web)
- Detección de componentes con vulnerabilidades conocidas
- Análisis estático de calidad de código
- Monitoreo e integridad de hosts

Actualmente existen un gran número de herramientas comerciales y libres. A
continuación se listan un conjunto de herramientas libres. Para más detalles,
ver [OWASP open source tools](https://owasp.org/www-community/Free_for_Open_Source_Application_Security_Tools).

1. SAST
   - *Github code scanning*, *GitGuardian* (detecta revelación de secretos), ...
   - *Coverity Scan Static Analysis*
   - *Aikido*
2. DAST
   - *ZAP*: Analizador de vulnerabilidades y pen-testing
   - *CI Fuzz CLI*: Aplicación de testing basada en *fuzzing*.
3. Testing interactivo: *curl*, *httpie*, *WebInspect*, ...
4. Componentes vulnerables: *OWASP dependency check*, *Github security alerts*,
   ...
5. Calidad de código: *SonarQube*, *Deepscan*, *MegaLinter*, ...
6. Monitoreo e integridad: 

## Seguridad en redes

En seguridad en redes es común el uso de herramientas de análisis y filtrado de
paquetes como firewalls (ej: Linux iptables o herramientas basadas en eBPF),
*discovering* de servicios, *firewalls de aplicación*, principalmente para
aplicaciones web (*WAF*) y *sistemas de detección de intrusiones (NDIS)*.

- Análisis de tráfico: *Wireshark*, *tcpdump*, ...
- Monitoreo: *Nagios*, ...
- Firewalls de aplicación: *open-appsec*, *NGINX WAF*, *FortiWeb*, ...
- Detección de intrusiones: *SNORT*, ...

Otra actividad requerida para la detección e identificación de vulnerabilidades
es el *testing de penetración* o *pentesting*.

> El ***pentesting*** es la actividad que intenta descubrir vulnerabilidades en
> aplicaciones o servicios de red mediante la realización de descubrimiento de
> servicios, identificación y ejecución de intentos (ataques) de penetración
> aprovechando las vulnerabilidades detectadas.

Las herramientas comúnmente usadas son las siguientes:

- [nmap](https://nmap.org/): Network mapper. Es una herramienta para *descubrir*
  servicios de red y hacer auditoría de seguridad en redes. Además soporta
  detección de sistemas operativos y versiones.
- [ZAP web proxy vulnerability scanner](https://www.zaproxy.org/): 
- [Metaexploit](https://www.metasploit.com/): Framework extensible en Ruby.
  Tiene módulos o *plugins* para usar como herramienta de análisis y testing de
  seguridad para una gran variedad de aplicaciones, incluyendo aplicaciones web.
  Es ideal para desarrollar módulos a medida en proyectos específicos.
- [Nessus](https://www.tenable.com/products/nessus): Herrameinta comercial de
  escaneo de vulnerabilidades.
- [OpenVAS](https://www.openvas.org/): Similar a Nessus pero es libre (licencia
  GPL).

El uso de *herramientas activas* como *Metaexploit* causan ataques concretos, lo
que puede ocasionar algunas consecuencias no deseadas. 
Algunos de sus módulos proveen *payloads* (código o comandos) a ejecutarse en el
host de la víctima. Uno de los *payloads* mas comunes es *reverse shell* que
permite iniciar una conexión tcp desde el host víctima hacia el host del
atacante permitiendo evadir *firewalls*.

Estas herramientas deben usarse con autorización del usuario o dueño del sistema
analizado, sino, puede constituir un delito informático.

## Severidad de vulnerabilidades

Algunas de estas herramientas *califican* las vulnerabilidades encontradas,
comúnmente en 4 categorías:

1. ***Alta***: La vulnerabilidad permite al atacante controlar aspectos
   importantes del sistema, posiblemente con elevados privilegios.
2. ***Media***: Permitirá al atacante acceder a información confidencial pero no
   garantiza el uso elevado de privilegios.
3. ***Baja***: La vulnerabilidad afectará mínimamente al sistema.
4. ***Informativa***: Comúnmente es una vulnerabilidad que revela alguna
   información que puede ser importante en la etapa de *recolección de
   información* o que no se consideran buenas prácticas de desarrollo o
   configuración de un sistema.

Las vulnerabilidades que deberían considerarse de alto nivel o muy riesgosas son:

- Acceso al sistema con credenciales débiles.
- Acceso a consola o shell remoto sin credenciales.
- Inyección de comandos.
- Inyección de código (SQL, Javascript, Python, ...).
- Cross Site Scripting (XSS). Comúnmente permite el *robo* de información
  confidencial como credenciales o identificadores de sesiones.
- Subida de archivos ejecutables.

Un *informe* de seguridad debe incluir las vulnerabilidades encontradas, su
severidad y las medidas o acciones correctivas a desarrollar y aplicar.

## Aplicaciones vulnerables de práctica

Existen varias aplicaciones web para practicar *pentesting*. La distribución
libre [Kali Linux](https://www.kali.org/) es una muy buena plataforma para la
práctica de seguridad. Contiene aplicaciones con vulnerabilidades y herramientas
de seguridad.

También existen varias aplicaciones web en línea desarrolladas específicamente
con vulnerabilidades para ser utilizadas como objetivos de análisis. Algunas de
ellas son [Gruyere](http://google-gruyere.appspot.com/),
[CtfLearn](https://ctflearn.com/), [bWAPP](http://www.itsecgames.com/) y [Hack
This Site](https://www.hackthissite.org/).

La práctica usando estas aplicaciones permite afianzar los conceptos y detalles
técnicos para la detección y prevención de vulnerabilidades.