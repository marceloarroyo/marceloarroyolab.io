# Firewalls, IDS y VPNs

En la actualidad prácticamente cualquier dispositivo de cómputo se encuentra
conectado a alguna red. Estas conexiones crean nuevos problemas de seguridad:
Ataques desde la red que pueden ser tanto internos como externos.

Esto requiere que cada dispositivo deba tener algún mecanismo de protección a
nivel de entrada-salida de paquetes o mensajes para disminuir el riesgo de
diferentes tipos de ataques posibles en red como:

- Intentos de aprovechamiento de vulnerabilidades en aplicaciones de servicio en
  red como web servers, servicios de login remoto o acceso a recursos
  compartidos como archivos, impresoras y otros.

- Explotar vulnerabilidades en el subsistema de red del sistema operativo.

- Ataques de *inundación de paquetes* para lograr *negación de servicio*.

## Firewall

Un *firewall* o *cortafuegos* permite definir reglas de filtrado de paquetes de
red permitiendo implementar una política de seguridad basada en entrada-salida
del tráfico de red. Generalmente un firewall permite sólo el tráfico de los
tipos permitidos en base a los servicios de red utilizados.

Las reglas de filtrado se basan en la coincidencia de patrones en las cabeceras
de los paquetes como direcciones (y puertos) de origen y destino y otras
opciones.

Comúnmente un firewall se interpone entre dos redes, como se muestra en la
siguiente figura.

``` td2svg
                                               +------------+  -+
                                           +---+ web server |   |
 +-------------+       +----------+        |   +------------+   |
 | ISP gateway +-------+ outer fw +--------+                    | DMZ
 +-------------+       +----------+        |   +-------------+  |
                                           +---+ mail server |  |
                                           |   +-------------+ -+
                                           |
                                           |   +-------------+
                                           +---+  inner fw   |
                                               +-------------+
                                                      |
                                                   internal
                                                   network
```

Figura 1: Organización de una red.

La *zona desmilitarizada (DMZ)* tiene acceso público limitado desde Internet a
los servidores de la organización. La red interna no es alcanzable y está
protegida por el *firewall interno*. Es posible que ambos firewalls puedan
implementarse en el *outer firewall*.

Un *state-full firewall* permite relacionar paquetes. Por ejemplo, aquellos
paquetes TCP que pertenecen a una misma conexión.

Comúnmente un firewall permite definir dos tipos de reglas:

- *Filtrado*: Aceptan o rechazan paquetes.
- *Network Address Translation (NAT)*: Modifican las direcciones y/o puertos de
  origen (*source NAT o SNAT*) o de destino (*DNAT*).
- Otras, que permiten cambiar alguna opción en los paquetes. Comúnmente usadas
  para implementar *balance de carga* entre ISPs, *calidad de servicio (QoS)*,
  entre otros.

Las reglas de NAT permiten por ejemplo *enmascarar (masquerading)* paquetes con
direcciones privadas provenientes de una intranet, cambiándose su dirección de
origen por la dirección pública del *gateway* conectado a internet (comúnmente
es el mismo dispositivo que el firewall) para que pueda rutearse en Internet.

Otro ejemplo de DNAT es *port forwarding*. En este caso el firewall cambia las
direcciones y puerto de destino públicas por direcciones/puertos de la intranet
para alcanzar un servicio ejecutándose en un host interno.

## Proxie

Un *proxie* es un *agente* que se sitúa entre dos extremos (*end-points*)
impidiendo una comunicación directa entre ellos. Comúnmente un proxy captura el
tráfico, lo analiza o modifica y lo reenvía al otro extremo.

Por ejemplo, un *antispam* o *antivirus* de correo electrónico se implementa
como un proxy.

Un proxy con funciones de *filtrado* se conoce como un *firewall a nivel de
aplicación*.

Otro ejemplo de la necesidad de proxies es el monitoreo o *packet filter* a
nivel de aplicación, por ejemplo en una aplicación web sobre conexiones HTTPS
(ver [mitmproxy](https://mitmproxy.org/)). El proxy actúa como end-point de
conexiones SSL/TLS y reenvía los paquetes descifrados a un servidor HTTP. En
éste punto los paquetes pueden ser analizados.

## Detección de intrusiones

Un sistema de detección de intrusiones (IDS) actúa con un *monitor* de
actividades en un sistema y *dispara alarmas* ante posibles anomalías o
secuencias de acciones realizadas en o por el sistema.

Un IDS de red (*NDIS*) analiza paquetes de red. Un IDS de host analiza
operaciones sobre él que pueden ser a nivel de red, aplicación o a nivel de
sistema.

Los mecanismos de detección usados pueden clasificarse en dos grupos:

1. Basados en *anomalías*: A partir de un modelo que describe el comportamiento
   *normal* del sistema, intenta detectar estados *anormales*.
2. Basados en *reconocimiento de patrones (firmas)*: De la misma manera que
   algunos antivirus, reconocen ataques *conocidos*.

El NIDS [snort](https://www.snort.org/) es software libre y se basa en un
sistema de reglas que permiten reconocer patrones de ataques conocidos. Las
reglas son definidas por el usuario.

## Redes privadas virtuales (VPN)

Una red privada virtual o VPN por sus siglas en inglés permite a un cliente
situado en una red arbitraria aparezca como un nodo más de otra red remota
comúnmente inalcanzable de manera directa. Este escenario es común cuando un
usuario móvil en una red desea acceder a la red interna de una organización o la
de su hogar tal como si estuviese conectado en su misma red local.

``` td2svg
 +--------+     +--------+ ip1            ip2 +------------+     +---------+
 | client +-----| ISP gw |----- Internet -----| vpn server |-----| service |
 +--------+     +--------+                    +------------+     +---------+
       192.168.1.0/24                                10.8.0.0/8
```

Figura 2: Ejemplo de una VPN.

En la [figura 2](#Figura-2) se muestra un escenario típico de una vpn. El
cliente se encuentra en una red de un proveedor de servicios de internet (ISP) y
desea acceder a un servicio interno de su organización. Una vpn consiste en
crear un *tunel seguro* para transportar los paquetes entre el cliente y el
servidor vpn *encapsulándolos* en un protocolo seguro como puede ser IPSec,
openVPN u otros.

Una aplicación en el cliente puede acceder a un servicio escuchando dentro de la
red privada 10.8.0.0 por medio de un programa cliente de vpn el cual encapsulará
los paquetes con las direcciones de la red privada en paquetes IP con dirección
de origen *ip1* y destino *ip2*. E protocolo seguro usado para formar este
túnel cifrará los paquetes originales para lograr confidencialidad y comúnmente
autentica los paquetes transmitidos entre el cliente y el servidor vpn.

El software de vpn crea *interfaces de red virtuales* `tun/tap` para establecer
una conexión segura entre el cliente y el servidor vpn. Además configura el
ruteo para que el tráfico de red pase por esas interfaces. En éste punto se
produce el *encapsulamiento* de los paquetes de red para enviarlos por un tunel
seguro. Este encapsulamiento puede hacerse usando IPSec u otros protocolos como
openvpn como se muestra en las siguientes figuras.

> [!NOTE|label:Interfaces virtuales]
> Una interfaz virtual `tap` es una interface de red a nivel de enlace
> (Ethernet) implementada en software en el kernel de un SO. Una interfaz
> virtual `tun` es una interfaz a nivel de red (en capa 3, como por ejemplo IP).
> En [TUN/TAP interfaces](https://en.wikipedia.org/wiki/TUN/TAP) se describen en
> mayor detalle.

``` td2svg
+--------------------------+ encapsulation                               
|           client         |  occurs
| +----------+    +------+ |   here
| |    IP    |--->| tun0 | |    |   +---------+-------------+
| | datagram |    +---+--+ |    |   | vpn hdr | IP datagram |
| +----------+        | <--+----+   +---------+-------------+
|                     |    |
|                 +---+--+ |    +-----+ internet   +-----+
|                 |  eth |-+--->| ISP |--- ...  -->| org |
|                 +------+ |    | gw  |            | gw  |
+--------------------------+    +-----+            +-----+
```

Figura 3: Encapsulamiento en el cliente.

En la figura 2 se muestra el encapsulamiento de un paquete enviado por una app
en el cliente con destino a una IP privada de la organización. Obviamente éste
paquete no podría ser ruteado ya que tiene como destino una IP privada. El
cliente vpn configuró reglas de ruteo que hace que el tráfico pase por la
interface `tun0` para tener la oportunidad de *encapsular* el paquete original
en otro. Si se usara IPSec se encapsula un paquete IP en otro IP. Openvpn
define un protocolo seguro propio sobre TCP o UDP con SSL/TLS.

``` td2svg
            +--------------------------+ extract
            |        vpn server        |  inner
            | +-----+----+    +------+ | datagram
 +-----+    | | vpn | IP |--->| tun0 | |    |
 | org |--->| | hdr | dg |    +---+--+ |    | +-------+
 | gw  |    | +-----+----+        |  <-+----+ | IP dg |
 +-----+    |                 +---+--+ |      +-------+
            |                 | eth  |-+---> to internal
            |                 +------+ |     network service
            +--------------------------+    
```

Figura 4: Des-encapsulamiento en el servidor.

Cuando una aplicación en el cliente quiera alcanzar un servicio de la red
privada `10.8.0.x`, enviará un paquete con esa dirección de origen. El
cliente vpn hará que pase por `tun0`.

Conceptualmente, el dispositivo cliente está *conectado* a la misma red que el
*servidor* de la red privada mediante las interfaces virtuales `tun/tap`.

Suponiendo que la interface `tun0` en el cliente tiene la IP `10.8.0.6` y la del
server `10.8.0.1`, el paquete saliente de `tun0` en el cliente tendrá la forma: 
`src-addr: 10.8.0.1, dst-add: org-gw, dst-port: 1194, data:sec-origdgm`, donde
`sec-origdgm` es el datagrama original cifrado y con información de
autenticación de origen.

Luego al enviarse por `eth0` y a traves del *gateway del ISP*, TCP/IP entregará
el paquete al *gateway* de la organización, el cual deberá estar configurado
para entregárselo al *servidor vpn* por la interface *tun0* ya que trae como
puerto de destino el puerto 1194.

El servidor vpn *des-encapsula* el paquete `sec-origdgm`, lo descifra y
autentica para finalmente re-enviarlo por `eth0` a su destino en la red interna.

Las respuestas siguen el camino inverso.

## OpenVPN

Openvpn es un protocolo flexible que puede usar IPSec o UDP o TCP para crear un
túnel seguro entre un servidor y los clientes.

Consta de una aplicación que en base a una configuración dada puede correr como
servidor o cliente.

Las interfaces de red virtuales pueden ser *tap/tun*.

A continuación se describen los pasos para configurar el cliente y servidor
openVPN.

Esta guía se basa en el [openvpn
howto](https://openvpn.net/community-resources/how-to/) que usa interfaces
virtuales TUN y el protocolo UDP como transporte de los paquetes encapsulados.

1. Instalar [OpenVPN](https://www.openvpn.org)
2. Instalar [Easy-RSA](https://easyrsa.org) para crear una CA para firmar los
   certificados del servidor y los clientes.
3. Crear un directorio para la configuración del servidor y los clientes
   
   `mkdir openvpn`

4. Crear la CA

   1. Crear un directorio para la CA: `make-cadir ca`
   2. `cd ca`

       renombrar el archivo de configuración `openssl-<version>.cnf` de mayor
       versión como `openssl.cnf` o hacer un enlace simbólico como

       `ln -s openssl-<version>.cnf openssl.cnf`

   3. Editar `vars`: Cambiar las variables `KEY_COUNTRY`, `KEY_PROVINCE`,
      `KEY_CITY`, `KEY_ORG` Y `KEY_MAIL`.

   4. Ejecutar los siguientes comandos:

      `. ./vars`

      `./clean-all`

   5. Generar la clave privada y el certificado (autofirmado) de la CA: `./build-ca`

      Este comando es interactivo y permite ingresar otros valores definidos en
      `vars`.

   6. Generar la clave privada y el certificado para el server:

      `./build-key-server server`

   7. Generar claves y certificados para los clientes: `./build-key client1`

   8. Crear parámetros Diffie-Hellman: `./build-dh`

   9. En la carpeta `ca/keys` generar una *clave secreta* compartida entre el
      servidor y los clientes para usar ***tlsauth*** para incrementar la seguridad
      SSL/TLS.

      `openvpn --genkey --secret ta.key`
      
      Para más detalles, ver [endureciendo la seguridad de
      openvpn](https://openvpn.net/community-resources/hardening-openvpn-security/).

   Los archivos de claves, parámetros Diffie-Hellman y los certificados quedaron
   en el directorio `keys`.

   Los archivos `.crt` son los certificados firmados por la CA y los `.key` son
   las claves privadas del servidor y de los clientes correspondientes.

5. Crear las carpetas `server` y `client` y copiar archivos de configuración de
   ejemplos de un server y un cliente desde `/usr/share/doc/openvpn` (o desde
   donde haya descargado openvpn si hizo una instalación manual) en las carpetas
   correspondientes.

6. Copiar los archivos `ca/keys/ca.crt`, `ca/keys/dh2048.pem` y `ca/keys/ta.key`
   en la carpeta del servidor y del cliente.

   Copiar `ca/keys/server.crt` y `ca/keys/server.key` a la carpeta `server` los
   archivos de la clave privada y el certificado del cliente (en `ca/keys`)
   a las carpeta del cliente.

7. Editar el archivo de configuración del server (sea `server.conf`)
   asegurándose que los parámetros `ca`, `cert`, `key` y `dh` tengan los nombres
   de los archivos adecuados.

   Hacer lo mismo con el archivo de configuración del cliente.

### Prueba

En la carpeta del servidor, lanzar el servidor. Si todo funciona correcto
debería funcionar como se muestra a continuación.

![server-launch](demos/run-openvpn-server.cast "Demo 1: Ejecución del servidor.").

El servidor escucha en la IP 10.8.0.1 en el puerto 1194.

En otra terminal, en la carpeta del cliente, lanzar el cliente como se muestra abajo.

![client-launch](demos/run-openvpn-client.cast "Demo 1: Ejecución del cliente.").

La salida muestra que el cliente y el servidor están conectados usando una
conexión SSL/TLS.

En otra terminal ver si hay conectividad con el servidor y el cliente haciendo

`ping 10.8.0.1` y `ping 10.0.0.n`, donde *n* corresponde con la IP del cliente
(comúnmente, *n=6* en ésta configuración). 

Como estamos corriendo el cliente y el servidor en la misma máquina, es posible
ver con el comando `ifconfig` que el servidor creó y usa la interface de red
`tun0` y el cliente la `tun1`.

En la configuración de los clietes y servidores puede agregarse *autenticación
de usuarios*.

### Acceso desde el exterior a la intranet

Generalmente el uso en organizaciones es dar acceso desde el exterior a usuarios
autorizados a servicios (de archivos, impresoras, aplicaciones, etc) de la
intranet.

Esto se logra haciendo que el tráfico entrante correspondiente al túnel vpn sea
redirigido desde el *gateway* o *router/firewall* conectado al ISP hacia el servidor vpn.

> [!NOTE|label:Ejemplo]
> El servidor openvpn está instalado en una máquina cuya interface de red está
> asociada a la IP (privada) 192.168.0.5, se está usando udp para el túnel y el
> puerto 1194 (el default). En el router/firewall se deberá agregar una regla de
> *redirección de puertos (port forwarding) o DNAT* que establezca que todo el
> tráfico entrante (desde la IP pública de internet) TCP y UDP con destino al
> port 1194 deberá *redirigirse* a la IP del servidor vpn (192.168.0.5).

Esto hará que el servidor vpn *extrae* los paquetes encapsulados en el protocolo
que usa la vpn y los rutea normalmente a los hosts correspondientes en su red
interna.

El router/firewall también deberá *enmascarar (masquerade)* el tráfico saliente.

### Uso de un servicio VPN

Es posible lograr *anonimato* en servicios de Internet accediendo a ésta por
medio de un servidor vpn. Estos permiten que el tráfico llegue desde
el cliente al servidor vpn en forma segura (confidencial y autenticada) y el
servidor reenvía los paquetes al destino con su propia dirección como origen.
Las respuestas recibidas son redirigidas al cliente por el túnel seguro.

A continuación se muestra el uso de un servicio openvpn gratuito de
[freeopenvpn.org](https://www.freeopenvpn.org/).

1. Seleccionar un servidor de la lista.
2. Descargar el archivo de configuración. Puede seleccionar que use TCP o UDP.
   El servicio genera un archivo de configuración que incluye el certificado de
   la CA, la clave privada del cliente y su certificado.
3. Ejecutar `sudo openvpn <config-file>`
4. Ingresar el usuario y la contraseña mostrada en la página web.

A partir de ese momento podrá verificar que todo el tráfico va por la vpn. Es
posible verificarlo accediendo a un sitio con el servicio *¿cuál es my ip?* como
se muestran en las siguientes capturas en las que se muestran la IP pública
asignada por nuestro ISP y la otra luego de lanzar el cliente vpn.

![antes-vpn](img/myISPpublicIP.png)

Figura 4: IP pública de nuestro ISP (sin vpn).

![antes-vpn](img/myVPNpublicIP.png)

Figura 5: IP pública usando una vpn.

Al estar conectados al servidor vpn, el sitio accedido recibe los requerimientos
HTTP desde este último haciendo posible una navegación *anónima*.