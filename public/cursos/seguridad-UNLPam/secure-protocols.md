# Protocolos de red seguros

Los protocolos de comunicaciones que permiten incorporar mecanismos de
seguridad, en rigor pueden definirse en cualquier capa del modelo OSI.

En este caso analizaremos algunos protocolos de seguridad para los protocolos de
Internet (familia TCP/IP).

## IpSec

El protocolo *Internet Protocol Security (IPSec)* permite autenticación de
origen de paquetes y cifrado de datos. Además incluye protocolos de negociación
de algoritmos de cifrado e intercambio de claves secretas.

IpSec se describe en las [RFC 4301](https://www.rfc-editor.org/rfc/rfc4301) y la
[RFC 4309](https://www.rfc-editor.org/rfc/rfc4309) y operan sobre las capas de
red IPv4 o IpV6 (capa 3 del modelo OSI).

Los protocolos de seguridad de IPSec son dos:

1. ***Authentication Header (AH)***: Ofrece integridad de paquetes y
   autenticación de origen.

2. ***Encapsulating Segurity Payload (ESP)***: Ofrece los mismos servicios de
   *AH* y *confidencialidad* permitiendo cifrar los datos.

Ambos protocolos soportan dos posibles *modos de operación*:

1. ***Transport mode***: Sólo opera (autenticación y cifrado) sobre el *payload*
   del paquete. Esto hace que la cabecera IP no se modifica por lo que el ruteo
   puede operar sin problemas.
2. ***Tunnel mode***: Opera sobre todo el paquete, debiéndose agregar otra
   cabecera IP y el paquete origina se *encapsula* como su *payload*.

   Este modo se usa para implementar *redes virtuales privadas (vpns)*.

Sólo se analiza *ESP* ya que incluye los servicios de *AH*.

*ESP* opera sobre IP e incluye un *header* (a continuación de la cabecera IP)
con los siguientes campos:

- *Security Parameters Index (SPI)*: Identifica los parámetros de seguridad
  (claves, certificados, algoritmos de cifrado, hashes, etc) y la dirección
  IP de destino. 
- *Número de secuencia (SN)*: Valor creciente, ofreciendo *secuencialidad*, es
  decir que el receptor debe entregar los paquetes a la capa superior de
  transporte en el orden enviado.

A continuación se encuentra el *payload* (datos) del paquete IP original.

Luego se incluye un *ESP trailer* que contiene los siguiente campos:

- *Padding*: De 0 a 255 bytes necesarios para que la longitud coincida con un
  múltiplo del tamaño de bloques usado por el algoritmo de cifrado simétrico.
- *Pad Length*: Nro de bytes del campo anterior.
- *Next header (NH)*: Tipo tal como lo establece el protocolo IP.

Finalmente se incluye el campo *Integrity Check Value (ICV)* usado para la
autenticación de origen del datagrama. IpSec puede usar algoritmos de
autenticación basados en criptografía simétrica (ej: HMAC codes usando una *pre
shared key*) o asimétrica (usando certificados).

![IPSec-ESP](img/Ipsec-esp.svg)

Figura 1: Modos ESP. [By Mpk1024 - Own work, CC BY-SA 4.0](https://commons.wikimedia.org/w/index.php?curid=97123608).

La figura anterior muestra cómo opera ESP en los diferentes modos.

El SPI se usa para determinar la *security association (SA)* de un paquete a
transmitir. Se puede configurar manualmente o se negocia con la otra parte
(*peer*) automáticamente por medio de protocolos como [Internet Key Exchange
(IKE)]([RFC 4301](https://www.rfc-editor.org/rfc/rfc4301), ISAKMP y otros.

Una *SA* es una entrada en una tabla conocida como la *security association
database (SADB)* y es identificada unívocamente por el campo *SPI*.

Para una comunicación bidireccional entre dos hosts, éstos deben negociar dos
*SAs*, ya que cada *SA* es unidireccional.

## SSL/TLS

Los protocolos *SSL (secure sockets layer)* o *TLS (Transport Layer for
Security) operan a nivel de la capa de transporte (comúnmente sobre tcp) en el
modelo OSI.

En realidad *TLS* es el sucesor de *SSL*, por lo que es común que se usen ambos nombres.

Estos protocolos se basan en la infraestructura PKI (certificados, cifrado y
firma digital).

Luego de establecer una conexión tcp, el cliente propone la negociación
(*TLS handshake*) de parámetros para poder realizar una conexión segura consiste
en los siguientes pasos:

1. *cliente -> servidor*: *(HELLOCLIENT, ciphersuites)*.
   Propuesta de inicio de una conexión segura.

2. *servidor -> cliente*: *(HELLOSERVER, selected_suite, certificate)*.
   El servidor acepta y selecciona una suite criptográfica a usar.

3. El cliente verifica la integridad del certificado 
   $cert=(id,valid\_dates,...,public\_key,signature)$ comparando
   $hash(cert) = D(signature, CA_{pk})$ y la verificación de la identidad del
   sujeto (en el caso de los browsers comparan el dominio ingresado en la barra
   de direcciones con la identidad del sujeto (web site).

   Nota: $hash(cert)$ aplica el hash al certificado excluyendo el campo que
   contiene la firma de la CA. El cliente debe tener el certificado de la
   *Autoridad de Certificación (CA)* para obtener su clave pública para poder
   descifrar la firma usando el algoritmo de descifrado asimétrico $D$.

   Si esta verificación falla, el cliente comúmnente produce un error.

4. *cliente -> servidor*: $(E(pre\_master\_key), server_{pk})$.

   El cliente genera una *pre master key* y la envía al servidor cifrada con la
   clave pública del servidor (extraída del certificado).

5. El servidor descifra la *pre master key* con su clave privada.

6. Usando la *pre master key*, ambos *acuerdan* una clave *shared secret (ss)*.
   Pueden usar algoritmos (acordados en el paso 2) del tipo Diffie-Hellman o
   algoritmos de cifrado asimétricos como RSA.

7. *cliente -> servidor*: $E'(n,ss)$.

   El cliente envía al servidor un mensaje ($n$ generado aleatoriamente) cifrado
   con un algoritmo $E'$ de cifrado simétrico usando como clave la *shared
   secret* acordada.
   
8. *servidor -> cliente*: $E'(f(D'(n,ss)),ss)$.

   El servidor descifra $n$ y envía una respuesta cifrada con la misma *shared
   secret* acordada, donde $D'$ es la función de descifrado simétrico correspondiente. La función *f(n)* podría ser, por ejemplo, $n-1$.

9. El cliente descifra y verifica que la respuesta del servidor es la esperada.
   Esto confirma que ambos acordaron la misma clave *ss*.

De aquí en adelante todo el tráfico se cifra y descrifra usando $E'$ y $D'$ y
$ss$ hasta el cierre de la comunicación.

Los protocolos a nivel de aplicación pueden usarse sobre TLS para lograr
autenticación de las partes, confidencialidad e integridad de mensajes.
Un ejemplo de estos protocolos es *https*, que es básicamente el uso del
protocolo de la web *http* sobre TLS.

Desarrollemos un ejemplo para comprender en mayor detalle la obtención de
certificados para un sitio web para lograr establecer una comunicación segura.

Se usa [openssl](https://www.openssl.org) como herramienta.

## Creación de una CA privada

Una CA se encarga de *firmar* los *CSRs* recibidos de los usuarios. La
herramienta [openssl](https://www.openssl.org) permite crear una infraestructura de emisión de
certificados firmados. En éste caso haremos una CA privada (no reconocida
oficialmente) y serviría para hacer seguras las comunicaciones en aplicaciones
internas o específicas de una organización.

Crearemos una carpeta `ca` para representar nuestra CA privada.

Destro de ésta carpeta debemos en primer lugar general la clave privada de la CA.

``` bash
openssl genrsa -des3 -out myCA.key 2048
```

La clave privada debería estar bien protegida, por lo que habría que darle sólo
permisos de lectura para el dueño al archivo `myCA.key`.

Ahora hay que generar el *certificado auto-firmado* de la CA.

``` bash
openssl req -x509 -new -nodes -key myCA.key -sha256 -days 1825 -out myCA.pem
```

Este comando es interactivo. Pregunta al usuario los datos de identidad del
sujeto y otros (país, provincia, localidad, organización, etc).

El archivo `myCA.pem` (la extensión se debe al formato comúnmente usado que se
almacena codificado en base-64) contiene la clave pública y la identidad del sujeto.

El certificado `myCA.pem` es *auto firmado* ya que la identidad del *emisor* y
del sujeto coinciden. Un certificado puede verse en forma legible con el comando

```
openssl x509 -in myCA.pem -text
```

La salida mostrará algo como lo siguiente

```
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            2c:7c:82:20:35:25:bc:e3:0a:d0:ed:d1:45:11:40:8d:58:25:24:51
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = AR, ST = La Pampa, L = General Pico, O = ACME, OU = ACME CA, CN = ACME Root CA
        Validity
            Not Before: Apr 13 19:47:32 2023 GMT
            Not After : Apr 11 19:47:32 2028 GMT
        Subject: C = AR, ST = La Pampa, L = General Pico, O = ACME, OU = ACME CA, CN = ACME Root CA
        Subject Public Key Info:
        ...
```

Ver que los campos `Issuer` y `Subject` coinciden.

## Obtención de certificados

Para obtener un certificado un sujeto debe generar un *certificate signing
requests (CSR)* y luego solicitarle a una CA que lo firme. En este caso queremos
crear un certificado para una aplicación web segura.

Crearemos una carpeta `server` con el programa `myhttps-server.py` cuyo
contenido se muestra a continuación.

``` python
# Execute with python3 myhttps-server.py
import http.server
import ssl

httpd = http.server.HTTPServer(('localhost', 4443), http.server.SimpleHTTPRequestHandler)
httpd.socket = ssl.wrap_socket (httpd.socket,
                                certfile='server.crt',
                                keyfile='server.key',
                                server_side=True,
                                ssl_version=ssl.PROTOCOL_TLS)
httpd.serve_forever()
```

Este código crea un servidor tcp, crea un *secure socket* y ejecuta un servidor
https en el puerto 4443 (el puerto estándar del protocolo tls es 443, pero usarlo requiere que se ejecute como *root*).

### Generación del CSR del servidor

Para generar un *Certificate Signing Request (csr)* hay que generar primero la
clave privada para el sition web.

``` sh
openssl genrsa -out server.key 2048
```

Luego hay que generar el csr, el cual es un certificado que contiene la
identidad del sujeto, su clave pública y otros parámetros.

``` sh
openssl req -new -key server.key -out server.csr
```

En éste caso, hay que ingresar los datos del sitio (aplicación web).

En éste ejemplo usaremos como *CN = mysite.com*. Los demás valores son a
criterio del usuario.

El resultado es un *certificate signing request*, esto es, un certificado sin
firmar. El objetivo es entregar este csr a una CA para obtener un certificado
firmado.

Nuevamente es posible visualizar el contenido del csr con el comando

```
openssl x509 -in server.csr -text
```

## Obtener el certificado firmado por la CA

El csr generado en el paso anterior hay que entregárselo a la CA para que lo
firme. Comúnmente una CA ofrece algún mecanismo o interfaz para realizar ésto.
En éste caso, simplemente copiaremos el archivo `server.csr` en la carpeta de la
CA para que ésta lo firme.

Actualmente, los browsers verifican la identidad de un sitio comparando con el
valor del campo `subjectAltNames` (SAN). El csr generado anteriormente no
contiene ese campo pero puede agregarse al momento de generar el certificado
firmado por la CA usando el siguiente archivo de configuración (sea 
`server.ext`) que habría que copiarlo en la carpeta `ca` simulando que también
se ha entregado a la CA como *información adicional*:

```
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
DNS.1 = mysite.com
DNS.2 = localhost
DNS.3 = *.mysite.com
```

Ahora la CA *firma* el CSR con el siguiente comando (en el directorio `ca`):

``` bash
openssl x509 -req -in server.csr -CA myCA.pem -CAkey myCA.key \
-CAcreateserial -out server.crt -days 825 -sha256 -extfile server.ext
```

Este comando generará el certificado `server.crt` firmado por la CA.

La salida del comando `openssl x509 -text -in server.crt` mostrará

```
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            4b:8a:dd:97:38:b6:8f:3d:f2:eb:ae:33:f6:7a:a3:dd:40:5d:62:34
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = AR, ST = La Pampa, L = General Pico, O = ACME, OU = ACME CA, CN = ACME Root CA
        Validity
            Not Before: Apr 13 20:00:42 2023 GMT
            Not After : Jul 16 20:00:42 2025 GMT
        Subject: C = AR, ST = La Pampa, L = General Pico, O = Secure Systems, OU = Dev, CN = mysite.com
        Subject Public Key Info:
```

Se debe notar que el *emisor* del cerfificado firmado para el sujeto *mysite.com* es nuestra CA.

## Ejecución del servidor

El servidor https (en la carpeta `server`) requiere el certificado, que deberá
copiarse desde la carpeta `ca` y su clave privada `server.key`.

El certificado deberá enviárselo al cliente durante el paso 2 del *handshake*.
A la clave privada la necesita para poder descifrar la *pre master key* en el
paso 5.

El servidor puede ejecutarse con el comando

``` bash
python3 myhttps-server.py
```

Podemos probar desde el navegador web acceder al sitio mediante el url 
`https://localhost:4443`.

El navegador seguramente dará un error del tipo *ERR_CERT_AUTHORITY_INVALID*.
Esto se debe a que el navegador no *conoce* a la CA, es decir, no contiene el
certificado de la CA que asocia su identidad con su clave pública. En este punto
se debería *importar* el certificado de la CA en la base de datos de
certificados de *autoridades* del navegador.

Luego de haberlo importado y decirle que lo use para *confiar* en ese sitio, al
reintentar acceder al sitio deberíamos obtener la respuesta del servidor (en
éste caso se muestra le contenido de la carpeta en el que se está ejecutando).

Si se quisiera usar el url `https://mysite.com:4443`, obtendríamos un error de
DNS, ya que el sistema a qué dirección IP resolver. Esto se puede solucionar
agregando (como root) la siguiente línea al archivo `/etc/hosts`:

```
127.0.0.1   mysite.com
```

Esto hace que el dominio `mysite.com` resuelva a la IP 127.0.0.1 que corresponde
al `localhost` o la interface *loopback* en la cual está *escuchando* nuestro
servidor https.

Con esto se ha descrito los pasos necesarios para obtener un certificado para
una aplicación web y armar una CA privada.

Es común que en organizaciones grandes o medianas se creen CAs privadas para
usar en sus *intranets* y para aquellas aplicaciones que requieran
comunicaciones seguras y autenticación de las partes. 

Cabe aclarar que TLS permite configurar o programar una aplicación *server* para
que éste requiera al cliente su certificado y autenticarlo. En éste caso TLS
permite hacer una *doble autenticación*.

## Redes privadas virtuales

Una red privada virtual o *vpn* por sus siglas en inglés permite crear un túnel
entre dos o más subredes interconectadas comúnmente por Internel u otra red
pública permitiendo el acceso seguro y transparente desde una subred a recursos
de la otra como si estuviesen en una misma *intranet*, tal como se muestra en la
siguiente figura.

Existen muchos protocolos para implementar vpns. Algunos se listan a continuación:

- Point-to-Point Tunneling Protocol (PPTP)
- Layer 2 Tunnel Protocol (L2TP/IpSec)
- OpenVPN
- Secure Socket Tunneling Protocol (SSTP)

Todos los protocolos se basan en la creación de un *túnel* de comunicación
cifrado entre dos hosts. IpSec se puede usar para crear ese túnel, espcialmente
con ESP en modo túnel. La nueva cabecera IP agregada contiene las direcciones IP
origen y destino IP reales mientras que el paquete IP *encapsulado* contiene las
direcciones IP de origen y destino de la vpn, que comúnmente se basan en
direcciones IP privadas por lo que se requiere implementar NAT.

En la figura 1 se muestra cómo un *remote worker* puede acceder a recursos de
una red como por ejemplo un servidor de archivos, de bases de datos o impresoras
como si estuviese dentro de la red local.

![vpn](img/vpn.png)

Figura 1: Diagrama de una vpn. Fuente: OpenVPN.

La transmisión de un paquete por el túnel vpn sigue los siguientes pasos (flujo
en naranja):

1. La estación de trabajo del *remote worker* en una red con una IP privada (sea
   192.168.8.25) conectada a un ISP envía un paquete al servidor de bases de
   datos (que escucha por ejemplo en la IP privada 192.168.8.1:4000).

2. El *cliente vpn* cifra el paquete y lo ***encapsula*** en otro con
   dirección origen de la IP pública del ISP (*masquerading*) y con destino a la
   IP pública de su organización (sea 200.7.47.50:xxxx), donde $xxxx$ es el
   *puerto* correspondiente al servicio vpn de bases de datos.
   
3. El *firewall+router* de la red de la organización redirecciona el datagrama
   al *vpn server* (logo en azul).
   
4. El vpn server ***desencapsula*** el paquete, lo descifra y autentica. Luego
   puede *reenviar* el datagrama extraído al servidor de bases de datos dentro de la red local.

La comunicación en el sentido inverso sigue los mismos pasos complementarios.

Si se usa IPSec, comúnmente el *des-encapsulado*, autenticación y descifrado se
puede hacer en el firewall o router.

En paquetes como *openVPN*, éstos túneles pueden crearse de diferentes maneras y
usa un protocolo específico a medida. En particular, openVPN usa UDP para
encapsular los datagramas IP y puede configurarse para que la autenticación se
haga usando *pre shared keys* (criptografía simétrica) o usando *certificados* y
*SSL/TLS*. El *openvpn server* realiza el trabajo del lado del servidor y es un
proceso que puede ejecutarse en un firewall/router o en otro host de servicio.

En el cliente se usa un *vpn client* que realiza el paso 2 de la secuencia
anterior.

## Redes móviles

Para la seguridad en redes inalámbricas del tipo IEEE 802.11 (wifi) se han
definido varios protocolos a nivel de la capa de enlace de datos (L2). El
objetivo es proveer mecanismos de control de acceso, integridad de datos y
confidencialidad (mediante el cifrado de los paquetes transmitidos).

### WEP

El estándar (hoy considerado obsoleto) *Wired Equivalent Privacy (WEP)* usa
[RC4](https://en.wikipedia.org/wiki/RC4) como algoritmo de criptografía
simétrica. RC4 toma como entrada una clave *k* (de 40 bits) y un *vector de
inicialización (iv)* (de 24 bits) y genera una *secuencia (stream) de números
pseudo-aleatorios*. 
RC4 se implementa como una función con estado, tal que en cada invocación
$RC4(iv,key)$ genera el próximo valor de la secuencia.

WEP anexa el *iv*, un *checksum* [CRC-32](https://en.wikipedia.org/wiki/CRC-32)
de integridad al paquete y luego lo cifra aplicando un *xor* con una secuencia
de bits generados por *RC4*.

- Emisor: $C = <P,iv,chksum(P)> \oplus RC4(iv,k)$
- Receptor: $C \oplus RC4(iv,k) = <P,iv,chksum(P)>$

La autenticación se hace en un *four-step challenge* entre un cliente
(dispositivo) y el *access-point (AP)*.

1. *client -> AP*: Authentication Request
2. *AP -> client*: `Challenge` (valor aleatorio en texto plano)
3. *client -> AP*: `Challenge` cifrado con la clave (secreta) WEP
4. El $AP$ descifra la respuesta. Si concide con `challenge` responde con un
   reconocimiento positivo (`ack`).

La principal debilidad de WEP es que el *iv* es de sólo 24 bits y es posible que
se repita (50% de chances luego de 5000 paquetes) y lo hace susceptible a un
*related-key attack* que permite descubrir la clave luego del análisis de un
conjunto de paquetes cifrados capturados.

Actualmente existen herramientas muy efectivas para quebrar WEP en pocos
minutos.

### WPA y WPA2

En respuesta al obsoleto WEP, el estándar IEEE 802.11i especifica *Wifi
Protected Access (WPA)*.

WPA soporta el uso de diferentes claves a distintos usuarios (*WPA enterprise*)
o *WPA personal*, el uso de una única *shared key*. 

WPA es similar a WEP pero usa RC4 con claves de 128 bits y un *iv* de 48 bits.
También usa el *Temporal Key Integrity Protocol (TKIP)* el cual cambia
periódicamente el *iv* para prevenir el ataque mencionado. Se usa *Message
Integrity Code (MIC)* en lugar de CRC-32.

La autenticación se basa en el *Extensible Authentication Protocol (EAP)*. Este
framework se basa en un cliente (*suplicant*) y un *authenticator* que controla
el algoritmo a usar y las credenciales de autenticación que debe presentar el cliente.

WPA2 mejora WPA usando AES en lugar de RC4, por lo que el algoritmo de cifrado
opera en bloques (128, 196 o 256 bits). También soporta *fast roaming* de
clientes, permitiendo la migración entre dos APs a los dispositivos móviles.