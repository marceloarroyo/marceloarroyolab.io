# Confinamiento

El objetivo de *confinar* un subsistema o proceso es limitar el conjunto de
recursos que puede afectar ante un error o ataque.

Los sistemas operativos (OS) multitarea proveen aislamiento de procesos que
limita los espacios de memoria a los cuales tiene acceso. Esto permite que un
proceso no acceda recursos de otros procesos o código y datos del kernel sin
usar los mecanismos provistos.

En cada cambio de contexto, el kernel cambia los mapas de espacios de memoria
(tablas de páginas).

Otro mecanismo similar ocurre con los modos de ejecución de la CPU. En modo
*user* la CPU dispara excepciones (que serán atrapadas por el kernel) ante una
instrucción inválida o privilegiada.

En seguridad de sistemas sería deseable *confinar* aplicaciones para que no
puedan afectar al resto del sistema ante una falla o ataque.

Las posibles soluciones pueden ser:

1. Aislar diferentes aplicaciones en diferentes computadoras.
2. Usar *máquinas virtuales* en una computadora.
3. Usar *contenedores*.

El aislamiento debería considerar:

- El sistema debería contener sólo los componentes necesarios para la 
  aplicación.
- Deberían implementarse mecanismos de backup, logging y monitoreo para cada 
  aplicación.
- *Confinamiento transitivo*: Si un proceso invoca o usa a otro proceso, éste
  último debe estar tan confinado con el primero.

## Máquinas virtuales

La tecnología de virtualización de hardware actualmente permite que en una
computadora se puedan ejecutar concurrentemente diferentes sistemas operativos,
sobre los cuales se pueden ejecutar un conjunto de aplicaciones.

La desventaja de este enfoque es que la virtualización tiene un costo
(*overhead*) en las transiciones entre el OS *huésped (host)* y el OS *anfitrión
(host)*, a pesar que el hardware actual soporta mecanismos razonablemente
eficientes.

El software de virtualización se conoce como *hypervisor*. 

Esta tecnología es útil para reutilizar hardware y ofrecer diferentes sistemas
operativos.

``` td2svg
  +--------------------+    +--------------------+
  |.vm  OS guest 1     |    |.vm  OS guest 2     |
  | +----------------+ |    | +----------------+ |
  | | user processes | |    | | user processes | |
  | +----------------+ |    | +----------------+ |
  | |     kernel     | |    | |     kernel     | |
  | +----------------+ |    | +----------------+ |
  +--------------------+    +--------------------+
  |.hw virt. hardw.    |    |.hw virt. hardw.    |
  +--------------------+    +--------------------+
          ^ enter                   ^ enter
       VM |                      VM |
          v exit                    v exit
  +----------------------------------------------+
  |#hv               Hypervisor                  | 
  +----------------------------------------------+
  |.hw                Hardware                   |
  +----------------------------------------------+

  <style>
  .vm {
    fill: lightgreen; 
    filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.4));
  }
  #hv {fill: salmon;}
  </style>
  ```

## Contenedores

> Un *container* es un *conjunto de procesos* que ejecuta aislado del resto de 
> procesos y recursos.

A diferencia de las máquinas virtuales, los *containers* tienen las siguientes
características:

1. Los diferentes containers ejecutan sobre el mismo OS kernel.
2. Para el OS son procesos comunes.

La principal ventaja es que los contenedores prácticamente no tienen el
*overhead* de las máquinas virtuales.

![vm vs containers](img/virtualization-vs-containers.png)

Para que un OS pueda soportar containers, este debe estar diseñado para que se
puedan definir diferentes *espacios de nombres* de procesos, dispositivos y
sistema de archivos. También deberá permitir *multiplexar dispositivos* o
brindar *dispositivos virtuales* como interfaces de red por medio de *bridges* u
otros.

En Free-BSD esos espacios de nombres se denominan *jails* y en GNU-Linux
*control groups (cgroups)*. Estos espacios de nombres se usan para *agrupar*
procesos, usuarios, sistemas de archivos y otros recursos. Los grupos pueden ser
independientes y usan un limitado conjunto de recursos.
El grupo *raíz* contiene los recursos usados normalmente en el sistema.

Estos *espacios de nombres* permiten *separar* procesos en grupos y que pueden
estar aislado de los demás. Además es posible que el grupo de procesos usen una
parte del sistema de archivos que verán como el sistema de archivos raíz (ver el
comando `chroot`).

GNU-Linux ofrece los siguientes *espacios de nombres*:

- *Process ids*: Diferentes grupos de procesos.
- *User/groups ids*: Separación de usuarios y grupos.
- *Network*: Interfaces de red, reglas de ruteo e IPTables.
- *Mount*: Permiten crear diferentes *filesystem layouts*
- *IPC*: Permite intercomunicación entre procesos en diferentes espacios de 
  nombres.

Este mecanismo permite crear *servicios* aislados. La ventaja es que cada
contenedor contendrá sólo los componentes necesarios para lograr su objetivo y
accederá a un conjunto reducido de recursos del sistema.

Esto facilita la tarea a administradores y desarrolladores. La ventaja ante una
falla de seguridad o falla del sistema es que no debería afectar al resto del
sistema y otros contenedores.

Software para containers como [Docker](https://www.docker.com/) o
[Kubernetes](https://kubernetes.io/) son usados ampliamente para el desarrollo y
gestión de contenedores.