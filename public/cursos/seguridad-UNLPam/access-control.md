# Control de acceso

En un sistema multiusuario, con múltiples recursos accedidos por varios sujetos
hay que usar mecanismos de control de acceso para garantizar su confidencialidad
e integridad.

Generalmente esto requiere al menos dos mecanismos

1. ***Autenticación***: Verificación de la identidad de sujetos.

2. ***Autorización***: Asignación y verificación de los *permisos* o modos de
   acceso a los recursos para cada *sujeto*.

## Identidades, grupos y permisos

Los sujetos y objetos deberán identificarse en un sistema. Hay muchas formas de
identificar unívocamente a sujetos y objetos.

En un sistema operativo los usuarios generalmente se identifican por nombres en
que son cadenas de caracteres. Cada usuario debe tener un nombre único.

Muchos sistemas permiten definir *grupos de usuarios*. Un grupo puede
representar usuarios de una sección de la organización, su jerarquía o su rol.
Comúnmente un usuario puede pertenecer a varios grupos.

Los archivos de datos y programas comúnmente forman un espacio de nombres
jerárquicos. El sistema de archivos tiene al menos un *directorio* raíz. Un
directorio puede contener archivos y otros *subdirectorios*.
Así es posible reusar nombres de archivos en diferentes directorios ya que
pueden ser identificados por su *path* o *camino* desde la raíz.

En redes de computadoras hay que identificar los *nodos* (estaciones de trabajo,
gateways, etc) y posiblemente los enlaces. También hay que identificar procesos
dentro la red para que puedan comunicarse. En la familia de protocolos TCP/IP un
nodo se asocia con una o más direcciones IP asociadas a sus dispositivos de
comunicaciones (ej: interfaz Ethernet). Así un nodo puede ser identificado por
alguna de sus direcciones IP. Un proceso con acceso a mecanismos de
comunicaciones ejecutándose en un nodo se identifica por una tupla
*(Protocol,IP, port)* al que está asociado (*binded*).

Los objetos generalmente se asocian a los usuarios por la relación de su
*dueño*, comúnmente quien lo creó.

Los procesos (instancias de programas en ejecución) también deben identificarse
en el sistema de alguna manera. En los sistemas tipo UNIX se utilizan números
(*process id* o *pid*) que el sistema asigna en su creación (`fork()`). En los
sistemas MS-Windows se identifican por un par *(program_name, instance)*.

Un recurso u objeto del sistema comúnmente se asocia a usuarios. Por ejemplo, un
proceso en UNIX tiene un *user id (uid)* y el *group id (gid)* que se
corresponde a la sesión del usuario que lo ejecutó y se denominan el *usuario* y
*grupo* *efectivo*, que puede no ser el mismo que el *dueño* del programa.

El conjunto de *permisos de acceso* a un recurso pueden variar dependiendo del
tipo de recurso. Un archivo de datos, por ejemplo, podría tener asociado los
permisos de *lectura (r)*, *escritura (w)*, *append (a)*. Un *programa* o
*archivo ejecutable* podría tener permisos de *ejecución (x)*.

Otros recursos como un dispositivo o un descriptor de un objeto de
comunicaciones podría tener asociados permisos que representan el conjunto de
nodos o sistemas con el cual puede conectarse o la familia de protocolos que
puede usar.

En un sistema generalmente existe al menos un usuario *administrador* (*root* en
sistemas tipo UNIX) que tiene los mayores privilegios en el sistema con acceso
todos los recursos del sistema con todos los permisos de acceso.

## Autenticación

Esta actividad comúnmente se basa en dos estrategias principales:

1. El *sujeto* debe presentar algo que él conoce o sabe.

   Ejemplo: Una contraseña u otro dato secreto

2. El *sujeto* debe presentar algo que sólo él *debe posser*.

   - Una credencial o *token*.
   - Algún *dato biométrico*.

Estos dos mecanismos obviamente se pueden combinar para lograr la autenticación
de *múltiples factores*.

## Autorización

En un sistema con un conjunto $S$ de *sujetos* y on conjunto $O$ de *objetos* o
recursos y un conjunto $R$ de *permisos (rights)* de acceso se necesita una
relación $access = S \times O times R$, esto es, una relación entre los sujetos,
recursos y los permisos de acceso.

La relación $access$ puede obviamente representarse como una *matriz de control
de acceso* como se muestra a continuación.

O/S          | $file_1$ | $file_2$ | $file_3$ | ...
------------ | :------: | :------: | :------: | -----
$process_1$  |   rw-    |   r--    |   ---    | ...
$process_2$  |   ---    |   rwx    |   r-x    | ...

Sea $A$ una matriz de control de acceso y dados $s \in S$, $o \in O$ entonces $
A[s,o]=r$ con $r \subseteq R$.

Si bien a nivel de aplicación ésta podría implementar una matriz de esta forma,
como por ejemplo en bases de datos, a nivel de un sistema operativo esto es incoveniente.

En primer lugar sería un cuello de botella ya que el sistema debe consultarla en
forma casi permanente y además sería un problema de seguridad en sí con respecto
si esta estructura de datos se corrompe por algún motivo.

Por eso a nivel de sistemas, comúnmente se utilizan diferentes implementaciones
como listas de control de acceso, capacidades y 

### Listas de control de acceso

En este caso se representa la matriz por columnas, es decir que a cada recurso
(*objeto*) se asocian los pares de *sujetos* y *permisos*.

  - $ACL(o \in O) = \{(s,r) \mid s \in S, r \subseteq R\}$

Para simplificar su representación, comúnmente los sujetos se *agrupan* o
clasifican en:

  1. *Dueño* (creador) del recurso
  2. *Grupos*. Ejemplo: *group ids (gid)* en UNIX

Es común que los sistemas de archivos en los SO representan esta información
como *metadatos* de los archivos (recursos).

En los sistemas tipo UNIX, cada archivo tiene asociado los sujetos en tres grupos:

- El *dueño* o *user* del archivo.
- Un *grupo*
- El resto de usuarios (*others*)

Para cada grupo se asocia el conjunto de permisos 
$rights=\{(r)read,(w)rite,e(x)ecution\}$.

Esto permite una representación muy compacta de los permisos de acceso a un
archivo ya que sólo se necesitan 9 bits: 3 para cada grupo de sujetos.

Algunos sistemas permiten, además de esta representación compacta, asociar
realmente una lista por usuarios o grupos. Esta implementación se conoce como
*full acls*. La mayoría de los sistemas operativos modernos soportan full acls
en sus sistemas de archivos.

### Capacidades

Una *capacidad de un sujeto* $s \in S$, 
$cap(o)=\{(o,r) \mid o \in O y r \subseteq R\}$. 
Es posible ver como una implementación de la matriz de control
de acceso por filas.

Ciertas operaciones en un sistema tienen una API que se basa que la operación
sobre ciertos objetos debe estar precedida por una operación de *obtención de
derechos de acceso* al objeto. Un ejemplo de este modelo es la API de
programación para trabajar con archivos de la forma `fd = open(filename, mode)`,
`read/write(fd, data)` y `close(fd)`. El *file descriptor* o *handle* `fd` 
representa una *capacidad* de acceso la cual debe presentarse (como un
argumento) en las operaciones de lectura y escritura.

### Locks and keys

Esta técnica combina las capacidades con las listas de control de acceso.

Cada objeto se asocia con un *lock (cerrojo)*. Cada *lock* a su vez tiene
asociado una o más *keys (claves)*. Estas claves se asocian a los sujetos. Para
acceder a un objeto, un sujeto debe presentar una *clave*.

Este mecanismo se puede implementar usando criptografía. El *lock* de un objeto
es el resultado de su *cifrado* con una clave secreta. Los sujetos con acceso al
objeto deberán poseer la clave para *descifrarlo*.

Se puede evitar compartir claves secretas a los sujetos si para cada objeto se
asocian un conjunto de *locks* generados con diferentes *keys*. Así la función
de verificación de acceso se conoce como `or-access` ya que el objeto es
accedido si dados el lock $l$ y la clave $k$, $decipher(l,k)=o$.

### Sistemas de tipos

Este mecanismo se basa en que el *tipo* de objeto determina las operaciones que
pueden efectuarse sobre él. Por ejemplo, en un sistema de archivos tipo UNIX, no
es posible aplicar las operaciones `read/write` sobre directorios. En lugar de
ello hay que usar las llamadas al sistema especiales para leer y escribir
entradas en el directorio.

Otro ejemplo es el mecanismo de protección usado en algunos sistemas operativos
que evitan ejecutar instrucciones en áreas de datos (*data*, *heap* o *stack*)
de un proceso. En este caso también se restringen operaciones en base al *tipo
del objeto*.

## Control de acceso en UNIX

En los sistemas tipo UNIX (y compatibles POSIX) el sistema de control de acceso
está distribuido principalmente entre los subsistemas de archivos y el de
gestión de procesos.

Cada usuarios y grupo del sistema tiene un *identificador*: *uid* y *gid*,
respectivamente. Esos identificadores son números naturales.

Existen herramientas del sistema para gestionar la base de datos de usuarios y
grupos. Ver los comandos `useradd/groupadd`, `usermod/groupmod`,
`userdel/groupdel`.

La base de datos de usuarios están en los archivos `/etc/passwd` que contiene
registros de la forma `username:x:UID:GID:comment:home:shell` y
`/etc/shadow` el cual contiene las contraseñas (*hash*).

Los grupos están definidos en el archivo `/etc/group`. Cada registro tiene la
forma `groupname:x:GID:members`.

El super-usuario o *root* tiene *uid=0*. Cada *proceso* o *instancia de un
programa de ejecución* también tiene un *process id (pid)* y un *padre id
(ppid)*. El primer proceso del sistema (*init*), tiene *pid=1* y no tiene padre.

Así, los procesos quedan configurados en forma de árbol.

Cada archivo tiene asociado una lista permisos para cada usuario del sistema.
Esto implementa una lista de control de acceso.

- ***Permisos***: $\{\textbf{r}ead, \textbf{w}rite, e\textbf{x}ecution\}$
  En el caso de directorios, el permiso *x* habilita a *entrar* (con el comando *cd*)
- ***Recursos***: $\{archivos, memoria\}$
- ***Sujetos***: $\{usuarios, grupos, procesos\}$

Para lograr una representación compacta los *sujetos* asociados a cada archivo
se dividen en los siguientes clases:

1. El ***id del dueño (owner)*** o *user (u)* del archivo: Comúnmente el 
   usuario que lo creó.
2. El ***id del grupo*** *g*.
3. El *resto de usuarios* u ***others*** (*o*).

Los permisos de un archivo es una relación $(C, P)$, donde $C \in \{u,g,o\}$ y
$P \in \{r,w,x\}$ y se puede representar en 9 bits.

Los comandos `chown` y `chgrp` permiten cambiar el dueño, y grupo de archivos,
respectivamente.

El comando `chmod` permite cambiar los permisos de archivos.

### Procesos

Un proceso se ejecuta con los siguientes atributos de control de acceso:

- *Real user id (UID)*: El user id del dueño del programa
- *Effective user id (EUID)*: El usuario que lanzó o creó el proceso.
  Esto define los permisos de acceso a archivos, *shared memory*, *message
  queues* y otros recursos.

El comando `sudo <user> <cmd>` permite ejecutar `cmd` con el *effective user id*
de `user`. En MS-Windows, el comando análogo a `sudo` es `runas`.

### Cambios de privilegio

Además de los permisos asociados a los usuarios, los bits *setuid* y *setgid*
son aplicables a programas (archivos ejecutables). Estos bits, si están
encendidos, hacen que al ejecutarse, los permisos de acceso a recursos se toman
desde los permisos asociados al *real user/group* y no al usuario/grupo
efectivo.

Esto permite que algunos programas del sistema que pueden ser ejecutados por
cualquier usuario (ej: `passwd`) pueda acceder a recursos a los que puede sólo
acceder el *root*, por ejemplo a los archivos `/etc/passwd` y `/etc/shadow`.

Este mecanismos permite *escalar privilegios* a estos programas.

### Full ACLs

Los sistemas tipo UNIX modernos, como GNU-Linux, soportan *full acls*, es decir
definir con mayor detalle la lista concreta de usuarios y permisos de acceso.
Para mas detalles de su uso ver los comandos `getfacl` y `setfacl`.

### Capacidades en GNU-Linux

El uso de escalado de privilegio en base a mecanismos como el *sticky bit* no
cumple con la política del *menor privilegio*.

Sistemas como GNU/Linux usan *capacidades* asociadas a cada proceso para
permitir *refinar* o restringir el conjunto de permisos y operaciones.

Las capacidades permiten definir los permisos de acceso de un proceso con mayor
granularidad. Por ejemplo, es posible limitar que un proceso privilegiado
(*effective uid = 0*), ejecute con todos los permisos.

Para ver la lista de capacidades en GNU/Linux hacer `man 7 capabilites`.

El sistema operativo permite asociar capacidades a ejecutables.

1. En cada operación privilegiada, el kernel debe verificar que el proceso
   dispone de la capacidad correspondiente.
2. El kernel ofrece *syscalls* y comandos `getcap` y `setcap` permitiendo
   obtener y otorgar capacidades a procesos y programas.

> Por ejemplo, el comando `tcpdump` require privilegios de *root* para ser
> ejecutado ya que necesita capturar todos los paquetes que pasan por una
> interfaz de red dada. El administrador podría reducir los permisos dándole
> sólo los permisos de acceso a las operaciones de administración de interfaces
> de red.
>
> ```
> sudo setcap cap_net_raw,cap_net_admin=eip /usr/sbin/tcpdump
> ```
>
> Esto reduce los riesgos que se pueda usar alguna vulnerabilidad de `tcpdump` y
> usarse para realizar alguna otra acción con los privilegios de *root*.