# Modelos de seguridad

Existen varias propuestas de modelos formales de seguridad. Algunos tratan de
enfocar sólo en algunos aspectos de la seguridad por lo que es común que se usen
en forma combinada.

## Bell-La Padula (BLP)

Este modelo tiene como objetivo brindar ***confidencialidad***. Permite definir
políticas de seguridad en base a las restricciones de ***flujo de la
información*** con el objetivo de *prevenir revelación de información no
autorizada*. Este modelo se utiliza en algunas agencias gubernamentales y
militares.

Este modelo combina los modelos de control de acceso discrecional (*DAC*) y el
mandatorio (*MAC*) ya que un sujeto con permisos de acceso a un objeto puede
restringir el acceso a otros usuarios pero sólo el administrador del sistema
asigna los *niveles de seguridad* correspondientes de los sujetos y objetos.

Cada sujeto tienen un *nivel de autorización* o *clearance*. Cada objeto o
recurso pertenece a una categoría tiene también un *nivel de seguridad*.

El objetivo de este modelo es prevenir el acceso a un objeto de una clase de
seguridad alta a un sujeto con un nivel más bajo.

El modelo BLP consiste en los siguientes componentes: 

- Sea $S$ el conjunto de *sujetos* y $O$ el conjunto de *objetos*.

- Un orden parcial de *niveles de seguridad $L=\{l_1,...,l_n\}$* con 
  $l_i < l_{i+1}$, $1 \leq i < n$.

- Una función de asignación de niveles de autorización de los sujetos:
  $clearance: S \rightarrow L$.

- Una función de *clasificación* de objetos: $class: O \rightarrow L$.

- Una matriz de control de acceso $M: S \times O \times R$, con $R=\{read,
  write, append\}$.

- Reglas de acceso:

  1. ***simple-security***: $can\_read(s,o) = clear(s) \geq class(o) \land read \in M(s,o)$

  2. ***\*-property***: 
     - $can\_write(s,o) = clear(s) == class(o) \land write \in M(s,o)$
     - $can\_append(s,o) = clear(s) \leq class(o) \land append \in M(s,o)$.
     

La regla 1 establece que un sujeto puede leer un objeto sólo si su nivel
de autorización es mayor o igual que el nivel de la clase del objeto.

La regla 2 establece que un sujeto puede escribir un objeto sólo si la clase del
objeto tiene el mismo nivel que el de autorización del sujeto.
Además establece que un sujeto puede *agregar información* a un objeto si el
nivel de autorización del sujeto es igual o más bajo que el nivel de la clase
del objeto.

El sistema también debe verificar los permisos de acceso en $M$.

El sistema requiere que cada sujeto *inicie sesión* en el sistema mostrando su
*credencial de nivel de autorización*. Puede iniciar su sesión con un nivel de
autorización menor.

Por ejemplo, una organización gubernamental o militar define la siguiente
política de seguridad.

- $L = \{topsecret, secret, conf, public\}$
- $clear(A)=topsecret$, $clear(B)=secret$
- $class(doc_1)=conf$, $class(doc_2)=topsecret$
- $M(A,*)=\{read,write\}$,
  $M(B,doc_1)=\{read\}$,
  $M(B,doc_2)=\{read, write, append\}$

En éste ejemplo, $A$ puede leer $\{doc_1, doc_2\}$ y escribir $doc_2$; mientras
que $B$ puede leer $\{doc_1\}$ y puede *agregar información* (pero no leer) en $doc_2$.

### Estado del sistema y operaciones

Sea $b=\{(s_i, o_j, access\_mode), ...\}$ un conjunto de triplas de acceso
asignadas al momento y sean las siguientes operaciones de gestión del sistema de
control de acceso:

1. *Get/Release access*: Agregar/eliminar una tripla de $b$. 

   Ejemplo: Operaciones como `login,logout,open(obj,mode),close(obj), ...`

2. *clear(s_i)/class(o_j)*: Cambiar niveles de seguridad

3. *M(s_i, o_j)=rights*: Cambiar permisos de acceso

4. Crear/eliminar objetos y sujetos

Un modelo de seguridad debe asegurar que estas operaciones sólo permiten
realizar transiciones de un estado seguro a otro estado seguro.

Por ejemplo, un sistema que permite abrir un archivo a un sujeto con permisos de
escritura y mientras lo está haciendo otro sujeto le quita el permiso de
escritura. Si el sistema permite al primer usuario salvar las modificaciones
realizadas (ya que los permisos se verificaron en la apertura del archivo), es
discutible si el sistema permite realizar transiciones de un estado seguro a
otro.

## Modelo Biba

El objetivo de este modelo se basa en asegurar la *integridad* de los recursos
de un sistema.

Se basa en *niveles de integridad* ($I$). Los niveles forman un orden parcial
($<$) como en BLP y representan *niveles de confianza* de sujetos y objetos.

Un nivel $i$ domina al nivel $j$ si $I(i) < I(j)$.

La función $i: S \bigcup O \rightarrow I$ retorna el *nivel de integridad* de un
sujeto u objeto.

Las acciones que se pueden producir en el sistema son *leer*, *escribir* y
*ejecutar* objetos.

> [!NOTE|label:Definición]
> Una ***camino de transferencia de información*** es una secuencia de objetos
> $o_1,...,o_{n+1}$ y una secuencia correspondiente de sujetos $s_1,...,s_n$ tal
> que $s_i \: read \: o_i$ y $s_i \: write \: o_{i+1}$, para $1 \leq i \leq n$.

Intuitivamente, la información sólo puede *fluir* desde un objeto $o_1$ al
objeto $o_{n+1}$ mediante una sucesión de lecturas y escrituras.

Este modelo permite definir diferentes políticas de integridad.

### Política de la marca de agua inferior

Esta política, cuando un sujeto $s$ accede a un objeto $o$, cambia el *nivel de
integridad* del sujeto al $min(i(s),i(o))$ en base a las siguientes reglas:

1. $s \in S$ puede ***escribir*** $o \in O$ sí y sólo si $i(o) \leq i(s)$.
2. Si $s \in S$ lee $o \in O$, entonces $i'(s)=min(i(s),i(o))$, donde $i'(s)$ es
   el *nuevo* nivel de integridad de $s$.
3. Si $s_1 \in S$ puede ***ejecutar*** $s_2 \in S$ sí y sólo si $i(s_2) <
   i(s_1)$.

Las dos primeras reglas son obvias. La tercera impide que un sujeto *menos
confiable* pueda controlar la ejecución de otro *más confiable* con el riesgo de
*corromperlo*.

Esta política fuerza que en una *transferencia de información* desde $o_1 \in O$
hacia $o_{n+1} \in O$, $i(o_{n+1}) \leq i(o_1)$, para todo $n > 1$.

El problema de esta política es que siempre degrada el nivel de integridad de
los sujetos impidiendo que luego accedan a objetos con mayor nivel de
integridad.

Esta restricción puede eliminarse si se relaja la regla 1, permitiendo que los
sujetos puedan *leer* cualquier objeto.

### Política estricta

Este modelo es el dual del modelo BLP. Las reglas son las siguientes:

1. $s \in S$ puede ***leer*** $o \in O$ sí y sólo si $i(s) \leq i(o)$.
2. $s \in S$ puede ***escribir*** $o \in O$ sí y sólo si $i(o) \leq i(s)$.
3. $s_1 \in S$ puede ***ejecutar*** $s_2 \in S$ sí y sólo si $i(s_2) \leq
   i(s_1)$.

## Modelo Clask-Wilson

Este modelo define las siguientes entidades:

- *Sujetos*: Usuarios (o procesos) que requieren acceso a datos.
- *Datos (items) sin restricciones de acceso*
- *Datos restringidos*

Acceso a un dato: $access(subject, t, data) = Auth(subject, t, data))$.

La ***función de transformación*** *t* genera los permisos de acceso del sujeto al dato
requerido. La función *Auth(s,t,d)* realiza la identificación y autenticación
del sujeto *s* para el acceso del dato *d* mediante la operación *t*. Si está
función es exitosa (retorna *true*) el sujeto puede acceder al dato.

El sistema contiene un conjunto de transformadores. El sistema define un
conjunto de *transformadores* para cada dato. Es posible ver que la función de
autorización se basa en un conjunto de triplas (relación) de acceso la forma
*(subject, transformer, data)*.

## Modelo de Brewer and Nash (muralla China)

Este modelo es *híbrido* ya que tiene como objetivos garantizar confidencialidad
e integridad. Las políticas en este modelo describen escenarios con *conflictos
de intereses*, principalmente en el ámbito empresarial y de negocios.

Por ejemplo, este modelo ha inducido la sanción de leyes en ámbitos de bolsas de
comercio para prevenir que un *corredor (trader)* representando dos clientes con
conflictos de intereses y el corredor podría usar información de un cliente para
favorecer al otro.

En este modelo se definen los siguientes componentes:

- *Conjuntos de datos de companías (datasets)* $DS = \{DS_1,...,DS_n\}$ donde
  cada $DS_i$, con $1 \leq i \leq n$ son los *objetos* de la companía $i$.
- Una relación de *conflictos de intereses* ($CI: DS_i \times DS_j$) con
  $DS_i,DS_j \in DS$.

Por ejemplo, sean los *datasets* correspondientes a bancos y empresas
petroleras. Los conflictos de intereses se dan entre los datasets de los bancos
y los de las petroleras.

- Datasets: $\{Bank_1, Bank_2, Oil_1, Oil_2...\}$
- CI: $\{\{Bank_1, Bank_2\},\{Oil_1, Oil_2\}\}$

> [!NOTE|label:Objetivo]
> Un sujeto sólo puede acceder a objetos que no están en conflicto con otros 
> objetos a los que ya accedió previamente

### Reglas de acceso

La notación $DS_i(o)$ con $o \in O$ significa $o \in DS_i$. $DS(o) = DS(o')$
significa que $o$ y $o'$ están en el mismo dataset. La función $accessed: S
\rightarrow O$, retorna el conjunto de objetos accedidos previamente por un
sujeto $s \in S$.

- $s \in S$ puede *leer* $o \in O$  ($can\_read(s,o)$) sólo si 

  1. $\exist o' \mid DS(o)=DS(o') \land o' \in accessed(s)$, **o**
  2. $o \in DS_i \land \forall o' \in DS_k$, $o \neq o'$ tal que $(DS_i,DS_k)
     \in CI, \implies o' \notin accessed(s)$

- $s \in S$ puede *escribir* $o \in O$ sólo si

  1. $s$ puede leer $o$, **y**
  2. $\forall o' \mid can\_read(s,o') \implies DS(o) = DS(o')$

Dados los datasets y conflictos de intereses dados anteriormente y las
siguientes acciones del sujeto $A$, las reglas de acceso establecen que:

1. $A$ *lee* $d \in Bank_1$
   - $A$ *puede leer* cualquier $d' \in Bank_1$
   - $A$ *no puede leer* ningún documento de $Bank_2$
   - $A$ puede leer documentos de $Oil_1$ o $Oil_2$

2. $A$ *escribe* $d \in Bank_1$
   - Luego, $A$ *puede escribir* archivos de $Bank_1, Oil_1, Oil_2$

3. $A$ *lee* $d \in Oil_1$
   - Luego, $A$ puede leer y escribir archivos de $Bank_1$ y $Oil_1$ pero no
     puede leer o escribir archivos de $Bank_2$ y $Oil_2$.

## Modelos y estándares de análisis de seguridad

Existen varios modelos y estándares de seguridad que permiten determinar el
nivel de seguridad en aplicaciones, sistemas, métodos de desarrollo y
organizaciones.

Algunos de los estándares ampliamente usados son aquellos publicados por la
[International Organization for Standarization - ISO](https://www.iso.org/) en
la sección de [IT security
standards](https://www.iso.org/search.html?PROD_isoorg_en%5Bquery%5D=it%20security).

El [Software Assurance Maturity Model - SAMM](https://owaspsamm.org/model/) es
un estándar abierto (libre) desarrollado por la
[OWASP Foundation](https://owasp.org/) que permite el análisis y la medición de
niveles de seguridad en sistemas de software y comunicaciones para cualquier
tipo de organización, incluyendo las de desarrollo de software o servicios
informáticos.

Este es un framework flexible simple de usar y que permite el involucramiento de
personas que no son especialistas en seguridad informática.

El modelo se basa en alrededor de 15 *prácticas de seguridad* estructuradas en 5
*funciones de negocio* o categorías.

Las *prácticas de seguridad* incluyen actividades y se dividen en dos *flujos* o
*streams*. Cada flujo cubre diferentes aspectos y pueden tener diferentes
objetivos.

Para cada *práctica* se definen tres *niveles de madurez*. La siguiente tabla
muestra una tabla que relaciona funciones o áreas, prácticas y flujos.

| Práctica              | Flujo A         | Flujo B       |
| --------------------- | --------------- | ------------- |
| Estrategia y métricas | Crear/Promover  | Medir/mejorar |
| Políticas             | Definir/adoptar | Gestión       |
| Educación/guías       | Entrenamiento   | Organización  |

Los *niveles de seguridad de las actividades* permiten medir los objetivos de
cada actividad:

1. Identificar riesgos y objetivos.
2. Definir una estrategia o políticas de seguridad.
3. Realizar mediciones de riesgos y vulnerabilidades y aplicar mecanismos.

El modelo conduce a la definición de actividades y su agrupación en base a
ejemplos con tablas de preguntas/respuestas y listas de verificación.

Las prácticas de seguridad son de los siguientes tipos:

- Identificación de riesgos y amenazas: Valoración de recursos y modelado de
  *amenazas*.
- Definición de *requerimientos y políticas de seguridad*.
- Definición o uso de una *arquitectura de seguridad* en software y en
  infraestructura. Esto puede incluir macanismos de respaldo de datos,
  autenticación, control de acceso, seguridad en la comunicaciones, etc.

El documento describe el modelo y presenta guías prácticas para definir
claramente cada una de las actividades y clasificarlas o asignarlas a diferentes
áreas de la organización.