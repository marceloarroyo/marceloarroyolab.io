<meta charset="utf-8" lang="en">

**Seguridad informática**
    Ingeniería en Computación - Fac. Ingeniería - UNLPam

<small><em>2023</em></small><br>
Marcelo Arroyo

---

# Algo de historia

---

## Los primeros ataques

<div class="small">

- 1971: Bob Thomas lanza el *gusano* [Creeper](https://en.wikipedia.org/wiki/Creeper_and_Reaper) (inofensivo) en ARPANET. *Reaper* fue el primer antivirus (para eliminar *Creeper*)

- 1986: Markus Hess accede a sistemas de universidades y militares en USA.

- 1988: Morris lanza un *gusano* en Internet

- 2007: USA e Israel atacan a refinerías de Irán vía MS-Windows

- 2015: Robo millonario a bancos chinos, rusos y otros

- 2017: Virus del tipo *ransomware* causan graves pérdidas

</div>

---

# Algunos conceptos

---

## Seguridad informática[^fn1]

### Definición

> "Conjunto de actividades, métodos y mecanismos usados en un sistema de computación para preservar la **confidencialidad**, **integridad** y la **disponibilidad** de los recursos a los sujetos autorizados"
> 
>                                                   -- NIST SP 800-16

[^fn1]: También conocido como *ciberseguridad*.

---

## Algunos conceptos principales

- ***Confidencialidad***: Los *recursos* no son accesibles a *sujetos* no 
  autorizados. ***Privacidad***: Los *sujetos* controlan cómo revelar sus recursos y a quién.

- ***Integridad***: Los *recursos* pueden ser alterados por *sujetos* autorizados y en 
  forma específica.

- ***Disponibilidad***: El sistema funciona adecuadamente y brinda los servicios a los 
  *sujetos* autorizados

!!!
   ***Sujeto***: programa/proceso/persona. ***Recurso***: archivo, carpeta, ...

---

## Otros conceptos adicionales

- ***Autenticidad***: Verificación de que los *sujetos* son quienes dicen ser.<br>
  Esto permite el *no repudio*

  1. ***de origen***: Autenticidad del *originador* del *dato* o *mensaje*
  2. ***de destino***: Autenticidad del *receptor* del *dato* o *mensaje*

- ***Contabilidad***: Bitácora (*log*) de operaciones ocurridas

  - Útil para *descubrir* brechas de seguridad y/o *recuperación*

---

## Terminología

<div class="small">

Adversario
:   Sujeto o grupo que intenta *aprovechar* alguna *vulnerabilidad*

Amenaza(*threat*)
:   Acctión que pone en *compromiso* la seguridad de un sistema

Ataque
:   Acción deliberada que intenta *quebrar* la seguridad de un sistema

Contramedida
:   Mecanismo para *prevenir* ataques

Política
:   Criterios o reglas que definen los servicios de seguridad

</div>

---

## Terminología (cont.)

<div class="small">

Mecanismo
:   Técnicas o algoritmos para *implementar* una política de seguridad

Riesgo
:   Medida o costo de un *recurso* afectado en un *ataque*

Vulnerabilidad
:   Debilidad en un sistema para lograr sus políticas de seguridad

Malware
:   Software que implementa un mecanismo o procedimiento para *esquivar* los mecanismos 
    de seguridad de un sistema

</div>

---

## Ataques

<div class="small">

Pasivo
:   El atacante *observa* recursos y/o comportamiento del sistema (análisis de tráfico, redirección de mensajes, ...)

Activo
:   El atacante logra *alterar* o *impedir acceso* a recursos (*replay*: captura y reenvío de mensajes, *enmascarado*, *impedimiento de servicios*, ...)

Interno
:   Tiene origen dentro de la organización/sistema

Externo
:   Se origina desde fuera de la organización/sistema

</div>

---

## Amenazas y ataques

Amenaza                 | Ataque
----------------------- | ---------------------------------------------
Acceso no autorizado    | Revelación de información confidencial
Engaño (deception)      | Sustitución de identidad, falsificación, ...
Ruptura (disruption)    | Interrupción de operaciones/servicios
Usurpación              | Toma de control no autorizado, ...

---

## Software malicioso (malware)

Nombre      | Descripción
----------- | -------------------------------------------------------
Virus       | Código integrado en programas/datos que se *inyecta* en otros
Gusano      | Programa que puede *copiarse* a otros nodos en redes
Troyano     | Programa malicioso enmascarado como otro de uso común
Logger      | Captura y almacena/transmite eventos del usuario
Bot         | Programa para lanzar un ataque programado

---

## Herramientas para atacantes

Nombre      | Descripción
----------- | -------------------------------------------------------
Spammer kit | Programas para envío masivo de mensajes (mail)
Malware kit | Herramientas de generación de programas de ataque
Root kit    | Programas instalados luego de ganar acceso como *root*
Exploit     | Código para explotar una vulnerabilidad específica

---

## Herramientas de defensa

Nombre      | Descripción
----------- | ------------------------------------------------------------
Antivirus   | Reconocedor de programas con *patrones* de malware conocidos
Firewall    | Filtrado de mensajes de red
Monitoreo   | Monitores de actividad maliciosa y/o intrusiones
Scanners    | Herramientas para detectar vulnerabilidades
Vuln. DBs   | Bases de datos de vulnerabilidades conocidas (ej: [NVD](https://nvd.nist.gov/))

---

# Diseño de la seguridad

---

## Arquitectura de seguridad (OSI X800)

<div style="columns: 2; margin-top: 4rem; vertical-align: middle;">

- Ataques: Pasivo/activo
- Mecanismos: firewalls, criptografía, ...
- Servicios: Autenticación, control de acceso, ...

![OSI X800](img/security-architecture.png)

</div>

---

## Requerimientos funcionales (servicios)

- ***Control de acceso***: Incluyendo *identificación* y *autenticación* de sujetos
- ***Entrenamiento/concientización en seguridad*** de técnicos y usuarios
- ***Auditoría (logging)***: Permiten el monitoreo y análisis de actividades
- ***Análisis de seguridad***: Pentesting, riesgos, ...
- ***Gestión de configuración***
- ***Verificación de integridad***
- ***Respuesta a incidentes***
- ***Gestión de claves públicas***: Cuando se usa *criptografía asimétrica*

---

## Principios de diseño de la seguridad

- Mecanismos simples y usables
- Mecanismos abiertos (no secretos) bien probados o verificados
- Acceso a recursos basado en *permisos* con el *default=no-access*
- Control de acceso *completo* (verificación en cada acceso al recurso)
- *Menor privilegio*: Una operación debe usar el conjunto mínimo de permisos 
  necesaria
- *Separación de privilegios*: Las *actividades* y *sujetos* se *dividen* en partes
  y cada una opera con los privilegios mínimos necesarios para acceder al recurso
- *Aislamiento* de componentes (procesos, máquinas virtuales, containers, ...)
- Diseño *modular* (generalmente en capas)

---

## Desarrollo de software seguro

- Definir políticas de desarrollo seguro
- Definir un conjunto mínimo de herramientas, *bibliotecas* y *frameworks* de 
  software a usar
- Definir y verificar *estilos* y/o *patrones* de diseño e implementación
- Implementar técnicas de *programación defensiva*
- Usar herramientas de análisis de errores y vulnerabilidades en código
- Incluir casos de tests de seguridad
- Formar al equipo sobre diseño e implementación de *desarrollo seguro*

---

## Política de seguridad

Es un documento que

> Especifica un conjunto de *reglas* y *prácticas* que regulan cómo una 
> organización o sistema provee servicios de seguridad. Debe hacer referencia a
> requerimientos de *confidencialidad*, *integridad* y *disponibilidad*.

1. Debe *identificar los recursos críticos*
2. Las posibles vulnerabilidades del sistema
3. Las amenazas y los ataques posibles

---

## Implementación de políticas de seguridad

- ***Prevención***: El objetivo ideal a alcanzar
  - difícil de conseguir plenamente
  - Técnicas: diseños e implementaciones seguros, uso de mecanismos de seguridad, ...
- ***Detección***: Un sistema de alertas permite actuar en consecuencia
  - Firewalls (cortafuegos)
  - Sistemas de detección de intrusiones
  - Monitores de fallas
- ***Respuesta***: Acciones para *frenar* o *atenuar* un ataque
- ***Recuperación***: *logging*, *respaldos*, *transacciones*, ...

---

# Estándares

---

## Organizaciones y estándares

- [National Institute of Standards and Technology (NIST)](https://www.nist.org): 
  Propone estándares en USA que son generalmente adoptados en otros países

- [Internet Society](https://www.internetsociety.org/): Incluye varios grupos como el 
  *Engineering Task Force (IETF)* y  el *Internet Architecture Board (IAB)*. Producen
  los *Request for Comments (RTFCs)*

- [International Telecommunication Union (ITU)](https://www.itu.int/): El sector de 
  estandarizaciones se conoce como *ITU-T* 

- [International Organization for Standarization (ISO)](https://webstore.ansi.org/): 
  Federación de organizaciones de estandarización a nivel mundial 

<!-- ======================================================================== -->
<style>
    .small {font-size: 0.75rem;}
    blockquote {background-color: lightgray !important;}
    .markdeepFooter {display: none;}
</style>

<!-- Markdeep slides stuff -->
<script>
    markdeepSlidesOptions = {
        aspectRatio: 16 / 9,
        theme: 'simple',
        fontSize: 28,
        diagramZoom: 1.0,
        totalSlideNumber: false,
        progressBar: true,
        breakOnHeadings: false,
        slideChangeHook: (oldSlide, newSlide) => {},
        modeChangeHook: (newMode) => {}
    };
</script>
<link rel="stylesheet" href="css/relativize.css">
<link rel="stylesheet" href="css/markdeep-slides.css">
<script src="js/markdeep-slides.js"></script>

<!-- Markdeep stuff -->
<script>
    markdeepOptions = {
        tocStyle: 'none',
        onLoad: function() {
            initSlides();
        }
    };
</script>
<style class="fallback">
    body{visibility:hidden}
</style>
<script src="js/markdeep.js" charset="utf-8"></script>
<script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script>
