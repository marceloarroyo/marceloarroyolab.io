# Seguridad informática

En la actualidad, prácticamente cualquier actividad utiliza sistemas de
computación. Estos sistemas comúnmnente se encuentran interconectados por redes
e Internet. Es común que las personas realicen sus actividades en forma remota,
accediendo desde su computadora a recursos de otros sistemas. Muchas de estas
actividades involucran el uso de recursos críticos como cuentas bancarias,
información personal, uso de geolocalización y otros.

Esta modalidad común de trabajo y funcionamiento de los sistemas tiene grandes
ventajas para la humanidad pero también genera problemas como garantizar la
confidencialidad de cierto tipo de información y privacidad de los usuarios.

El hardware y software han evolucionado en sistemas muy complejos en los cuales
lograr su completo entendimiento y control es extraordinariamente difícil.

Esto hace que el campo de las ciencias de la computación que se encarga de
estudiar los problemas y soluciones de seguridad actualmente haya adquirido una
gran importancia ya que se requiere que los profesionales y técnicos de la
disciplina adquieran capacidades de comprender los problemas, definir políticas
de seguridad e implementar mecanismos para su implementación, tanto a nivel de
administración de sistemas y redes como también en el proceso de desarrollo de
software.

## Algo de historia

Desde los primeros desarrollos de las redes de computadoras se comenzaron a
registrar problemas de seguridad, como se muestra en algunos de los siguientes
casos relevantes.

- 1971: Bob Thomas lanza el *gusano*
  [Creeper](https://en.wikipedia.org/wiki/Creeper_and_Reaper) (inofensivo) en
  ARPANET. *Reaper* fue el primer antivirus (para eliminar *Creeper*)

- 1986: Markus Hess accede a sistemas de universidades y militares en USA.
  Descubierto por Cliff Stoll y descrito en *The cuckoo's egg*, ISBN13:
  9781416507789, (traducido al castellano como *El gato y el ratón*).

- 1988: Morris lanza un *gusano* en Internet, como un experimento. El programa
  trataba de aprovecharse (*explotar*) de vulnerabilidades de los programas
  `sendmail` y `finger` y de cuentas de usuarios sin contraseñas vía *ejecución
  remota* o *login remoto* (*remote shell*).

- 2007: Se descubre que el gusano
  [stuxnet](https://en.wikipedia.org/wiki/Stuxnet) fue diseñado específicamente
  para acceder y controlar PLCs y SCADA de Siemens usados en el control de reactores
  nucleares. Comúnmente se usaron pendrives infectados para su distribución.
  Irán fue una de las naciones principalmente atacadas, aparentemente con el
  objetivo de retrasar sus avances en el uso de energía nuclear por parte de USA
  e Israel.

- 2015-2017: Robo millonario a bancos coreanos, chinos, rusos y otros.

- 2017-actualidad: Virus del tipo *ransomware* causan graves pérdidas en todo el
  mundo.

Además de los casos mencionados se pueden encontrar numerosos informes de
ataques a gobiernos, sistemas de voto electrónico, empresas tarjetas de crédito,
bancos y a individuos en todo el mundo.

## Algunos conceptos

La seguridad en informática, también conocida como *ciberseguridad* tiene varias
definiciones. A continuación se muestra la traducción al castellano de la de
definición propuesta por el [National Institute of Standards and
Technology](https://www.nist.gov/).

> [!NOTE|label:Definición]
> ***Seguridad informática***: "Conjunto de actividades, métodos y mecanismos
> usados en un sistema de computación para preservar la **confidencialidad**,
> **integridad** y la **disponibilidad** de los recursos a los sujetos
> autorizados"
> 
>  -- NIST SP 800-16

Los problemas o *propiedades* de seguridad pueden clasificarse en los siguientes grupos.

- ***Confidencialidad***: Los *recursos* no son accesibles a *sujetos* no 
  autorizados. 
  
  La ***privacidad*** se refiere a la capacidad de los *sujetos* de poder
  controlar cómo se pueden revelar sus recursos y a quiénes.

- ***Integridad***: Los *recursos* pueden ser alterados por *sujetos* autorizados y en 
  la forma específicada por el sistema.

- ***Disponibilidad***: El sistema funciona adecuadamente y brinda los servicios a los 
  *sujetos* autorizados en tiempo y forma.


> [!NOTE|label:Definiciones]
> - ***Sujeto***: programa/proceso/persona. 
> - ***Recurso***: archivo, carpeta, conexión de red, ...

Algunos autores extienden los *tres pilares* de la seguridad con las siguientes
propiedades de seguridad.

- ***Autenticidad***: Verificación de que los *sujetos* son quienes dicen ser.
  Esto permite el *no repudio*

  1. ***de origen***: Autenticidad del *originador* del *dato* o *mensaje*
  2. ***de destino***: Autenticidad del *receptor* del *dato* o *mensaje*

## Terminología

En seguridad se utilizan un conjunto de *términos* que es conveniente aclarar su
significado.

- ***Adversario***: Sujeto o grupo que intenta *aprovechar* alguna *vulnerabilidad*

- ***Amenaza(*threat*)***: Acción que *compromete* la seguridad de un sistema.

- ***Ataque***: Acción deliberada que logra *quebrar* la seguridad de un sistema.

- ***Contramedida***: Mecanismo para *prevenir* o *repeler* ataques.

- ***Política de seguridad***: Criterios o reglas que definen los servicios de
  seguridad que debe cumplir un sistema.

- ***Mecanismo***: Técnicas, herramientas y algoritmos para *implementar* una
  política de seguridad.

- ***Riesgo***: Medida o costo de un *recurso* afectado en un *ataque*. Esto
  permite definir los mecanismos a usar para asegurar su seguridad.

- ***Vulnerabilidad***: Debilidad en un sistema para lograr sus políticas de
  seguridad. Esto se puede deber a un error de diseño (*flaw*) o un error de
  implementación (*bug*).

- ***Malware***: Software que implementa un mecanismo o procedimiento para
  *esquivar* o aprovechar vulnerabilidades de los mecanismos de seguridad de un
  sistema.

## Ataques

Un ataque puede clasificarse en dos grandes grupos:

1. ***Pasivo***: El atacante puede *observar* recursos y/o comportamiento del
   sistema (análisis de tráfico, redirección de mensajes, etc).

2. ***Activo***: El atacante logra *alterar* o *impedir acceso* a recursos.

   Un *replay attack* captura y reenvío de mensajes (posiblemente alterados).
   
   Un ataque por *masking* es cuando el atacante se hace pasar por otro sujeto.
   
   Un ataque por *impedimiento de servicios* (*deny of service* o *DoS*) es
   cuando el ataque logra alterar el funcionamiento del sistema para que éste no
   pueda brindar los servicios a sus usuarios apropiadamante. Esto se puede
   lograr por *sobrecarga*, *caída* o de un sistema o *impidiendo el acceso* a
   los usuarios legítimos.

Otra clasificación se basa en el lugar de origen del ataque.

1. ***Interno***: Tiene origen dentro de la organización o sistema.

2. ***Externo***: Se origina desde fuera de la organización o sistema

## Amenazas y ataques

A veces no es fácil distinguir entre qué es una amenaza y un ataque. La
siguiente tabla permite aclarar estos conceptos mediante algunos ejemplos.

Amenaza                 | Ataque
----------------------- | ---------------------------------------------
Acceso no autorizado    | Revelación de información confidencial
Engaño (deception)      | Sustitución de identidad, falsificación, ...
Ruptura (disruption)    | Interrupción/alteración de operaciones/servicios
Usurpación              | Toma de control no autorizado, ...

Tabla 1: Amenzas versus ataques.

Por ejemplo, un intento de engaño a un sujeto para qu éste divulge alguna de sus
credenciales de acceso (secretos) puede habilitar (en caso de ser exitoso)
ataques como sustitución de  identidad o acceso a operaciones del sistema que
pueden terminar en un ataque de toma de control de parte o totalidad del
sistema, dependiendo del *nivel de privilegio* del usuario engañado.

## Software malicioso (malware)

Los ataques a un sistema pueden hacerse con *programas maliciosos* o *malware*.

Dependiendo del modo de ataque que implementan se clasifican en las siguientes
categorías.

- ***Virus***: Programa que al ser ejecutado *inyecta* o *propaga* código
  malicioso en otros programas de uso común. Esto hace que los usuarios ejecuten
  el código malicioso de forma *transparente*.
- ***Gusano (worm)***: Programa que puede *copiarse* a otros nodos en redes. En
  cada nodo puede a su vez actuar como un virus.
- ***Troyano***: Programa malicioso enmascarado como otro de uso común.
  Comúnmente se comporta normalmente pero además realiza acciones maliciosas. La
  diferencia con un virus es que comúnmente se instalan modificando el código
  del programa original y los usuarios lo descargan engañados, pensando que se
  trata de un programa benigno.
- ***Logger***: Programa que captura y almacena/transmite eventos del usuario.
  Este es un ejemplo de un *malware pasivo*. Esto permite al atacante
  *descubrir* secretos, formas de uso del sistema, etc.
- ***Bot***: Programa para lanzar un ataque programado. Comúnmente un bot
  participa de un ataque en red coordinado.

## Herramientas

Los atacantes y los especialistas en seguridad desarrollan herramientas para
realizar ataques o hacer análisis de seguridad de sistemas.

Estas herramientas pueden clasificarse en las siguientes categorías.

- ***Spammer kit***: Programas para envío masivo de mensajes por mail u otros
  servicios de mensajería.

- ***Malware kit***: Conjunto de aplicaciones de generación de programas de
  ataque en base a vulnerabilidades conocidas.

- ***Root kit***: Conjunto de *programas del sistema adulterados como malware*
  instalados luego de ganar acceso como usuarios privilegiados (*root* o
  *administrador*).

- ***Exploit***: Código para explotar una vulnerabilidad específica.

Para implementar mecanismos de defensa y de búsqueda o análisis de
vulnerabildades se pueden mencionar las siguientes clases de herramientas.

- ***Antivirus***: Reconocen de programas instalados o en ejecución con
  *patrones* o fragmentos de malware conocidos. Estas herramientas no son
  efectivas ante *ataques de día cero*, es decir nuevos virus aún no
  catalogados.

- ***Firewall***: Permite implementar reglas de control (filtrado) de mensajes
  de red.

- ***Monitoreo***: Permiten registrar actividades de un sistema y detectar
  comportamientos maliciosos o amenazas.

- ***Scanners***: Herramientas para detectar vulnerabilidades en aplicaciones.
  Estas herramientas comúnmente se basan en vulnerabilidades conocidas y pueden
  usar diferentes estrategias:

  1. Buscar versiones de aplicaciones vulnerables.
  2. Simular ataques (usando *exploits*) para detectar vulnerabilidades.

La comunidad internacional ha desarrollado *bases de datos de vulnerabilidades*
conocidas para su análisis y el desarrollo de contramedidas. 

Estas bases de datos se conocen como *vulnerability database* y son de acceso
público, como por ejemplo [NVD](https://nvd.nist.gov/ "National Vulnerability Database")).


## Diseño de la seguridad

Existen varias propuestas y estándares para el desarrollo de una arquiitectura
de seguridad en origanizaciones y sistemas. Un estándar comúnmente referenciado
para seguridad en redes es el modelo [OSI X800: Security Architecture for Open
Systems Interconnection for CCITT
Applications](https://www.itu.int/rec/T-REC-X.800-199103-I/en) desarrollado por
la [International Telecommunication Union](https://www.itu.int/).

Este estándar propone diferentes mecanismos y servicios de seguridad y en qué
capas debería aplicarse. Propone los siguientes servicios.

1. Autenticación: Confirma que la identidad de una o más entidades conectadas a
   una o más entidades sea verdadera. Entiéndase por entidad un usuario,
   proceso, mensaje o sistema. De igual forma corrobora a una entidad que la
   información proviene de otra entidad verdadera. Este servicio puede
   implementarse en cualquier capa. Por ejemplo, la autenticación de usuarios
   comúnmente se puede hacer a nivel de sesión o de aplicación, mientras que la
   autenticación de hosts podría hacerse a nivel de red. Los protocolos de
   comunicación segura, como IPSec o SSL comúnmente utilizan mecanismos como
   *message authentication codes (MAC)*.
    
2. Control de acceso: Protege a una entidad contra el uso no autorizado de sus
   recursos. Este servicio de seguridad se puede aplicar a varios tipos de
   acceso, por ejemplo el uso de medios de comunicación, la lectura, escritura o
   eliminación de información y la ejecución de procesos.
   
3. Confidencialidad: Protege a una entidad contra la revelación deliberada o
   accidental de cualquier conjunto de datos a entidades no autorizadas.
   Ese servicio comúnmente se implementa a nivel de red (ej: IPSec), de
   transporte (ej: SSL) o de aplicación.
   
4. Integridad: Asegura que los datos almacenados en las computadoras y/o
   transferidos en una conexión no fueron modificados. Estos servicios
   comúnmente se integran en los protocolos seguros como por ejemplo, mediante
   el uso de funciones hash y *MAC*.
   
5. No repudio: Este servicio protege contra usuarios que quieran negar
   falsamente que enviaran o recibieran un mensaje. Este servicio se implementa
   en protocolos seguros o a nivel de aplicación comúnmente usando mecanismos
   basados en firma digital.

## Requerimientos funcionales (servicios)

Los requerimientos de seguridad generalmente requieren que se implementen los
siguientes servicios de seguridad:

- ***Control de acceso***: Incluyendo *identificación* y *autenticación* de sujetos
- ***Entrenamiento/concientización en seguridad*** de técnicos y usuarios
- ***Auditoría (logging)***: Permiten el monitoreo y análisis de actividades
- ***Análisis de seguridad***: Pentesting, riesgos, ...
- ***Gestión de configuración***
- ***Verificación de integridad***
- ***Respuesta a incidentes***
- ***Gestión de claves***: Servicios de manejo de claves secretas y públicas.

## Principios de diseño de la seguridad

Los especialistas en seguridad proponen los siguientes principios de diseño en
seguridad.

- Mecanismos simples y usables

- Mecanismos abiertos (no secretos) bien probados o verificados

- Acceso a recursos basado en *permisos* con el *default=no-access*

- Control de acceso *completo* (verificación en cada acceso al recurso)

- *Menor privilegio*: Una operación debe usar el conjunto mínimo de permisos 
  necesaria para que pueda realizarse

- *Separación de privilegios*: Las *actividades* y *sujetos* se *dividen* en partes
  y cada una opera con los privilegios mínimos necesarios para acceder al recurso

- *Aislamiento* de componentes (procesos, máquinas virtuales, containers, ...)

- Diseño *modular* (generalmente en capas)

## Desarrollo de software seguro

Los desarrolladores de software deben contemplar las propiedades de seguridad
requeridas para la aplicaciones. Actualmente las propiedades de seguridad son
los principales requisitos no funcionales de un sistema.

Un método o proceso de desarrollo de software debería incluir las siguientes actividades.

- Definir políticas de desarrollo seguro
- Definir un conjunto mínimo de herramientas, *bibliotecas* y *frameworks* de 
  software a usar
- Definir y verificar *estilos* y/o *patrones* de diseño e implementación
- Implementar técnicas de *programación defensiva*
- Usar herramientas de análisis de errores y vulnerabilidades en código
- Incluir casos de tests de seguridad
- Formar al equipo sobre diseño e implementación de *desarrollo seguro*

## Política de seguridad

Una *política de seguridad* Es un documento que

> Especifica un conjunto de *reglas* y *prácticas* que regulan cómo una 
> organización o sistema provee servicios de seguridad. Debe hacer referencia a
> requerimientos de *confidencialidad*, *integridad* y *disponibilidad*.

La forma en que se especifica una política debe incluir:

1. La *identificación de los recursos críticos*
2. Las posibles vulnerabilidades del sistema
3. Las amenazas y ataques posibles

Una política de seguridad debe *implementarse* mediante *mecanismos de
seguridad*. Una política debe tener algunos de los siguientes objetivos.

- ***Prevención***: El objetivo ideal a alcanzar
  - difícil de conseguir plenamente
  - Técnicas: diseños e implementaciones seguros, uso de mecanismos de seguridad, ...
- ***Detección***: Un sistema de alertas permite actuar en consecuencia
  - Firewalls (cortafuegos)
  - Sistemas de detección de intrusiones
  - Monitores de fallas
- ***Respuesta***: Acciones para *frenar* o *atenuar* un ataque
- ***Recuperación***: *logging*, *respaldos*, *transacciones*, ...

En los próximos capítulos se analizan diferentes macanismos para implementar
políticas de seguridad.

## Organizaciones y estándares

- [National Institute of Standards and Technology (NIST)](https://www.nist.org): 
  Propone estándares en USA que son generalmente adoptados en otros países

- [Internet Society](https://www.internetsociety.org/): Incluye varios grupos como el 
  *Engineering Task Force (IETF)* y  el *Internet Architecture Board (IAB)*. Producen
  los *Request for Comments (RTFCs)*

- [International Telecommunication Union (ITU)](https://www.itu.int/): El sector de 
  estandarizaciones se conoce como *ITU-T* 

- [International Organization for Standarization (ISO)](https://webstore.ansi.org/): 
  Federación de organizaciones de estandarización a nivel mundial 
