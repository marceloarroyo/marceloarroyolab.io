<!-- slide -->

# Identidad y credenciales

## Seguridad Informática

#### Fac. Ingeniería - UNLPam

Marcelo Arroyo

<!-- slide -->

#### Control de acceso basado en atributos (ABAC)

!!! Las autorizaciones se basan en *atributos* de los sujetos, objetos y del *ambiente*

***Atributos***: Características requeridas y definidas por una *autoridad*

1. De *sujetos*: Identificador, organización, título, etc.

2. De *objetos*: Título, autor, tipo, etc.

3. Del *ambiente*: Fecha/hora actual, ubicación, etc.

<!-- slide -->

#### Políticas de seguridad en ABAC

Una política de seguridad se define en base a

1. Definición de los atributos

2. Asignación: $attrs(S)$, $attrs(O)$ y $attrs(E)$

3. *Reglas de acceso* de la forma:
   $acc(s,o,e) = f(attr(s), attr(o), attr(e))$

!!! note: Política de seguridad
    Conjunto de reglas

<!-- slide -->

### Un ejemplo

**Servicio** de alquiler de películas

1. La política define accesos basado en la edad de los usuarios

2. Las películas están clasificadas

Clasificación (rating)   | Acceso
------------------------ | ------------
Adulto (A)               | Edad >= 18
Adolescente (D)          | Edad >= 13
General (G)              | True

<!-- slide -->

### Ejemplo (cont.)

Una regla de acceso podría definirse como:

$ R1:acc(u,m,e) = (age(u) > 17 \land rating(m) \in \{A,D,G\})$
$\lor (\leftarrow age(u) \geq 13 \land rating(m) \in \{D,G\}) \lor true$

Otras reglas podrían ser:

$R2:acc(u,m,e) = membershipType(m) = premium \land isNew(m)$
$R3:acc(u,m,e) = R1 \land R2$

<!-- slide -->

### Identidad, credenciales y gestión

!!! note: ICAM
    *Identidades digitales*, *credenciales* y *gestión de acceso*

Una ***credencial*** es un objeto que *asocia* la *identidad* de un sujeto a un *token* de acceso

Ejemplo:

Un **certificado digital** es una **credencial** que asocia un sujeto con su clave pública (*veremos esto en mayor profundidad mas adelante*).


<!-- slide -->

### Frameworks de *confianza*

Los sistemas actuales (principalmente en red) deben intercambiar identidades de manera confiable.

**Arquitectura**

1. **Service providers**: Aplicaciones, web services, ...
2. **Credentials providers**: Autoridades de certificación, ...
3. **Identity providers**: Autoridades de identidad de firma digital, ...
4. **Autorization servers**: C

<!-- slide -->

#### Algunos estándares (trust frameworks)

- OpenID: Estándar abierto

  - El protocolo de autenticación [OpenId-Connect](https://openid.net/connect/) extiende a [OAuth 2.0](https://oauth.net/2/).

- ICF: Information Card Foundation
- OIX: Open Identity eXchage
- AXN: Attribute eXchange Network 

<!-- slide -->

## Protocolo Oauth-2

![oauth-2](img/Oauth2-abstract-flow.png)
