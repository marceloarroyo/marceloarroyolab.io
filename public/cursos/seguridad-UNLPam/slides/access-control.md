---
presentation:
  width: 1000
  height: 600
---

<!-- slide -->

# Seguridad Informática

## Control de Acceso

Marcelo Arroyo - Fac. Ingeniería - UNLPam

2023

<!-- slide -->

### Control de Acceso

> Determina el **modo de acceso** de los **sujetos** a los **recursos** de un sistema.

**Requiere**

  1. ***Autenticación***
  2. ***Autorización***

<!-- slide -->

### Autenticación

> Verificación de la **identidad** de un **sujeto**.

El sujeto debe presentar:

- Algo que sólo él conoce (ej: *contraseña*), y/o
- Algo que sólo él *posee*: (*token*, dato biométrico, ...)

> ***Esta información debe estar almacenada y accederse en forma segura***

<!-- slide -->

### Autorización

> Se deben verificar los **permisos de acceso** a los recursos en cada **operación**

**Matriz de control de acceso:**

$AC_m=S \times O \times R$, donde 

- $S$ es el conjunto de *sujetos*
- $O$ es el conjunto de *objetos*
- $R$ es el conjunto de *permisos de acceso* (*rights*)

<!-- slide -->

### Matriz de control de acceso.

Ejemplo:

S/O       | $user_1$ | $user_2$ | $user_3$ | ...
--------- | :------: | :------: | :------: | -----
$file_1$  |   rw-    |   r--    |   ---    | ...
$file_2$  |   ---    |   rwx    |   r-x    | ...

Implementación:

  - Listas de control de acceso, Capacidades, ...
  - Grupos de *usuarios/sujetos*

<!-- slide -->

### Dominios de protección

> Un **dominio de protección** es un conjunto de pares $(object, rights)$


  - En UNIX, el dominio de protección de un proceso está dado por el par $(gid,uid)$ ya que permite *inferir* el conjunto de objetos y permisos al cual tiene acceso 
  
  - En un *syscall*, el proceso cambia temporalmente de dominio (al del *kernel*)

<!-- slide -->

### Listas de control de acceso

  - Representación de la matriz por filas
  - $ACL(o \in O) = \{(s,r) \mid s \in S, r \subseteq R\}$
  - Agrupación (y roles) de sujetos
    1. *Dueño* (creador) del recurso
    2. *Grupos*. Ejemplo: *group ids (gid)* en UNIX

Ejemplo en unix: ACLs en *sistemas de archivos*

  - *Righs*: lectura (*r*), escritura (*w*), ejecución (*x*)
  - Sujetos: $(User/Owner, Group, Others)$
  - Representación compacta (9 bits)

<!-- slide -->


  - **Capacidades**: $C(s \in S) = \{(o,r) \mid o \in O \land r \subseteq R\}$
  - Acceso de operaciones: $op(s: S, c_o: cap)$, con $c_o \in C(s) \land r(c_o) \in M(s,o(c_o))$
  - Ejemplos:

    - CPU protection levels (objetos *etiquetados*)
    - Tablas de páginas de un proceso
    - Archivos abiertos de un proceso

<!-- slide -->

### Locks and keys

Funciones criptográficas $E$: cifrado, $D$: descifrado, $h$: hash

1. $locks(o) = \{l_1=E(h(o), k_1), ...\}$
2. $keys(s)$: Claves del sujeto $s$
3. $or\_access(s, o) = \exist k \in keys(s) \mid D(h(o), k)$
4. $and\_access(s, o) = \forall k \in keys(s) \mid D(h(o), k)$

<!-- slide -->

### Autenticación

- Sistema de autenticación = $(S,P,A,F)$, donde

  - $S$: Conjunto de pares $(id, credential)$
  - $P$: Algoritmo de *presentación de credenciales*
  - $A$: Algoritmo de *verificación*
  - $F$: Funciones de gestión de $S$

<!-- slide -->

### Sistema de autenticación: Ejemplo

- $S$: $\{(userid, hashed\_password), ...\}$
  - Nunca almacenar *secretos en forma plana*. Almacenar $h(secret)$, siendo $h$ un *hash criptográfico*
- $P$: Función $login\_ui \rightarrow (id, password)$
- $A$: $verify(id, secret) \equiv (id, h(secret)) \in S$
- $F$: comandos `useradd`, `passwd`, ...

<!-- slide -->

### Autenticación por contraseñas

  - Ventajas: Simple y eficiente
  - Desventajas
    1. Uso de contraseñas *débiles*
    2. Olvido contraseñas compartidas
    3. Susceptibles a *ataques por diccionarios*
  - Gestión de contraseñas: 
    - Vencimientos
    - Calificación de *dureza*
    - Doble autenticación (*two-factor*)
    - ... 

<!-- slide -->

### Ataque por diccionario

1. El atacante logra *leer* la BD de contraseñas
   $pwd=\{(id_1,h_1),(id_2,h_2), ...\}$
2. Usa un *diccionario* precomputado:
   $d=\{(p_1,h_1), (p_2,h_2), ...\}$
3. Busca coincidencias:
   ```python
   for (_,h) in pwd:
      for (p,m) in d:
        if m == h:
          return p  # password found!
    return "not found"
   ```

<!-- slide -->

#### Tipos de control de acceso

  1. *Discrecional (DAC)*: Los *dueños* de los recursos pueden definir los permisos de acceso de sus recursos

  2. *Mandatorio (MAC)*: Una autoridad central asigna los permisos de los *objetos* y los niveles de seguridad de los *sujetos*

  3. *Role based (RBAC)*: Permisos de acceso en base a las *funciones* de los *sujetos*

  4. Otros: Basados en atributos y combinaciones de los anteriores (controlado por el dueño original, etc)