
<!-- slide --> 

# **Criptografía**

## Facultad de Ingeniería - UNLPam

#### Marcelo Arroyo

2023

<!-- slide -->

!!! Note: Criptografía
    *Escritura secreta*. El **criptoanálisis** es el estudio de las *debilidades* de un código criptográfico.

***Sistema criptográfico***: $(E,D,P,K,C)$ donde:

- $P$ son los ***textos planos*** y $C$ los ***textos cifrados***
- $K$ es el conjunto de ***claves***
- Funciones de cifrado: $E: P \times K \rightarrow C$
- Funciones de descifrado: $D: C \times K \rightarrow P$

<!-- slide --> 

#### Ejemplo: El código César

- $P=C$: Letras del alfabeto. *n=#P*
- $K=\{k=3\}$
- $E=(c_0.c_1...c_m,k) = c'_0...c'_m$ con $c'_i=(c_i+k) \: mod \: n$

    $E("hola",k) = "krod"$

- $D = ???$

<!-- slide --> 

## Sistemas criptográficos

> Un sistema criptográfico debería basarse en que el ***único secreto*** es un  subconjunto de claves.
>
> Lo demás debería ser ***público*** (principio de Kerckhoffs)

!!! Note: Ejercicio
    Determinar la *fortaleza* del sistema *César*

<!-- slide -->

### Ataques criptográficos

1. **Ataque por fuerza bruta**: Probar con *todas* las claves posibles.
2. ***Cibertext***: El atacante conoce sólo los textos cifrados. Objetivo:
   obtener los textos planos correspondientes.
3. ***Plaintext known***: Tiene pares $(p,c)$, desconoce $k$
4. ***Choosen plaintext***: El atacante puede cifrar textos con claves elegidas

<!-- slide -->

#### Sistemas clásicos: Criptografía simétrica

- Se usa la misma clave $k$: $c=E(p,k), p=D(c,k)$
- $k$ secreta:  Sólo conocida por los participantes
- Técnicas:
   1. Sustitución: Reemplazo de símbolos
   2. Transposición: Cambios de lugar en la secuencia de símbolos
- Propiedades:
  1. Muy eficientes, implementables en hardware
  2. Problema: Distribución de claves (secretos)

<!-- slide -->

### Cifrado XOR (aditivo)

$0 \oplus 0 = 0$, $0 \oplus 1 = 1$, $1 \oplus 0 = 1$, $1 \oplus 1 = 0$

1. $A \oplus 0 = A$
2. $A \oplus A = 0$
3. $A \oplus B = B \oplus A$
4. $(A \oplus B) \oplus C = A \oplus (B \oplus C)$
5. $(B \oplus A) \oplus A = B \oplus 0 = B$

<!-- slide -->

### One-time pad: $M \oplus K$

Teóricamente _inquebrable_ si se cumple:

1. La clave $K$ debe ser de longitud $\ge$ que el mensaje
2. $K$ debe ser ***aleatoria*** (*independiente* del mensaje y *uniformemente distribuida*)
3. $K$ nunca debe ser re-usada
4. $K$ debe mantenerse *secreta* entre las partes

<!-- slide -->

### Data Encription Standard (DES)

- Clave ($k$) de 64 bits. Opera sobre bloques de 64 bits
- Aplica 16 rondas (iteraciones)
- Cada ronda opera sobre una *subclave* de 48 bits generadas desde $k$
  - Aplica substituciones y transposiciones

!!! warning:
    En 1990 se encuentra un ataque efectivo, por lo que se considera quebrado.

<!-- slide -->

### DES

![DES](img/des.gif)

<!-- slide -->

## Triple DES (3-DES)

- Aplicación triple de DES con tres claves diferentes
- $c = E_{k3}(D_{k2}(E_{k1}(p)))$
- $p = D_{k1}(E_{k2}(D_{k3}(c)))$
- Cifrado en bloques (de 64 bits)

> Considerado vulnerable y obsoleto por el National Institute of Standards and Technology (*NIST*) en 2017

<!-- slide -->

#### American Escription Standard (AES, Rijndael)

- Adoptado como estándar por el NIST en 2001
- Claves (bloques) de 128, 192 y 256 bits
- 10, 12 y 14 rondas (pasadas) a cada bloque, respectivamente
- Cada ronda realiza siguientes pasos (entre otros):
  1. *Key expansion*
  2. *Subbytes*: Sustitución no lineal
  3. *Shiftrows*: Transposición
  4. *MixColumns*: Mezclado lineal

<!-- slide -->

### Modos de operación (cifrados por bloques)

Métodos de aplicación a secuencias de bits de mayor longitud

- ***Electronic Codeblock (ECB)***: Se aplica a cada bloque de manera independiene. ***No usar!!!***
- ***Cipher Block Chaining (CBC)***: Cada bloque se cifra en base a la salida del bloque anterior: $c_i=E(m_{i_1} \oplus c_{i-1})$. $c_0$ se llama *initialization vector (iv)*
- ***Counter mode***: $c_i=E(m_{i_1} c_{i-1} + 1)$

<!-- slide -->

### HASH criptográfico (MD5, SHA, ...)

> Una función de ***hash criptográfico*** (***crypographic checksum***) es una función de la forma $h: A \rightarrow B$ con las siguientes propiedades

1. $B$ es *finito*: $B=\{m \mid length(m)=k\}$
2. $h(x)$ es eficiente (para cualquier $x \in A$)
3. $\forall y \in B$, es *muy difícil* encontrar $x \in A$ tal que $h(x)=y$
4. Dado $h(x)=y$, es *muy difícil* encontrar $x' \neq x$ tal que $h(x')=y$

<!-- slide -->

### Message Authentication Code

- Un ***MAC*** es un *tag* que puede _anexarse_ a un mensaje para su
  *autenticación* (similar a la firma digital)
- $mac(k,m) \rightarrow tag$

Un receptor debe conocer la clave secreta $k$ para *recomputar* el *tag* desde el mensaje recibido y compararlos

- ***HMAC***: ***MAC*** que usa un *hash criptográfico* para generar el *tag*

<!-- slide -->

### Gestión de claves

!!! note: Clave de intercambio
    Se asocia a un *sujeto* en una comunicación

!!! note: Clave de sesión
    Se asocia a una comunicación

- Protocolos de intercambio de claves y autenticación
  - Enfoque: Usar un tercero de confianza 

<!-- slide -->

#### Intercambio de claves

- Intento: $C$ es un *llavero* con las claves de $A$ y $B$ ($k_A$, $k_b$)
  1. $A$ pide $k_B$: $A \rightarrow C$: $E("B", k_A)$
  2. $C \rightarrow A$: $(E(k_s,k_A),E(k_s),k_B)$
  3. $A \rightarrow B$: $E(k_s,k_B)$
  4. $A$ y $B$ intercambian mensajes cifrados usando $k_s$ (clave de sesión)

<!-- slide -->

### Intercambio de claves (cont.)

!!! warning: Reply attack (man-in-the-middle)
    
    1. Intercepta el mensaje 3 $E(k_s,k_B)$
    2. Luego, intercepta mensajes $E(m,k_s)$ y *reenvía* $(m',k_s)$

    $A$ (o $B$) no puede asegurar que los mensajes vienen de $B$ (o $A$)

<!-- slide -->

### Protocolo de *Needham-Schroeder*

1. $A \rightarrow C$: $A,B,N_A$
   $N_A$: *nounce* generado por $A$
2. $C \rightarrow A$: $\{N_A,\{k_{AB},A\}_{k_B}\}_{k_A}$
   ($k_{AB}$: *clave de sesión para* $A$ y *B* generada por $C$
3. $A \rightarrow B$: $\{k_{AB},A\}_{k_B}$
4. $B \rightarrow A$: $\{N_B\}_{k_{AB}}$
   $N_B$: *nounce* generado por $B$
6. $A \rightarrow B$: $\{N_B - 1\}_{k_{AB}}$

<!-- slide -->

## Kerberos

- Basado en el protocolo Needham-Schroeder
- Componentes:
  1. *Servidor de autenticación*
  2. *Servicios (service providers)*
- Se basa en que los sujetos desean acceder a un *servicio* $S$
- Otorga a los sujetos *tickets* (credencial) de autenticación para cada servicio
- Un *ticket* tiene un tiempo de validez

<!-- slide -->

## Intercambio de claves: Diffie-Hellman

> Sea $G_n=\{g^i \: mod \: n \mid i \in \mathbb{Z}\}$ un ***grupo cíclico***. $g \in \mathbb{Z}^*_n$ se denomina el ***generador*** de $G_n$. Si $G_n=\mathbb{Z^*_n}$, $g$ es una ***raíz primitiva módulo n***

Ejemplo: Con $n=13$ y $g=2$, entonces

$G_{13}=\{0,1,2,3,4,5,6,7,8,9,10,11,12\}=\mathbb{Z}^*_{13}$

- Si $p$ es *primo*, siempre existe una ***raíz primitiva módulo p***

<!-- slide -->

## El protocolo Diffie-Hellman

1. *Alice* y *Bob* acuerdan usar $g$ y $p$ ($g$ raíz primitiva módulo $p$)
2. *Alice* elige $a \: (1 < a < p)$ y envía a *Bob*: $A=g^a \: mod \: p$
3. *Bob* elige $b \: (1 < b < p)$ y envía a *Alice*: $B=g^b \: mod \: p$
4. *Alice* computa $k=B^a \: mod \: p$. *Bob* computa $k=A^b \: mod \: p$

> *Alice* y *Bob* computaron el mismo valor ($k$) ya que 
> 
> $A^b \: mod \: p = g^{ab} \: mod \: p = g^{ba} \: mod \: p = B^a \: mod \:
> p$

<!-- slide -->

#### Diffie-Hellman: Seguridad

<br>

1. Un atacante (*Eve*) conoce $g$, $p$, $A$ y $B$ 
2. Computar $k$ requiere $a$ o $b$: $x$ tal que $g^x \: mod \: p$

!!! note: Fortaleza
    No se conocen algorimos eficientes para computar algoritmos discretos

- ***clave pública***: $(g,p)$
- ***claves  secretas*** $a$ y $b$ (de *Alice* y *Bob*, resp.)

<!-- slide -->

### Criptografía asimétrica

También conocida como criptografía de clave *pública-privada*

1. $keygen: \rightarrow (pk,sk)$. Genera un par de claves pública y privada
2. Cifrado: $E(pk,m) \rightarrow c$
3. Descifrado: $D(sk,c) \rightarrow m$

- ***ElGamal***: Basado en Diffie-Hellman
- ***RSA***: Propuesto en 1977 por Rivest, Shamir y Adleman
- ...

<!-- slide -->

### RSA

- Sean $p,q$ dos números *primos* y $n=p \times q$ (el *módulo*)
- $\Phi(n)=(p-1)\times(q-1)$
- $gekgen(n)$:
  1. Elegir $e<n$ tal que $e$ es *primo relativo a* $n$
  2. Elegir $d$ tal que $(e \times d) \: mod \: \Phi(n) = 0$
  3. *Clave pública*: $pk=(e,n)$
  4. *Clave privada*: $sk=d$

<!-- slide -->

### RSA (continuación)

4. Función de cifrado $E: c = m^e \: mod \: n$
5. Descifrado $D: m = c^d \: mod \: n$

Notar que $E = D^{-1}$ y $D = E^{-1}$

!!! note: Seguridad
    El mejor ataque es computar la inversa de $E$ factorizando $n$ para obtener $p$ y $q$ y no se conoce un algoritmo eficiente de factorización de *primos*

<!-- slide -->

### Aplicaciones

<br>

- *Confidencialidad*: 
  1. $A \rightarrow B$:  $\{m\}_{B_{sk}}$
  2. $m=\{\{m\}_{B_{sk}}\}_{B_{pk}}$
- *No repudio (de origen)*:
  1. $A \rightarrow B$: $\{m\}_{A_sk}$
  2. $B$: $m=\{\{m\}_{A_sk}\}_{A_pk}$

> $E$ y $D$ son costosas de computar. Uso común: RSA o Diffie-Hellman para *intercambiar* *claves de sesión* y/o *autenticación* y luego usar AES para cifrar datos

<!-- slide -->

### Firma digital (digital signature)

  1. $A$: $keygen() \rightarrow (A_{sk},A_{pk})$.
  2. $A$: *firma* un mensaje *m*: 
     $sign(m)=(m,A_{pk},\{hash(m)\}_{A_{sk}})$
  3. *Verificación de firma*: 
    $verify(m,A_{pk},\sigma)$: $hash(m) \equiv \{\sigma\}_{A_{pk}}$

!!! Provee **integridad** de *m* y *no repudio de origen*

<!-- slide -->

## Cadena de bloques

Sean $m_1, ..., m_n$ mensajes/documentos, cada bloque $b_i=(m_i, ts_i, hash(b_i-1), \sigma_i)$

- $ts_i$ es el ***timestamp*** (*nounce*)
- $\sigma_i = \{(m_i,ts_i,hash(b_i-1))\}_{s_{sk}}$ con $s_{sk}$ es la clave secreta del emisor de $m_i$

!!! note: Usos
    Log de transacciones en criptomonedas, expedientes, ...

<!-- slide -->

### Cadena de bloques: Robustez

1. Modificar un bloque $b_i$ requiere modificar todos los $b_j$ sucesivos ($i > j \leq n$)
2. Sino un *validador* del log lo podría detectar.

!!! note: Uso
    Una operación `append(b)`, se *valida* por un algoritmo de *consenso distribuido*

<!-- slide -->

### Distribución de claves públicas

!!! note: ¿Cómo saber que $A_{pk}$ al sujeto $A$?
    ***Infraestructura de claves públicas (PKI)*** al rescate

- ***Certificados***: Emitidos por autoridad de confianza (*CA*)
- ***Web of trust***: Sistema descentralizado de difusión de claves públicas

<!-- slide -->

#### Certificados: Contenido (X.509)

- La *identidad* (*Common Name o CN*) del sujeto y otros datos
- Tiempo de validez
- Su *clave pública*
- La *firma digital* de la CA 

**Obtención de un certificado**

1. Un sujeto genera un ***Certificate Signing Request (CSR)***
2. La CA genera el ***certificado firmado***

<!-- slide -->

## Verificación de identidad

Cuando *A* quiere *identificar* a *B*:

1. *A* recibe el certificado de *B*
2. *A* ***valida*** el certificado
   1. Su *integridad* (validando la firma)
   2. La *identidad* del sujeto
   3. Período de validez
   4. Que no haya sido ***revocado***. Ver *certificate revocation lists (CRLs)*

<!-- slide -->

### Cadenas de certificados

1. La *validación* de un certificado requiere acceder al certificado de la CA para obtener la clave pública
2. El certificado de una CA se *firma* por una CA de un nivel superior
3. Una CA *raíz (root)* es una que se encuentra en el máximo nivel
   - Su certificado está *autofirmado*


!!! note: ¿Dónde están los certificados de las CAs?
    Un sistema debe tener una DB de certificados de las CAs reconocidas

<!-- slide -->

### Pretty Good Privacy (PGP)

- Basado en *web of trust*: Certificados firmados por otros usuarios (*introducers*)
- Usado en sistemas de mensajería (e-mail). Los usuarios *anexan* o *publican* su clave pública o *fingerprint* (hash del certificado)
- Usa criptografía simétrica para cifrar mensajes
  - Se envían junto con la *clave de sesión cifrada* con la clave privada 
- Ofrece *firma digital* para *autenticación* e *integridad*
- Software: [OpenPGP](https://www.openpgp.org/), [GnuPG](https://www.gnupg.org/), ...

<!-- slide -->

![pgp](img/PGP.png)

<!-- slide -->

### Referencias

<br>

1. *Introduction to Computer Security*. Matt Bishop. Capítulos 8 y 9.
2. *The Joy of Criptography*: Libro *libre* descargable desde https://joyofcryptography.com/ 
3. *OpenSSL Cookbook*: https://www.feistyduck.com/library/openssl-cookbook/online/
