<!-- slide -->

# Modelos de seguridad

## Seguridad Informática

#### Facultad de Ingeniería - UNLPam

Marcelo Arroyo

<!-- slide -->

## Bell-La Padula (MBLP)

- Modelo para ***confidencialidad***

- *Niveles de seguridad ($L=\{l_1,...,l_n\}$)* con 
  $l_i > l_{i+1}$, $1 \leq i < n$

- *Clearance* o de sujetos: $clear: S \rightarrow L$

- *Clases* de objetos: $class: O \rightarrow L$

- Un *sujeto* puede *iniciar sesión* con un nivel menor

- Permisos (modos): *read*, *write*, *append*

<!-- slide -->

### MBLP. Ejemplo: 

- $L = \{topsecret, secret, conf, public\}$
- $clear(A)=topsecret$, $clear(B)=secret$
- $class(doc1)=conf$, $class(doc2)=topsecret$
- $M(Alice,*)=\{read,write\}$,
  $M(B,doc1)=\{read\}$,
  $M(B,doc2)=\{read, write\}$

!!! Note: *A* tiene acceso a $\{doc1, doc2\}$, *B* sólo a $\{doc1\}$

<!-- slide -->

#### MBLP: Reglas de acceso

!!! ***Objetivo***: Impedir que sujetos con *menor clearance* accedan a objetos con mayor confidencialidad.

1. <b>simple-security</b>: $can\_read(s,o) = clear(s) \geq class(o) \land read \in M(s,o)$

2. <b>\*-property</b>: $can\_append(s,o) = clear(s) \leq class(o) \land append \in M(s,o)$. $can\_write(s,o) = clear(s) == class(o) \land write \in M(s,o)$

<!-- slide -->

**Estado del sistema y operaciones**

Sea $b=\{(s_i, o_j, access\_mode), ...\}$: Conjunto de triplas de acceso asignadas al momento.

**Operaciones**

1. *Get/Release access*: Agregar/eliminar una tripla de $b$. Operaciones *login/logout/open(object, mode)/close(object)* 

2. *clear(s_i)/class(o_j)*: Cambiar niveles de seguridad

3. *M(s_i, o_j)=rights*: Cambiar permisos de acceso

4. Crear/eliminar objetos y sujetos

<!-- slide -->

### Modelo Biba (integridad)

- Se basa en *niveles de seguridad* ($I$) como en MBLP
- Modos de acceso: *read*, *write*, *execute*, *invoque* (comunicación entre  sujetos)

Reglas:

1. $can\_write(s,o) = I(s) \geq I(o) \land write \in M(s,o)$
2. $can\_read(s,o) = I(s) \leq I(o) \land read \in M(s,o)$
3. $can\_invoke(s_i, s_j, m) = I(s_i) \geq I(s_j) \land invoke \in M(s_i,s_j)$

<!-- slide -->

### Modelo de Brewer and Nash (muralla China)

***Objetivos*** 

  - *integridad* y *confidencialidad*
  - Prevención de *filtrado* de información entre sujetos con *conflictos de intereses*

***Se basa en***

- Datasets ($DS$): Clases o *tipos* de objetos
- Cada objeto pertenece a un dataset
- Conflictos de intereses ($CI$): Conjunto de subconjuntos de clases

<!-- slide -->

### Muralla china: Ejemplo

- Datasets: $\{Bank_1, Bank_2, Oil_1, Oil_2...\}$
- CI: $\{\{Bank_1, Bank_2\},\{Oil_1, Oil_2\}\}$

!!! note: Objetivo
    Un sujeto sólo puede acceder a objetos que no están en conflicto con otros objetos a los que ya accedió previamente

<!-- slide -->

### Muralla china: Reglas

- $s \in S$ puede *leer* $o \in O$  ($can\_read(s,o)$) sólo si 

  1. $\exist o' \mid DS(o)=DS(o') \land o' \in accessed(s)$, **o**
  2. $o \in CI \land \forall o' \in CI, o \neq o' \implies o' \notin accessed(s)$

- $s \in S$ puede *escribir* $o \in O$ sólo si

  1. $s$ puede leer $o$, **y**
  2. $\forall o' \mid can\_read(s,o') \implies DS(o)=DS(o')$

<!-- slide -->

### Muralla china: Ejemplo

Sean $DS$ y $CI$ los conjuntos dados anteriormente

1. $A$ *lee* $d \in Bank_1$
   - $A$ *puede leer* $d' \in Bank_1$
   - Luego, $A$ *no puede leer* $d' \in Bank_2$

2. $A$ *escribe* $d \in Bank_1$
   - Luego, $A$ *sólo puede escribir* $d' \in Bank_1$