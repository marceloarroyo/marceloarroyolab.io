# Seguridad Informática

Curso correspondiente a la carrera *Ingeniería en Computación* de la [Universidad Nacional de La Pampa](https://www.ing.unlpam.edu.ar/carreras?p=ingenieriaencomputacion).


Equipo docente
--------------

- *Profesor responsable*: Marcelo Arroyo
- *Colaborador*: Juan Carlos Hernández

Contenidos
----------

En este curso se analizarán todos los contenidos relacionados con la identificación de problemas de seguridad en sistemas de computación y las estrategias y mecanismos de defensas ante posibles ataques. A modo de resumen, en este curso se analizarán en profundidad los siguientes temas.

- *Introducción*: Conceptos de seguridad.
- *Control de acceso*
- *Modelos de seguridad*
- *Criptografía*: Introducción y sus aplicaciones
- *Seguridad en sistemas*
- *Seguridad en redes*: Protocolos de seguridad
- *Desarrollo de aplicaciones seguras*
- *Gestión de la seguridad*

Modalidad de dictado
--------------------

Se dictan clases teóricas o teórico-prácticas. En las clases prácticas se incluyen problemas y pequeños proyectos a realizar en computadora con aplicaciones en red.

Todas las herramientas utilizadas son software libre y de uso común en la práctica de seguridad en sistemas y redes.

Bibliografía
------------

Estas notas se basan en varios de los libros listados a continuación.

1. William Stallings. ***Cryptography and Network Security. Principles and Practices***.
   Fourth edition. Prentice Hall. ISBN-10: 0-13-609704-9. 2018.

1. Matt Bishop. ***Introduction to Computer Security***. Prentice Hall.
   ISBN: 0-321-24744-2. 2015.

3. Jaydip Sen (editor). ***Cryptography and Security in Computing***. Intechweb.org. 2012.

4. John R. Vacca (editor). ***Network and System Security***. Elsevier. 
   ISBN: 978-1-59749-535-6. 2012.

5. Pravir Chandra, Matt Messier, John Viega. ***Network Security with OpenSSL***. 
   O'Reilly. ISBN: 0-596-00270-X. 2002.

6. Chris McNab. ***Network Security Assessment, 2nd edition***. O'Reilly. 
   ISBN-10: 0-596-51030-6. 2008.

7. Ross Anderson. ***Security Engineering – A guide to Building Dependable Distributed 
   Systems***. 2nd edition. Wiley. ISBN-10: 0470068523. 2008.

8. N. P. Smart. ***Cryptography Made Simple***. Springer. ISBN:  978-3-319-21935-6. 2016.

9. J. Kizza. ***Guide to Computer Network Security***. Fourth edition. Springer.
    ISBN: 978-3-319-55605-5. 2017.
