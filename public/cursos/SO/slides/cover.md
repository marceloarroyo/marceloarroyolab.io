
# Sistemas Operativos

## Marcelo Arroyo

[Departamento de Computación](https://dc.exa.unrc.edu.ar/) - [FCEFQyN](https://www.exa.unrc.edu.ar/)

[Universidad Nacional de Río Cuarto](https://www.unrc.edu.ar/)
![](../img/escudounrc.jpg ':id=escudo-cover')

## Slides

[Comenzar](README.md)

![cover image](../img/cover.webp)