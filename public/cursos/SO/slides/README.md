## Sistemas Operativos

### ¿Qué veremos?

- Sistemas de computación
- Diseño e implementación de SO
- Taller: xv6 (mini Unix OS) sobre RISC-V

### Pre-requisitos

- Programación C y assembly
- Compilación, linking
- Lectura de manuales técnicos (hardware y software)

----------------------------------------------------------------

## Bibliografía

1. G. Wolf, E. Ruiz, F. Bergero, E. Meza. [Fundamentos de Sistemas
   Operativos](http://ru.iiec.unam.mx/2718/1/sistemas_operativos.pdf). 2015.

2. A. Tanenbaum, H. Bos. *Modern Operating Systems*. Fourth edition. Pearson.
   ISBN-10: 0-13-359162-X, ISBN-13: 978-0-13-359162-0. 2015.

3. Silberschatz, Galvin, Gagne. *Operating Systems Concepts*. 9th Ed. ISBN:
   978-0-470-88920-6. 2011.

4. Estas notas.