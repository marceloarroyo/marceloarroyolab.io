# Sistemas Operativos

Estas son las notas del curso ***"Sistemas Operativos"*** de la Licenciatura
en Ciencias de la Computación, respectivamente, dictadas por el
[Departamento de Computación](https://dc.exa.unrc.edu.ar/) -
[FCEFQyN](https://www.exa.unrc.edu.ar/), de la [Universidad Nacional de Río
Cuarto](https://www.unrc.edu.ar/), Argentina.

Constituyen una guía principal de los temas estudiados en el curso y
contienen los temas fundamentales desarrollados, con referencias a la
bibliografía clásica utilizada, manuales técnicos, artículos y las herramientas
de software utilizadas.

En los trabajos prácticos y en los proyectos de taller se utilizan herramientas
exclusivamente de software libre. 

Durante el desarrollo del curso se desarrolla un taller sobre diseño e
implementación de sistemas operativos sobre hardware real, basado principalmente en
[xv6](https://github.com/mit-pdos/xv6-riscv), desarrollado por el MIT en el
curso ([Operating System Engineering 2022](https://pdos.csail.mit.edu/6.1810/2022/)).

## Equipo docente (2023)

- Profesor responsable: [Marcelo Arroyo](https://marceloarroyo.gitlab.io/)

## Bibliografía

1. G. Wolf, E. Ruiz, F. Bergero, E. Meza. [Fundamentos de Sistemas
   Operativos](http://ru.iiec.unam.mx/2718/1/sistemas_operativos.pdf). 2015.

2. A. Tanenbaum, H. Bos. *Modern Operating Systems*. Fourth edition. Pearson.
   ISBN-10: 0-13-359162-X, ISBN-13: 978-0-13-359162-0. 2015.

3. Silberschatz, Galvin, Gagne. *Operating Systems Concepts*. 9th Ed. ISBN: 978-0-470-88920-6. 2011.
