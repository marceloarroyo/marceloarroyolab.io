# Introducción

Un sistema de computación está formado por múltiples componentes de _hardware_ y
_software_. La única manera de lidiar con la complejidad de estos sistemas es
*modularizar*, es decir, crear pequeños componentes interconectados para
construir sistemas más complejos. La siguiente figura muestra una estructura
típica de componentes en diferentes capas de abstracción un sistema de
computación actual de propósitos generales.

``` td2svg
+-----------------------------------------+  \
|#p        Programas de usuarios          |  |
|        ofimática, navegador web,        |  |
|                juegos, ...              |  |
+-----------------------------------------+  :> modo usuario
|#s     Herramientas del sistema          |  |
|   biblioteca estándar, linker, shells,  |  |
|        servicios (impresión, ...)       |  |
+-----------------------------------------+  /
|#k             OS kernel                 |<--- modo supervisor
+-----------------------------------------+ 
|.hw             Hardware                 |
|  CPUs, Memoria (RAM), discos, red, ...  |
+-----------------------------------------+

<style>
.hw {fill: crimson;}
#k {fill: coral;}
#s {fill: cornflowerblue;}
#p {fill: cyan;}
</style>
```

Figura 1: Capas de un sistema de computación.


Para comprender mejor el rol que cumple un *sistema operativo* es conveniente
analizar la historia del desarrollo y la evolución de sistemas de computación.

Los primeros sistemas de cómputo no tenían *interfaces* para el usuario/
programador. Los códigos de las instrucciones y los valores de los datos de un
programa se debían introducir manualmente en la memoria principal del sistema
por medio de cableados y conmutadores.

Obviamente esto era una tarea sumamente tediosa y propensa a errores y sólo era
posible hacer pequeños programas para las pequeñas memorias de aquellos días.

A medida que la capacidad de memoria crecía, se desarrollaron dispositivos de
almacenamiento para los programas y datos de entrada y salida:
  1. Tarjetas y cintas de papel perforadas
  2. Medios magnéticos como las cintas, cilindros y discos
  3. Actualmente: Memorias de estado sólido

Para el caso de las cintas y tarjetas perforadas de papel, se desarrollaron
dispositivos que comúnmente eran computadoras de propósitos específicos o
*teletipos (tty)* adaptados que permitían que los programadores *escriban* el
programa en la *teletipo* y ésta producía las cintas o tarjetas perforadas. 
La computadora incluía un *lector* de tarjetas o cintas que introducía las
instrucciones y datos de los programas en la memoria y un *impresor* que
perforaba en las tarjetas o cintas los datos de salida.

En esos días (principios de 1950) no existía el concepto de sistema operativo.
Cada programa debía ser *auto-contenido*, o sea que debía contener todas las
instrucciones necesarias aún para las operaciones rutinarias de entrada-salida,
por lo que cada programador debía conocer todos los detalles del sistema.

Actualmente podemos encontrar sistemas sin SO comúnmente en pequeños
*micro-controladores* o *digital signal processors (DSPs)* en donde la
única aplicación que ejecutan es un programa monolítico que contiene todas las
funciones necesarias. Estos sistemas implementan un mini sistema de cómputo de
propósitos específicos.

Algunos SOs para pequeños procesadores tienen la forma de una *biblioteca* que
se enlaza con el código de la aplicación, como en algunos *Real-Time Operating
Systems (RTOS)* usados para el desarrollo de *embedded systems*. Esto es similar
a los primeros sistemas operativos de la década de 1950.

## Los primeros sistemas operativos

Obviamente, los programadores debían escribir mucho código que podría ser
*reusable* por todos. En particular las operaciones de entrada-salida y otras
funciones de uso común o *rutinario* (de ahí el nombre *routines*) como
funciones matemáticas y otras.

Los sistemas denominados ***por lotes*** o ***batch systems*** permitían
cargar programas y *bibliotecas* (contenedores de rutinas). Los programadores
depositaban sus programas y datos de entrada(*jobs*). Luego un operador los
agrupaba (en lotes) y los ordenaba en una cola para que el sistema los cargara
y ejecutara en secuencia. Cada lote iba precedido por una secuencia de comandos
que especificaba las *bibliotecas* requeridas. El sistema operativo (SO) las
cargaba en memoria si no habían sido cargadas previamente.

En estos tipos de sistemas cada programa tenía acceso a todos los recursos (CPU,
memoria, dispositivos de entrada-salida, etc) del sistema.

### Sistemas operativos multitarea

Las operaciones de entrada-salida dejan demasiado tiempo a la CPU ociosa, la
cual sólo debía esperar (en un ciclo) a que esas operaciones finalicen.

Con el objetivo de maximizar el uso de CPU, los próximos sistemas operativos
evolucionaron para cargar en memoria varios programas (*procesos*) y ejecutarlos
concurrentemente asignando la CPU a otra tarea cuando la anterior iniciaba una
operación de entrada-salida. Estos sistemas se denominan ***sistemas de
multiprogramación*** o *multitareas*.

De esta manera se logra que se utilice la cpu y los dispositivos de E/S en
paralelo.

Los dispositivos generan interrupciones al ocurrir un evento, como por ejemplo,
la finalización de una operación de lectura en un disco.

Ante una interrupción recibida por la CPU, ésta hace que el flujo de ejecución
*salte* a una *rutina de atención* o *trap handler*. De ese modo, el kernel
retoma el control.

Estos sistemas requieren de algún mecanismos de *protección* entre las tareas y
el código del SO (*kernel*)

- **Confinamiento**: Cada proceso debe acceder *sólo a su propio espacio de
  memoria*.
- **Control**: Los procesos no deberían poder ejecutar ciertas *instrucciones
  privilegiadas*, como deshabilitar interrupciones o usar registros especiales
  que permitan quitarle el control al kernel.

La protección de memoria requiere de un mecanismo especial para que una tarea
requiera un servicio del SO ya que no se puede simplemente hacer una llamada a
función ya que ésta no estaría en su espacio de memoria.

Es necesario que el hardware disponga de un mecanismo para acceder a servicios
del SO como por ejemplo, una instrucción `trap` (trampa).

Las ***llamadas al sistema (system calls)*** permiten *saltar* del espacio de
memoria de una tarea al espacio del núcleo *saltando* a una rutina de servicio
del kernel.

Generalmente, en un salto al kernel, la CPU cambia su ***nivel de privilegio***
o ***modo de operación*** de la CPU. La mayoría de las CPU modernas incluyen al
menos dos modos de ejecución: ***modo kernel (o supervisor)*** y ***modo
usuario***.

> La CPU ejecuta el código del núcleo del SO en *modo kernel* o *supervisor*
> que es más privilegiado que el **modo usuario** en el cual ejecutan los
> ***procesos de usuario***. En **modo kernel** la CPU permite la ejecución
> de *instrucciones privilegiadas* y acceder a registros especiales y puertos
> de dispositivos, habilitar y deshabilitar interrupciones y otras. Estas
> instrucciones privilegiadas no son accesibles en *modo usuario*.

## Sistemas de tiempo compartido

A medida que se incorporaron dispositivos interactivos como las *consolas*
(teclado + pantalla), se comenzaron a desarrollar aplicaciones *interactivas*,
las cuales permitían a usuarios a ingresar datos y visualizar resultados en una
*sesión* de ejecución de un programa.

En un sistema con multi-programación una tarea puede monopolizar la CPU o hacer
que el sistema sea muy poco interactivo ante usuarios (éstos podrían tener
demoras significativas).

Para evitar este problema estos sistemas evolucionaron para poder *quitarle
(preempt) la CPU* a una tarea que ya la ha usado por un intervalo de tiempo
determinado y asignársela a otra tarea y garantizar un progreso más uniforme.
Esto se logra mediante el manejo de las *interrupciones de un reloj*,
las cuales son ***atrapadas*** por el *kernel* y actuando en consecuencia.

Estos sistemas habilitaron los sistemas *multi-usuarios* que soportan diferentes
espacios de trabajo para diferentes usuarios con sistemas de permisos de acceso
a los recursos. Muchos de estos sistemas permiten la conexión de múltiples
*terminales* o *consolas* a la computadora la cual se usa de manera compartida.

Actualmente las computadoras personales permiten *múltiples consolas virtuales*
conectadas por red. Una *consola* o *terminal* virtual es un programa que
*emula* una terminal física (hardware).

## Objetivos de un sistema operativo

Un SO tiene varios objetivos. El principal es ofrecer una ***máquina
abstracta*** que oculta los detalles del hardware.

Para lograr multiplexar o virtualzar recursos se deben crear ***abstracciones***
de alto nivel.

Por ejemplo, un *proceso* representa una unidad de ejecución en una CPU
(virtual) y su memoria.

Otro ejemplo de *abstracción* del hardware es el ***sistema de archivos*** el cual
ofrece a los procesos una *interfaz* de operaciones sobre *archivos* y
*directorios (carpetas)*, ocultando los detalles técnicos de los dispositivos de
almacenamiento como los discos.

El diseño de UNIX permite abstraer la interacción con los dispositivos de E/S
presentando a éstos como *archivos especiales* que se pueden leer y/o escribir
mediante la conocida API de llamadas al sistema sobre archivos como `open(...)`,
`read(...)`, `write(...)`, `close(...)` y otras.

Esta *virtualización* o *multiplexado* requiere que el SO también sea sea un
***administrador de recursos*** ya que debe incluir algoritmos de planificación
de la asignación de los recursos para optimizar su uso.

## Hardware

El siguiente diagrama muestra un esquema simplificado de una arquitectura
general del hardware.

``` td2svg
                                        +---------+
                                        |   ROM   |
              +---------+               +---------+
  +-------+   |#cpu     |  memory bus   |#ram     |
  | clock |-->|   CPU   +-----------+---+   RAM   |
  +-------+   |         |           |   |         |
              +----+----+           |   +---------+
                   |     +-----+    |
               cpu |     | DMA +----+     +--------+
               I/O |     +--+--+    |     | memory |
               bus |        |       +-----+ mapped |
                   |        |             | device |
                   |        |             +--------+
      I/O bus      |        |
     -----------+--+--------+-----+--------------+----
                |                 |              |
                |                 |              |
          +-----+------+    +-----+------+  +----+-------+
          |  keyboard  |    |    disk    |  |    bus     |
          | controller |    | controller |  | controller |
          +------------+    +----+---+---+  +------------+
              ^                  |   |
  +-----+     |        +-------+ |   | +-------+
  | kbd +-----+        | disk1 +-+   +-+ disk2 |
  +-----+              +-------+       +-------+

<style>
#cpu {
  fill: burlywood;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.4));
}
#ram {
  fill: LightBlue;
  filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.4));
}
</style>
```

Figura 2: Estructura típica de hardware.

Como se puede apreciar en la figura de arriba, el hardware de un sistema de
computación consta de diferentes componentes.

- *CPU*: Uno o más procesadores.
- *RAM*: Memoria principal (*random-access-memory*). Esta memoria es *volátil*.
- *I/O bus and ports*: Bus de I/O al cual se conectan los dispositivos de
  entrada/salida como por ejemplo, el bus *Peripherical Control Interface
  (PCI)*.
- *Device controller*: expone un conjunto de *puertos* de control y estado para
  un cierto tipo de dispositivos. Ej: Discos IDE. Un *controller* puede manejar
  varios dispositivos del mismo tipo.
- *Memory mapped devices (MMIO)*: Dispositivos de entrada salida *mapeados*
  (conectados diretamente) al *memory bus*. Ejemplos: *display/video
  controllers* y otros.
- *Direct-Memory-Access (DMA)*: Dispositivo que inter-conecta el bus de
  entrada/salida con el bus de memoria para permitir la copia de datos directa
  por los controladores de dispositivos sin necesidad que intervenga la CPU.

Un SO debe conocer los detalles de estos componentes. Los módulos de software
que controla los dispositivos se denominan *device drivers* y comúnmente forman
parte del subsistema no portable ya que es totalmente dependiente de la
arquitectura y dispositivos de hardware.

## Diseño

Los SO están diseñados en forma modular. Generalmente encontramos los siguientes
subsistemas:

1. Núcleo (kernel):

    - Gestión de *procesos* y usuarios
    - Gestión de la *memoria*
    - Subsistema de *entrada-salida*: Conjunto de *device drivers*
    - Sistemas de *archivos*
    - Subsistema de *red* (comunicaciones)
    - Mecanismos de *Seguridad*: Generalmente es un sistema transversal a los
      demás subsistemas

2. Aplicaciones y *bibliotecas* deñ sistema (ej: `libc` en sistemas tipo UNIX)

    - *Utilidades*:  Shells, comandos básicos, editores de texto, ...
    - *Servicios*:   Procesos de usuario (*demonios*) que brindan servicios de
      uso común como el servicio de impresión u otros.

Algunos de las procesos de usuario que forman parte de las utilidades y
servicios del sistema ejecutan con mayores *privilegios* que los programas de
usuario comunes.

La siguiente figura muestra la arquitectura de un SO con sus subsistemas.

``` td2svg
+--------------------------------------------------+
|#u                user processes                  |
| +----------------------------------------------+ |
| | +-------+ +-------+ +-------+ +-------+      | |
| | | proc1 | | proc2 | | proc3 | | proc4 |  ... | |
| | +-------+ +-------+ +-------+ +-------+      | |
| +----------------------+-----------------------+ |
|                        |                         |
|                        v                         |
| +----------------------------------------------+ |
| |#l             system libraries               | |
| |                   syscalls                   | |
| +----------------------+-----------------------+ |
|                        |                         |
+------------------------+-------------------------+
                         |
+------------------------+-------------------------+
|#k                      v                         |
|                    OS kernel                     |
|  +------+ +-----+ +-----+ +----+ +---------+     |
|  | proc | | mem | | dev | | fs | | network | ... |
|  +------+ +-----+ +-----+ +----+ +---------+     |
|       +----------------------------------+       |
|       | hardware abstraction layer (HAL) |       |
|       |           device drivers         |
|       +----------------------------------+       |
+--------------------------------------------------+

<style>
#u {
    fill: lightcyan;
    filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.4));
}
#l {fill: burlywood;}
#k {fill: LightCoral;} 
</style>
```

Figura 3: Arquitectura de un SO.

En la figura 3, los programas de usuario utilizan (*se enlazan con*) las
bibliotecas del sistema y otras de propósitos específicas. La biblioteca del
sistema contiene la *interfaz del programador de aplicaciones (API)* la cual
incluye las funciones que implementan las *llamadas al sistema* y otras
utilidades de uso rutinario.

El subsistema de E/S, representado como `dev` en la figura 2, incluye los
*device drivers* los cuales son módulos de software que *controlan* una cierta
clase de dispositivos, como discos IDE, teclado, mouse, pantalla, etc.

Uno de los desafíos en el diseño de un SO es encapsular el software dependiente
de la plataforma de hardware del resto del código independiente de la
plataforma. Esto se logra definiendo abstracciones de los componentes de
hardware para facilitar su portabilidad a otras arquitecturas.

Esta capa de abstracción en el *kernel* se conoce como *hardware abstraction
layer (HAL)* y se considera la capa más baja dentro del kernel.

Los diseños de los SO se puede clasificar en:

- ***Monolíticos***: Los subsistemas del *kernel* se *enlazan* en un solo
  *ejecutable*. Cada módulo puede *invocar* a funciones de otros módulos.

  - Ventajas: Simple y eficiente.
  - Desventajas: Un error en un módulo puede afectar a todo el núcleo.

  Ejemplos: UNIX, [GNU-Linux][https://www.gnu.org/gnu/linux-and-gnu.en.html], 
  [FreeBSD](https://freebsd.org/) y 
  [Fuchsia](https://fuchsia.dev/fuchsia-src/concepts).

- ***Microkernels***: Cada subsistema del *kernel* se compila como un
  *ejecutable* independiente. Para *invocar* un servicio de otro subsistema se
  usa un mecanismo de comunicación basado en *mensajes*.

  - Ventajas: Un error (ejemplo, de memoria) en un subsistema no afecta a los
    demás ya que cada componente es independiente y está confinado en su propia
    área de memoria. Fácilmente se puede convertir en un SO *distribuido'
    (subsistemas ejecutándose en  diferentes nodos de una red).
  - Desventajas: El mecanismo de pasaje de mensajes crea una *sobrecarga*
    computacional importante afectando su rendimiento.

  Ejemplos: [Minix](https://www.minix3.org/) y [L4](https://www.l4ka,org/)

- ***Máquinas virtuales***: El sistema tiene un ***hypervisor*** que se encarga
  de ***multiplexar*** el hardware a los SO *huéspedes* que se ejecutan encima
  (en modo usuario). El *hypervisor* ofrece a cada SO huésped un *ambiente
  aislado* de ejecución.

  ``` td2svg
  +--------------------+    +--------------------+
  |.vm  OS guest 1     |    |.vm  OS guest 2     |
  | +----------------+ |    | +----------------+ |
  | | user processes | |    | | user processes | |
  | +----------------+ |    | +----------------+ |
  | |     kernel     | |    | |     kernel     | |
  | +----------------+ |    | +----------------+ |
  +--------------------+    +--------------------+
  |.hw virt. hardw.    |    |.hw virt. hardw.    |
  +--------------------+    +--------------------+
          ^ enter                   ^ enter
       VM |                      VM |
          v exit                    v exit
  +----------------------------------------------+
  |#hv               Hypervisor                  | 
  +----------------------------------------------+
  |.hw                Hardware                   |
  +----------------------------------------------+

  <style>
  .vm {
    fill: lightgreen; 
    filter: drop-shadow(0px 4px 4px rgba(0, 0, 0, 0.4));
  }
  #hv {fill: salmon;}
  </style>
  ```

  Figura 4: Esquema de un SO basado en máquinas virtuales.

  Cuando el *hypervisor* está integrado en un SO, a éste se le denomina el
  *kernel anfitrión* como por ejemplo [Linux KVM](https://www.linux-kvm.org/)
  que provee mecanismos básicos para las *transiciones* entre un SO *huésped*
  y el kernel Linux *anfitrión* y redirigir a un virtualizador en modo usuario
  como [qemu](https://www.qemu.org/) o [VirtualBox](https://virtualbox.org/).

  Generalmente para poder implementar un hypervisor, el hardware debe proveer
  mecanismos como nuevos *modos de operación* de la CPU como por ejemplo *Intel
  VMX root mode* (modo del *anfitrión*) y *VMX non-root-mode* (modo del
  *huésped*) y otras estructuras de datos como *registros sombras (shadowed)*.

  En el capítulo [procesos](procesos.md) se describe en mayor detalle los
  problemas y mecanismos para el diseño e implementación de virtualización.

  Ejemplos: [Xen](https://xenproject.org/) e IBM System 370.

Muchos SO actuales tienen una arquitectura híbrida, generalmente con un kernel pequeño
monolítico y otros componentes *fuera de él* comunicándose por mensajes.
Ejemplos de estos diseños son MS-Windows, Mac-OS y FreeBSD.

Algunos SO modernos, aún cuando sean monolíticos como GNU-Linux, son altamente
modulares, permitiendo el desarrollo de ciertos subsistemas como *device
drivers* o *filesystems* como *módulos de carga y descarga dinámica* (ver [LInux
Kernel Modules](https://tldp.org/HOWTO/Module-HOWTO/x73.html)),
permitiendo al administrador del sistema cargar o descargar componentes *en
caliente*, es decir sin necesidad de reiniciar (*reboot*) el sistema.

> Un principio de diseño sugerido para los SO es definir e implementar
> ***mecanismos*** en las capas mas bajas y que las ***políticas*** se definan
> en los niveles mas altos. Esto garantiza una buena *adaptabilidad* de un
> kernel a diferentes escenarios.

## Interfaz de un SO

Un SO presenta a los desarrolladores de aplicaciones interfaces, tanto a nivel
de código fuente como en formatos binarios.

> **Application Programming Interface (API)**:
> Interfaz a nivel de *código fuente* que incluye las llamadas al sistema y las
> funciones (y tipos de datos) de la biblioteca estándar.

El estándar [portable OS interface (POSIX)](https://en.wikipedia.org/wiki/POSIX)
define una *API* de portabilidad a nivel de sistema y usuario como también con
un conjunto de *utilidades* (como *shells* y otros comandos) con el fin de
compatibilizar variantes del SO *UNIX*.

En particular, éste estándar define la *API* como declaraciones de funciones y
tipos de datos en `C`.

> **Application Binary Interface (ABI)**:
> Interfaz de bajo nivel que especifica los formatos binarios y convenciones
> para permitir la interacción de programas de usuario con las *bibliotecas* y
> con el *kernel* del sistema operativo.

Los sistemas operativos generalmente especifican una *ABI* para cada plataforma
de hardware (x86, x86-64, ARM, risc-v, etc). La *ABI* incluye:

1. Convenciones para las *invocaciones a funciones*.
2. Convenciones para los *syscalls*.
3. Formato de archivos binarios (*objetos* y *ejecutables*)

A modo de ejemplo, GNU-Linux usa binarios en formato [Executable and
Linking Format (ELF)](http://www.skyfree.org/linux/references/ELF_Format.pdf)
para todas las plataformas soportadas. La convención para las llamadas a
funciones en x86 (32 bits) es que los parámetros se *apilan* por el invocante en
orden reverso (de derecha a izquierda) y en x86-64 se pasan hasta 6 argumentos
en los registros `rdi`, `rsi`, `rdx`, `rcx`, `r8`, `r9` y el resto en la pila en
orden reverso. En ambas arquitecturas se usa la instrucción `call` para una
invocación.

Estas convenciones deben ser respetadas por cualquier compilador en la
plataforma.

## Usuarios, sesiones, shell y variables de ambiente

Generalmente el uso de un sistema multi-usuario se basa en *sesiones*. Al
comienzo un usuario debe presentar sus *credenciales* como su identificador y
una *contraseña*. Luego el sistema lanza un *shell*, el cual puede basarse en
una interfaz de texto (*consola*) o gráfica. 
Luego el usuario puede *interactuar* con el sistema lanzando otros programas
para finalmente cerrar la sesión.

El *shell* define un conjunto de *variables de ambiente* que el usuario y los
programas pueden usar y modificar.

Algunas variables de ambiente comunes en sistemas tipo UNIX son:

- `HOME`: Directorio o espacio de trabajo del usuario.
- `PATH`: Lista de directorios de búsqueda de programas.
- `PWD`: Directorio corriente (*print working directory*).
- `SHELL`: Programa (path) shell.
- `LANGUAGE`: Lenguaje de la interfaz de usuario

Se puede usar el comando `echo` para acceder al valor de una variable de
ambiente. En UNIX se desreferencia (accede a su valor) a una variable `VAR`
usando `$VAR` en sistemas UNIX. En MS-Windows se usa `%VAR`. 

Es posible listar las variables de ambiente con el comando `printenv` en UNIX o
`set` en MS-Windows. Una variable de ambiente se puede crear o modificar su
valor mediante el comando `var=value` o `export` (variable global). En
MS-Windows se puede crear/modificar una variable con el comando `set`.

El siguiente ejemplo muestra parte de la *API* que nos ofrece un SO *POSIX*.

``` c
#include <sys/types.h>  /* system data types */
#include <unistd.h>     /* UNIX standard API */
#include <stdio.h>      /* I/O standard API  */

int main(int argc, char *argv[], char *envv[])
{
    int i;
    printf("Process id: %d\n", getpid()); /* getpid() is a syscall */
    printf("Command line arguments:\n");
    for (i = 0; i < argc; i++)
        printf("%s\n", argv[i]);
    printf("Session environment variables:\n");
    for (i = 0; envv[i]; i++)
        printf("%s\n", envv[i]);
    exit(0); /* exit syscall: process exit code */
}
```

En este ejemplo se puede observar que:

1. Un programa de usuario comienza por una función denominada `main` que recibe
   como parámetros el número de los argumentos de la línea de comandos, un
   arreglo de punteros (o strings) a ellos y un arreglo (null-terminated) de
   strings con las variables de ambiente de la forma `variable=valor`.

2. Los programas interactúan con los dispositivos de entrada/salida (en este
   caso la *consola* o *terminal*) indirectamente por medio de funciones de
   biblioteca, las cuales a su vez ejecutan *syscalls*. En este caso `printf()`
   ejecuta el *syscall* `write(1, buffer, count)`. El *descriptor de archivo*
   cero (0) está conectado a la entrada estándar (generalmente el teclado). Los
   descriptores 1 y 2 refieren a la *salida* y *salida de errores* estándar,
   comúnmente asociados a la pantalla.

3. Las llamadas al sistema (como `getpid()` o `exit(n)`) están *implementadas*
   como funciones de la *biblioteca estándard*.

4. Al compilar este programa (con `gcc -o myprog myprog.c`) el *linker* lo
   *enlaza* con la biblioteca estándar.

[^1]: El primer argumento de `write` es el descriptor del archivo a escribir. El
    descriptor `1` está asociado a la *salida estándar* (la *consola*
    generalmente).

<!--
-------------------------------------------------------------------------------

[Slides](slides/slides.html ':ignore')
-->
