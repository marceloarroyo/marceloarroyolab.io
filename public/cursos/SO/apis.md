# APIs de Sistemas Operativos

Un sistema operativo ofrece a las aplicaciones servicios que permiten acceder a recursos del sistema y facilidades como mecanismos de comunicación y sincronización entre procesos y otros.

El sistema operativo UNIX, creado por Ken Thompson y Dennis Ritchie provee interfaces simples y coherentes. La mayoría de los SO modernos se basan en estas APIs.

La API de UNIX se describe como un conjunto de *funciones C* que implementan *syscalls*. A ccontinuación se muestra una lista parcial de llamadas al sistema correspondientes a operaciones sobre procesos y archivos.

**Procesos**

- `fork()`: Crea un nuevo proceso (hijo) el cual es una copia del proceso invocante.
- `exit()`: Finaliza el proceso.
- `wait()`: Espera (se bloquea) hasta que finalice un proceso hijo.
- `getpid()`: Retorna el identificador del proceso corriente.
- `kill(pid)`: Envía una señal (terminación) al proceso con id `pid`.
- `sleep(n)`: Espera (se bloquea) por `n` segundos.
- `exec(filename, args)`: Carga el programa `filename` y lo ejecuta. Reemplaza la 
  imagen de código y datos del proceso corriente.
- `sbrk(n)`: Asigna `n` bytes de memoria adicionales al proceso corriente.

**Archivos**

- `open(filename, mode)`: Abre un archivo en el modo (*read*, *write*, ...) dado.
- `read(fd, buffer, count)`: Lee `count` bytes del archivo en `buffer`.
- `write(fd, buffer, count)`: Escribe `count` bytes de `buffer` al archivo.
- `close(fd)`: Cierra el archivo (libera recurso).
- `dup(fd)`: Dumplica (copia) `fd` en el primer descriptor no usado.
- `pipe()`: Crea un *pipe* para comunicación entre procesos.
- `chdir(dirname)`: Cambia de directorio.
- `mkdir(dirname)`: Crea un directorio.
- `mknod(name, major, minor)`: Crea un archivo *especial* (device).
- `fstat(fd)`: Retorna el *estado* (y metadatos) del archivo.
- `link(f1, f2)`: Crea un *alias* (`f2`) para el archivo `f1`.
- `unlink(filename)`: Borra el archivo.

En algunas versiones modernas de SO tipo UNIX algunas de estas *llamadas al sistema* se han extendido ampliamente, como por ejemplo en GNU-Linux.

Todas las llamadas al sistema se presentan al programador como funciones de biblioteca. Su implementación dispara un `syscall`, es decir una entrada a un punto del kernel. A partir de allí se ejecuta código del kernel hasta su retorno.

Todas las llamadas al sistema retornan un entero. Generalmente en caso de falla retornan un valor negativo (comúnmente -1).

## Interfaz del usuario

Un SO presenta algún tipo de interfaz al usuario. Generalmente el programa que se encarga de interactuar con el usuario es un *shell*, el cual puede ser basado en *línea de comandos* (como *bash*, *zsh*, *csh* de UNIX o *cmd* en MS-Windows).

Un *shell* de línea de comandos permite al usuario ingresar comandos para ejecutar programas y soportan operadores sobre el control, sincronización y comunicación entre los procesos creados.

El shell se dispara al inicio del sistema, generalmente luego que el usuario inicia su sesión (*login*) asociada a la *consola* o *terminal*.

Típicamente un *shell* presenta al usuario un *prompt* que puede incluir su identificador de usuario, finalizado comúnmente por el caracter `#`. Cabe aclarar que el prompt de un shell es configurable por el usuario (Ej: `.bash_profile`).

El sistema presenta al usuario un *sistema de archivos*. Hay archivos de diferentes tipos:

1. *Programas*: archivos ejecutables (o *comandos*).
2. *Datos*: Contienen datos en algún formato (binario o texto).

Un archivo de textos contiene una secuencia de códigos de caracteres (ej: ASCII, UTF-8, ...). Generalmente se estructuran en líneas, es decir, secuencias de caracteres finalizados por `\n` (*newline*) o `\n\r` (*newline*/*carriage-return*).

El conjunto de archivos del sistema se organizan en una estructura jerárquica (de árbol) con *directorios* (o *carpetas*), los cuales a su vez contienen otro subárbol de directorios y archivos.

Para hacer referencia a un archivo se puede hacer de dos formas:

1. *Full path*: Camino desde la raíz del sistema de archivos hasta el archivo. 
   ejemplo: `cat /home/user/docs/myfile.txt`
2. *Relativa*: En base al *directorio corriente*, es decir, el directorio de trabajo 
   actual al que se accede mediante el comanddo `cd dir-path`).
   Ejemplo: `cd docs ; cat myfile.txt`

En su forma más simple el shell reconoce los siguientes operandos y operadores:

- Operandos: Pueden ser un *comando* o un *nombre de archivo*
- Operadores:

  - `cmd1 ; cmd2`: Ejecuta `cmd1` para luego ejecutar `cmd2` (cuando `cmd1` finalice)
  - `cmd1 && cmd2`: Ejecuta `cmd2` sólo si `cmd1` finaliza exitosamente.
  - `cmd1 || cmd2`: Ejecuta `cmd2` sólo si `cmd1` finaliza con error.
  - `cmd &`: Lanza el `cmd` de fondo (en *background*), el shell no espera a su 
    terminación.
  - `cmd n> file`: Se *redirige* el *descriptor de archivo de salida* `n`. Si `n` se 
    omite, se asume 1 (salida estándar).
    ,probablemente asociado a la salida estándar (pantalla) al archivo `file`.
  - `cmd < file`: Redirige la entrada estándar (comúnmente el teclado) a que sea 
    desde el archivo `file`.
  - `cmd1 | cmd2`: Ejecuta `cmd1` y `cmd2` concurrentemente, *redirige* la salida 
    estándar de `cmd1` a un *pipe* y la entrada estándar de `cmd2` a la *salida del pipe*.

## Procesos

En esta sección se analizan las llamadas al sistema para la creación, destrucción,sincronización y comunicaciones entre procesos (*Inter Process Communication - IPC*).

En una API tipo UNIX la única llamada al sistema para crear un nuevo proceso es `fork()`, la cual crea una copia del proceso corriente, conjuntamente con su estado.

El estado de un proceso (mantenido por el kernel) tiene generalmente los siguientes atributos:

- Su estado: `RUNNING`, `READY`, `SLEEPING`, ...
- Su *identificador* o *pid*: Un número > 0
- Su *imagen de mamoria*: Código, datos globales y stack.
- El conjunto (tabla) de *archivos/pipes* abiertos.
- Una referencia a su padre (*parent*)
- Un stack para ejecución en modo *kernel*
- Espacio para *salvar* su *contexto* (estado de la CPU) cuando cambie de estado.

La llamada al sistema `fork()`, ejecutada inicialmente por un proceso *padre* hace que el kernel cree una copia (el *hijo*) del proceso corriente, el cual *hereda* su estado, en particular la *tabla de archivos abiertos*.

Al retornar de la llamada al sistema, en el proceso padre retorna el `pid` ($>0$) del nuevo proceso (*hijo*).

El nuevo proceso, al heredar el estado del padre, inicia su ejecución en el retorno del `fork()`, aunque aquí retorna 0.

El siguiente ejemplo muestra el uso de `fork()`.

```c
#include <unistd.h> /* UNIX syscalls */
#include <stdio.h>  /* portable i/o functions */

int main(void) {
    if (fork() == 0) {
        printf("I'm the child. Pid: %d\n", getpid());
    } else {
        printf("I'm the parent. Pid: %d\n", getpid());
    }
}
```

Un proceso *padre* puede esperar hasta que alguno de sus hijos termine para continuar. La llamada al sistema `wait(&status)` retorna en el parámetros de salida `status` el código de terminación del proceso hijo.

A continuación se muestra un ejemplo del uso de `wait` y `exit`.

```c
#include <unistd.h> /* UNIX syscalls */
#include <stdio.h>  /* portable i/o functions */

int main(void) {
    if (fork() == 0) {
        printf("I'm the child. Pid: %d\n", getpid());
        exit(0);
    } else {
        int status;
        printf("I'm the parent. Pid: %d\n", getpid());
        wait(&status);
        printf("In parent: child finished\n");
    }
}
```

La llamada al sistema `exec(program, args)` reemplaza la imagen de memoria (código y datos) del proceso corriente con el código y datos de `program`.
Cabe aclarar que `exec()` no crea un nuevo proceso.

Es común que un programa *lance* otro programa para realizar alguna tarea para luego continuar otras operaciones. En este modelo se debe hacer como en el siguiente ejemplo, el cual ejecuta el comando `ls -l`:

``` c
#include <unistd.h> /* UNIX syscalls */
#include <stdlib.h> /* wait() */
#include <stdio.h>  /* portable i/o functions */

int main(void) {
    char *args[] = {"ls", "-l", 0};

    if (fork() == 0) {
        /* in child */
        execve("/bin/ls", args, 0);
        printf("Ooops, exec() failed!\n");
        exit(-1); /* finish with error exit code */
    } else {
        wait(0);
        printf("In parent: child finished\n");
    }
}
```

## Redirección de entrada/salida

Cada proceso inicia comúnmente con tres archivos abiertos (heredados) desde `init`, el primer proceso del sistema, lanzado por el *kernel* luego del *boot*.

El *file descriptor 0* está asociado a la entrada de la *consola*, comúnmente un *teclado*.

Los descriptores 1 y 2 están asociados a la salida de la *consola*, típicamente la pantalla o *display*.

Es posible ver los *archivos abiertos* de un proceso como una tabla de la forma

``` td2svg
     ofiles
   +--------+
 0 | stdin  |
   +--------+
 1 | stdout |
   +--------+
 2 | stderr |
   +--------+
 3 |  ...   |
```

La llamada al sistema `open(filename, mode)` retorna un nuevo descriptor de archivo, el cual puede verse como un *índice* de la tabla *ofiles*.

El modelo de dos pasos `fork()/exec()` permite al proceso padre *modificar el ambiente de ejecución* del nuevo programa a lanzar. En particular es posible *redireccionar* las entradas y salidas.

El siguiente ejemplo permite muestra cómo es posible lanzar un proceso hijo que en lugar de escribir en la entrada estándar lo hará en el archivo `/tmp/out.txt`.

``` c
#include <unistd.h> /* UNIX syscalls */
#include <fcntl.h>  /* file operations and flags */

int main(void) {
    if (fork() == 0) {
        /* in child: open/create file with permissions (user: rw, group:r) */
        int fd = open("out.txt", O_WRONLY | O_CREAT, 0640);

        close(1);                       /* close stdout */
        dup(fd);                        /* duplicate fd (copy in ofiles[1]) */
        write(1, "Hello world\n", 12);
    }
}
```

## Pipes

Un *pipe* es un mecanismo de comunicación (unidireccional) entre procesos. Un comando de shell de la forma `cmd1 | cmd2` hace que se lancen concurrentemente `cmd1` y `cmd2` previa redirección de la salida estándar al extremo de escritura del *pipe* y la entrada estándar de `cmd2` al extremo de lectura del pipe. Gráficamente:

```td2svg
+------+    +------+    +------+
| cmd1 |--->| pipe |--->| cmd2 |
+------+    +------+    +------+
```

La llamada al sistema `pipe(p)` retorna en el argumento de salida `p` dos *file descriptors* (enteros). El primer elemento del arreglo corresponde al extremo de lectura del pipe y el segundo al de escritura. El siguiente ejemplo, muestra su uso.

```c
#include <unistd.h> /* UNIX syscalls */
#include <stdio.h>

#define N 20

int main(void) {
    int p[2];

    pipe(p);    /* create pipe */

    if (fork() == 0) {
        /* in child: read from pipe */
        char buffer[N];
        close(p[1]);            /* close write pipe descriptor */
        read(p[0], buffer, N);
        printf("Child read: %s", buffer);
    } else {
        /* in parent */
        close(p[0]);            /* close read pipe descriptor */
        write(p[1], "Hello from parent\n", 18);
    }
}
```

## Señales

Las señales son un mecanismo básico de *comunicación entre procesos* para
implementar la notificación de *eventos*. El kernel en algunos casos abstrae una
*interrupción* o *excepción* en una *señal* enviada a un proceso. También un
proceso puede enviar una *señal* a otro proceso. Este mecanismo fue desarrollado
en las primeras versiones de UNIX en la década de 1970.

La llamada al sistema `kill(pid, signal)` envía la señal `signal` al proceso
identificado con `pid`. Cada señal tiene identificadores numéricos. El sistema
envía señales a los procesos cuando el usuario en una terminal pulsa las
combinaciones de teclas como por ejemplo `Ctrl-C`, la cual envía `SIGINT`
(*interrupt*) o `Ctrl-Z`, que envía `SIGTSTP`.

Un proceso puede *manejar* una señal *s* instalando un *signal handler* por
medio del syscall `signal(sig, handler)`, donde `sig` es la señal a manejar y
`handler` es una función de la forma `void handler(int signal)`.

Por omisión cada señal tiene un *default handler* implementado en el kernel. Su
comportamiento depende de cada señal. Por ejemplo, para la señal `SIGINT` el
*default handler* finaliza el proceso, mientras que el *default handler* para
`SIGTSTP` causa que el proceso se *suspenda* (queda en estado `SLEEPING`).

Cuando un proceso recibe una señal, se altera su flujo de ejecución normal y se
dispara su *handler*. Luego del retorno, el proceso continúa con su ejecución en
el estado que estaba antes de la recepción de la señal.

La única señal que no puede ser atrapada (manejada) es `SIGKILL` la cual se usa
para *terminar* un proceso (si es que el usuario es el *usuario efectivo*, es
decir, quien lo ejecutó).

## Alarmas

Las llamadas al sistema `alarm(seconds)` y `setitimer(...)` permiten definir una
alarma, es decir que luego de transcurrido ese tiempo, el kernel lanzará la
señal `SIGALRM` al proceso.

Una alarma generalmente se usa cuando una aplicación realiza una operación (por
ejemplo, enviar un mensaje) y si no tiene respuesta dentro de un intervalo de
tiempo (*timeout*), requiere realizar alguna acción de recuperación o
re-intento.

## Trazado de procesos

Un proceso puede controlar la ejecución de otro proceso. Al primero se lo conoce
como el *trazador* y al otro el *trazado*. Comúnmente un *debugger* se
implementa usando este mecanismo, entre otros.

La llamada al sistema `ptrace(request, pid, addr, data)`. El proceso *trazado*
se suspende cada vez que envíe una señal o haga una llamada al sistema. El
*trazador* será notificado en un `waitpid(pid, &status)`. En `status` se indica
la causa de la suspensión del proceso trazado.

Luego, el *trazador* puede realizar alguna de las siguientes acciones sobre el
*trazado*:

- Identificar qué llamada al sistema realizó.

- Leer o escribir en el área de código o datos. Así un debugger, puede por
  ejemplo, definir un *breakpoint* por software, reemplazando una instrucción
  por una llamada al sistema.

- Acceder al estado de la CPU (valores de los registros) del proceso bloqueado

- Hacer que continúe la ejecución

- Otras. Para más detalles, hacer `man ptrace`.