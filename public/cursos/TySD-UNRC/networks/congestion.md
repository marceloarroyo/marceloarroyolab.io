# Control de congestión

Una red es un conjunto de recursos requeridos en forma *competitiva* por las
aplicaciones en ejecución. Cada nodo (hosts y routers) debe administrar sus
recursos (CPU, memoria, conexiones TCP, etc), para poder brindar un servicio
razonable a cada aplicación.

Los routers generalmente deben *encolar* temporalmente los paquetes para
poderlos reenviar pos algún enlace compartido. Cuando un paquete debe esperar en
colas, se incrementa el tiempo de *round trip (RTT)* entre los extremos. Cuando
esas colas o *buffers* desbordan se deben descartar paquetes. Cuando las colas
alcanzan ciertas longitudes o están desbordados, se dice que existe
*congestión*.

La congestión puede aliviarse si se asignan o reservan más recursos como ancho
de banda, enlaces, buffers y otros. Otra estrategia es *clasificar* los paquetes
en base al tipo de aplicación. Una aplicación de tiempo real como streaming de
sonido o video debería tener mayor *prioridad* que los paquetes de una
aplicación como por ejemplo, de correo electrónica u otro servicio de
mensajería.
Asignar políticas de prioridades a diferentes *flujos* se conoce como *Calidad
de servicio (QoS)*. En su implementación generalmente se usan buffers con
diferentes *disciplinas de colas*, como *weighted fair queuing (WFQ)*, las
cuales se asignan a diferentes flujos.

> La diferencia entre *control de flujo* y *control de congestión* es que el
> primero se basa en mecanismos para que el emisor no *desborde* al receptor
> mientras que el segundo ataca el problema de la red, teniendo en cuenta el
> estado de los routers y enlaces intermedios.

## Control de congestión en TCP

TCP provee mecanismos para detectar y tratar de aliviar o detectar la congestión
de la red, más allá del *control de flujo* entre los extremos visto.

El objetivo es tratar de determinar la *capacidad* de la red entre los extremos
y aún es tema de investigación lograr mejores mecanismos.

TCP mantiene una variable de estado, llamada `CongestionWindow` y usa 
``` clike
MaxWindow = MIN(CongestionWindow, AdvertisedWindow)
```

como valor máximo de bytes a transferir, donde `AdvertisedWindow` es la recibida
en el último *ACK*. El problema de TCP es inferir el valor de
`CongestionWindow`. El hecho que expire el timer de un segmento antes de recibir
el ACK correspondiente es una señal de eventual congestionamiento.

La idea de los algoritmos es ir variando la cantidad de bytes a transferir
tratando de no generar congestionamiento mientras tanto mantienendo la tasa de
transmisión mas alta posible. Estos dos objetivos, obviamente se contraponen.

Un primer enfoque es decrementar su valor ante la ocurrencia de *timeouts*, así
dejar de contribuir al congestionamiento. El problema de este enfoque es que al
descongestionarse la red no incrementa el valor y se pierde eficiencia en el uso
de la red.

Una mejora a esta estrategia es ir incrementando el valor de `CongestionWindow`
mientras reciba *ACKs* antes de los *timeouts*.

El mecanismo usado inicialmente en TCP se basa en un incremento exponencial,
curiosamente conocido como *slow start*. Inicialmente, `CongestionWindow` toma
el valor 1. Al recibir el *ACK* lo incrementa en 1. El emisor transmite el doble
de segmentos del valor `CongestionWindow`.  

Al ocurrir un *timeout* TCP reduce el valor a la mitad. 

A partir de este punto, el emisor usa un crecimiento lineal, aumentando de a un
paquete en cada ACK recibido hasta alcanzar el valor de `CongestionWindow`
cuando finalizó el ciclo anterior.

La siguiente figura muestra el comportamiento de TCP con este algoritmo.

![tcp-congestion](img/TCP-congestion-control.png ':size=70% :class=imgcenter')

#### Figura 11.1: Control de congestión en TCP.

Los puntos encima del gráfico muestran los timeouts. Las barras horizontales
superiores muestran el tiempo en que se transmite un paquete y las barras
verticales muestran el inicio de una transmisión o retransmisión.
La línea azul muestra en valor de `CongestionWindow`.

Uno de los problemas con este algoritmo es que la espera por timeouts arroja
tiempos de espera con inactividad. La mejora introducida posteriormente se
conoce como ***Fast Retransmit***. La idea consiste en que el receptor al
recibir un segmento *fuera de orden* reenvía el último ACK. Este ACK duplicado
indica al emisor que posiblemente los segmentos anteriores se hayan perdido o
estén aún en tránsito. El emisor, luego de recibir tres ACKs duplicados reenvía
los segmentos aún no reconocidos, adelantándose a los timeouts.

Una variante de este algoritmo, denominado CUBIC, implementado por primera vez
en el kernel GNU-Linux, se enfoca en ajustar periódicamente la ventana de
congestión cada vez que se recibe un ACK duplicado. Usa la siguiente función
cúbica sobre el tiempo para ajustar la ventana de congestión:

$$ CW(t) = C \times (t-K)^3 + W_{max} $$

donde 

$$ K = \sqrt[3]{W_{max} \times (1 - \beta) / C} $$

*C* es el *factor de escala* y $\beta=0.7$ es el factor de decrecimiento.
$W_{max}$ es el máximo valor alcanzado por la ventana de congestión.

Este último enfoque se comporta mejor en redes con grandes y pequeños RTTs.

La curva generada por los cambios del tamaño de la ventana de congestión es
menos variable y se ajusta rápidamente a los cambios.

## Otros algoritmos

El algoritmo descripto para TCP se basa en la *detección* de congestión. Los
nuevos enfoques, algunos de ellos implementados en routers e implementaciones de
TCP, se basan en la *prevención* de congestión (severa).

El algoritmo conocido como *DECbit* se basa en que cada router setea un bit si
su cola de paquetes tiene longitud mayor que su longitud media. Este bit es
incluido en el ACK. El emisor cuenta el número de paquetes enviados con el bit
seteado. Si ese número es menor que el 50% de los paquetes enviados, la ventana
se incrementa linealmente (en 1). En otro caso, la ventana se decrementa por
0.875 (*multiplicative decrease*).

*Random Early Detection (RED)* se basa en la misma idea que DECbit pero con
notificaciones implícitas al emisor. Los routers congestionados *descartan
(drop)* paquetes aleatoriamente usando alguna estrategia. Luego, el emisor
recibirá ACKs duplicados u ocurrirán timeouts y el algoritmo estándar
actualizará la ventana de congestión.

Otro enfoque, conocido como *basados en el origen* se basan en que el emisor
intenta predecir congestión midiendo los RTTs de paquetes y si éstos se
incrementan por sobre el RTT promedio, la ventana de congestión se decrementa
(por ejemplo, por 1/8).

Algunos de los algoritmos basados en este último enfoque son *TCP Vegas* y *BBR*
de Google. Ambos se basan en medir *delay*, lo cual permite inferir que las
colas de los routers intermedios han incrementado su longitud.