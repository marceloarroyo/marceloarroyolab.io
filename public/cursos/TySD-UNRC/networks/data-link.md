# Enlace de datos

Los protocolos a nivel de enlace de datos (capa 2 del modelo OSI) deben resolver
los problemas de *framing (empaquetado)*, gestión del medio (canal de
comunicación) y *detección de errores*. El modelo OSI denomina las unidades de
transmisión en esta capa como _frames_.

Los datos a transmitir comúnmente se dividen en *frames* para resolver dos
problemas principales:

1. *Control de errores*: Los medios físicos son propensos a interferencias,
   ruido y otros problemas que pueden afectar a la comunicación.
2. *Sincronización*: Es común que el transmisor (*tx*) y el receptor (*rx*) se
   deban *sincronizar* en el tiempo para que el receptor pueda tomar las
   muestras de la señal y reconstruirla adecuadamente.

Es posible clasificar los protocolos en base a cómo empaquetan los datos
transmitidos.

- ***Orientados a bytes***: Un frame consiste en una secuencia de bytes.
- ***Orientados a bits***: Los datos en el frame se interpretan como una
  secuencia de bits.

Un protocolo en esta capa generalmente interactúa directamente con uno o más
dispositivos de hardware de comunicaciones.

> Una interface de red es un dispositivo de hardware/firmware y se denomina
> ***Network Interface Controller (NIC)***.

Algunos protocolos, como *PPP*, son independientes del medio físico y *NICs*.
Otros protocolos, como ATM y Ethernet, también especifican los medios físicos,
detalles del tipo se señales, la modulación y técnicas de multiplexado
soportados. En estos últimos protocolos podríamos decir que también incluyen la
especificación de la capa física.

## Point-To-Point (PPP) protocol

Este protocolo (RFC 1661) se usó inicialmente para la conexión de terminales a
mainframes, por lo cual es *orientado a bytes* y soporta frames de longitud fija
o variable.

La siguente figura muestra un frame PPP.

![ppp](img/ppp-frame.png ':id=ppp-frame :class=imgcenter :size=60%')

#### Figura 3.1: Formato de un frame PPP.

Con PPP, los *peers* deben negociar los parámetros de la comunicación antes de
comenzar la transmisión de datos. Esta negociación se realiza mediante el
***Link Control Protocol (LCP)***, el cual encapsula los mensajes en frames PPP.

LCP permite la negociación de parámetros como tamaño de los frames (datagramas),
símbolos de escape, tasa de transmisión, compresión de datos y autenticación
([PAP](https://en.wikipedia.org/wiki/Password_Authentication_Protocol "Password
Authentication Protocol") y
[CHAP](https://en.wikipedia.org/wiki/Challenge-Handshake_Authentication_Protocol
"Challenge-Handshake Authentication Protocol")).

Cada frame (ver [Figura 3.1](#ppp-frame) tiene delimitadores de inicio y fin
(`flag`) que es el valor binario `01111110`. Si ese valor debe incluirse en los
datos, deberá ir precedido por el *caracter de escape*.

El campo `protocol` identifica el protocolo de la capa superior (LCP, IP, ...).

Es un protocolo muy flexible y puede utilizar en la capa física desde simple
enlaces seriales (RS-232,
[SLIP](https://es.wikipedia.org/wiki/Serial_Line_Internet_Protocol "Serial Line
Internet Protocol")) hasta enlaces del tipo usado en líneas telefónicas como
[X25](https://en.wikipedia.org/wiki/X.25 "Estándar ITU-T").

## Protocolos orientados a bits

Un protocolo orientado a bits interpreta su *payload* como una secuencia de
bits. Para determinar los límites se usan dos enfoques:

1. Un campo que contenga la cantidad de bits del payload
2. Usar delimitadores (marcas) de inicio y fin

A modo de ejemplo, el ***High-Level Data Link Control (HDLC)*** usa los mismos
delimitadores de PPP. Para evitar que un dato arbitrario no produzca la marca de
fin, cada 5 bits consecutivos con valor 1 en el payload se inserta un cero.
El receptor luego de cada 5 1s consecutivos, si recibe un cero, lo descarta. Si
el siguiente es un 1, o es la marca de fin y el siguiente debe ser un 0. En el
caso que el siguiente bit sea un uno tiene que ser producto de un error.

## Ethernet

La familia de tecnologías y protocolos *Ethernet* (estándar IEEE 802.3) es muy
amplia, flexible y mantiene compatibilidad hacia atrás. Generalmente se utiliza
en LANs.

Cada *Network Interface Controller (NIC)* Ethernet tiene una dirección única de
48 bits. Una dirección (conocida como *Medium Address Control (MAC)*) Ethernet
se denota como una secuencia de 6 valores de dos dígitos hexadecimales separados
por dos puntos (`:`). Las direcciones Ethernet se asignan según el [Internet
Assigned Numbers Authority
(IANA)](https://www.iana.org/assignments/ethernet-numbers/ethernet-numbers.xhtml).

En Ethernet original ([10Base5](https://en.wikipedia.org/wiki/10BASE5) y
[10Base2](https://en.wikipedia.org/wiki/10BASE2)) usaba cable coaxial y los
nodos se conectaban a cada segmento formando un *medio compartido* logrando una
*topología de bus*.

Cada versión define la tasa de transmisión soportada. Ethernet es un protocolo
de comunicación con ***flujo de control*** basado en una tasa de transmisión
fija, desde 10Mbps en 10Base5 y 10Base2, 100Mbps en 100Base-TX, hasta 1Gbps en
1000BaseX sobre fibra óptica.

La norma *10BaseT* reemplaza el coaxial por cables de *pares trenzados (UTP)*,
mas flexibles y de menor costo. En *10BaseT* cada nodo se conecta inicialmente a
un *hub* que actuaba como un *repetidor* de los frames transmitidos a los demás
nodos. Esto resulta en una *topología de estrella*. *10BaseT* permite
comunicación ***full duplex*** (bidireccional simultánea).

Estas dos tecnologías *comparten* el medio físico de comunicación por lo que el
protocolo necesita un mecanismo de *arbitraje* en el acceso al medio. El
algoritmo usado es ([Carrier Sense-Multiple Access with Collision Detection
(CSMA-CD)](https://en.wikipedia.org/wiki/Carrier_sense_multiple_access_with_collision_detection)).

*CSMA-CD* se basa en que el dispositivo tiene que *esperar* hasta detectar el
canal libre (señal portadora) para luego iniciar la transmisión de un frame.
Esto no impide *colisiones* ya que dos estaciones pueden iniciar su transmisión
simultáneamente. En este caso, las estaciones transmisoras, detectan la colisión
(comparando lo que están transmitiendo con lo que están recibiendo por el canal)
y abortan la transmisión, reintentando luego de una espera aleatoria (algoritmo
*backoff*).

Notar que esto define un *sistema distribuido*, ya que no requiere de una
coordinación centralizada.

Actualmente se utilizan ***switchs*** que permiten conexiones *punto a punto*
entre el switch y cada estación, formando una topología de estrella.

A diferencia de un *hub*, un *switch* retransmite un frame sólo al puertos
de la estación *destino*, analizando la dirección de destino de la cabecera del
frame. De esta forma se evitan colisiones y se permiten comunicaciones
simultáneas sobre subconjuntos disjuntos de estaciones.

Los *switches* pueden interconectarse entre sí para formar generalmente una
topología de árbol. Un *switch* moderno también hace de *puente (bridge)*
transformando frames de diferentes versiones de Ethernet, permitiendo la
interconexión de dispositivos heterogéneos como NICs de 10Mbits con otros de
100Mbits.

<div class="divcenter">

![bus](img/bus.png ':size=40%') 
![switchs](img/switchs.jpg ':size=50% :class=imgsep')

#### Figura 3.2: Topologías de bus y uso de switchs.

![switch](img/switch.webp ':size=30%')

#### Figura 3.3: Foto de un switch (puertos RJ-45).
</div>

Un *switch* o *bridge* usa la técnica de ***store and forward*** almacenando en
memoria temporalmente los frames, para analizar sus cabeceras y retransmitirlos
por los *puertos* correspondientes.

> ***¿Cómo sabe un switch a qué puerto tiene conectada una interface con una
> dirección MAC dada para enviar un frame con ese destino?*** Bien, en principio
> no sabe, pero va asociando pares de la forma *(MAC, port)* a medida que recibe
> frames desde el puerto correspondiente. De esta manera un switch en el inicio
> se comporta como un *hub* ya que retransmite cada frame por cada puerto aún no
> asociado a una dirección Ethernet. El switch va *aprendiendo* qué MAC está
> conectado a cada port a medida que recibe frames.

La [figura 3.4](#eth-frame) muestra el formato de un *frame Ethernet*.

![frameEthernet](img/Ethernet_frame.png ':size=70% :class=imgcenter
:id=eth-frame')

#### Figura 3.4: Formato del frame Ethernet.

El *preámbulo* es un patrón de bits que facilita la *sincronización* de los
NICs. El campo *SDF (start delimiter frame)* indica el inicio del frame.

Las direcciones de origen y destino permiten determinar los dispositivos que
intervienen en la comunicación. Una interfaz que recibe un frame con destino no
se corresponde con su MAC, simplemente lo descarta.

La dirección `FF:FF:FF:FF:FF:FF` está reservada para *broadcast*, es decir
especifica que el frame tiene como destino a todas las demás interfaces
conectadas en la LAN.

El campo *EtherType* puede usarse con dos propósitos: Si si valor es menor o
igual a 1500, indica la longitud del *payload* (datos). Si es mayor a 1500,
indica el tipo del *payload*, es decir, qué tipo de *paquete* contiene. Por
ejemplo, si contiene un paquete o *datagrama IP*, su valor es *0x0800*.

> Los números de protocolos (*payload type*) también son asignados por la
> [Internet Assigned Numbers Authority](https://www.iana.org/).

Cuando el campo *Ethertype* no representa el tamaño del *payload*, el final del
frame puede detectarse o por un símbolo representando el *fin del data-stream*
(por ejemplo, un [código 8b/10b](https://en.wikipedia.org/wiki/8b/10b_encoding)
en Gigabit Ethernet) o por la pérdida de la señal de portadora sobre la que se
modulan los datos. En éste último caso se define un intervalo entre frames
(*inter-frame gap*).

El *payload* corresponde a los datos transmitidos de longitud entre 42 y 1500
bytes. El *frame check sequence (FCS)* es un *cyclic redundance check (CRC)*
para detección de errores como el que se describe en la sección
[CRC](errors#crc).

## Asynchronous Transfer Mode (ATM)

[ATM](https://en.wikipedia.org/wiki/Asynchronous_Transfer_Mode "Asynchronous
Transfer Mode") fue diseñado como un protocolo flexible para proveer servicios
de transmisión de datos para diversos tipos de aplicaciones, desde aplicaciones
de VoIP (voz sobre IP) hasta aplicaciones no interactivas.

Un _frame_ ATM es de longitud fija de 53 bytes. Permite establecer _circuitos
virtuales_ entre los extremos, ofreciendo un servicio _orientado a conexión_.

Ha sido utilizado ampliamente en redes de telefonía para la transmisión de datos
digitales como *Digital Line Subscriber (DSL)*. El protocolo permite el uso de
enlaces físicos de diferentes tipos como cables telefónicos o fibra óptica.

Muchos proveedores de servicios de internet (ISPs) usan
([PPPoE](https://en.wikipedia.org/wiki/Point-to-Point_Protocol_over_Ethernet
"PPP over Ethernet")) sobre ATM. Esto es, PPP *encapsulado* en paquetes Ethernet
para aprovechar las funciones de negociación de parámetros de enlace como
compresión de datos, limitación de tasa de transmisión en base al plan del
suscriptor y autenticación de usuarios.

Comúnmente cada *modem DSL* de usuario se conecta a un equipo tipo
[DSLAM](https://en.wikipedia.org/wiki/DSLAM "Digital Subscriber Line Access
Multiplexer") del ISP por enlaces (cable telefónico, coaxial, fibra óptica, etc)
usando ATM o Ethernet. El *modem DSL* también opera como un *switch L2* para la
red local del usuarios y *router (L3)* o *gateway* que permite rutear paquetes
desde/hacia el router del ISP. Algunos ISPs encapsulan PPPoE sobre ATM (PPPoEoA)
en los enlaces entre el *modem/router* del usuario y el *DSLAM*.

El tamaño del frame (ATM _cell_) permite la transmisión de pequeños mensajes, lo
cual es común en aplicaciones de transmisión de audio o mensajes de
control, permitiendo minimizar el _jitter_ o _fluctuación_ entre los tiempos (o
separación) entre frames.

### Formato de frames ATM

Una _celda_ ATM tiene una cabecera (_header_) de 5 bytes y un _payload_ o cuerpo
de 48 bytes. Los principales campos de la cabecera son:

- ***Virtual Path Identifier (VPI)***: 1 byte
- ***Virtual Channel Identifier (VCI)***: 2 bytes
- ***Payload Type (PT)***: 1 byte
- ***Header Error Control (HEC)***: 1 byte CRC (polinomio $x^8+x^2+x+1$)

El par de valores _(VPI,VCI)_ se usa para determinar el _circuito virtual_ a
seguir entre los extremos. Un switch ATM usa estos valores para determinar el
próximo dispositivo a retransmitir la celda. Cada switch utiliza una función de
_labeling (rotulado)_ implementada como una tabla con entradas de la forma
$(vpi_{in},vci_{in},vpi_{out},vci_{out},vlc)$ que mapea una terna
$(vpi_{in},vci_{in}, vcl)$ al puerto de salida representado por
$(vpi_{out},vci_{out})$ para continuar con el próximo salto perteneciente al
_virtual circuit link (VCL)_.


## Wifi (IEEE 802.11)

Basado en IEEE 802, permite comunicación inalámbrica (aire). Usa ***Carrier Sense
Multiple Access with Collision Avoidance (CSMA-CA)***. La idea es similar a
*SCMA* pero se transmite todo el frame. En una colisión los receptores
detectarán frames inválidos. Se usan *bandas libres* de frecuencias (centrales)
como 2.4 o 5Ghz.

El protocolo en realidad no evita colisiones, principalmente cuando dos
estaciones inician su transmisión por estar fuera de alcance entre sí (cada una detecta el canal libre). 
Este problema se conoce como el de la *estación oculta*, y se muestra en la [figura 3.5 a)](#hidden-exposed).

Otro problema a resolver es el de *estación expuesta*, como se muestra en la [figura 3.5 b)](#hidden-exposed), en la cual `B` podría transmirir a `C` sin problemas ya que C no recibiría interferencias.

![hidden-exposed](img/hidden-exposed.png ':class=imgcenter :id=hidden-exposed')

#### Figura 3.5: a) Estación oculta. b) Estación expuesta.

Para evitar estos casos, una estación que desea transmitir, envía un mensaje `RTS` (*request to send*) indicando el destino y la duración del uso del canal. El receptor, responde con `CTS` (*clear to send*). El emisor comienza a transmitir luego de la recepción del `CTS`. El receptor envía mensajes de *reconocimiento de recepción de cada frame* (`ACK`).

Si el *timer* disparado por el transmisor expira, se asume una colisión u error
y se reinicia el intento de transmisión.

Esto implementa un protocolo distribuido de acceso al canal, permitiendo el
*modo ad-hoc*, también llamado *Distributed Coordination Function*, es decir sin
uso de *access point (AP)* actuando como coordinador central.

Los protocolos 802.11 fue evolucionando en versiones *a,...,g*. Esta última utiliza ***OFDM*** y  usa *canales* con un ancho de banda de 20Mhz alcanzando una tasa de transmisión de 54Mbps. La última versión, 802.11n puede alcanzar tasas de transmisión de hasta 450Mbps, usando más ancho de banda usando hasta 4 antenas.

La siguiente figura muestra el formato de un frame 802.11.

![802.11](img/802.11-Frame.png ':class=imgcenter')

#### Figura 3.6: Formato de un frame 802.11.

La *dirección 1* contiene la dirección de destino, la *dirección 2* la de origen
y la *dirección 3* generalmente contiene la dirección del *default gateway* (que
puede ser el *access point*).

En ***modo infraestructura*** (la mas común actualmente), se usan ***access
points (AP)***, los cuales hacen de *switch* y *bridge* (L2) y *router* (L3),
permitiendo su conexión a una red cableada Ethernet. Un *AP* representa una red
local, con su identificador (*SSID*) y canales utilizados. Periódicamente
transmite un *beacon* con el *SSID* mas información sobre su acceso (parámetros
de seguridad) y actúa como un coordinador central de la red
(*Point Coordination Control*).

Un AP permite que una estación se *una* a la red (con posible autenticación)
usando un *protocolo de asociación*. También existe la operación de *salida* o
*disasociación*. Este modo provee una *topología estrella*. Un *AP* puede
interconectarse con otros *APs* y/o switches.

En este modo puede usarse un esquema de seguridad. Actualmente se utiliza
[Wi-fi Protected Access(WPA)](https://es.wikipedia.org/wiki/Wi-Fi_Protected_Access)
que brinda autenticación con contraseñas para asociación y canales seguros (cifrado
de datos).

Otras tecnologías inalámbricas ampliamente usadas son
[Bluetooth](https://en.wikipedia.org/wiki/Bluetooth) para distancias cortas,
redes de telefonía celular (3, 4 y 5G) y
[LoRa](https://en.wikipedia.org/wiki/LoRa), usada para largas distancias y bajas
tasas de transmisión.

Bluetooth usa frecuencias similares que 802.11 (2.4Ghz, con un ancho de banda de
83Mhz) por lo que puede causar interferencias si se usan simultánemente. Para
evitar esto ambas tecnologías usan multiplexado en canales y *saltos
periódicos*. Bluetooth divide el ancho de banda en 79 canales, cada uno de 1Mhz
y usa *Frecuency-hopping Spread Spectrum*, saltando de canal 1600 veces por
segundo. Wi-Fi divide el canal en 11 (o 14) canales donde cada transmisión ocupa
5 canales. Wi-Fi usa una técnica de modulación conocida como [Direct-Sequence
Spread Spectrum](https://en.wikipedia.org/wiki/Direct-sequence_spread_spectrum)
que permitiendo la coexistencia con otras redes Wi-Fi y Bluetooth donde el
receptor puede *extraer* la señal requerida. Esta técnica permite implementar
*Code-Divission Multiple Access* como se analizó en el capítulo de [enlaces
directos](direct-links.md).

### Interconexión con switches

Generalmente se interconectan varias redes Ethernet mediante switches con
enlaces redundantes para proveer tolerancia a fallas.

Esto hace que posiblemente se formen *ciclos*, los cuales debe evitarse a nivel
lógico para que las tablas de switching de cada switch se mantengan coherentes.

Para ello, los switches corren el [Spanning Tree
Protocol (STP)](https://en.wikipedia.org/wiki/Spanning_Tree_Protocol) que forma un
árbol de switches, designando un *root bridge*.

Cada switch se configura con un ID, el cual se concatena a su dirección MAC. STP
selecciona como *root bridge* al valor menor.
El árbol se arma seleccionando los *caminos* más cortos en base al ancho de
banda de cada enlace.

STP designa a cada puerto que conecta a otro switch con una de las siguientes
categorías:

- ***Blocking (BP)***: Desactivado para prevenir *loops*.
- ***Designed (DP)***: Puerto (el de menor ID del switch) que conecta a la LAN.
- ***Root (RP)***: Puerto que conecta en un *path* al *root*.

La siguiente imagen muestra en a) el árbol creado por STP en una red de ejemplo
y en la parte b) la reconfiguración obtenida ante la falla del enlace entre el
switch 24 y la red *c*.

<div class="divcenter">

![st](img/Ethernet-STP-working.png ':size=40%') 
![failure](img/Ethernet-STP-failure.png ':size=40% :class=imgsep')

#### Figura 3.7: a) Spanning tree. b) Reconfiguración luego de una falla.

</div>

## Maximum Transmission Unit (MTU)

Un protocolo de enlace de datos si opera con frames de tamaño máximo restringe
el tamaño máximo del *payload* para los protocolos de las capas superiores. Esto
comúnmente se conoce como *Maximum Transmission Unit (MTU)*.

El MTU indica a un protocolo de red, como por ejemplo IP, que si debe enviar un
payload de mayor tamaño que el MTU deberá *fragmentarlo*, es decir enviar una
secuencia de paquetes más pequeños. La *fragmentación* puede afectar al
rendimiento del sistema de comunicaciones, ya que se agregan intervalos de
inter-frames, latencias (tiempo de preparación para el envío de un paquete),
etc.

## Virtual LAN

Una *vLAN* es una red a nivel de enlace que permite dividir una red física en
varias redes lógicas. Un bridge vLAN *rutea* frames entre las redes lógicas.
El estándar Ethernet con soporte vLAN es el
[802.1ad](https://en.wikipedia.org/wiki/IEEE_802.1ad) que extiende el formato
del frame conteniendo un *tag* o identificador de una vlan. Este formato también
es aplicable en el estándar 802.3 (Ethernet por cable).

## Hubs, switches, bridges y routers

Todos estos elementos de hardware de red permiten interconectar otros
dispositivos, como hosts y otros dispositivos de interconexión para
interconectar redes.

La diferencia de nombres se determina en base en qué capa del modelo OSI operan,
como e muestra en la siguiente tabla.

<div class="center">

Dispositivo | Capa en el modelo OSI
----------- | ---------------------
Hub         | Capa física (1). Reenvía cada frame (señal) en todos los demás puertos (broadcast)
Switch      | Capa de enlace (2). Rutea tráfico sólo a los puertos de destino.
Bridge      | Capa de enlace (2). Conforma una red a partir de dos o más *segmentos* o redes.
Router      | Capa de red (3).

</div>

Cabe aclarar que muchos dispositivos implementan servicios en todas esas capas,
por lo que es común denominarlos *routers* o *gateways*.