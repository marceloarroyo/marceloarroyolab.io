# Internetworking

La interconexión de diferentes redes generalmente *heterogéneas*, es decir, cada
una con sus propios protocolos y tecnologías de comunicación, requiere la
definición de protocolos que permitan abstraer las diferencias en las
tecnologías y topologías de cada subred.

Los dispositivos de interconexión de redes toman diferentes nombres en base al
nivel de la capa de red del modelo OSI en que operan. Si operan en la capa 2
(enlace de datos) generalmente se denominan *switchs* y si operan en capa 3
(*red*) se denominan *routers*. Para independizarnos de la capa, los llamaremos
*gateways*.

Las técnicas de ruteo pueden clasificarse en tres estrategias:

- Ruteo basado en *datagramas*
- Establecimento de *circuitos virtuales*
- Ruteo de origen

La primera estrategia se basa en que la decisión de ruteo se basa en la
dirección de destino del paquete. Cada *gateway* se basa en una *tabla de ruteo*
(*forwarding table*), definida estática o dinámicamente que indica por qué
puerto/dispositivo tiene que reenviar el paquete.

El problema de cómo se definen esas tablas se conoce como *routing*.

La segunda estrategia se basa en la idea de un *servicio orientado a conexión*.
Si dos nodos deben intercambiar datos, se debe establecer un *circuito virtual*
entre ellos, incluyendo los *gateways* intermedios.

Un circuito virtual queda representado en forma distribuida por una entrada en
una tabla de cada gateway. Cada entrada está definida por la tupla *<vc-id,
in-port,out-port>*.

Una vez establecido un circuito virtual entre dos nodos, cada paquete incluye un
*identificador del circuito virtual*, el cual será usado por cada *gateway* para
determinar a qué otro debe reenviarlo. La siguiente figura muestra un ejemplo de
los circuitos virtuales establecidos.

![vc](img/virtual-circuit.png ':size=70% :id=vc :class=imgcenter')

#### Figura 7.1: Ejemplo de circuitos virtuales.

El establecimiento de los circuitos virtuales generalmente se hace en forma
automática. El protocolo incluye mensajes de *signaling* para definir un
circuito entre dos nodos (*hosts*). Esto requiere que inicialmente los gateways
*sepan* alcanzar al nodo destino, por ejemplo, usando una *tabla de ruteo*.

Las redes basadas en [Asynchronous Transfer Mode
(ATM)](https://en.wikipedia.org/wiki/Asynchronous_Transfer_Mode), actualmente en
deshuso, se basa en el uso de cicuitos virtuales.

La última estrategia, conocida como *source routing*, se basa en que el emisor
incluye el *camino* o la secuencia de gateways intermedios a usar para alcanzar
el destino. Este esquema requiere que cada nodo conozca la *topología de la red*
y hace que la *cabecera (header)* de un paquete sea de tamaño variable.

El protocolo IP incluye una opción para soportar esta estrategia. La aplicación
*Unix-to-Unix Copy (uucp)* de basa en *source routing*.

## Switchs Ethernet

Ethernet permite interconectar diferentes segmentos mediante *switches*
enlazados entre sí. Esto define un protocolo de interconexión de redes de *nivel
2* (*L2*), ya que se define en un protocolo de enlace de datos (capa 2 del
modelo OSI).

En grandes organizaciones, la interconexión de muchas redes Ethernet hace que su
rendimiento decaiga ya que los paquetes de *broadcast* (con destino a todos los
demás nodos) y *multicast* (con destino a un grupo de nodos) generalmente se
retransmiten por toda la red. El protocolo de *ruteo* (o *forwarding*) de frames
Ethernet usados en switches, permite que cada uno *construya dinámicamente* una
*tabla de ruteo* que mapea las direcciones de destino de cada nodo/switch con
cada *puerto* del switch.

Este protocolo requiere resolver el problema de *circularidad* que haría que
algunos frames circularan indefinidamente por la red al no encontrar un nodo con
la dirección de destino.

Para eso, cada *switch* adquiere un *rol* por medio de *identificadores*,
formando una *topología virtual de árbol* (el switch con menor identificador es
el raíz) construyendo un *spanning tree*.

### VLANs

Una *virtual lan (VLAN)* es una tecnología para definir *redes virtuales* sobre
redes a nivel de enlace (L2), ampliamente usada en redes Ethernet.

Permite que una LAN (posiblemente extendida) pueda *particionarse* en diferentes
*redes lógicas*. A cada VLAN se le asigna un identificador único y se usa para
el *reenvío de frames*. Un frame se reenvía de un segmento a otro si ambos
segmentos tienen el mismo identificador.

La versión del protocolo Ethernet 802.1Q soporta el manejo de VLANs, extendiendo
el formato del frame como se muestra en la siguiente figura.

![vlan-frame](img/vlan.png ':size=70% :id=vlan :class=imgcenter')

#### Figura 7.2: Formato de frame Ethernet 802.1Q.

## Internet

Internet es actualmente la *red de redes* y es obviamente la red mundial más
amplia del mundo. Interconecta subredes de diferentes tecnologías utilizando la
familia de protocolos TCP/IP, desarrollados principalmente por [Vinton
Cerf](https://es.wikipedia.org/wiki/Vinton_Cerf) y [Robert
Khan](https://es.wikipedia.org/wiki/Robert_Kahn) en la década de 1970 en un
proyecto financiado por [DARPA](https://en.wikipedia.org/wiki/DARPA).

Esta familia de protocolos se basa en un diseño mas simple que el modelo OSI,
organizado en 4 capas. La capa inferior, denominada *capa de enlace* cubre la
capa física y la de enlace de datos del modelo OSI.

La capa de red está representada por el protocolo IP y un conjunto de protocolos
auxiliares que se describen en el próximo capítulo. Esta capa provee un servicio
orientado a *datagramas* no confiable, es decir no garantiza la entrega de
paquetes a destino ni el orden.

La capa de transporte incluye los protocolos UDP y TCP. El primero simplemente
extiende IP para el multiplexado de paquetes con los procesos de cada host. TCP
ofrece un servicio orientado a conexión confiable, es decir, garantizando la
entrega ordenada de paquetes al receptor con retransmisiones de paquetes
perdidos o con errores.

Las capas de sesión y presentación quedan como responsabilidad de las
aplicaciones.