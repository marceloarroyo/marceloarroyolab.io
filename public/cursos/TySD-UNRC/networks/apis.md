# Interfaces de programación

Un sistema operativo (SO) provee llamadas al sistema como operaciones básicas
(primitivas) de comunicación.

Es común que un SO provea mecanismos de comunicación entre procesos locales,
conocidos como *Interprocess comunication (IPC)* como pipes, FIFOs, memoria
compartida y colas de mensajes. 

El subsistema de red provee llamadas al sistema para comunicación remota
generalmente con soporte de diferentes familias de protocolos.

## Berkeley y POSIX sockets

La API de BSD sockets soporta *Unix* e *Internet Domain Sockets*. Los primeros
permiten comunicación entre procesos locales, mientras que los segundos permiten
comunicación de sistems y procesos remotos usando la familia de protocolos
*TCP/IP*.

> Un *socket* es una abstracción de un *extremo local (endpoint)* de
> comunicación. Se representa como un *descriptor de archivo* por lo que soporta
> operaciones como `read(s, buf, count), write(s, buf, count)` y `close(s)`.

La API de BSD sockets fue tomada por el estándar
[POSIX](https://en.wikipedia.org/wiki/POSIX) con algunas leves modificaciones.

La API se describe en C aunque cada lenguaje de programación define *bindings* a
las operaciones de bajo nivel ofrecida por el SO.

La API ofrece varios archivos de cabecera como los que se listan en la siguiente
tabla.

Archivo         | Descripción
--------------- | -------------------------------------------------------------
`sys/socket.h`  | Funciones y estructuras de datos
`netinet/in.h`  | Protocolos y direcciones IP y puertos UDP/TCP
`arpa/inet.h`   | Funciones de manipulación de direcciones IP

Las funciones pueden estar implementadas directamente como llamadas al
sistema o como funciones de biblioteca, dependiendo del SO.

Función            | Descripción
------------------ | ----------------------------------------------------------
`socket()`         | Retorna un socket de un tipo dado
`bind()`           | Asocia un socket a una dirección (IP/port)
`listen()`         | El socket entra en un estado *listen*
`connect()`        | Conexión a un *servidor*. Usado por un *cliente TCP*
`accept()`         | Acepta una conexión. Usado por un *server TCP*
`send()/write()`   | Envía datos por el socket en *connected state*
`sendto()`         | Envía datos por el socket a una dirección dada (UDP)
`recv()/read()`    | Recibe datos por el socket (conectado)
`close()`          | Cierra el socket. En TCP termina la conexión
`gethostbyname()`  | Obtiene la IPv4 dado el *nombre* o *dominio* de un host
`gethostbyaddr()`  | Obtiene la IPv4 dada la dirección IP como un string
`getaddrinfo()`    | Obtiene una lista de direcciones IPv4/v6 desde un nombre/dirección
`select()`         | Espera por actividad en un arreglo de descriptores
`poll()`           | Verifica el estado de descriptores en un arreglo

La función `int socket(domain, type, protocol)` crea un socket de un dominio,
familia y protocolo específico dado.

Los dominios (entre otros) pueden ser `AF_UNIX`, `AF_INET` o `AF_INET6`, para
UNIX, IPv4 e IPv6 respectivamente.

El parámetro *type* identifica el tipo de servicio requerido como `SOCK_STREAM`,
`SOCK_DGRAM`, `SOCK_SEQPACKET` o `SOCK_RAW` que representan servicios de
transmisión de *streams* (como TCP), *datagramas* (IP/UDP), paquetes con
garantía de secuencialidad o un servicio *plano* (IP, Ethernet, ...).

El último argumento es el protocol específico como `TCP` o `UDP`. Si se omite se
determina un protocolo *por omisión* (ej: `TCP` para `SOCK_STREAM`).

## API con protocolos orientados a datagramas

En un protocolo sin conexión, como UDP, el patrón de uso de la API de sockets se
muestra en la siguiente figura.

![dgm-sockets-api](img/UDPsockets.jpg ':class=imgcenter :size=50% Patrón de
comunicación UDP')

Una de las partes (ej: el servidor) deberá estar esperando un mensaje
(comúnmente bloqueado) ejecutando un `recvfrom()`. Otro proceso enviará un
mensaje usando `sendto()`. La función
`sendto(s, buf, len, dst_addr, dst_addr_len)` incluye la dirección de destino.
La función `recvfrom()` recibe en un parámetro de salida la dirección del emisor
del mensaje, comúnmente la IP/port.

## API con protocolos orientados a conexión

En un protocolo orientado a conexión, como TCP, el patrón de uso de la API de
sockets se muestra en la siguiente figura.

![conn-sockets-api](img/TCPsockets.jpg ':class=imgcenter :size=50% Patrón de
comunicación TCP')
 
En éste escenario el cliente deberá conectarse al servidor (usando
`connect()/accept()`) para obtener un *socket conectado*. Luego, ambas partes
pueden comuncarse usando `send()/write()` y `recv()/read()`.

Cabe hacer notar que es posible usar las llamadas al sistema
`read(s, buf, count)` y `write(s, buf, count)` de entrada/salida con archivos.

Esto permite reusar componentes (funciones/bibliotecas) que leen/escriben desde
archivos pasando sockets como descriptores. El SO distingue que al operar sobre
sockets se debe redirigir los datos al subsistema (stack) de red.

## Uso de notificaciones de entrada-salida

Las llamadas al sistema `select()` (UNIX) y `pool()` (BSD) permiten al proceso
invocante bloquearse hasta que haya algún evento de cambio de estado en
descriptores de entrada-salida como archivos y sockets.

La función `select(inputs, outputs, exceptions, timeout)` toma tres conjuntos
(bitsets) de descriptores de archivos, pipes o sockets de entrada, salida y de
excepciones (descriptores en los que se quieren manejar los errores).
Opcionalmente se puede pasar un timeout que la desbloquea.

Cada elemento del bitset se puede manipular mediante el uso de las macros
`FD_ZERO(&bitset)`, `FD_SET(fd, &bitset)`, donde `fd` es el valor del
descriptor de archivo.

El uso de estas funciones permiten implementar aplicaciones que requieren de
múltiples conexiones sin necesidad de implementar un servidor concurrente
(usando threads o procesos hijos). Esto permite el desarrollo de una
arquitectura típica de un *sistema reactivo*.

La operación `select()` se desbloquea al ocurrir un evento de entrada/salida en
alguno de los descriptores.

Al retornar de `select()`, la macro `FD_ISET(fd, &bitset)` determina si el
descriptor `fd` tiene un evento en el set. Si ocurrió un evento en un descriptor
en *inputs* significa que hay datos disponibles para leer. Si ocurrió un evento
en un descriptor en *outputs* significa que hay espacio en el buffer interno y
se puede escribir en él. Si ocurrió un evento en un descriptor de *exceptions*
indica un error o excepción como un cierre de conexión.

En lenguajes de alto nivel como Python, los descriptores se pasan en listas y la
función `select()` retorna tres listas con los descriptores que han recibido
algún evento de entrada-salida.

## Arquitecturas de servidores

En los diagramas de las secciones anteriores las aplicaciones del servidor son
*iterativas*, es decir que pueden atender a un requerimiento de un cliente a la
vez.

Para permitir interactuar con múltiples clientes en forma simultánea, es común
que diseñar e implementar *servidores concurrentes*. Es típico en el caso de
protocolos orientados a conexión, que el proceso (o thread) principal, sólo
acepte conexiones y luego delegue la atención a cada cliente en procesos o
threads *hijos* como se muestra en el siguiente extracto de código.

``` clike
// snip
while (true) {
    conn_socket = accept(s, ...);
    spawn(conn_socket); // create new child process or thread
}
// snip
```

Esta estrategia tiene el problema que un servidor muy sobrecargado de conexiones
dispara muchos procesos o threads lo cual genera mucha sobrecarga en el sistema,
además de que los tiempos de respuesta pueden ser largos por el tiempo de
creación de threads/procesos.

### Threadpool

Una mejora es crear un número predefinido de *workers* (procesos o threads) y el
thread/proceso principal (*master*) encola cada requerimiento para que sea
procesado por algún *worker* desocupado (*idle*), como se muestra en el
siguiente diagrama.

``` td2svg
  +--------+    +--------+    +--------+    +--------+    +--------+
  | worker |    | worker |    | worker |    | worker |    | worker |
  +--------+    +--------+    +--------+    +--------+    +--------+
       ^             ^             ^             ^             ^
       |             |             |             |             |
       +-------------+-------------+-------------+-------------+
                                   |
                             +-----------+
                             |  request  |
                             +-----------+
                             |    ...    |
                                   ^
                                   |
                             +-----+-----+
                             |   master  |
                             +-----------+
```

La cola de requerimientos es un recurso compartido por lo cual se debe
sincronizar el acceso. Una estrategia de scheduling con balance de carga
semi-automática es *job stealing* donde cada *worker idle* intenta *tomar* el
primer *job* de la cola de requerimientos.

### Entrada/salida asincrónica

En la arquitectura de software basada en entrada/salida (*E/S*) asincrónica mas
básica el programa define un patrón de *concurrencia* sin usar paralelismo o
threads. En ciertos casos en sistemas distribuidos puede lograr un mejor
rendimiento sin usar threads.

A bajo nivel se pueden usar las APIs basadas en `select(), pool(), epool()`
típicas de los sistemas UNIX. Otros lenguajes proveen mecanismos similares o
abstracciones como *promises* o *futures* basados en el patrón `async/await`.

Este paradigma provee un mecanismo similar a las *corrutinas* o *multitarea
cooperativa*, ofreciendo una abstracción de alto nivel.

Intuitivamente, un *future* o *promise* puede verse como un valor que
eventualmente estará disponible, es decir puede estar en un estado *ready* o
*not ready*. También es posible que tenga un tercer estado de error o *failed*.

Una operación (función o bloque) *async* retorna una *promise* (o *future*). Una
operación como `v = await future` bloquea al thread hasta que el valor esté
disponible, retornando el control al *loop event*. La primitiva `await` es
similar a `yield` usado en *generators* aplicables a *iteradores*.

El mecanismo requiere de un *executor* o *event loop* encargado de invocar el
progreso de las tareas asincrónicas.

Es posible encontrar este patrón en los lenguajes de programación modernos como
Javascript, Python o Rust.

Los principales conceptos son los siguientes:

- *Event loop*: El ejecutor central de *tasks*.
- *Coroutines*: Función con un *estado*: *(program pointer, env, state)*. El
  *environment* (o *clausura*) es un registro de activación asociado con los
  *bindings* a las variables y parámetros a las que tiene acceso. El estado
  determina si esta completada (*ready*), o *pending* (continuará después de un
  *await*).
- *Futures*: Objetos que representan valores eventuales.
- *Tasks*: Estado de corrutinas (encapsuladas en *futures*) por el ejecutor para
  invocar (o continuar con) su progreso.
- *async function or block*: Define una *coroutina*.
- *await keyword*: Retorna el control al *event loop* y actualiza el estado
  (program pointer)de la coroutine. Sólo puede usarse dentro de una función
  *async*.

Abajo se muestra un ejemplo simple de éste modelo en Python.

``` python
import asyncio

async def task1():
    await asyncio.sleep(1)
    print("Hello, from task 1!")

async def task2():
    print("Starting task 2...")
    await asyncio.sleep(1)  # Simulates doing something else f    or 1 second
    print("Finished task 2!")

async def main():
    # Schedule both tasks to run concurrently
    await asyncio.gather(
        task1(),
        task2(),
    )

asyncio.run(main())
```

La función `asyncio.run()` representa el *event loop*. Además provee otras
funciones para permitir disparar otras coroutines como `asyncio.gather()` o
`asyncio.create_task(fn)`.