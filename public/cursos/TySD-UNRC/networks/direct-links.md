# Enlaces directos

En este capítulo se analizan los mecanismos utilizados en enlaces directos (de
computadora a computadora u otros dispositivos) y de cómo lograr conectar
varios dispositivos (nodos) entre sí para formar una red.

Para lograr esto hay que resolver al menos los siguientes problemas:

1. ***Conversión analógica-digital***
2. ***Codificación*** de los bits o símbolos a transmitir/recibir
3. ***Transmisión de señales***
4. ***Empaquetado (framing)*** de secuencias de bits
5. ***Detección de errores*** en la recepción de frames
6. ***Compresión de datos***. Los veremos en el capítulo [presentación](presentation.md).
6. ***Acceso al medio***, en el caso de usar un medio compartido como un cable
   (bus) o aire

Actualmente muchas de estas funciones se implementan en dispositivos de hardware
de comunicaciones (chips Ethernet, wifi, USB, ...).

## Conversión analógica-digital

Una señal de entrada analógica $f(t)$, por ejemplo una señal generada por un
micrófono puede convertirse en una digital o discreta, denotada como $d[i]$
donde el dominio y rango de $d$ son discretos.

El proceso consiste en los siguientes pasos:

1. ***Muestreo (sampling)***: Captura de las amplitudes de la señal en ciertos
   puntos en el tiempo. El intervalo de tiempo entre las muestras tomadas
   determina la *frecuencia de muestreo*.

2. ***Cuantización o discretización***: Discretización de los valores obtenidos
   en el paso anterior. Comúnmente se mapea un conjunto finito de intervalos
   continuos a un conjunto finito de valores discretos.

Esto resulta en una señal digital o *función escalonada* $d[i]$.

## Codificación

Nuestro interés se centra en la comunicación de *señales digitales*, es decir la
transmisión de un conjunto finito de símbolos, como por ejemplo el conjunto
${0,1}$ (binario).

En primer lugar hay que determinar cómo se representa o *codifica* una
secuencia de bits a transmitir.

Los bits se deben representar de forma tal que sean distinguibles por un
receptor y deben tener una cierta duración en el tiempo que se conoce como
*tiempo de bit*. Se usa un reloj (u oscilador) que sincroniza el transmisor
(*Tx*) y el receptor (*Rx*). Cada *pulso (tick)* del reloj indica el comienzo
del próximo bit.

En la transmisión de datos digitales por pulsos (señales cuadradas), para
distinguir los símbolos digitales transmitidos se usa comúnmente *modulación por
amplitud*. Por ejemplo: Un 1 se representa con un *pulso* de un voltaje
(*amplitud*) de 5V y un 0 por un *pulso de 0V o -5V*.

El tiempo de bit determina la ***frecuencia de muestreo*** y se mide en
***Hertz***: *pulsos o ciclos por segundo*.

Si el transmisor y receptor tienen relojes independientes, como en el caso de
las redes, surge el problema de *sincronización*. Los relojes no son perfectos y
tienden a *desincronizarse* luego de un breve tiempo.

Una solución posible es el ***framing***, es decir la transmisión en *grupos de
bits* de una longitud determinada, lo que permitirá la *re-sincronización* entre
dos frames consecutivos. Es común el uso de *inter-frame gaps* de una longitud
determinada o el uso de *preámbulos* o patrones de bits que representan el
símbolo de comienzo de un frame.

Otra forma es evitar secuencias largas del mismo bit transmitido, haciendo que
las *transiciones* sirvan como puntos de re-sincronización. Esto motiva algunas
de las codificaciones mostradas en la siguiente figura.

![encodings](img/encodings.png ':id=encodings :class=imgcenter :size=60%')

#### Figura 2.1: Algunas posibles codificaciones.

La *codificación de Manchester* por ejemplo, aplica un *o exclusivo (XOR)* del
valor del reloj con el bit de datos.

La transmisión de *pulsos* es común en circuitos electrónicos y algunos medios
de transmisión por cables de cortas distancias como por ejemplo Ethernet y USB.

## Transmisión de señales electro-magnéticas

La transmisión de datos digitales por pulsos tiene el inconveniente que éstas
señales sufren *atenuaciones* importantes cuando se desea transmitir a
distancias mayores, como lo es en el caso de los sistemas telefónicos o redes de
datos.

Además, ese tipo de transmisión se denomina ***baseband*** ya que permite
transmitir sólo una secuencia de datos.

La transmisión de señales analógicas en forma de ondas electromagnéticas
permiten codificar (o *modular*) señales digitales y pueden usarse también en
medios de comunicación como el aire o el vacío.

También es posible transmitir por un medio diferentes ondas electromagnéticas a
distintas frecuencias en paralelo sin que se interfieran. Esta técnica de conoce
como *broadband* o *multiplexado de frecuencias*.

> Una ***señal electro-magnética (EM)*** se genera al hacer variar el sentido de
> la corriente *periódicamente* en un material conductor (antena).

El proceso es reversible, es decir que un receptor puede generar corriente desde
la *EM*. La [figura 2.2](#em) muestra las características de una señal.

![em](img/electromagnetic-wave.gif ':size=40% :id=em')
![wl](img/amplitude-period-shift.svg ':size=50% :class=imgsep')

#### Figura 2.2: Señales electromagnéticas.

Matemáticamente, en una señal periódica de la forma 

$$y(t) = a \times g(p(t+s)) + d$$

siendo *g* una función periódica (seno, coseno, etc), los parámetros *a, p, s y
d* determinan:

- *a*: La amplitud de la señal.
- *p*: El período $\frac{2\pi}{p}$ es el tiempo que la función completa un
  ciclo. 
- *f*: La frecuencia. Es la inversa del período: $f=\frac{1}{p}$.
- $\lambda$: *Longitud de onda*. Es la distancia entre dos *crestas* (o
  *valles*) consecutivos.
- *s*: El desplazamiento (*phase shift*) en el dominio.
- *d*: El desplazamiento vertical.

En transmisión de señales es interesante conocer su *velocidad (de propagación)*:

$$ v = \frac{\lambda}{T} = \frac{\lambda}{p} = \lambda \times f $$

Así, $\lambda = \frac{v}{p}$

> El ***ancho de banda (bandwidth)*** de un medio es el *rango de frecuencias*
> que puede transmitir sin que las señales sufran *atenuaciones* severas.

Se debe notar que el ancho de banda es una propiedad del medio físico.

En la práctica se limita el ancho de banda mediante la aplicación de *filtros*,
dependiendo de la aplicación. Por ejemplo, en una línea telefónica analógica se
utiliza un ancho de banda de 3000Hz, adecuado para transmitir un buen
rango de tonos de voz en forma analógica a largas distancias por cable.

Es común que a mayores frecuencias las señales sufran de mayor *atenuación*
(pérdida de potencia) debido a la generación de mas ruido en el espacio. Por
esto en la práctica se usan comunicaciones a altas frecuencias para conectar
dispositivos en distancias cortas.

> [Jean-Baptiste Fourier](https://es.wikipedia.org/wiki/Joseph_Fourier) demostró
> que una función periódica $g(t)$ con período $T$ se puede construir desde la
> suma de términos (serie) dados por
> $$ g(t) = \frac{1}{2}c + \sum_{n=1}^\infty a_n sin(2\pi n ft) +
> \sum_{n=1}^\infty a_n cos(2\pi n f t) $$

donde $f=1 / T$ es la *frecuencia*, $c$ es una constante y los coeficientes
$a_n$ y $b_n$ de cada término son las ***amplitudes*** de la ***n-ésima
armónica***.

La [transformada de Fourier](https://en.wikipedia.org/wiki/Fourier_analysis)
comúnmente se emplea para descomponer una señal en sus componentes (armónicas).
De cada componente puede extraerse su frecuencia, amplitud y su fase (ángulo o
desplazamiento sobre el eje temporal) inicial.

En la siguiente figura se considera la transmisión digital de 8 bits.

![armonics](img/armonics.png ':size=50% :class=imgcenter :id=armonics')

La figuras b), c), d) y e), muestran la reconstrucción de la señal usando 1 a
8 armónicas. Se puede apreciar que si se transmiten 8 armónicas la señal puede
ser reconstruida por el receptor con una buena aproximación, haciendo
innecesario transmitir la señal a mayor frecuencia (notar que cada armónica
tiene una frecuencia múltiplo de la frecuencia original $f$).

Por ejemplo, una línea telefónica de voz estándar tiene un ancho de banda de
3000Hz debido los filtros utilizados para transmisión de voz analógica. Si se
usa para transmitir datos digitales a una cierta tasa de *b bits por segundo*,
el tiempo para transmitir 8 bits es *8/b* segundos y la frecuencia de la primera
armónica (la *fundamental*) es de *b/8Hz*.

En éste caso, la *armónica de mayor frecuencia* será de *3000/(b/8)=24000/b*.
Esto indica que a una tasa *b=19200* bps ya no pueden pasar más de una armónica,
lo cual daría un límite para *b*. 

La tabla siguiente describe el número de armónicas que podrán transmitirse
efectivamente para diferentes tasas de transmisión.

<div class="divcenter">

Bps   | T (ms) | Primera armónica (Hz) | Armónicas
----: | -----: | :-------------------: | :-------:
  300 | 26.67  |           37.5        |    80
  600 | 13.33  |           75          |    40
 1200 |  6.67  |          150          |    20
 2400 |  3.33  |          300          |    10
 4800 |  1.67  |          600          |     5
 9600 |  0.83  |         1200          |     2
19200 |  0.42  |         2400          |     1

#### Tabla 2.1: Tasa de transmisión y armónicas transmitidas.

</div>

Se puede notar que para transmitir a 9600 bps sólo se transmitirán 2 armónicas
(como en el caso (c) de la figura anterior), dificultando la reconstrucción de
la señal original en el receptor.

## Tasa de transmisión

> La ***tasa de transmisión (data rate)*** es el número de bits transmitidos por
> segundo.

Es común que en comunicaciones digitales se use este término como sinónimo de
*ancho de banda*, aunque éste último, como ya se describió es el *rango de
frecuencias permitido por el medio*.

Estos dos valores están directamente relacionados.
La tasa de transmisión depende del ancho de banda ($B$) de medio y el número
valores discretos ($V$) codificados. En 1924 Nyquist estableció que en un canal
filtrado sin ruido con un ancho de banda *B*, la señal puede reconstruirse (en
el receptor) tomando exactamente $2B$ muestras por segundo. Realizar el muestreo
a frecuencias mayores no contribuye ya que los componentes de las demás
frecuencias se han filtrado.

Así el teorema de Nyquist define:

> La ***tasa máxima de transmisión*** $= 2 B \: log_2 V$

Por ejemplo, un canal (sin ruido) con un ancho de banda $B=3Khz$ puede
transmitir 6000bps usando señales digitales binarias ($V=2$).

La tabla de la sección anterior muestra que a tasas de transmisión bajas se
pueden reconstruir las señales con mayor fidelidad (pasan más armónicas). Para
aumentar la tasa de transmisión se debe aumentar el ancho de banda o usar
codificaciones y modulaciones apropiadas para representar más de un bit
(conjunto más amplio de valores discretos *V*).

### Ruido

La ecuación anterior asume un canal *libre de ruido*. En la práctica siempre hay
*ruido*, es decir otras señales electromagnéticas *espúreas* generados por la
radiación solar, motores eléctricos y otros dispositivos de transmisión.

La *cantidad de ruido en un canal* $= \frac{S}{N}$, donde $S$ es la potencia de
la señal transmitida y $N$ es la potencia de las señales de ruido y se expresa
en ***Decibeles***: $Db = 10 \: log_{10} \frac{S}{N}$.

Así, $\frac{S}{N}=100=20Db$, $\frac{S}{N}=1000=30Db$, ...

> Shannon postuló que la ***capacidad máxima de un canal*** $=B \: log_2
> (1+\frac{S}{N})$.

Al comparar la capacidad del canal con la tasa de transmisión es posible
determinar el número de valores discretos eficaces $V$.

$$V=\sqrt{1 + \frac{S}{N}}$$

Esto determina un límite a la tasa de transmisión en un canal con ruido.

## Modulación

Para transmitir datos sobre ondas electromagnéticas, se usa comúnmente una
*señal portadora*, la cual se modifica su forma en intervalos en el tiempo para
representar los diferentes valores que se desean transmitir.

En el caso de las comunicaciones digitales la señal portadora se modifica en
base a la señal digital (cuadrada) de entrada.

Como ya se describió, una señal digital se genera como una función con dominio y
rangos discretos. En comunicaciones digitales, el dominio (tiempo discreto)
determina el *tiempo de bit*. El rango determina el conjunto de valores posibles
a transmitir. El receptor debe reconstruir los componentes de la señal recibida
para *reconocer* los valores digitales *codificados* en la señal.

![modulación](img/digital_modulation.png ':id=modulation :class=imgcenter
:size=50%')

#### Figura 2.3: Modulación de datos digitales.

En la [Figura 2.1](#modulation) se muestran las técnicas básicas de modulación.
A partir de una señal de entrada y una *señal analógica portadora
(carrier)*, éstas se combinan para producir la señal que *codifica* los datos.

En comunicaciones digitales la señal de entrada es una función discreta (o señal
cuadrada).

Es posible *codificar* información en una señal en el *tiempo de bit* (intervalo
de tiempo) correspondiente modificando algunos de sus parámetros.

- Una señal de ***modulación por frecuencia (FM)*** cambia la frecuencia (usa un
oscilador variable modificando $p$).

- La ***modulación por amplitud (AM)*** cambia la *amplitud* (variando el
voltaje de la señal).

- La ***modulación por desplazamiento de fase (PSM)*** desplaza la onda
en una cierta cantidad de grados.

Por supuesto, es posible combinar estas técnicas. Una técnica muy usada es la
modulación PQSK, que combina desplazamiento de fase (0, 90, 180 y 270 grados)
con modulación por amplitud y permite codificar 4 valores discretos por cada
tiempo de bit (genera 4 formas diferentes de la señal) lo que es equivalente a
transmitir 2 bits por unidad de tiempo.

Algunas técnicas de modulación y codificación actuales permiten transmitir hasta
8 valores discretos por tiempo de bit.

## Multiplexado

El _multiplexado_ permite múltiples comunicaciones sobre un mismo medio
compartido. Un receptor deberá _separar_ o _demultiplexar_ las diferentes
comunicaciones (_streams_) recividas.

Las técnicas utilidadas se pueden clasificar en:

1. ***División de frecuencias (FDM)***: El ancho de banda del canal se divide en
   sub-canales evitando que se interfieran entre sí. Entre cada canal
   generalmente existe un _gap_ o separación que hace que ocurra un pequeño
   desperdicio del ancho de banda total. Técnicas de modulación como [Orthogonal
   Frecuency, Division Modulation
   (OFDM)](https://en.wikipedia.org/wiki/Orthogonal_frequency-division_multiplexing).
2. ***División de tiempo (TDM)***: Cada dispositivo utiliza la totalidad del
   canal por turnos y por un intervalo de tiempo determinado, generalmente con
   una estrategia _round-robin_.
3. ***División por códigos (CDM)***: La idea es que cada participante genera dos
   señales digitales superpuestas: La de datos, a una frecuencia $f_d$ y otra de
   alta frecuencia $f_c$ (el código, asignado a cada comunicación). La señal
   transmitida es el _XOR_ entre las dos señales (con frecuencia $f_c$), como se
   muestra en la [Figura 2.2 c)](#CDMA). Así varias secuencias de datos pueden
   transmitirse simultáneamente. Un receptor puede *extraer* la secuencia de una
   comunicación en particular, haciendo nuevamente el XOR entre la señal
   recibida y el código correspondiente. Además de permitir el multiplexado,
   este tipo de multiplexado también permite definir un mecanismo de acceso al
   medio (como en *CDMA*). Una técnica comúnmente utilizada es la asignación de
   códigos *ortogonales* entre sí (vistos como vectores, su producto interior es
   cero).

![FDM](img/FrequencyDivision.jpg ":id=FDM :class=imgcenter :size=50%")

#### a)

![TDM](img/Time-Division-Multiplexing.gif ":id=FDM :class=imgcenter :size=50%")

#### b)

![CDMA](img/CDMA.png ":id=CDMA :class=imgcenter :size=50%")

#### c)

#### Figura 2.4: FDM a),  TDM b) y CDM c).

Una analogía con las conversaciones entre grupos de personas es la siguiente:
*FDM* es como diferentes personas hablando simultáneamente lo hacen con
diferentes tonos de voz, *TDM* es análogo a que hablen por turnos y *CDM* serían
cuando diferentes grupos hablan simultáneamente en el mismo tono pero en
diferentes idiomas. En *CDM* cada receptor percibe a las otras conversaciones en
idiomas que desconoce como *ruido*.

Por supuesto, estas técnicas se pueden combinar como es el caso de las técnicas
usadas en el sistema de telefonía celular, donde se utilizan división de tiempo
y de frecuencias para soportar varias comunicaciones simultáneas compartiendo
bandas (canales) entre diferentes dispositivos en una celda.