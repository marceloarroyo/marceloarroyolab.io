# Infraestructura

La interconectividad lograda en la actualidad se basa en diferentes tipos de
tecnologías para formar una gran red de redes entre sí a nivel mundial.

Aquí se describen algunas tecnologías usadas para acceder a Internet desde
nuestros hogares, oficinas de trabajo o inclusive desde teléfonos móviles
(celulares).

Un *Internet Service Provider (ISP)* provee servicios de interconexión a
Internet. Estas organizaciones generalmente tienen una red propietaria que a su
vez está interconectada a una red regional (un país, por ejemplo) principal o
*backbone* que permite la interconexión entre ISPs. Estos a su vez utilizan los
servicios de otras empresas proveen servicios de telecomunicaciones
internacional, contando con una infraestructura de enlaces intercontinentales
basados en comunicaciones satelitales, cables o fibra óptica.

Actualmente la mayoría de las comunicaciones globales atraviesan los enlaces
intercontinentales de cableado submarino (de fibras ópticas), como se muestran
en el siguiente [mapa](https://www.submarinecablemap.com/).
En
[nic.ar](https://nic.ar/es/enterate/novedades/como-se-conecta-argentina-a-internet)
se pueden encontrar mas detalles de la interconexión de Argentina a Internet.

Cada ISP brinda el servicio de conectividad a sus clientes mediante algunas de
las siguientes tecnologías:

- ***Digital Subscriber Line (DSL)***: Uso de líneas telefónicas (cables UTP de
  cobre).
- ***Cable modem***: Basada en la infraestructura generalmente usada en los
  proveedores de TV por cable.
- ***Wi-fi***: Enlaces punto a punto usando antenas direccionales de medio o
  largo alcance.
- ***Fibra óptica***: Líneas de fibra óptica.
- ***Telefonía celular***: Servicios de datos usando redes inalámbricas 3G, 4G o
  5G (de rápido desarrollo).

## DSL

La tecnología *Digital Subscriber Line (DSL)* es usada principalmente por las
empresas de telefonía fija ya que permite reusar la infraestructura de cableados
existentes, agregando *repetidores* para filtrar y amplificar las señales por
tramos en la red. Cada línea de cada cliente, llega a la central y se conectan a
un sistema [DSLAM](https://en.wikipedia.org/wiki/Digital_subscriber_line_access_multiplexer)
que multiplexa las comunicaciones de la intranet en los enlaces de alto ancho de
banda del *backbone* del ISP.

En el cliente, la línea telefónica se conecta a un *modem DSL* que codifica y
modula las señales digitales de la red local en la red local. Este modem también
actúa de *switch Ethernet* y/o *wi-fi* para interconectar los dispositivos de
la red local, tal como se muestra en la siguiente figura.

![dsl](img/DSL.jpg ':size=70% :class=imgcenter')

El servicio generalmente es asimétrico (*ADSL*) por lo que la tasa de
transmisión desde el cliente (subida) es menor que la tasa de recepción o
descarga.

Las señales digitales se transmiten en un rango de frecuencias mas alto que el
usado en telefonía tradicional, lo que permite la comunicación de voz y datos
simultáneamente. Para efectivamente separar las frecuencias de voz y datos se
usan filtros, como se puede apreciar en la figura de arriba.

Las versiones modernas como VDSL pueden alcanzar tasas de transmisión de hasta
54Mbps, requiriendo segmentos cortos (hasta 300 metros) entre la conexión del
cliente hasta el repetidor.

Comúnmente, en los enlaces entre cada *modem* y la central del ISP se utiliza
[PPPoE](https://en.wikipedia.org/wiki/Point-to-Point_Protocol_over_Ethernet),
que permite _encapsular_ frames *PPP* dentro de frames _Ethernet_. Esto permite
que el ISP pueda utilizar los servicios de control de tasa de transmisión
(dependiente del plan del subscripción del usuario) y autenticación provistos
por _PPP_. Es común que entre los enlaces del *modem/router* del ISP y la central (DSLAN) se use ATM para la transmisión de frames PPPoE.

Como ya se analizó en el capítulo de enlaces de datos, el uso de *PPPoE* afecta al tamaño máximo del frame o *maximum transmition unit (MTU)*, ya que habrá que informar a la capa de red que en este caso la MTU ya no es de 1500 bytes sino de 1492 (la cabecera de un frame PPPoE consume 8 bytes).

## Fibra óptica

El servicio de enlaces por fibra óptica al cliente se basa en las tecnologías
conocidas como ***passive optical network (PON)***. El ISP en su oficina central
cuenta con múltiples *terminales ópticas (OLT)* que a si vez se conectan a su
*backbone* mediante *switchs*. De cada *OLT* se distribuyen enlaces de fibra que
se divide en ramas por medio del uso de *splitters* formando un árbol hasta cada
cliente que cuenta con una *optical network unit (ONU)*, tal como se muestra en
la siguiente figura.

![dsl](img/pon.png ':size=70% :class=imgcenter')

Los *splitters* utilizados son *pasivos* (de ahí el nombre), ya que simplemente
son *repetidores* de las señales ópticas (no realizan *store and forward* de
frames).

La comunicación es bidireccional en cada cable (*pelo*) de fibra ya que se usan
dos frecuencias diferentes (*wavelengths*) en las señales ópticas de subda y
bajada.

Los frames se multiplexan usando *división de tiempo*. Cada *ONU* filtra los
frames sólo dirigidos a su unidad.

## Telefonía celular

Una red de telefonía celular o móvil se basa en un conjunto de nodos
interconectados a una oficina central (generalmente por enlaces de fibra
óptica). Estos nodos se conocen como *celdas* o *estaciones base (Broadband Base
Unit o BBU)*. 

Cada celda contiene equipos de computación y comunicación por radio con un
alcance que varía entre los 5 y 20km. Los equipos móviles se denominan *user
equipment (UE)*.

Las celdas vecinas usan diferentes rangos de frecuencias para evitar
interferencias.

La oficina central implementa una *Evolved Packet Core (EPC)* que brinda
servicios de *rastreo* de *UEs*, conocida como *Mobility Management Entity
(MME)*, consultas de usuarios suscritos (*Home Subscriber Server o HSS*) y un
par de *gateways* de *sesiones (SWG)* y de *paquetes (PGW)* que mantienen las
*sesiones* (comunicaciones) de usuarios y procesan los paquetes entre la red
celular e Internet.

La siguiente figura muestra una posible configuración.

![dsl](img/cellular-network.png ':size=70% :class=imgcenter')

Cada UE es *manejada* por una celda. Para establecer una comunicación entre dos
UEs se requiere determinar un *circuito virtual* entre los dos nodos extremos
(los que manejan cada UE), generalmente usando nodos y oficinas intermedias.
Cuando una UE se mueve desde una celda a otra, la celda que logra mejor
intensidad de señal *captura* la UE. Si una comunicación estaba en curso, la red
debe transferir la sesión de una celda a otra.

La red celular está interconectada a la red telefónica permitiendo
comunicaciones entre teléfonos móviles y fijos.

Actualmente, las diferentes versiones de tecnología (3G, 4G, 5G) se especifican
en las normas LTE (*Long-Term Evolution*). LTE describe principalmente cómo se
utiliza el espectro radio-eléctrico usado por los UEs. 

LTE usa una estrategia basada en *reserva de slots* y utiliza *Orthogonal
Frecuency-Division Multiple Access (OFDMA)* para datos de *bajada* y *Single
Carrier Frecuency-Division Multiple Access (SC-FDMA)* de subida. Este último
esquema permite reducir el consumo energético de los UEs.

En cada comunicación, la celda asigna a un UE una frecuencia de subida y una de
bajada, permitiendo comunicaciones *full-duplex*.

En un canal de subida las comunicaciones de múltiples usuarios se *comparten*
usando *TDM*. Un canal de bajada (usando OFDMA) se multiplexa usando *FDM* y
*TDM*.

En OFDMA (4G-LTE) se divide un canal en 12 frecuencias ortogonales moduladas
independientemente. Cada banda (subportadora) es de 15khz. 

La codificación y modulación usadas (*Quadrature Amplitud Modulation*) permiten
representar el conjunto de valores discretos o símbolos (4 bits en 16-QAM, 6
bits en 64-QAM).

La siguiente figura muestra una representación bidimensional del esquema usado
en OFDMA.

![dsl](img/lte.png ':size=70% :class=imgcenter')

Un *resource element* representa un símbolo transmitido. Los datos se transmiten
en bloques de 1ms de duración.

La *asignación* de *resource elements* o *slots* a cada UE lo realiza un
*planificador (scheduler)*. Este es un problema complejo de optimización y cada
operador generalmente utiliza sus propios algoritmos.

En la figura, cada color representa comunicaciones de diferentes UEs.

Las comunicaciones de voz se digitalizan, comprimen y se cifran en cada UE, por
lo que la red sólo se basa en comunicación de datos digitales.

Las diferencias entre 3G, 4G y 5G son principalmente los rangos del espectro
utilizados y algunas diferencias en las técnicas de codificación, modulación y
multiplexado utilizados. 

5G, flexibiliza el manejo en base a las distintas bandas (espectro) utilizadas,
permitiendo usar diferentes formas de ondas. Las frecuencias bajas pueden usarse
para canales de bajo ancho de banda como transmisión de mensajería de texto. Las
señales por debajo de 1GHz se utilizan para comunicaciones de largo alcance, las
bandas entre 1 y 6GHz ofrecen mayor ancho de banda, principalmente para acceso a
Internet o videoconferencia y las frecuencias por encima de los 24GHz ofrecen un
gran ancho de banda en distancias cortas (de pocos metros).

## Satélites

El uso de satélites permite interconectar nodos separados por grandes
distancias, ya que hacen de *repetidores en gran altura*, visibles desde antenas
direccionales terrestres.

Los satélites geoestacionarios se ubican en órbitas cercanas a los 36000km sobre
el Ecuador (latitud 0). Giran a una velocidad de 3.07km por segundo para lograr
la misma velocidad angular que la superficie terrestre. Así su posición relativa
desde un punto en la superficie terrestre se mantiene fija. Esto facilita las
estaciones terrenas ya que sus antenas no necesitan movilidad.

Estos tipos de satélites son ampliamente usados en servicios de *broadcasting*
como radio y TV. Uno de los principales problemas en el uso de satélites es la
*latencia* debido a los *tiempos de propagación de las señales*.

A menudo se utiliza una *constelación* de satélites en órbitas mas bajas. En
este escenario, se usan estaciones terrestres con antenas omnidireccionales o
direccionales móviles y los satélites forman una red de retransmisores.

Actualmente el mayor porcentaje de las comunicaciones globales se realizan por
medio de los tendidos de cables (de fibra óptica) intercontinentales.
