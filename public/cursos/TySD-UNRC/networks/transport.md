# Protocolos de transporte

Los protocolos de nivel de red tienen como objetivo la entrega de paquetes entre
hosts. Las aplicaciones requieren establecer la comunicación entre *procesos*
ejecutándose en diferentes hosts. Esto es realizado por los protocolos de la
capa de transporte, también conocidos como *end-to-end protocols*.

Un protocolo de transporte puede ofrecer servicios orientados a datagramas u
orientados a conexión.

En la familia TCP/IP los dos protocolos fundamentales son [User Datagram
Protocol (UDP)](https://en.wikipedia.org/wiki/User_Datagram_Protocol) y
[Transport Control Protocol
(TCP)](https://en.wikipedia.org/wiki/Transmission_Control_Protocol).

Ambos protocolos deben actuar como *multiplexores* de paquetes transmitidos
desde procesos hacia la capa de red y viceversa.

Los paquetes de los protocolos de red incluyen un campo que identifica su
payload, como el campo `protocol` en datagramas IP. Típicamente
contiene el identificador del tipo de paquete (o protocolo) de la capa de transporte.

De esa forma, ante la recepción de un paquete, el protocolo de red *entrega* al
módulo correspondiente de la capa superior.

Estos protocolos deben *abstraer* el *end-point* o proceso en un host. En
TCP/IP, estos *end-points* se representan como *puertos (ports)*, que son
valores de 16 bits.

## UDP

UDP actúa como un simple *multiplexor* de datagramas con los procesos de un host.

Un datagrama UDP relaciona proceso (port) del host (IP address) emisor con el
proceso (port) del host receptor, tal como se muestra en la siguiente figura.

![Datagrama UDP](img/udp-datagram.png ':size=50% :class=imgcenter :id=udp-dgrm')

#### Figura 10.1: Formato de un datagrama UDP.

La cabecera sólo agrega los *puertos* de origen y destino, el tamaño del
datagrama y un *checksum*, el cual se computa a partir del *payload*, la
cabecera y del *pseudoheader* formado por las direcciones IP origen y destino,
el número de protocolo y el campo *length*.

La operación de la API sockets `int bind(int s, struct sockaddr*, ...)` asocia
al proceso invocante con el *end-point <address,port,protocol>*. En TCP/IP estos
valores se corresponden con los valores de los campos `sin_family=AF_INET`,
`sin_port` de la estructura `sockaddr_in`. El protocolo se selecciona desde el
tipo de socket creado (ver `socket()` syscall).

Las aplicaciones del tipo cliente-servidor generalmente se basan en que los
clientes envían los requerimientos a *well-known ports* los servidores. Por
ejemplo, los servidores del sistema DNS usan el puerto 53. En los sistemas tipo
UNIX, el archivo `/etc/services` lista los puertos reservados para las
aplicaciones comunes tanto para UDP como para TCP.

Un *port mapper*, es una aplicación cliente-servidor que usa un *puerto
conocido* (el 111) y los clientes le requieren el puerto asociado a un
*servicio* (dado como una cadena de caracteres). El servidor *port mapper*
responde con el número de puerto en el que escucha el servicio.

El módulo que implementa UDP principalmente se encarga del *demultiplexado* de
datagramas, como se muestra en la siguiente figura.

![udp-demux](img/udp-multiplexing.png ':size=50% :class=imgcenter
:id=udp-demux')

#### Figura 10.2: Demultiplexado de datagramas UDP.

Cabe aclarar que una aplicación UDP usando el puerto *p* no tiene conflictos con
una aplicación TCP asociada al mismo puerto.

Generalmente, los sistemas operativos permiten asignar un puerto con valor al
1025 a una aplicación (*bind*) si la aplicación corre con privilegios de
*administrador* (*root* en sistemas tipo UNIX).

## TCP

El [Transport Control
Protocol](https://en.wikipedia.org/wiki/Transmission_Control_Protocol) provee un
servicio orientado a conexión, confiable (retransmisión de paquetes faltantes o
con errores), con entrega ordenada de secuencias de bytes (*byte-stream*). 

También ofrece mecanismos de *control de congestión* para prevenir la sobrecarga
de la red.

La aplicación (*peer*) debe primero establecer una *conexión* con el otro
(*peer*) extremo. Una vez que la conexión se estableció, los *peers* pueden
comenzar a intercambiar datos.

Un *peer* emisor envía una secuencia de bytes que son almacenados en *buffers*
por el subsistema TCP, el cual arma *segmentos* a transmitir, como se muestra en
la siguiente figura.

![tcp-segments](img/tcp-segments.png ':size=60% :class=imgcenter')

#### Figura 10.3: Transmisión de segmentos TCP.

Cada segmento tiene un encabezado que incluye los puertos de origen y destino, y
un número de secuencia. Los campos *acknowledgment* y *advertised window* se
usan para *reconocer* los segmentos recibidos y *notificar* al emisor sobre el
tamaño del espacio libre en el buffer del receptor. Estos dos últimos campos
permiten implementar el protocolo de *sliding window (ventana deslizante)* que
***controla el flujo*** entre los extremos. El *window size* indica el número de
bytes que el emisor puede enviar sin esperar por un reconocimiento. Cuando TCP
recibe un *Ack* con *window size=0*, *bloquea* al proceso emisor hasta recibir
otro con *window size > 0*.

![tcp-header](img/tcp-header.png ':size=60% :class=imgcenter :id=tcp-header TCP header')

#### Figura 10.4: Formato de un segmento TCP.

El campo *HdrLen* contiene el tamaño del header en palabras de 32 bits ya que el
header soporta *opciones*, lo que lo hace de tamaño variable. Los *Flags* se
usan para implementar operaciones de control como el establecimento y fin de la
conexión (bits *SYN* y *FIN/RESET*). El flag *ACK* determina que el segmento
contiene un reconocimiento. El flag *URG* determina que el segmento contiene
*datos urgentes*, apuntados por el campo *UrgPtr*. El resto de los campos son
análogos a UDP.

### Establecimiento de conexión

El establecimiento de conexión se describe en la siguiente figura. Se requieren
tres mensajes para asegurar que ambas partes arriben al mismo estado de
conexión.

![tcp-handshake](img/tcp-handshake.png ':size=50% :class=imgcenter')

#### Figura 10.5: Handshake de conexión TCP.

Luego de haberse establecido la conexión los peers pueden intercambiar datos. La
siguiente figura muestra el sistema de transición de estados de TCP.

![tcp-states](img/tcp-states.png ':size=50% :class=imgcenter')

#### Figura 10.6: Sistema de transición de estados TCP.

### Transmisión y recepción

Para garantizar confiabilidad y secuencialidad de los datos transmitidos existen
varios algoritmos/protocolos.

El más simple es ***stop-and-wait***, el cual se basa en el uso de
*reconocimientos (acks)* y *vencimientos (timeouts)*. En este protocolo, el
emisor envía un paquete y *espera* por un cierto intervalo de tiempo por un
*acknowlegment* del receptor para luego enviar el próximo. Si el reconocimiento
no llega dentro del *timeout* establecido, el paquete se reenvía.
Cada paquete incluye un número de secuencia (que puede ser 0 o 1) para
relacionar cada paquete con su *ack*.

Este algoritmo, si bien es simple, está lejos de hacer una buena utilización de
la capacidad del enlace.

TCP utiliza el *algoritmo de sliding window (ventana deslizante)*, el cual
permite lograr una mejor utilización del canal y *control de flujo*.

El algoritmo se basa en el uso de *buffers* de emisión y recepción. El emisor en
cada mensaje envía un número de secuencia `seq n` y mantiene la *ventana* de
bytes transmitidos pero aún no reconocidos, tal como se muestra en la siguiente
figura.

![sliding-window](img/tcp-sliding-window.png ':size=70% :class=imgcenter')

#### Figura 10.7: Buffers del emisor y del receptor.

El campo `AdvertisedWindow` (`aw`) de un segmento representa el número de bytes
que un emisor puede enviar sin esperar por *acks*.

Cuando el emisor  recibe un `ack n,aw`, mueve `LastByteAcked += n` y ahora puede
enviar segmentos hasta `aw` bytes. Cuando envía un nuevo segmento con número de
secuencia *n* se planifica su *timeout* (alarma). Si el *timeout* se dispara
antes de recibir un *ack m* (con $n \leq m \leq n+aw$), se reenvía el paquete.
En otro caso, se cancela la alarma ya que el receptor recibió el segmento. Si el
`aw` recibido es cero, el emisor debe *bloquear* al proceso.

El receptor también mantiene una ventana sobre su buffer de recepción. Si recibe
un segmento con `seq = n + 1`, adelanta `NextByteExpected=n+1`, y eventualmente
*despierta* al proceso receptor. Luego envía al emisor un `ack n+1, rws`, donde
el valor de `rws` será el número de bytes libres en el buffer de recepción. Si
recibe un segmento *fuera de orden*, lo almacena en el buffer en la posición
correspondiente sin responder con un `ack`.

Este algoritmo establece un mecanismo de ***control de flujo*** gobernado por el
receptor lo que previene que el emisor sobrecargue (*overflows*) el buffer de
recepción.

Un factor a considerar es el valor del *timer* de cada segmento en el emisor.
Este debería tener el cuenta el tiempo de ida y vuelta o *round-trip time (RTT)*
de mensajes entre el emisor y el receptor.

Un *timer < RTT* generará retransmisiones aunque los segmentos hayan arribado
efectivamente al receptor pero el *ack* aún estaba en tránsito. Por el
contrario, un valor alto, causará demoras excesivas antes de retransmitir
paquetes perdidos. TCP incluye algoritmos para determinar el *RTT* midiendo las
diferencias entre los tiempos de envío de segmentos y de recepción de *acks*
correspondientes, comúnmente teniendo en cuenta su *varianza*.

TCP usa un *algoritmo adaptativo* que permite encontrar valores adecuados del
tamaño máximo de segmentos a transmitir usando una *ventana de congestión*. Esto
hace que TCP incluya una estrategia de *control de congestión* que se analiza en
el próximo capítulo.

## Protocolos *request/reply*

Estos tipos de protocolos de transporte han ganado mucha atención principalmente
para implementación de *Remote Procedure Calls (RPC)*. La idea de *RPC* se basa
en que una aplicación *invoca* a una operación a ejecutarse en un host remoto al
estilo de invocaciones de una función, pasando valores como argumentos y
recibiendo un resultado. El objetivo de RPC es proveer una abstracción al
programador que haga transparente la ejecución (local o remota) de una función
ocultando los detalles de las comunicaciones.
Esto simplifica el diseño e implementación de aplicaciones distribuidas.

Un ejemplo de un protocolo *reply/request* es *HyperText Transport Protocol
(HTTP)*, usado en la *world wide web*, comúnmente implementado sobre TCP.

Los análisis realizados indican que TCP puede que no sea el ideal para
implementar estos protocolos, por lo que recientemente se han propuesto otros,
específicos para este modelo, entre los que podemos mencionar [Stream Control
Transmission Protocol
(SCTP)](https://en.wikipedia.org/wiki/Stream_Control_Transmission_Protocol) y
[QUIC](https://datatracker.ietf.org/doc/html/draft-ietf-quic-transport-34). 

*QUIC* fue desarrollado por Google y aceptado para su estandarización por la
[IETF](https://www.ietf.org/). Actualmente ya está soportado en algunos
*browsers* como Google Chrome y Mozilla Firefox. Básicamente tiene las
siguientes características:

1. Simplifica el *handshake* de conexión incorporando los parámetros para
   establecer conexiones seguras.
2. Soporta *reconexiones* rápidas en dispositivos con múltiples interfaces de
   red como teléfonos celulares (wifi + 3/4G).
3. Se basa en UDP, o sea que es un protocolo de transporte sobre otro en la
   misma capa de abstracción. Esta técnica se conoce como *tunneling*.

## Transporte en tiempo real

Las aplicaciones multimedia, como VoIP (voz sobre IP), streaming de audio y
video requieren protocolos de transporte que provean servicios como
secuencialidad, *timing*, sincronización entre flujos (ejemplo, audio, video y
subtítulos) y no necesariamente deben garantizar confiabilidad. Por ejemplo, no
tiene sentido retransmitir un paquete de VoIP perdido fuera de secuencia, ya
que el receptor podría escuchar *ecos* del pasado.

El [Real-time Transport Protocol
(RTP)](https://en.wikipedia.org/wiki/Real-time_Transport_Protocol) fue diseñado
para cubrir esta necesidad. Generalmente se usa con el *Real-time Control
Transport Protocol (RCTP)* que permite intercambiar información de control por
un canal alternativo en una sesión o flujo RTP.

Generalmente se utiliza RTP sobre UDP.

![rtp](img/rtp-header.png ':size=50% :class=imgcenter :id=rtp')

#### Figura 10.8: Formato de un frame RTP.

La cabecera de un frame RTP permite soportar secuencialidad e incluye un
*timestamp* para el *timing* de la *reproducción* y sincronización entre flujos.
Un receptor generalmente calcula la diferencia entre *timestamps* para
determinar los tiempos de reproducción de los frames recibidos.

El campo *synchronization source identifier* permite identificar un *flujo* de
datos, por ejemplo, un flujo de audio mp3. Esto permite implementar una *pseudo
conexión* o flujo de paquetes relacionados.

Un flujo puede ser la transmisión de un *contenedor multimedia*, como
[MPEG](https://mpeg.chiariglione.org/), [OGG](https://www.xiph.org/ogg/) u otros
(AVI, QuickTime, ...).

Los *contributing source identifiers* se usan cuando diferentes flujos se
*mezclan* o multiplexan en un único flujo, por ejemplo la voz en diferentes
idiomas en un video. El mixer identifica cada origen para que el
decodificador pueda de-multiplexarlos. El número de *contributing sources* está
dado en el campo `CC` de 4 bits.

El protocolo RCTP permite asociar a cada sesión funciones de control como

1. Información (feedback) sobre el rendimiento de la aplicación. Esto permite
   intercambiar parámetros como por ejemplo, resolución de video y frames por
   segundo dependiendo del rendimiento de la red.
2. Relacionar y sincronizar diferentes flujos del mismo emisor como por ejemplo
   audio y video, ya que valores de RTP como el *timestamp* de cada frame de
   diferentes flujos pueden tener diferentes escalas (*clocks*) y es necesario
   correlacionarlos.
3. Identificación (metadatos) de emisores que las aplicaciones pueden mostrar.

Generalmente, las aplicaciones usan el *port n* para RTP y el *n+1* para RCTP.

Para el acceso a un flujo generalmente se usa el *Real Time Streaming Protocol
(RTSP)* que permite que una aplicación se conecte a un servidor RTSP (por TCP),
usando un URL de la forma `rtsp://domain/resource`, el cual retorna una
descripción del flujo en formato [Session Description Protocol
(SDP)](https://en.wikipedia.org/wiki/Session_Description_Protocol), luego el
cliente requiere el flujo desde el servidor usando RTP.

RTSP permite el control del flujo con mensajes `PLAY`, `PAUSE`, `RECORD`
(upload) y otros.

Estos protocolos son comunes en aplicaciones de streaming, cámaras IP y
circuitos cerrados de TV (CCTV).

Otro protocolo alternativo a RTSP es el [Real Time Messaging Protocol
(RTMP)](https://en.wikipedia.org/wiki/Real-Time_Messaging_Protocol), creado por
Adobe para la transmisión de video en Flash y liberado en 2012.

RTMP usa TCP y permite crear *canales virtuales* independientes, por ejemplo,
para audio, video y mensajes (chat). Existen varias extensiones para proveer
seguridad y transmisión sobre túneles HTTP (RTMPT).

Actualmente es posible ver que Youtube usa RTMP para *live streaming* ofreciendo
un URL como `rtmp://a.rtmp.youtube.com/live2` para recibir una presentación en
vivo usando un *encoder* como [Open Broadcaster Software
(OBS)](https://obsproject.com/) u otros.

Otro protocolo para transmisión en vivo es [HTTP Live Streaming
(HLS)](https://en.wikipedia.org/wiki/HTTP_Live_Streaming), desarrollado por
Apple y liberado en 2009. Aprovecha las facilidades de reproducción de sonido y
video de navegadores HTML5. Codifica video en formato
[H.264](https://en.wikipedia.org/wiki/H.264/MPEG-4_AVC), audio en formatos
[AAC](https://en.wikipedia.org/wiki/Advanced_Audio_Coding),
[MP3](https://en.wikipedia.org/wiki/MP3) y otros. Usa
[MPEG-2](https://en.wikipedia.org/wiki/MPEG_transport_stream) o
[MPEG-4](https://en.wikipedia.org/wiki/MP4) como *contenedores multimedia*. 

El protocolo [Dynamic Adaptive Streaming over HTTP
(DASH)](https://en.wikipedia.org/wiki/Dynamic_Adaptive_Streaming_over_HTTP),
es ampliamente usado actualmente (por Youtube, Netflix y otros). 

La ventaja de los protocolos sobre HTTP es que están soportados por los
*browsers* modernos y se usan como clientes en aplicaciones web de streaming o
video-conferencias.

En [Computer Networks: A System Approach.
RTP](https://book.systemsapproach.org/e2e/rtp.html) se describen en mayor
detalle estos protocolos.