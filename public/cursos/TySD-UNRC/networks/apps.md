# Aplicaciones

Cada aplicación debe definir su propio protocolo que a su vez se basarán en
protocolos de transporte y de sesión. También deberá definir el formato o
presentación de los mensajes. Por último deberá especificar la *coreografía* de
las comunicaciones, que pueden ser simples del tipo *request/reply* o más
complejas dependiendo si se basa en sesiones y otros detalles de la aplicación.

La definición completa de un protocolo de aplicación incluye:

1. Entidades participantes
2. Nombrado de recursos
3. Formato de mensajes
4. Interacción. Generalmente dado por un conjunto de sistemas de transición de
   estados.
5. Protocolos de transporte y otros utilizados

## Aplicaciones de Internet

En Internet las aplicaciones iniciales, descriptas en la RFC 1123 son las
siguientes:

- Login remoto: Telnet y login
- Transferencia de archivos: File Transfer Protocol (FTP y TFTP)
- Correo electrónico: Simple Mail Transfer Protocol (SMTP y POP o IMAP)
- Soporte: Domain Name System (DNS)
- Inicialización de hosts: BOOTP
- Gestión y monitoreo de redes: Simple Network Management Protocol (SNMP)

A continuación se describen las aplicaciones mencionadas y finalmente se
analizan las aplicaciones modernas como servicios web, aplicaciones multimedia y
otras.

## Dominios y DNS

Identificar hosts y routers por medio de direcciones no es cómodo para los seres
humanos, por lo que es necesario nombrar hosts y redes en Internet usando
nombres o identificadores mas legibles y fáciles de recordar.

La idea es particionar Internet en *dominios* y usar una estructura de nombres
jeráquica. Cada dominio define un espacio nombres lógico en que se incluirán
servidores, sitios web, etc. La siguiente figura muestra algunos dominios
usados en Internet.

![domains](img/domains.png ':size=70% :class=imgcenter')

#### Figura 13.1: Espacios de nombres o dominios de Internet.

En el novel superior encontramos los dominios raíz. La idea es que las redes que
pertenezcan a instituciones educativas estén en el dominio `edu`, las
organizaciones sin fines de lucro en `org`, etc. También existen dominios raíz
para cada país, denotados por una abreviatura de dos letras, para definir
dominios geográficos.

La asignación de nombres se realiza por diferentes organizaciones. Los dominios
*top-level* son manejados por la *Internet Corporation for Assigned Names and
Numbers (ICANN)*. Dentro de cada país, los nombres son gestionados por el
*Network Information Center (NIC)* correspondiente.

En la argentina el [nic.ar](https://nic.ar/), depende de la Secretaría Legal y
Técnica de la Presidencia de la Nación y está integrada por el gobierno
nacional, empresas de servicios de Internet y otras organizaciones.

Quien haya obtenido su dominio, tiene la libertad de dividir su propio espacio
de nombres en *subdominios*. Por ejemplo, Wikipedia tiene el dominio
`wikipedia.org` y a su vez ha definido subdominios para diferentes lenguajes
como `en.wikipedia.org` o `es.wikipedia.org`.

Un dominio puede referir a un host o una aplicación o servicio. Por ejemplo,
`www.example.com` refiere a la IP de un host que corre un servicio web (web
site).

El *Domain Name System (DNS)* es un sistema distribuido de soporte formado por
una red de servidores que implementan un protocolo del tipo
*requerimiento/respuesta* cuya función es dado un nombre o dominio, retonar un
valor.

Generalmente el uso común del DNS es hace consultas para obtener la IP de un
nombre asociado a un host o una aplicación.

El espacio de nombres está divido en *zonas*. Una zona debe desplegar al menos
un servidor DNS que pueda responder a los nombres dentro de su zona. Los
dominios *top-level* están manejadas por el ICANN. Luego las diferentes
instituciones u organizaciones manejan los dominios bajo su control, como se
muestra en la siguiente figura.


![domains](img/DNS-zones.png ':size=70% :class=imgcenter')

#### Figura 13.2: Zonas en Internet.

Dentro de cada zona, un servidor DNS define su base de datos local. La base de
datos consta de registros que tienen los siguientes campos:

- *Name*: El nombre del recurso
- *Value*: El valor asociado al nombre
- *Type*: Tipo de registro como `A` (host), `NS` (name server), `MX` (mail
  exchanger) y otros.
- *Class*: En Internet tiene siempre el valor `IN`.
- *Time To Live (TTL)*: Tiempo de validez. Este valor es usado por los
  servidores que almacenan registros *aprendidos* en su caché.

Por ejemplo, el siguiente registro (obtenido con in cliente DNS como `dig
dc.exa.unrc.edu.ar`

```
dc.exa.unrc.edu.ar.	1630	IN	A	200.7.141.44
```

describe un host que es el servidor web del departamento de computación de la
Facultad de Ciencias Exactas de la Universidad Nacional de Río Cuarto, que está
dentro del dominio `edu.ar`.

Ante la consulta `dig dc.exa.unrc.edu.ar MX` (registos del tipo MX), se obtiene 

```
dc.exa.unrc.edu.ar.	3600	IN	MX	20 ALT1.ASPMX.L.GOOGLE.COM.
dc.exa.unrc.edu.ar.	3600	IN	MX	20 ALT2.ASPMX.L.GOOGLE.COM.
...
```

Una aplicación que deba acceder a un cierto host dado su nombre deberá consultar
a su DNS local, el cual resolverá a su IP para que pueda comunicarse.

Recordemos que cada host tiene en su configuración de red la IP de al menos un
servidor de nombres primario. En muchos sistemas tipo UNIX, como GNU-Linux, se
definen en `/etc/resolv.conf`. Es común que también se defina la IP de un DNS
secundario. Este DNS se considera el DNS local para el host, lo cual no
significa que resida en su red local.

La siguiente figura muestra la resolución de un nombre desde un host y describe
la secuencia de mensajes entre los servidores DNS hasta llegar al DNS de la zona
correspondiente, el cual contendrá el valor asociado a la consulta realizada.

![name-resolution](img/name-resolution.png ':size=70% :class=imgcenter')

#### Figura 13.3: Resolución (recursiva) de nombres.

El DNS local luego de obtener el registro, lo almacena en su caché para futuras
consultas.

Esta arquitectura configura un sistema de bases de datos jerárquica distribuida.

En una red local, generalmente el router actúa como un *resolver* DNS que
simplemente *reenvía* las consultas desde el interior de la red local hacia un
DNS del ISP y usa una caché para futuras consultas. Esto previene mayor tráfico
DNS hacia el exterior de la red local.

El protocolo DNS utiliza UDP aunque existen propuestas experiementales basadas
en TCP y usando protocolos seguros.

## Acceso (login) remoto

En los primeros años de Internet una de las primeras aplicaciones desarrolladas
fue un servicio de acceso remoto (a mainframes, principalmente). Accediendo
remotamente a su cuenta de trabajo un usuario podía usar aplicaciones de acceso
a su correo electrónico (en esa época el acceso a los buzones de correo era
local) y otras aplicaciones (talk) en sistemas multiusuario.

Una de las primeras aplicaciones de acceso remoto fue `telnet` (RFC 15 y 854). Esta
aplicación cliente-servidor permite acceder a un servidor remoto, el usuario
presenta sus credenciales y luego interactúa con una sesión de shell.

El protocolo de `telnet` usa TCP y tiene una coreografía muy simple: El servidor
interactúa con un sistema de *login* y luego con un *shell*. El servidor
notifica al cliente de eventos ocurridos enviando *urgent data* en mensajes TCP.
El puerto asignado a *telnet* es el 23.

El cliente es un programa genérico que lee comandos ingresados por el usuario,
envía al servidor, el cual los procesará y retornará una respuesta. Es posible
ver que la sesión es un ciclo de un protocolo *request/reply*. En una terminal o
consola de comandos, generalmente se utiliza de la siguiente manera:

``` sh
telnet <host> [port]
```

Actualmente aún es usado pricipalmente en administración de servidores dentro de
una red segura, ya que no ofrece mecasnimos de confidencialidad (todo se
transmite en texto plano) y para testing de protocolos. Generalmente se ha
reemplazado por *secure shell (ssh)* ya que éste brinda confidencialidad por
medio de SSL/TLS.

Las aplicaciones de acceso remoto modernas permiten sesiones con *shells*
gráficos, tal como en el sistema [X](https://www.x.org/) usado en interfaces
gráficas en los sistemas tipo UNIX. Su arquitectura se muestra en la siguiente
figura.

![X](img/X.png ':size=30% :class=imgcenter')

#### Figura 13.4: El sistema X-Window.

En *X* las aplicaciones gráficas toman el rol de clientes, las cuales
interactúan con el *X-server* por medio de mensajes (TCP en conexiones remotas y
UNIX sockets en conexiones locales). El *X-server* interactúa con el hardware
gráfico (por medio de una interface provista por el sistema operativo) y corre
como una aplicación de usuario.

Se debe notar que el *X-server* corre en la máquina *cliente*, es decir con la que
usuario interactúa.

Los clientes generalmente utilizan una biblioteca gráfica (*Xt*, *GTK*, *KDE*,
etc) cuyas funciones abstraen los detalles de la comunicación. Las funciones
permiten enviar al server comandos de dibujo como puntos, líneas, imágenes y
otras. El X-server envía a los clientes (aplicaciones gráficas) eventos de
mouse, teclado y otros dispositivos de interacción usados.

Cuando se usa *X* en forma remota, generalmente por un *túnel* en una sesión
*ssh*, las aplicaciones gráficas (clientes *X*) corren en el host remoto, el
cual no necesariamente debe tener hardware gráfico.

Algunos protocolos usados ampliamente para control remoto son *Remote Desktop
Protocol (RDP)* de Microsoft y el *Remote Frame Buffer Protocol (RFB)* usado en
VNC y alternativas libres como [KasmVNC](https://www.kasmweb.com/kasmvnc.html).

Actualmente es común utilizar servicios de acceso remoto como *TeamViewer* o
similares. Algunos permiten control vía Internet utilizando HTTP como protocolo
de transporte.

La conexión entre el cliente y el servidor (la máquina bajo control) puede ser
directa si la conexión lo permite. Muchas veces no es posible cuando un host
está detrás de routers que realizan NAT o firewalls. 
En este último caso ambos extremos se conectan a un servidor que hace de *relay*
en ambas direcciones.

## Correo electrónico

El correo electrónico es un servicio de mensajería aún muy utilizado.
Inicialmente sólo soportaba mensajes de texto. En la actualidad los mensajes
pueden ser *multiparte* donde cada parte define su formato. Los formatos pueden
ser textos con diferentes codificaciones y anexos con contenidos de archivos
binarios, mutimedia, etc. Estas extensiones se conocen como
[Multipurpose Internet Mail Extentions
(MIME)](https://en.wikipedia.org/wiki/MIME).

La arquitectura del sistema consiste en una red de servidores (*Mail Transport
Agents (MTA)*). 
Cada dominio generalmente tiene un servidor de correo electrónico en el cual se
registran los usuarios. La identidad de un usuario (naming) se denota de la forma
`user@domain`. Cualquier usuario puede enviar mensajes a cualquier
otro en Internet. 

Los usuarios normalmente utilizan una aplicación cliente o *Mail User Agent
(MUA)* como *Mozilla Thunderbird*, *MS-Outlook*, *clientes web* como GMail u
otros. El cliente se conecta a un servidor *Simple Mail Transport Protocol
(SMTP)* para el envío de nuevos mensajes y a un servicio de *acceso al buzón*
basados comúnmente en *Internet Access Message Protocolo (IMAP)* o *Post Office
Protocol (POP/POP3)*. Estos últimos dos protocolos permiten al usuario gestionar
los mensajes enviados y recibidos como clasificarlos en diferentes *mailboxes* y
definir su *estado* como leídos/no leídos y otros.

Los mensajes inicialmente almacenados en el servidor, pueden moverse o copiarse
al cliente, según la configuración del usuario.

SMTP (port 25) es orientado a conexión y actualmente es común el uso de su
versión extendida *ESMTP* o *SMPTS* (port 587) que permite comunicaciones
seguras sobre TLS y alternativas de autenticación. 

Para el envío de mensajes desde el cliente al servidor (*mail submission agent o
MSA*) también se usa SMTP generalmente en una sesión autenticada. El MSA
re-envía el mensaje al MTA local para que lo entregue al servidor destino (en
una sesión SMTP no autenticada), el cual a su vez lo entregará al MSA en el
servidor del destinatario, como se muestra en la siguiente figura:

![email](img/Email.svg ':size=50% :class=imgcenter')

#### Figura 13.5: Arquitectura de Internet Email.

Generalmente las instancias del MSA y MTA se implementan en el mismo código del
servidor y son simplemente diferentes instancias del servidor SMTP lanzadas con
diferentes opciones. La instancia del MSA generalmente requiere una sesión con
autenticación y generalmente escucha en el puerto 587, mientras que el servicio
SMTP de los MTAs usan el puerto 25.

Como se puede apreciar en la figura, el MTA de origen obtiene la dirección IP
del servidor de destino consultando por registros MX al sistema DNS. En el caso
que no pueda conectarse en ese momento el servidor realizará varios reintentos.

En caso de falla de un envío ya sea porque no es posible contactar al servidor
del dominio de destino o porque éste retorna un error al emisor indicando que el
usuario no existe en su dominio, el sistema envía un mensaje al emisor
notificando del error.

El formato de un mensaje (ver RFCs 5321 y 5322) consiste en una *cabecera* y su
*cuerpo* o contenido. La cabecera mínimamente incluye las direcciones de emisor
y receptores, fecha y hora del envío y el título o *subject*. Cada servidor SMTP
por los cuales pasó el mensaje, generalmente incluye una línea de cabecera de la
forma 

```
Received: from us11-010mrr.dh.atmailcloud.com ([10.10.5.20])
	by us11-011ms.dh.atmailcloud.com with esmtp (Exim 4.90_1)
	(envelope-from <someuser@example.atmailcloud.com>)
	id 1gYlz6-0005Df-Hx
	for someuser@example.atmailcloud.com; Mon, 17 Dec 2018 16:02:32 +1000
```

Un mensaje puede pasar por varios servidores porque es común que en un dominio
los usuarios se particionen en grupos o subdominios manejados por varios
servidores y un mensaje deba seguir una *ruta* hasta su destino. 

La cabecera se separa del cuerpo por una línea en blanco.

A continuación se muestra un ejemplo de una sesión SMTP entre el cliente y un
servidor.

```
C: telnet smtp.example.com 587
S: 220 smtp.example.com ESMTP Postfix
C: HELO relay.example.org
S: 250 Hello relay.example.org, I am glad to meet you
C: MAIL FROM:<bob@example.org>
S: 250 Ok
C: RCPT TO:<alice@example.com>
S: 250 Ok
C: RCPT TO:<theboss@example.com>
S: 250 Ok
C: DATA
S: 354 End data with <CR><LF>.<CR><LF>
C: From: "Bob Example" <bob@example.org>
C: To: "Alice Example" <alice@example.com>
C: Cc: theboss@example.com
C: Date: Tue, 15 Jan 2008 16:02:43 -0500
C: Subject: Test message
C:
C: Hello Alice.
C: This is a test message with 5 header fields and 4 lines in the message body.
C: Your friend,
C: Bob
C: .
S: 250 Ok: queued as 12345
C: QUIT
S: 221 Bye
{The server closes the connection}
```

El servidor inicia la sesión con un mensaje de bienvenida al cual el cliente
debe responder con el comando `HELO` (`EHLO` para indicar que desea usar ESMTP).
Luego El cliente envía mensajes `MAIL FROM` para establecer el usuario emisor y
`RCPT TO` para cada destinatario. Ante cada comando, el servidor responde con un
código de estado o resultado.

El comando `DATA` indica el comienzo del mensaje compuesto de la cabecera y del
cuerpo. Una línea conteniendo un punto (caracter `.`) indica el final de la
entrada del mensaje ante lo cual el servidor responde con el identificador
asignado. En la sesión es posible continuar interactuando con el servidor SMTP
hasta que el cliente envíe el comando `QUIT`, momento en que el servidor cierra
la conexión TCP.

Algunos de los servidores SMTP con licencias de software libre mas utilizados son
[sendmail](https://www.proofpoint.com/us/products/email-protection/open-source-email-solution) y [postfix](http://www.postfix.org/).

### Acceso a *mailboxes*

Los protocolos de acceso y gestión de *mailboxes* como IMAP o POP3 también
tienen una arquitectura cliente-servidor. Se aconseja asociar diferentes
nombres a cada servicio como `smtp.example.com` e `imap.example.com` aunque
ambos servicios estén ejecutándose en el mismo servidor.

Actualmente el protocolo IMAP se ha constiuido como el protocolo de acceso
preferido por sobre POP3 ya que es mas flexible y permite, por ejemplo
transferir sólo las cabeceras de mensajes para que el usuario decida si
recuperar el cuerpo, reduciendo así el volumen de transferencia.

Tanto IMAP como POP utilizan TCP. Los puertos asignados para POP3 son el 110 y
el 995 en forma segura y 143 o 993, respectivamente para IMAP. Ambos se basan en
comandos provistos por el cliente. Los comandos mas relevantes de IMAP se listan
en la siguiente tabla:

| Comando            | Descripción                              |
| ------------------ | ---------------------------------------- |
| `SELECT <mb>`      | Selecciona el *mailbox* `mb`             |
| `CREATE <mb>`      | Crea un nuevo *mailbox*                  |
| `DELETE <mb>`      | Borra un *mailbox*                       |
| `LIST <mb> <f>`    | Lista los mensajes usando el filtro `f`  |
| `STATUS`           | Muestra el estado de los mensajes        |
| `FETCH`            | Lista los mensajes y su estado           |
| `SET <flags>`      | Setea flags (no/leído, ...)              |
| otros              | Mover, borrar mensajes, ...              |

Uno de los servidores IMAP (software libre) ampliamente utilizados es
[dovecot](https://www.dovecot.org/).

## Transferencia de archivos

Unas de las primeras aplicaciones para transferencias de archivos fue el *File
Transfer Protocol (FTP)* y tiene una arquitectura cliente-servidor sobre TCP.
Una vez lograda la conexión (por ejemplo, mediante el comando `ftp
ftp.example.com` o un cliente gráfico) generalmente el servidor requiere la
auntenticación por medio de usuario y contraseña.
Muchos servidores proveen *ftp anónimo* mediante el usuario *anonymous*.

Luego el cliente puede realizar comandos como listar (`list`) el directorio
actual (inicialmente, po lo general la carpeta *home*), cambiar de directorio
(`cd`), borrar, renombrar archivos o descargar (`get`) y subir (`put`) archivos.

En un comando `get` o `put` FTP abre una conexión de datos desde el servidor al
cliente para realizar la transferencia. Como eso puede ser inconveniente para
clientes detrás de servicios de NAT o firewalls, se puede hacer conexiones
*pasivas* mediante el comando `PASV`, es decir desde el cliente al servidor.
Para esto, previamente el cliente envía un mensaje solicitando al servidor la IP
y puerto para establecer la conexión desde el cliente.

Existen otros protocolos para la transferencia de archivos. Actualmente se
utiliza ampliamente HTTP, el protocolo de transporte de la web, el cual es mas
simple y usa una única conexión TCP. A diferencia de FTP, HTTP es sin estado, es
decir no existe el concepto de directorio corriente y otros detalles. Los
archivos se acceden mediante URLs que el usuario debe conocer.

Recientement los navegadores web han dejado de soportar el protocolo ftp.

*Secure shell (ssh)* también incluye un protocolo de transferencia segura. Ver
el comando `scp` (secure copy).

## WWW

La web se ha convertido en una de las aplicaciones de mayor uso en Internet. Los
sitios web han pasado de ser conjuntos de documentos estáticos a aplicaciones
que permiten generar contenido dinámico y dan posibilidades de interacción a los
usuarios, mediante la creación o actualización de contenidos o mediante
comentarios integrados con otras aplicaciones de mensajería o redes sociales.

La web fue desarrollada por [Tim
Barners-Lee](https://en.wikipedia.org/wiki/Tim_Berners-Lee) con el objetivo de
formar una red de documentos de interés científicos inter-relacionados. Un
documento puede contener enlaces a otros documentos en otro sitio.

La idea es simple: Los usuarios utilizan una aplicación cliente, como un *navegador
(browser) web* u otra aplicación que envía requerimientos a servidores web los
cuales responden con contenidos como documentos HTML, objetos multimedia u otros
datos, los cuales son procesados por el cliente.

Como un documento puede contener referencias a otros objetos, posiblemente en
otros sitios, el navegador realiza esos requerimientos adicionales
automáticamente hasta obtener todos los objetos necesarios.

Inicialmente los navegadores usaban una interfaz de texto. El primer navegador
gŕafico fue [Mosaic](https://en.wikipedia.org/wiki/Mosaic_(web_browser)),
desarrollado por NCSA. Luego este proyecto derivó en la creación de *Netscape
Communications*, que lanzó el *Netscape Navigator*, que a su vez es el predeceso
del *Mozilla Firefox*. El código de *Internet Explorer (Microsoft)* también fue
un derivado inicial de Mosaic. Actualemente existen varios navegadores y los más
utilizados son Firefox (software libre) y Google Chrome.

El primer servidor web fue el *httpd* desarrollado en el
[CERN](https://en.wikipedia.org/wiki/CERN) en 1991. Actualmente existen varias
alternativas. Los más usados son productos con licencias libres como
[Apache](https://httpd.apache.org/) y [NGINX](https://www.nginx.com/).

El formato de los documentos, el [HyperText Markup Language
(HTML)](https://www.w3schools.com/html/), se basa en XML y ha evolucionado
(actualmente, HTML5) para contener objetos multimedia como audio y video
permitiendo construir documentos complejos e interactivos.

La interactividad y contenido dinámico se logra gracias a que un documento HTML
permite incluir programas (scripts) en lenguajes de programación como Javascript
que permiten manipular el documento, realizar comunicaciones, almacenar
información localmente (en almacenamiento gestionado por el navegador web),
validar datos de formularios y otras funciones.

La apariencia de los elementos en un documento se describe por medio del
lenguaje [Cascade Style Sheets (CSS)](https://www.w3schools.com/css/) que
contienen reglas de formato aplicables a las diferentes partes de un documento.

El protocolo de transporte para el acceso a recursos, como documentos o páginas
web, imágenes u otros datos es el [HyperText Transport Protocol
(HTTP)](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol). 

El protocolo es del tipo cliente/servidor y aunque generalmente opera sobre TCP
es sin estado.

![http](img/http_diagram.png ':size=50% :class=imgcenter')

#### Figura 13.6: Requerimiento/respuesta HTTP.

Este protocolo evolucionó desde HTTP/1.0 descripto en la [RFC
1945](https://datatracker.ietf.org/doc/html/rfc1945) en 1996, pasando por HTTP
1.1 ([RFC 2616](https://www.ietf.org/rfc/rfc2616.txt)) en 1999 hasta
[HTTP/2](https://datatracker.ietf.org/doc/html/rfc7540) propuesto por Mozilla en
2015 que permite conexiones sobre TCP seguras usando TLS. HTTP/3, propuesto en
2020, aún se encuentra en estado *borrador (draft)*. Se basa en el uso de
[QUIC](https://en.wikipedia.org/wiki/QUIC), el cual se analizó en el capítulo de
protocolos de transporte y actualmente ya está soportado por algunos navegadores
web y usado opcionalmente en algunas aplicaciones web como los servicios de
Google.

Los mensajes se representan en texto y como se puede apreciar en la figura de
arriba y tienen las siguientes partes:

1. El comando (`GET`, `POST`, `PUT`, `DELETE`, ...) en un requerimiento o la
   línea de estado en una respuesta (por ejemplo: `200 OK`, ...)
2. Una secuencia de líneas de encabezados o *headers* de la forma `header: value`
3. Una línea en blanco
4. El cuerpo o contenido opcional. 
   En el caso de las respuestas puede ser el texto HTML o datos multimedia
   representado en alguna codificación como base64 o directamente en binario.
   En un requerimiento `POST` generalmente van los datos ingresados por el
   usuario  en un formulario web.

Cada recurso se identifica por su [Uniform Resource
Identifier](https://en.wikipedia.org/wiki/Uniform_Resource_Identifier) que
tiene el siguiente formato: 

`URI = scheme:[//authority]path[?query][#fragment]`

Un *Uniform Resource Locator (URL)* es un URI en el cual se indica su método de
acceso, como por ejemplo su localización en la red, como por ejemplo:
`https://mysite.com/mypicture.png`. Un *Uniform Resource Name (URN)* identifica
un recurso por su nombre o identificador. Por ejemplo, el URN
`urn:isbn:0-486-27557-4` identifica una edición específica del libro *Romeo y
Julieta*.

### Web APIs

Para el desarrollo de aplicaciones web se desarrollan APIs básicamente sobre el lenguaje de programación Javascript, ampliamente aceptado como lenguaje de scripting en navegadores web y también para la implementación de servicios generalmente sobre plataformas del tipo [nodejs](https://nodejs.org/ "Intérprete y APIs multiplataforma").

Algunas APIs soportadas por los navegadores modernos son las siguientes:

- *Document Object Model (DOM)*: Manipulación de la representación del documento HTML
- *Fetch*: Requerimientos a servidores y recepción de respuestas
- *Canvas*: Generación de gráficos
- *Geolocalización*: Permite descubir la ubicación del dispositivo
- 

## Monitoreo y control

La administración de redes de tamaño considerable requieren herramientas de
control y monitoreo.

Uno de los primeros protocolos desarrollados para este fin fue el *Simple
Network Management Protocol (SNMP)*. Se basa en que cada nodo de importancia
como un servidor o un router ejecuta el servicio el cual responde a consultas
sobre su estado por parte de un cliente usado por el administrador de la red.

SNMP está implementado sobre UDP y permite que el administrador defina
*variables de estado* o *Management Information Bases* en la cual se pueden
asignar y consultar sus valores por medio de comandos `GET` y `PUT`. 

Estas variables se actualizan mediante un *SNMP agent* que monitorea el
dispositivo y actualiza su estado en el *SNMP server*, el cual puede ser
consultado por sus clientes.

Otro sistema de control y monitoreo muy usado actualmente es
[OpenConfig](https://www.openconfig.net/). Se basa en que los fabricantes de
dispositivos especifican los *modelos* de datos, como un conjunto de variables y
sus significados. Estos modelos se especifican en el lenguaje
[YANG](http://www.yang-central.org/). Los dispositivos transmiten su estado
(telemetría) por medio de  *Network Configuration Protocol (NETCONF)* o *gNMI
(gRPC Network Managemen Interface)*, lo cual permite implementar RPC sobre HTTP.

Lo atractivo de OpenConfig es que es posible el monitoreo y control de la red
con prácticamente *cero configuración*.

## Aplicaciones multimedia

Las aplicaciones multimedia como telefonía IP (VoIP), chats y video-conferencias
requieren de otros servicios como localización y estado de los usuarios,
establecimientos y cierres de sesiones, como por ejemplo en una video-llamada y
negociación de formatos multimedia y otros parámetros entre los extremos.

El [Session Initiation Protocol
(SIP)](https://en.wikipedia.org/wiki/Session_Initiation_Protocol) es un
protocolo de *señalización (signaling)* usado para inicio de sesiones
generalmente multicast. Usa mensajes en formato de texto y se usa principalmente
para implementar las operaciones de inicio y finalización de llamadas de audio
y/o video como se muestra en la siguiente figura.

![http](img/SIP-call.jpg ':size=70% :class=imgcenter')

#### Figura 13.7: Llamada con SIP.

El usuario utiliza un *SIP phone*, una aplicación o dispositivo de comunicación
de voz/video. Cada dispositivo se conecta a un *proxy* y se *registra* en un
servicio que provee localización de los dispositivos, dando soporte a
disposiivos móviles. Generalmente cada dominio establece uno o más *proxies* de registro de los usuarios.

En una llamada, SIP realiza los siguientes pasos:
  
1. Envía un mensaje `INVITE` al proxy con el dominio de destino.
2. El proxy de destino solicita la *localización* del dispositivo al servidor de localización correspondiente al dominio.
3. Reenvía el mensaje `INVITE` al destino. Esto hace que el dispositivo *suene*.
4. Si el receptor acepta la llamada, comienza la comunicación multimedia.
5. Cualquier extremo puede finalizar la llamada (mensaje `BYE`).

La comunicación multimedia generalmente incluye la transmisión de un descriptor
del medio, generalmente usando el [Session Description Protocol
(SDP)](https://en.wikipedia.org/wiki/Session_Description_Protocol) el cual,
entre otras cosas, indica el protocolo de transporte de tiempo real a utilizar,
como por ejemplo RTP.

```
v=0
o=jdoe 2890844526 2890842807 IN IP4 10.47.16.5
s=SDP Seminar
i=A Seminar on the session description protocol
u=http://www.example.com/seminars/sdp.pdf
e=j.doe@example.com (Jane Doe)
c=IN IP4 224.2.17.12/127
t=2873397496 2873404696
a=recvonly
m=audio 49170 RTP/AVP 0
m=video 51372 RTP/AVP 99
a=rtpmap:99 h263-1998/90000
```

En [multimedia
Applications](https://book.systemsapproach.org/applications/multimedia.html#session-control-and-call-control-sdp-sip-h-323)
del libro en línea *Computer Networks: A System Approach* se describe en mayor
detalles los protocolos SIP y estándares recomendados por la ITU como H.323 y H.225. 

### WebRTC

[Web Real Time
Communication](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API)
permite realizar comunicaciones en tiempo real peer-to-peer, es decir, browser a browser. Estas APIs son comúnmente usadas en aplicaciones web de video-conferencia o video/voice/chat.

WebRTC define varias APIs Web:

- Captura de *streams* desde dispostivos multimedia (cámara, micrófono, captura
  de pantalla). 
- Permite intercambiar datos como texto y streams multimedia.
- Los navegadores web modernos no requieren *plugins* adicionales.

Uno de los principales problemas a resolver es la conectividad entre browsers en máquinas con IP privadas que están detrás de routers o firewalls que hacen NAT (en particular masquerading).

Para lograr de encontrar una forma de interconectar los peers se usa
[Interactive Connectivity Establishment (ICE)](https://developer.mozilla.org/en-US/docs/Glossary/ICE), el cual usa generalmente *STUN servers*. 

[STUN](https://developer.mozilla.org/en-US/docs/Glossary/STUN) es un protocolo que permite descubrir el par *IP:PORT* de un peer detrás de un *NAT router*.

ICE recolecta esa información conocida como *ICE candidates*.

En el caso que no se pueda lograr conectividad directa entre los peers se puede usar un [TURN relay server](https://developer.mozilla.org/en-US/docs/Glossary/TURN), el cual *retransmite* los datos entre los peers.

![http](img/webRTC.png ':size=60% :class=imgcenter')

#### Figura 13.8: Comunicaciones peer-to-peer con WebRTC.

Cada peer deberá comunicar a los demás la forma de alcanzarlo (*ICE candidates*) y además intercambiar los parámetros de la sesión usando mensajes de [Session Description Protocol (SDP)](https://developer.mozilla.org/en-US/docs/Glossary/SDP). Estos mensajes incluyen información sobre los medios (flujos) a transmitir, como video, audio o datos, su codificación, formatos, compresión, etc.

Un peer generalmente iniciará la sesión (o llamada), mediante el envío de un mensaje SDP *offer*, que incluye los parámetros de la comunicación, a los demás peers, los cuales responderán con un mensaje SDP de tipo *answer* (incluyendo también sus parámetros).

Además cada peer deberá enviar los *ICE candidates* a los demás, para que puedan conectarse entre sí.

Este intercambio de mensajes se debe hacer bajo un *protocolo de inicio de sesión (SIP)*, conocido también como un *signal channel* o *signaling service*.

Este servicio no está especificado en WebRTC y generalmente se realiza mediante un servidor simple de intercambio de mensajes usando [websockets](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API) o cualquier otro protocolo de comunicación.

Este servidor simplemente tiene como objetivo *reenviar* un mensaje de un peer a los demás. No necesita realizar ningún análisis de los mensajes.

Finalmente, los peers pueden intercambiar los distintos flujos de datos usando RTP u otros protocolos de transporte de tiempo real, según lo descripto en los mensajes *SDP*.

En [WebRTC samples](https://webrtc.github.io/samples/) se pueden encontrar
ejemplos para experimentar y analizar su código.

## Servicios distribuidos

Actualmente muchas empresas basan su modelo de negocios ofreciendo parte de sus
recursos de computación, almacenamiento e infraestrucuras de software a
clientes.

Esto tiene la ventaja que muchas empresas pequeñas o medianas no necesitan
desplegar instalaciones importantes de hardware para desplegar sus aplicaciones.

Además es posible implementar servicios complejos como geolocalización (mapas),
seguimiento de mercadería (tracking) y otros utilizando servicios web de
terceros.

> Un ***web service*** es una aplicación que usa HTTP y eventualmente
> tecnologías de RPC como SOAP o REST con una API definida. A su vez un servicio
> web puede utilizar otros. Muchas aplicaciones web modernas ofrecen una API
> para acceder a sus servicios como [Google Cloud](https://cloud.google.com/) y
> [AWS](https://aws.amazon.com). Los servicios pueden ser variados, desde
> información de clima hasta servicios de computación o despliegue de software
> en la nube.

Con la aparición de la web, surgió el negocio de *hosting* de sitios y
aplicaciones web. Estas aplicaciones corren en servidores (generalmente
virtuales) ofrecidas por el proveedor de hosting.

Los avances en las tecnologías de virtualización y los *containers* permitieron
que se ofrezcan plataformas de hardware y software de base y frameworks para el
desarrollo de diferentes tipos de aplicaciones, incluyendo aquellas que
requieren alto poder de cómputo. Esto derivó en lo que se conoce como
*computación en la nube*.

> El término [cloud computing](https://en.wikipedia.org/wiki/Cloud_computing) se
> refiere a servicios de computación y almacenamiento ejecutando en *data
> centers* distribuidos mundialmente. Los usuarios *alquilan* poder de cómputo
> en máquinas virtuales y/o contenedores y junto con software de base y
> frameworks necesario para el desarrollos de sus aplicaciones permitiendo su
> despliegue en una plataforma totalmente transparente en cuanto a su ubicación
> geográfica.
> 
> Los usuarios pueden contratar mas recursos a medida que los necesiten.

### Overlay networks

Estos servicios y otras aplicaciones distribuidas, como las redes sociales,
generalmente desarrollan *overlay networks*, las cuales forman una red de
*data centers* interconectados sobre Internet, generalmente formando sus propios
sistemas autónomos.

Un ejemplo de esto son las *redes de distribución de contenidos (CDNs)* que
ofrecen servicios de almancenamiento de recursos generalmente ampliamente
usados por otros servicios, como aplicaciones web basadas en frameworks de
Javascript usadas en aplicaciones web, imágenes y videos.  Cada *data center*
contiene los recursos *replicados* y actualizados con alguna política de
*caché*. 

Una aplicación cliente (como un web browser) al acceder a un recurso mediante
su URL a una CDN, en realidad se conecta generalmente con un *redirector*, el
cual le dará información de conexión al servidor mas conveniente, ya sea por
cercanía o disponibilidad. Estos *redirectores* generalmente se basan en
servidores DNS que realizan *balance de carga*.

Generalmente utilizan un conjunto de ISPs a nivel mundial que les permiten
interconectar los diferentes data centers de manera efectiva, formando una red
de un servicio especializado *sobre* Internet.

Servicios como los de Google, Youtube y Netflix operan sobre este tipo de redes.
