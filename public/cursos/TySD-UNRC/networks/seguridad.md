# Seguridad

Los protocolos analizados hasta ahora no contemplan mecanismos de seguridad.
Internet es una red pública y no se debería confiar en ninguno de los routers o
extremos en una comunicación crítica.

Actualmente es muy común las transacciones bancarias u operaciones de compras y
ventas en Internet, lo que requiere que se garanticen la *autenticación* de las
partes y la *confidencialidad* de los datos como por ejemplo los de una tarjeta
de crédito. Además hay que tener en cuenta ciertos tipos de ataques como el *man
in the middle* en el cual el atacante puede interceptar y modificar o reemplazar
los mensajes entre los extremos, por lo cual es necesario proveer *integridad*
de los mensajes: Un mecanismo que permite determinar que un mensaje no ha sido
alterado en el camino.

También existen otros ataques conocidos como *denial of service (DoS)* que
consisten en *bombardear* a un servidor con mensajes que lo sobrecargan,
impidiéndole procesar y responder a los mensajes lícitos de los clientes. Esta
caractersítica que se debe preservar se conoce como *disponibilidad*.

En este capítulo se describen los problemas de seguridad y las técnicas,
mecanismos y protocolos para sus soluciones.

Si bien la seguridad en redes normalmente se basa en el uso de protocolos
seguros y mecanismos auxiliares también hay que tener en cuenta otros aspectos,
principalmente en el desarrollo de aplicaciones, como las políticas y mecanismos
de *control de acceso*, para evitar acceso a recursos por usuarios no
autorizados.

Hay varios modelos de control de acceso:

- *Discrecionales*: Los usuarios determinan quién y cómo pueden acceder a sus
  recursos. Un ejemplo de este modelo son las *listas de control de acceso*
  usados en los sistemas de archivos de los sistemas operativos.
- *Obligatorios*: El acceso a los recursos es definido por el *administrador de
  seguridad* y es el modelo comúnmente usado en bases de datos.

## Criptografía

La criptografía es uno de los mecanismos principales para asegurar propiedades
como autenticación, confidencialidad e integridad.

Asumiendo que las comunicaciones se realizan sobre una red pública y por lo
tanto un atacante puede *observar secretamente (eavesdropping)* los mensajes, es
necesario *cifrar* los mensajes para garantizar la confidencialidad.

Un sistema de cifrado es una tupla $(P,C,E,D,K)$ donde

- $P$ es el conjunto de los *textos planos*
- $C$ es el conjunto de los *textos cifrados*
- $E: P \rightarrow C$ es la *función de cifrado*
- $D: C \rightarrow P$ es la *función de descifrado*
- $K$ es el conjunto de *claves*

Un sistema *simétrico* usa la misma clave (*secreta*) para el cifrado y
descifrado.
Algunos de los algoritmos más usados son
[3-DES](https://en.wikipedia.org/wiki/Triple_DES) y [American Encription
Starndard (AES)](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard).
AES soporta claves de 128, 192 y 256 bits y opera sobre bloques de 128 bits.

Estos algoritmos aplican varias etapas o *rondas* de operaciones de
*sustitución* (reemplazo de bits por otros) y *transposición* (cambios de
posición y desplazamientos) sobre los bits de datos y la clave (o valores
derivados). Una función de *cifrado* de $n$ rondas es de la forma:

$$E(P,K)=f_n(\ldots f_2(f_1(P,K), K_2), K_n)$$

donde $P$ es el dato de entrada o *texto plano* y $C$ es el *texto cifrado* y
cada $f_i(p_i,k_i) \rightarrow c_{i}$, $1 \leq i \leq n$, es decir, genera un
texto cifrado y toma una *subclave* ($k_i$) generada desde el par $(c_{i-1},K)$ o
$(c_{i-1},k_{i-1})$.

Generalmente los algoritmos están diseñados para que al aplicar las rondas en
orden inverso se obtiene la función inversa, es decir la de *descifrado*.

$$D(C,K)=f_1(\ldots f_{n-1}(f_n(C,K),K_{n-1}), K_1))$$

Los algoritmos que operan sobre bloques tienen el problema que bloques idénticos
generan la misma salida, habilitando técnicas de análisis diferencial y otros que
permitirían inferir las claves. Por este motivo, generalmente se extienden para
operar en forma dependiente del contexto, llamados *modos de operación*. Un modo
comúnmente usado es *cipher block chain (CBC)* en el que a cada bloque de texto
plano se aplica la operación *XOR* con el bloque cifrado previo. Al primer
bloque se le aplica el XOR con un valor *aleatorio*, conocido como el *vector de
inicialización*, el cual se transmite con el texto cifrado para que el receptor
pueda aplicar el algoritmo de descifrado. Otro modo de operación usado, el
*counter mode* se basa en que se agrega como entrada a la función de cifrado un
*contador* o *nounce* (un valor usado por única vez).

Uno de los principales problemas con los sistemas de criptografía simétrica es
que ambos participantes tienen que compartir la clave secreta, lo que genera el
problema de distribución de claves. Una solución propuesta por W. Diffie y M.
Hellman, conocido como el protocolo de *intercambio de claves Diffie-Hellman*,
el cual permite calcular el mismo secreto a partir de ciertos datos públicos.

El protocolo *Diffie-Hellman* toma dos parámetros, un número primo *p* y un
generador *g*, el cual debe ser una *raíz primitiva de p*, es decir que para
todo *n* entre 1 y *p-1* debe existir *k* tal que $n = g^k \: mod \: p$.

El protocolo se consta de los siguientes pasos:

1. *Alice* y *Bob* acuerdan usar $p=5$ y $g=2$.
2. *Alice* envía a *Bob* $A = g^a \: mod \: p$ donde $a \in [1..p-1]$ es un
   número aleatorio generado por *Alice*. 
   Por ejemplo, con $a=3$, $A = 2^3 \: mod \: 5 = 3$. 
3. *Bob* hace lo propio, por ejemplo, generando $b=4$, envía a *Alice* 
   $B = 2^4 \: mod \: 5 = 1$.
4. *Alice* computa $s=B^a \: mod \: 5 = 1$.
5. *Bob* computa $s=A^b \: mod \: 5 = 1$.

Así *Alice* y *Bob* han computado *s=1* como su *clave secreta*. La fortaleza
del algoritmo se basa en que un atacante, quien conoce *p*, *g*, *A* y *B*, para
calcular *s* debe conocer *a* o *b* para lo cual deberá calcular un [logaritmo
discreto](https://en.wikipedia.org/wiki/Discrete_logarithm) para el cual no se
conoce un algoritmo eficiente (problema *NP*).

Esto sentó las bases de la *criptografía asimétrica*. En *Diffie-Hellman* es
posible ver el par *(p,g)* como la *clave pública* y *a* y *b* como las *claves
privadas* (secretas) de *Alice* y *Bob*, respectivamente. Lo interesante de este
esquema es que las claves secretas o privadas no necesitan distribuirse.

Se debe notar que este protocolo no autentica a las partes por lo que es
vulnerable a un ataque *man-in-the-middle* (el atacante captura y reenvía los
mensajes, haciéndose pasar por *Alice* y *Bob*).

Algunos de los algoritmos de cifrado/descifrado de criptografía asimétrica
ampliamente usados son [RSA](https://en.wikipedia.org/wiki/RSA_(cryptosystem)) y
[El-Gamal](https://en.wikipedia.org/wiki/ElGamal_encryption). 

> RSA:
> 1. Seleccionar dos números primos. Sean $p$ y $q$. Sea $n=p \times q$
> 2. Obtener el *exponente* $1 < e < \Theta(n)$, *coprimo* a $\Theta(n)$, donde
>    $\Theta(n)=(p-1)\times(q-1)$
> 3. La clave pública es el par $(n,e)$
> 4. Obtener $d=(k \times \Theta(n) + 1) / e$
> 5. Función de cifrado con clave pública: $E(d,(e,n))=d^e \: mod \: n=c$
> 6. Función de descifrado (con la clave privada): $D(c,(d,n))=c^d \: mod \: n$

La seguridad de RSA se basa en que para obtener el secreto $d$ es necesario
conocer $p$ y $q$, para lo cual hay que *factorizar* $n$, lo cual se considera
un problema computacionalmente muy complejo para valores grandes.

Actualmente se requiere claves de al menos 2048 bits para ser lo suficiente
robusta a ataques con el poder de cómputo actual, mientras que El-Gamal se basa
al igual que Diffie-Hellman en el problema de los logaritmos discretos.

La cripografía asimétrica tiene las siguientes propiedades:

Si $E(P,S_{sk}) = C$ entonces $D(C,S_{pk})=P$ y si $E(P,S_{pk}) = C'$ entonces
$D(C',S_{sk})=P$, donde $S_{pk}$ y $S_{sk}$ son las claves públicas y privadas
del sujeto $S$, respectivamente.

Estos sistemas criptográficos incluyen algoritmos para la generación de las
claves públicas y privadas que aseguran sus relaciones matemáticas requeridas.
La computación de cifrado y descifrado es mucho más costosa que los usados en
criptografía simétrica por lo que generalmente son usados para autenticación y
generación e intercambio de *claves de sesión* temporarias para luego usar algún
algoritmo simétrico, los cuales son muy eficientes, para cifrar y descifrar
mensajes.

### Hashes criptográficos

Un *hash criptográfico*, como [MD5](https://en.wikipedia.org/wiki/MD5) o
[SHA](https://qvault.io/cryptography/how-sha-2-works-step-by-step-sha-256/), es
una función que oman una entrada de longitud arbitraria y genera una salida de
longitud fija con las siguientes propiedades:

1. La función distribuye uniformemente sobre su imagen.
2. El algoritmo $h(x)$ es eficiente (generalmente lineal).
3. Dado $d$, es computacionalmente impráctico encontrar $x$ tal que $h(x)=d$.
4. Dados $x$ y $d$ es muy difícil encontrar $y \neq x$ tal que $h(y)=d$.

Su uso fundamentamente provee *integridad* ya que permite verificar que un
mensaje u archivo no ha sido modificado. 
En comunicaciones, el *digest* se anexa al mensaje. El receptor puede verificar
que el mensaje no contiene errores o ha sido modificado en su camino.

Otro uso común es su uso como *autenticador*: El *digest* se anexa *cifrado* por
el emisor y el receptor puede verificar que ha sido enviado por el emisor
descrifrando el hash con la clave correspondiente y luego verificar la
integridad del mensaje.

En algunos sistemas que permiten autenticación por *usuario/contraseña*, como
por ejemplo el programa *login* de los sistemas tipo UNIX, se compara el hash de
la contraseña ingresada por el usuario contra el hash almacenado en el archivo o
base de datos de usuarios del sistema.

> El *digest* de un mensaje, cifrado con la clave privada de un sujeto *s*
> constituye una ***firma digital***.
> 
> *Verificación* de la firma: $hash(message)=D(Sig_s,s_{pk})$, donde $Sig_s$ es la
> *firma* del sujeto $s$ y $s_{pk}$ es la clave pública de $s$.
> La firma digital garantiza:
> 1. *No repudio (de origen)*: El firmante no puede desconocer su firma
> 2. *Integridad* del mensaje: El mensaje no ha sido modificado luego de ser
>    firmado.

Un *Message Authenticator Code (MAC)* usa una función de hash tradicional (no
criptográfico) con una clave secreta conocida por las partes. El valor hash
obtenido se anexa al mensaje. Una variante, conocida como *HMAC* aplica un hash
criptográfico a la concatenación del mensaje y la clave secreta tal como se
muestra en la siguiente figura.

![mac-hmac](img/MAC-HMAC.png ':size=70% :class=imgcenter')

Figura 14.1: a) MAC b) HMAC.

## Distribución de claves

La distribución de claves secretas puede hacerse usando *Diffie-Hellman* o
usando un *centro de distribución de claves*, una entidad de confianza como en
*Kerberos*, descripto en la sección de autenticación.

Las claves públicas en principio podrían publicarse en el sitio web de cada
persona u organización o enviarse por e-mail u otro servicio de mensajería. 
Un problema a resolver es que el sitio web podría haber sido vulnerado y el
atacante la podría haber cambiado por su propia clave pública. Así podrá
descifrar los mensajes con su clave privada. Lo mismo podría ocurrir si el
atacante logra enviar su clave pública por email con el remitente falso.

El problema es que es necesario un mecanismo para asociar la verdadera
*identidad* de un sujeto con su clave pública. Generalmente se utilizan dos
enfoques: Confiar en terceros usando una *infraestructura de claves públicas
(PKI)* o en base a otro concepto como la *web de confianza (web of trust)*.

Una PKI se basa en una jerarquía de *autoridades de certificación (CA)*. Una CA
es una organización que se encarga de brindar un servicio de asociar la
*identidad de un sujeto* con su *clave pública*. Esta asociación se plasma en un
*certificado*, firmado por la CA.

La clave pública de una CA está en un certificado de una CA de mayor nivel. Una
CA *raíz* su certificado conteniendo su identidad y su clave pública está *auto
firmado*.

Un usuario puede generar sus pares de claves y genera un *certificate signing
request (CSR)*, el cual se envía a una CA para que lo firme. La CA deberá
verificar la identidad del sujeto, generalmente solicitando información por otro
medio y luego le devolverá el certificado firmado. Uno de los formatos usados
para los certificados es el estándar X.509.

En una comunicación segura, el emisor usa el certificado del receptor, lo valida
(verificando la autenticidad de la firma y su integridad) usando el
certificado (el cual contiene la clave pública) de la CA firmante y transmite
cifrando con la clave pública del receptor. El receptor puede descifrar el
mensaje usando su clave privada.

El paquete de software [openssl](https://www.openssl.org/) incluye una gran
variedad de algoritmos criptográficos y las utilidades necesarias para
la gestión de certificados para desarrollar una CA.


En [Pretty Good
Privacy(PGP)](https://en.wikipedia.org/wiki/Pretty_Good_Privacy), usado
principalmente para cifrado de mensajes por email se basa en que los usuarios
deberían confiar en las claves públicas de otros en principio por relaciones
directas. Esto se conoce como la *web of trust*.

Un certificado PGP es auto-firmado y puede contener firmas de otros usuarios que
pueden acreditar su autenticidad, aumentando su confiabilidad. Esto permite un
sistema descentralizado de confianza.

Generalmente cada usuario distribuye su clave pública con la forma de una
*huella o fingerprint* (hash) del certificado. El software PGP (ver
[OpenPGP](https://www.openpgp.org/)) permite generar claves, firmar certificados
y almacenar y buscar claves públicas de usuarios en servidores públicos.

La siguiente figura muestra los pasos en el cifrado de un mensaje usando PGP.

![mac-hmac](img/PGP.png ':size=50% :class=imgcenter')

Figura 14.2: Preparación de un mensaje usando PGP.

## Autenticación

En una comunicación se debe asegurar que cada mensaje es auténtico, es decir
que realmente fue enviado por alguna de las partes en esa sesión. La adición de
un autenticador en cada mensaje y cifrarlo no siempre alcanza.

Un *reply attack* consiste en que  el adversario transmite una copia de un
mensaje anterior, el cual para el receptor podría ser un mensaje auténtico. Para
prevenir este ataque se necesita un mecanismo que garantice la *originalidad* de
mensajes. Si el adversario retiene un mensaje y lo reenvía mas tarde
(*supress-reply attack*) genera un mensaje fuera de orden (*no timeliness*).

Estas dos propiedades caracterizan *integridad*. Además es necesario establecer
una *clave de sesión* para poder usar criptografía simétrica.

Una técnica para resolver los problemas de originalidad y secuencialidad es
incluir en los mensajes un *timestamp*, lo que requiere que las partes tengan
sus relojes sincronizados.

Esos *timestamps* actúan como *nounces*: valores aleatorios usados una única vez, lo
que requiere que el receptor almacene los nounces recibidos para verificar que
el recibido recientemente no hay sido usado previamente.

Es común usar una combinación de ambas técnicas: pseudo-timestamps y nounces
para la sesión, únicamente.

Un protocolo simple para lograr *timeliness* y autenticación es un protocolo del
tipo *challenge-response* como se muestra en la siguiente figura.

![c-r-auth](img/challenge-response-auth.png ':size=50% :class=imgcenter')

Figura 14.3: Protocolo challenge-response.

Alice envía su timestamp y Bob responde con el timestamp recibido con su firma
anexada. Así Alice puede determinar que Bob es realmente quien responde al
mensaje original.

Una alternativa es que Bob retorne una respuesta incluyendo el timestamp de
Alice cifrada con su clave privada en lugar de usar una firma.

### Autenticación con clave pública/privada

La siguiente figura muestra un protocolo de autenticación usando criptografía
asimétrica.

![pk-auth](img/public-key-auth.png ':size=50% :class=imgcenter')

Figura 14.4: Autenticación con clave pública/privada.

Alice envía su identidad y su *timestamp* firmado. Bob responde con el timestamp
de Alice, su propio timestamp y su identidad, con su firma anexada. Finalmente,
Alice responde con el timestamp de Bob (en forma plana) y la clave secreta de
sesión generada cifrada con la clave pública de Bob, así éste la podrá descifrar
y ambos la pueden usar para cifrar los siguientes mensajes.

Se debe notar que los timestamps son como nounces y los relojes no necesitan
estar sincronizados.

### Autenticación con cifrado simétrico

En la siguiente figura se muestra el protocolo de autenticación propuesto por
Needham y Schroeder. 

![sk-auth](img/secret-key-auth.png ':size=50% :class=imgcenter')

Figura 14.5: Protocolo de Needham-Schroeder.

Se basa en el uso de una *key distribution center (KDC)* el cual contiene las claves
secretas compatidas por él y cada participante. A continuación se describen los
pasos del protocolo.

1. $A \rightarrow KDC : A, B, N_1$

   A envía al KDC un mensaje con las identidades A y B junto con un *nounce*.
2. $KDC \rightarrow A: \{N_1,A,K_{AB},\{K_{AB},A\}K_{B-KDC}\}K_{A-KDC}$

   El KDC responde con el nounce, una nueva clave de sesión entre A y B
   ($K_{AB}$) y la clave de sesión con la identidad de A cifrada con la clave
   secreta de Bob.  Todo el mensaje se cifra con la clave secreta compartida
   entre A y el KDC.
3. $A \rightarrow B: \{K_{AB},A\}K_{B-KDC}$

   Alice envía el par recibido (cifrado con la clave secreta de Bob) a Bob.
   Notar que Alice no puede descifrar este mensaje.
4. $B \rightarrow A: \{N_2\}K_{AB}$

   Bob envía a Alice un nuevo nounce (challenge) a Alice.
5. $A \rightarrow B: \{N_2+1\}K_{AB}$

   Alice responde al reto aplicando una función (+1) al nounce recibido.

Cabe analizar que el protocolo es vulnerable a un *reply attack*. Si el atacante
usa un viejo mensaje comprometido del paso 2, podría enviarle
$\{K_{AB},A\}K_{B-KDC}$ a Bob y éste lo reconocerá como un mensaje válido
proveniente de Alice.

### Kerberos

El protocolo Kerberos se basa en la idea del protocolo anterior. En el paso 2 el
KDC envía a Alice una especie de certificado o *token*, el cual consiste en una
clave de sesión y la identidad de Alice. Kerberos separa esta funcionalidad en
un *authentication server (AS)* que sólo da acceso a un *ticket granting server
(TGS)* el cual otorga un *token* a Alice para presentar a Bob, como se muestra
en la siguiente figura.

![sk-auth](img/Kerberos.png ':size=50% :class=imgcenter')

Figura 14.6: Kerberos.

La ventaja de este esquema es que si Alice debe contacta a otro host, sólo debe
obtener un nuevo token del TGS, sin necesidad de re-autenticarse con el AS.

### OAuth y OpenId

Actualmente muchas aplicaciones (generalmente web) requieren acceder a recursos
del usuario, por ejemplo, su foto y datos de su perfil de su cuenta de Google, 
Facebook u otra aplicación. En estas aplicaciones se presenta al usuario una
ventana donde tiene la posibilidad de seleccionar la cuenta (Google, Twitter, 
etc) a la que quiere permitir el acceso y debe *consentir* que dará acceso a los
recursos que se le solicitan.

Actualmente muchas aplicaciones usan este escenario simplemente para el *login*
del usuario usando algunas de sus cuentas existentes en aplicaciones de uso
común como las mencionadas para no tener que crear nuevas cuentas y recordar su
usuario y contraseña.

El protocolo [OAuth](https://oauth.net/) fue desarrollado para *autorizar* a una
aplicación cliente el acceso a recursos adueñados por un usuario residiendo en
otra aplicación. 
Los recursos son generalmente APIs que permiten acceder a recursos como datos
del perfil del usuario o para la obtención y envío de mensajes en una red social
u otras aplicaciones.

OAuth se basa en HTTP y se usa ampliamente en aplicaciones Web, móviles y
nativas. En la siguiente figura se muestra el escenario correspondiente.

![sk-auth](img/OAuth2.jpg ':size=100% :class=imgcenter')

Figura 14.6: Autorización usando OAuth.

1. La aplicación solicita al usuario que se autentique. En ese punto se le
   presentará un diálogo para ingresar su usuario y contraseña o usando alguna
   de sus cuentas de Google, GitHub, Facebook, Twitter, etc. 
2. Si elige una de las cuentas (por ejemplo Google), la aplicación envía un
   requerimiento con los recursos requeridos y una URL de redirección, al
   *servidor de autorización* OAuth de Google el cual pide el consentimiento al
   usuario sobre qué recursos la aplicación original está solicitando.
3. Luego que el usuario dió su consentimiento, se le redirige a la página dada
   por la aplicación pasándole un *código*.
4. La aplicación solicita un *token de acceso* al *servidor de autorización*
   pasándole el código recibido en el paso anterior.
5. El servidor responde con el *token* luego de verificar el código recibido.
6. La aplicación puede acceder a la API del servicio usando el token recibido.
   Los tokens se representan como [JSON Web Tokens (JWT)](https://jwt.io/).

Para poder usar OAuth el desarrollador de una aplicación debe registrarla en
aquellos servidores OAuth de los servicios a los que quiere acceder. Por
ejemplo, en la página [Google OAuth 2.0
sign-in](https://developers.google.com/identity/sign-in/web/sign-in) se
describen los pasos y las APIs provistas por Google para desarrolladores que
quieran integrar en su aplicación este servicio.

Una vez registrada la aplicación se obtiene un *client ID* y un *client secret*.
Estos valores deben pasarse al servidor de autorización para obtener el token de
acceso.

Como se puede apreciar, OAuth es un protocolo de *autorización*, aunque muchas
aplicaciones lo usan para *autenticación*.

[OpenID Connect](https://openid.net/) es un protocolo que opera sobre OAuth
específicamente para autenticación, es decir sólo con el objetivo de permitir
acceso a usuarios con cuentas registradas en otras aplicaciones.

## Protocolos seguros

En esta sección se describen algunos protocolos y aplicaciones seguras.

### SSH

Secure shell (ssh) provee un servicio de acceso (login) remoto y transferencia
de archivos con servicios de autenticación y canales seguros. 

La autenticación entre hosts se hace usando criptografía de clave
pública/privada. Cada host tiene su par de claves que se generan en el momento
de instalación del software. Una vez autenticado los hosts, se autentica el
usuario (por password o su propio par de claves pública/privada).
Finalmente se acuerda una clave de sesión, comúnmente usando Diffie-Hellman,
para cifrar el tráfico usando cifrado simétrico (usualmente AES).

Consiste de tres protocolos:

1. *SSH-TRANS (RFC 4344)*: Provee un canal seguro (cifrado) sobre TCP usando un
   cifrado simétrico (generalmente AES) usando una clave de sesión obtenida luego
   que el cliente autentica al servidor usando comúnmente RSA. Puede haber una
   doble autenticación, donde el server autentica al cliente. Cada host SSH
   contiene su par de claves pública/privada.
2. *SSH-AUTH (RFC 4252)*: Un protocolo de autenticación de usuarios. Puede ser
   basado en usuario/contraseña, usando claves pública/privada o *host* based.
3. *SSH-CONN (RFC 4254)*: Permite correr otras aplicaciones no seguras como SMTP
   o X11 sobre un canal (túnel) seguro. Es común su uso para realizar una sesión
   gráfica remota (ver [X app over ssh](https://www.cyberciti.biz/tips/running-x-window-graphical-application-over-ssh-session.html)).

Una implementación como [openSSH](https://www.openssh.com/) ofrece comandos para
gestionar claves (`ssh_add`, `ssh-keygen`, ...), realizar operaciones remotas
como login remoto y ejecución remota de comandos (`ssh user@server [cmd]`) y
transferencia  archivos (`scp file user@server:path`).

En el servidor se ejecutan los servicios (daemons) `sshd`, `sftp-server` y
`ssh-agent` (autenticación).

Para usar autenticación de usuario usando criptografía de claves pública/privada
se debe agregar (*append*) la clave pública del usuario en el host cliente en el
archivo `$HOME/.ssh/authorized_keys` de la cuenta de usuario correspondiente en
el servidor. Se puede hacer con el script `ssh_copy_id`. Luego el usuario será
autenticado sin solicitar ningún password.

### TLS/SSL y HTTPS

*Transport Layer for Security (TLS)* es la evolución de *Secure Sockets Layer
(SSL)*, este último desarrollado por Netscape communications. Es un protocolo
que opera sobre TCP y provee servicios de transporte seguro. El protocolo HTTP
usando SSL/TLS se conoce como HTTPS (puerto 443).

El protocolo se basa en autenticación basada en certificados y usa hashes y
HMACs para integridad de datos, establecimiento de claves de sesión usando
Diffie-Hellman o RSA para luego usar 3-DES o AES para el cifrado de mensajes.

La siguiente figura muestra el *handshake* de alto nivel entre el cliente y
servidor.

![tls](img/TLS-handshake.png ':size=30% :class=imgcenter')

Figura 14.4: Handshake TLS.

El cliente y servidor *acuerdan* los algoritmos de hash, HMAC y cifrado a usar.
Cada mensaje inicial incluye el *nounce* (valor aleatorio usado una única vez).

Posteriormente, el server envía al cliente su certificado para que el cliente
valide. El server a su vez puede indicarle al cliente que requiere autenticarlo.
En este caso el cliente envía su certificado.

A partir de la *PMS*, y los *nounces* se genera la *master secret* y de esta las
claves de sesión. Finalmente, el cliente envía un mensaje que incluye un hash de
todos los mensajes anteriores. El server responde similarmente.

Cuando el handshake finaliza, la comunicación se basa en el *record protocol*,
el cual provee confidencialidad e integridad al protocolo de transporte
subyacente.

Cada mensaje entregado por la aplicación se fragmenta en bloques, los cuales se
comprimen, se les agrega un HMAC como un autenticador y se cifran usando un
cifrado simétrico como AES.

Las nuevas versiones de TLS soportan *re-establecimiento de sesión*, donde el
cliente envía en su mensaje inicial un *identificador de sesión* previamente
establecido. Si el servidor la reconoce, utilizan los parámetros negociados
previamente.

En HTTPS los navegadores web incluyen una base de datos de los certificados de
las CAs mas populares o reconocidas. Así la validación de un certificado
recibido de un servidor no requiere que las CAs estén en línea.

### IPSec

Este protocolo ofrece seguridad a nivel de capa de red. Es opcional en IPv4 pero
de uso obligatorio en IPv6. Es modular y muy flexible.

Es posible analizar IPSec en sus dos grupos de protocolos. En el primer grupo
hay dos protocolos para brindar servicios de control de acceso, interidad de
mensajes, autenticación y *forward secrecy* (resistencia a descifrar mensajes
anteriores al descubrir una clave) y se conocen como *Authenticaion Header (AH)*
y *Encapsulating Security Payload (ESP)*. El segundo grupo permite la gestión e
intercambio de claves conocido como *Internet Security Association and Key
Management Protocol (ISAKMP)*.

Si bien IP es un protocolo sin conexión, IPSec crea una *asociación de seguridad
(SA)* entre las partes usando ISAKMP. Una SA es unidireccional. Así, por
ejemplo, para una comunicación TCP se requieren dos SAs. A cada SA recibida se
asigna un *índice de parámetros de seguridad (SPI)* (índice en una tabla que
describe los algoritmos a usar). Una SA se identifica por el par *(SPI,
destination-address)*. El ESP header incluye la SPI así el receptor puede
determinar la SA a usar para descifrar el paquete, como se muestra en la
siguiente figura.

![tls](img/IPSec-ESP.png ':size=60% :class=imgcenter')

Figura 14.4: Formato de la cabecera ESP.

La cabecera incluye un número de secuencia de los paquetes para prevenir
*ataques de réplicas (reply attacks)*. En IPv4 el ESP header sigue a la cabecera
IP. En IPv6 es un extension header.

IPSec puede usarse en *transport mode* el ESP payload contiene los datos de la
capa superior (UDP, TCP o aplicación). En modo *tunnel* el ESP payload es
directamente otro paquete IP permitiendo implementar una VPN.

### Seguridad en Wifi

Actualmente es común el uso de *access points (APs)* que utilizan *Wifi
Protected Access (WPA)* (802.11i). Hay dos modos de autenticación. El primero se
basa en que todos los clientes comparten una *pre shared key (PSK)* generada a partir
de una *contraseña* definida en el AP. Cada cliente deriva su clave de sesión a
partir de la PSK y otros parámetros como su *MAC address*. 

El otro modo mas fuerte se basa en el uso de un *authentication server*,
conectado en forma segura al (o corriendo en el mismo) AP. Esto permite definir
una clave por cada cliente.

En WPA2 la clave de sesión generada se usa como clave de AES en modo *counter* para
cifrar los paquetes y una MAC como autenticador.

## Firewalls, IDSs y VPNs

Un *cortafuegos* usa una política basada en entrada-salida, es decir actúa como
un *filtro de paquetes* para prevenir tráfico no deseado.

Generalmente se basan en *reglas* las cuales tiene dos partes: EL *matching
pattern* y la *acción* a ejecutar en cada paquete coincidente.

Las acciones pueden ser *Drop*, *Accept*, *Reject* u operaciones de *NAT* o
operaciones de *marcado de paquetes (mark)*. Las operaciones de NAT pueden
usarse para hacer *maskerading*, *port forwarding*, *balance de carga* u otras
operaciones como el encolado en *buckets* de diferentes prioridades para
implementar políticas de *calidad de servicio (QoS)*.

Los patrones permiten reconocer paquetes por medio de las *cabeceras* de los
mensajes ya que generalmente se aplican internamente en el procesamiento de los
paquetes en su arribo interiormente en el kernel del sistema operativo.

Un ejemplo de un firewall es el módulo
[iptables](https://linux.die.net/man/8/iptables) de GNU-Linux. El siguiente
listado muestra un conjunto de reglas para *filtrar* paquetes en un router
GNU-Linux hogareño.

```sh
#!/bin/sh
# reject all initially
iptables -A INPUT DROP

# accept all on internal interface
iptables -A INPUT -i eth1 -j ACCEPT

# accept tcp connections to port 80 on external interface
iptables -A INPUT -i eth0 -p tcp -m tcp --dport 80 -m state --state NEW -j ACCEPT

# accept packets on established TCP connections
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT

# forwar packets comming from external interface
iptables -A FORWARD -i eth0 -j ACCEPT

# accept output traffic
iptables -A OUTPUT -j ACCEPT

# NAT: all incoming tcp to port 80 redirect to internal web server
iptables -A PREROUTING -i eth0 -p tcp --dport 80 -j DNAT --to-destination 192.168.0.149:8080

# marquerade all outgoing traffic
iptables -A POSTROUTING -o eth0 -j MASQUERADE
```

Listado 14.1: Ejemplo de reglas de **iptables**.

Un firewall o *filtro de aplicación* contiene reglas de filtrado de mensajes
para aplicaciones o protocolos específicos. Es común usar filtros de
aplicaciones web de un servidor web o de aplicaciones que impida el paso de
mensajes que no cumplan con una API HTTP determinada. También es posible filtrar
mensajes de salida HTTP para prevenir que los usuarios de una organización no
accedan a sitios específicos. Comúnmente se basan en el uso de expresiones
regulares para hacer matching con un conjunto de URLs. Otro ejemplo son las
aplicaciones de control parental a menores de edad en una red hogareña.

-------------------------------------------------------------------------------

Un *Intrussion Detection System (IDS)* es un servicio generalmente corriendo en
modo usuario que permite identificar tráfico no deseado principalmente
analizando el *payload* de los paquetes. Son útiles para analizar ataques
conocidos identificando sus patrones o firmas, de manera similar a los
antivirus o antispam de mail.

Otros mecanismos usados en IDS es la detección de tráfico anómalo o no habitual,
generalmente usando técnicas de *machine learning*.

Un IDS libre ampliamente usado es [SNORT](https://www.snort.org/).

-------------------------------------------------------------------------------

Una *Virtual Private Network (VPN)* permite conectar hosts de diferentes
subredes físicas interconectadas por una red pública como Internet con el fin de
simular que están en la misma red lógica. 

Un software de *vpn* consiste de al menos un programa cliente instalado en cada
estación de trabajo y un servidor, instalado comúnmente detrás de un router en
la red remota donde están los servicios o recursos a alcanzar.

La siguiente figura muestra un esquema de una vpn.
Un host en *network 1* desea enviar un mensaje a un host de *network 2*. Ambas
redes comúnmente usan direcciones *privadas*.

El programa cliente de la vpn intercepta el paquete (en el host o en R1) y lo
*encapsula* (forma un *túnel*) en un paquete con dirección de destino la IP
pública del router R2. El router R2 ejecuta (o reenvía el paquete recibido a) el
servidor vpn el cual *extrae* el paquete encapsulado y lo redirige al host de
destino.

Para realizar este proceso comúnmente se requieren configurar reglas de firewall
o NAT en los routers (R1 y R2).

Para garantizar autenticación y confidencialidad comúnmente se usa IPSec o un
protocolo de transporte como UDP o TCP sobre TLS, como en
[OpenVpn](https://openvpn.net/).

![vpn](img/tunnel-vpn.png ':size=70% :class=imgcenter :id=vpn')

Figura 14.5: Esquema de un túnel VPN.

Un servicio de *vpn proxy* ofrece el servicio de *relay* de paquetes entre los extremos
de la comunicación (*proxy*). Esto provee *anonimato* del cliente ya que el servidor vpn
es el que realmente interactúa con el servidor, reenviando los mensajes entre
ambos extremos como se muestra en la siguiente figura.

![vpn proxy](img/proxy-server.webp ':size=70% :class=imgcenter :id=vpn-proxy')

Figura 14.6: VPN Proxy service.

## Servicios de Proxy (o relay)

El uso de proxies permite interceptar mensajes de clientes y realizar
redirecciones o filtrado y pueden usarse con diferentes fines como puentes
(conversores de protocolos), seguridad o balance de carga.

Es posible usar dos configuraciones (ver las figuras de abajo):

- *Forward proxy*: Se sitúa en frente de un grupo de hosts o clientes.
- *Reverse proxy*: Se sitúa en frente de servidores.

<div class="center">

![forward proxy](img/forward_proxy_flow.png ':size=45%')
<span style="width: 1em"></span>
![reverse proxy](img/reverse_proxy_flow.png ':size=45%')

</div>

Figura 14.7: Forward y reverse proxies.