# Presentación

Los protocolos analizados hasta ahora se basan en encabezados con un formato
predeterminado.
A diferencia de los protocolos de transporte, las aplicaciones generalmente
requieren comunicarse con diferentes tipos de mensajes.

Para lograr independencia de plataformas un protocolo debe definir la
*representación* de los datos. A modo de ejemplo, la API de sockets ya vista
provee funciones para convertir valores numéricos como por ejemplo direcciones
de red o puertos, desde y hacia el formato esperado por el protocolo, ya que la
arquitectura del hardware puede representar en la memoria esos valores con los
bytes en diferente orden. Por ejemplo, algunas arquitecturas almacenan los
valores numéricos multi-byte en *little-endian* y otras en *big-endian*, como se
muestra en la siguiente figura.

![endianness](img/endianness.png ':size=70% :class=imgcenter')

#### Figura 12.1: Posibles representaciones del valor (en 32 bits) 34677374.

A diferencia de los protocolos de transporte, las aplicaciones generalmente
requieren comunicarse con diferentes tipos de mensajes.

La representación de *strings* generalmente no es inconveniente porque se
representan en secuencia.

Los protocolos de red, además deben tener en cuenta otras consideraciones a
diferencia de los formatos usados en almacenamiento secundario. En particular,
se debe tener en cuenta el orden en que un receptor procesa los datos para
evitar exesivo uso de buffers para procesamiento posterior, como se verá mas
adelante en las aplicaciones multimedia.

## Marshalling

El término *marshalling* se refiere a la codificación y decodificación de las
estructuras de datos para su almacenamiento o transmisión.
Este proceso también se conoce como *serialización* ya que se debe generar una
representación *plana*, es decir en una secuencia en formato binario o textual
que la represente.

Para serializar los diferentes tipos de datos generalmente se usa algún esquema
de *marcado (tags)* que permita al receptor decodificar y reconstruir la
estructura de datos transmitida.

A modo de ejemplo, en algunos formatos cada valor se representa como una tupla
de la forma `(type, length, value)`, donde `type` representa el tipo de dato,
`length` determina el número de bytes del valor que se encuentra a continuación.
De esta forma es simple representar los tipos básicos y tipos estructurados como
arreglos y registros.

Uno de los mayores desafíos es representar estructuras enlazadas como listas,
árboles o grafos arbitrarios. Una estrategia es *linearizar* con alguna
estrategia sus elementos y las referencias remotas pueden representarse como
índices en la secuencia. La otra, mas flexible, es agregar identificadores a
cada valor y valores que representen *referencias*. Esta última técnica se usa
generalmente en las notaciones como XML o JSON para identificar a *objetos
remotos*.

## XML

El *Extensible Markup Language (XML)* se basa en que cada dato se *encierra* en
*tags* de apertura y cierre, tal como se muestra en el siguiente ejemplo que
describe un registro representando los datos de un empleado.

``` xml
<?xml version="1.0"?>
<employee>
   <name>John Doe</name>
   <title>Head Bottle Washer</title>
   <id>123456789</id>
   <hiredate>
      <day>5</day>
      <month>June</month>
      <year>1986</year>
   </hiredate>
</employee>
```

Cada aplicación deberá definir la sintaxis de tags usados y de los atributos
permitidos en cada uno de ellos. Esta definición puede hacerse con *XML Schema*,
el cual se describe en un *XML Schema Document (XSD)*, el cual se basa también
en XML.

A continuación se muestra un posible esquema que define los tags y atributos
permitidos para la definición de un empleado.

```xml
<?xml version="1.0"?>
<schema xmlns="http://www.w3.org/2001/XMLSchema">
  <element name="employee">
    <complexType>
      <sequence>
        <element name="name" type="string"/>
        <element name="title" type="string"/>
        <element name="id" type="string"/>
        <element name="hiredate">
          <complexType>
            <sequence>
              <element name="day" type="integer"/>
              <element name="month" type="string"/>
              <element name="year" type="integer"/>
            </sequence>
          </complexType>
        </element>
      </sequence>
    </complexType>
  </element>
</schema>
```

El esquema define que el elemento (tag) `employee` es un tipo complejo (una
estructura) que puede contener una secuencia de otros elementos con tags `name`,
`title`, `id` y `hireddate`. Este último también es un tipo estructurado (una
fecha). Se debe notar que el documento indica para cada tag el tipo de dato que
puede encerrar.

Una aplicación puede usar el XSD para *validar* (typecheck) el documento XML y
extraer e interpretar cada valor.

Para evitar conflictos de nombres entre esquemas XML, un XSD incluye una línea
definiendo un *espacio de nombres* de la forma:

```
targetNamespace="http://www.example.com/employee"
```

De esta forma en un XSD es posible referenciar tags de otro XSD, como en el
siguiente ejemplo:

```
xmlns:emp="http://www.example.com/employee"
```

Un documento XML que use varios espacios de nombres deberá desambiguarlo
prefijando el espacio de nombres en el tag. Por ejemplo:

```
<emp:title>Head Bottle Washer</emp:title>
```

## JSON

En aplicaciones web se ha popularizado JSON (siglas derivadas originalmente de
*Javascript Object Notation*). Se basa en la notación de valores de datos de
Javascript, el cual soporta tipos básicos como números (enteros y reales),
lógicos (`True`, `False`), caracteres y los agregados como arreglos y objetos
(registros). El siguiente ejemplo muestra la notación usada para describir el
empleado de la sección anterior.

```json
{
   "name": 'John Doe',
   "title": 'Head Bottle Washer,
   "id": 123456789,
   "hiredate": {
      "day": 5,
      "month": 'June',
      "year": 1986
   }
}
```

También existen implementaciones de *BSON*, el cual representa un objeto JSON en
formato binario.

La popularidad de JSON en aplicaciones web se debe a que es muy simple
serializar (`str=JSON.stringify(value)`) y des-serializar
(`value=JSON.parse(txt))` en Javascript usando la API JSON soportada por los
browsers.

## Remote procedural call (RPC)

En los programas distribuidos es deseable ocultar los detalles de la
comunicación e implementar la interacción entre los componentes del sistema
usando un mecanismo familiar como el de invocación a funciones de la forma
`target.f(args)`, donde `target` es el componente (remoto) destino, `f` es el
servicio requerido pasándole los argumentos correspondientes.

Para lograr esta abstracción en un lenguaje de programación, el sistema de
ejecución basado en *RPC* transforma una invocación en un mensaje que será
recibido por el destinatario, lo decodificará e invocará a la función de
servicio correspondiente. Para eso el sistema debe hacer una codificación del
mensaje con la serialización/de-serialización de valores de los argumentos y del
resultado en la respuesta.

El pasaje de mensajes puede ser síncrona (el cliente espera por la respuesta) o
asincrónica (el cliente continúa con su ejecución).

Un sistema de RPC independiente del lenguaje de programación, comúnmente tiene
la siguiente arquitectura:

- *Interface Definition Language* o *IDL*.
- Un IDL-compiler: Genera los *stubs* del cliente y del servidor.
- *Client stubs*: Funciones que generan los mensajes que representan las
  invocaciones remotas.
- *Server stubs*: Esqueletos de funciones que el desarrollador debe implementar.
  Comúnmente también se genera una *dispatching table* que el servidor usará
  para invocar a las funciones en la recepción de mensajes.

La siguiente figura muestra un escenario de RPC.

![rpc](img/rpc-stubs.png ':size=70% :class=imgcenter')

#### Figura 12.2: RPC con stubs generados a partir del IDL.

Una de las primeras implementaciones de RPC fue desarrollada por la empresa SUN
y se conoce como SUNRpc. Usa una técnica de serialización en binario y soporta
todos los tipos de datos de C.

Entre las implementaciones modernas de RPC es posible mencionar
[SOAP](https://en.wikipedia.org/wiki/SOAP) y [gRPC](https://grpc.io/) de Google.

SOAP se usa principalmente en la implementación de servicios web. Se basa en XML
e incluye un lenguaje de definición de interfaces, el [Web Services Description
Language
(WSDL)](https://en.wikipedia.org/wiki/Web_Services_Description_Language), XML
schema y un mecanismo de publicación y búsqueda de servicios web
[UDDI](https://en.wikipedia.org/wiki/Universal_Description_Discovery_and_Integration).

SOAP es independiente del protocolo de transporte a utilizar pero es muy común
que se use sobre HTTP, permitiendo implementar aplicaciones conocidas como *web
services*.

gRPC es un framework o *middleware* moderno y libre. La definición de los
servicios y la serialización se basa en *Protocol Buffers*, desarrollado por
Google, como se muestra en el siguiente ejemplo.

```
service PersonsDB {
    rpc addPerson (Person) returns (bool);
}

message Person {
    required string name = 1;
    required int32 id = 2;
    optional string email = 3;

    enum PhoneType {
        MOBILE = 0;
        HOME = 1;
        WORK = 2;
    }

    message PhoneNumber {
        required string number = 1;
        optional PhoneType type = 2 [default = HOME];
    }

    required PhoneNumber phone = 4;
}
```

gRPC compila los *stubs* en el cliente. En el servidor una biblioteca permite
decodificar los mensajes y hacer el *dispatching* a las funciones definidas como
servicios. Actualmente gRPC está implementado en la mayoría de los lenguajes de
programación modernos.

Actualmente, una convención muy usada, principalmente como representación de
mensajes RPC en aplicaciones y servicios web es [Representational State Transfer
(Rest)](https://en.wikipedia.org/wiki/Representational_state_transfer), la cual
consisten en los siguientes principios o técnicas:

1. Arquitectura cliente-servidor.
2. Sin estado: El servidor no mantiene estado con los clientes (no hay
   sesiones).
3. Interfaz (API) uniforme:
   - Un recurso se identifica por medio de [URIs](https://en.wikipedia.org/wiki/Uniform_Re  source_Identifier)
   - Uso de comandos (verbos) HTTP: `GET`, `POST`, `PUT`, `DELETE` para la
     obtención, modificación, creación y eliminación de recursos/objetos.
   - Uso de JSON o XML para la representación de datos. Un recurso debe contener
     sus datos y los metadatos necesarios para que el cliente pueda manipularlo.

## Datos multimedia y compresión

La transmisión de datos multimedia presentan un desafío adicional ya que estos
datos son generalmente muy voluminosos. Las aplicaciones multimedia de tiempo
real deben utilizar formatos especializados y generados a partir de técnicas de
compresión. 
> Un cuadro de video de TV de alta definición de 1080x1920 pixels, cada uno
> representado en 24 bits (RGB) ocupa $1080 \times 1920 \times 24 = 50MB$.
> Para transmitir a una tasa de 24 cuadros por segundo (fps), se requiere una
> tasa de transmisión de más de 1Gbps.

Sin utilizar técnicas de compresión se requeriría un ancho de banda,
generalmente no disponible.

Las técnicas de compresión se pueden categorizar en:

1. Sin pérdida de información: Generalmente se aplica a la compresión de
   documentos.
2. Con pérdida: Aplicable a datos multimedia (imágenes, sonido y video).

Todos los métodos se basan en explotar la redundancia de datos para luego
codificarlos en la menor cantidad de bits posibles.

Los algoritmos de compresión sin pérdida de información se basan en las
siguientes estrategias:

- *Run length encoding (RLE)*: La idea es reemplazar las ocurrencias
  consecutivas de un símbolo por una sola copia del mismo mas un indicador de
  multiplicidad.
  Por ejemplo, la cadena `AAABBCDDDD` se codifica como `3A2B1C4D`.

- *Differential Pulse Code Modulation*: A partir de un símbolo de referencia, el
  resto se reemplaza por la diferencia o distancia en el código original. Por
  ejemplo, la cadena `AAABBCDDDD` se reemplaza por `A0001123333`, dadas las
  diferencias en el código ASCII desde el caracter `A`.

  Las diferencias sólo ocuparían sólo 5 bits (sobradamente para representar
  diferencias de hasta 25, tomando en cuenta los 26 caracteres alfabéticos).

- Basados en *diccionarios*: La idea es que a partir de un diccionario en forma
  de tabla que contenga el conjunto de símbolos (por ejemplo palabras, en
  archivos de texto), se reemplaza cada símbolo por su índice en la tabla.

  Estos métodos pueden usar *códigos de longitud variable*: Los símbolos que
  aparecen mas frecuentemente se codifican en menos bits. Un *código de Huffman*
  se basa en el diseño de un código de prefijos óptimo.

### Códigos de Huffman

Un código de Huffman se obtiene con el siguiente algoritmo:

Dados un alfabeto $A = \{a_1, a_2, \ldots, a_n\}$ y un mapping $F:A
\rightarrow \mathbb{R}$ que asocia las frecuencias de ocurrencias de cada
símbolo, la salida es un código $C(F)=\{c_1, c_2, \ldots, c_n\}$.  

El objetivo es lograr un código óptimo, es decir que $L(C(F)) < L(T(F))$ para
cualquier código $T(F)$, donde 

$$ L(C(F)) = \sum_{i=1}^n F(c_i) \times length(c_i) $$

La propuesta de Huffman propone la construcción en forma ascendente de un
árbol binario a partir de los símbolos y sus frecuencias convertidas en
probailidades como las hojas, inicialmente en una cola ordenados de mayor a
menor frecuencia.

1. Mientras haya más de un nodo en la cola
 1. Sacar dos nodos del frente de la cola (los de menor probabilidad).
 2. Crear un nodo que sea el padre de los dos con probabilidad igual a la suma
    de sus hijos.
 3. Insertar ordenadamente el nuevo nodo en la cola.
2. El nodo restante es la raíz (con probabilidad 1)

Luego se recorre el árbol desde la raíz hasta las hojas asignando a cada rama
descendente de cada nodo un 0 a la rama izquierda y un 1 a la derecha. El
código de cada símbolo es la concatenación de los rótulos de cada arco desde la
raíz hasta el símbolo, como se muestra en la siguiente figura.

![huffman](img/Huffman_coding.png ':size=50% :class=imgcenter')

#### Figura 12.3: Codificación de Huffman.

Así el diccionario para el ejemplo de la figura queda formado como en la
siguiente tabla:

| Símbolo | Código |
| ------- | -----: |
|   a1    |    0   |
|   a2    |   10   |
|   a3    |   110  |
|   a4    |   111  |

Cada código puede ser decodificado fácilmente por un receptor ya que el 0 actúa
como marca de fin.

Los algoritmos del tipo Lempev-Ziv (zip, gzip, ...) se basan en estas ideas y
construyen el diccionario en base al contenido y luego generan códigos de
Huffman. El diccionario se incluye en el archivo.

### Compresión de imágenes

Una imagen *raster* es conceptualmente una matriz de píxeles, donde cada pixel
se representa como una tupla de tres colores: rojo, verde y azul (RGB).
Generalmente un dispositivo de captura (cámara) representa la intensidad de un
color en 8 bits, por lo que cada pixel se representa en 24 bits.

Una técnica de compresión, como la usada en el formato GIF, se basa en
representar cada pixel en un byte, el cual constituye un índice de una tabla
conocida como la *paleta de colores* con 256 entradas. Cada entrada en la paleta
define un color. Esto hace que cada pixel se asocie a un color *aproximado* en
la paleta.

GIF además aplica una variante del algoritmo LZ para codificar en menos bits
aquellos valores mas frecuentes. Así, GIF logra un radio de compresión
aproximado de 10:1. 

Otra técnica mas compleja es la desarrollada por el *Joint Photographic Experts
Group (JPEG)*. A diferencia de GIF, JPEG no reduce el número de colores, sino
que transforma cada pixel RGB en YUV, un espacio de colores que toma en cuenta
la percepción del ojo humano. El espacio YUV se basa en que un color tiene una
*luminancia (brillo)* y U y V determinan el *color o crominancia*.
Generalmente se usan las siguientes equaciones de transformación:

```
Y = 0.299R + 0.587G + 0.114B
U = (B-Y) x 0.565
V =  (R-Y) x 0.713
```

El valor `U` representa la *projección desde el azul* y `V` la *proyección desde
el rojo*. Esta representación de colores viene desde la transmisión de TV
analógica en color.

> Existen otros espacios de colores, como el `CMYK` (cyan, magenta, yellow y
> black), comúnmente usada en impresoras color por inyección de tinta. Otros son
> HSB, y HSL. Para comparar espacios y sus transformaciones ver
> [colorizer.org](http://colorizer.org/).

El espacio YUV permite comprimir (con pérdida de información) los componentes UV
mas agresivamente que el componente Y, el cual es el más perceptivo por el ojo
humano.

JPEG luego realiza tres pasos para lograr la compresión como se muestra en la
figura siguiente:

![jpeg](img/JPEG-compression.png ':size=60% :class=imgcenter')

#### Figura 12.4: Compresión JPEG.

Cada paso opera sobre submatrices de 8x8 pixels. El primero aplica una función
llamada *discrete cosine transformation*, similar a la transformada de Fourier,
dando por cada pixel una *frecuencia*, la cual representa el *cambio* de valores
tanto en el eje `x` como en el `y`. Un cambio más brusco genera una frecuencia
más alta y viceversa.

El segundo paso, llamado *cuantización*, es que produce la pérdida de
información, ya que *aproxima* valores descartando bits menos significativos.

El paso final es codificar los valores de cada submatriz siguiendo un recorrido
en zigzag desde la esquina superior izquierda hasta la inferior derecha usando
RLE.

JPEG comúnmente logra radios de compresión de 30:1.

Una alternativa imágenes raster son los gráficos *vectoriales*. Estos contienen
una descripción de los elementos gráficos, como líneas, elipses, texto, etc, de
la imagen.

Es muy común en la web el uso de imágenenes vectoriales como
[svg](https://developer.mozilla.org/en-US/docs/Web/SVG), los cuales se basan en
un formato en XML y los navegadores web los pueden mostrar sin
problemas. Las ventajas de los gráficos vectoriales es que son muy compactos y
escalables, es decir que se pueden redimensionar sin perder resolución.

### Compresión de video

Un video básicamente es una secuencia de imágenes (cuadros o frames) junto con
alguna información o metadatos que describe su tasa de reproducción y otros.

El estándar [MPEG](https://www.mpegstandards.org/) define el formato de
contenedores mulimedia (video, audio, subtítulos, ...) y algoritmos de
compresión.

La compresión de video en MPEG crea cuadros de tres tipos:

1. *I-frames*: Intrapicture frames, los cuales son imágenes JPEG.
2. *P-Frames*: Predicted frames, contienen diferencias con el I-frame anterior.
3. *B-frames*: Bidirecttional frames, contiene diferencias con los I-frames o
   P-frames anteriores y siguiente.

Los frames de diferencias generalmente tienen una gran cantidad de valores
iguales por lo que se pueden comprimir agresivamente.

Los *encoders* deciden cada cuánto incluir I-frames y cuándo generar P-frames y
B-frames.

Este esquema hace que este formato sea conveniente para su transmisión,
generalmente usando potocolos basados en UDP como RTP. Si se pierde un frame de
dierencias, el reproductor simplemente puede mostrar nuevamente el frame
anterior, sin cambiar demasiado la ilusión de movimiento del espectador.

MPEG comúnmente alcanza radios de compresión de 90:1, aunque se han alcanzado
radios de 150:1.

Existen otros estándares de formatos de video, como los de las series H
definidos por el ITU-T, aunque difieren de MPEG sólo en pequeños detalles. Uno
de los formatos mas actuales es el H.264/MPEG, desarrollado en conjunto por los
dos grupos.

### Compresión de audio

El audio digital se genera a partir de tomar muestras (*sampling*) discretas de
la señal analógica a una cierta frecuencia.

Por ejemplo, el audio de calidad de CD se obtiene mediante un muestreo a 44.1KHz
(aproximadamente cada 23$\mu$s) y cada valor discreto es de 16 bits. Transmitir
en estéreo (2 canales) una tasa de 2x44.1x1000x16=1.41Mbps.
En telefonía digital se toman muestras a 8KHz usando valores de 8 bits, lo que
requiere una tasa de transmisión de 64kbps.

El estándar MP3 (MPEG level III) utiliza las mismas técnicas que MPEG. Primero
divide el audio en subbandas, similar a la transformación YUV de MPEG, luego
los divide en bloques, se quantizan y finalmente se codifican de manera similar.

La clave está en la selección de las frecuencias, lo cual se basa en modelos
acústicos propuestos por otros investigadores.

## Contenedores multimedia

Un contenedor multimedia se basa en formatos para representar secuencias o
flujos de video, audio y otros datos, como por ejemplo subtítulos y otros. 
Tienen cabeceras con metadatos que describen los flujos que contienen y la
información necesaria de sincronismo y tasa de reproducción (fps).

Algunas aplicaciones de *streaming* de video, como YouTube o Netflix, soportan
*streaming adaptable*. Un video se almacena en diferentes containers con
diferentes calidades y tasas de reproducción. Los clientes solicitan de a 
grupos de frames de cierta calidad y dinámicamente se determina si el enlace
permite o requiere cambiar de calidad, en cuyo caso el cliente requiere un nuevo
grupo desde otro contenedor.

En aplicaciones web que usan DASH o HLS (como Youtube o Netflix), cada
contenedor generalmente se asocia a un *URL* diferente para que el cliente
solicite mediante solicitudes HTTP.
