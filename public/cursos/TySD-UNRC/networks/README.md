# Telecomunicaciones y Sistemas Distribuidos

Estas son las notas de los cursos ***"Telecomunicaciones y Sistemas
Distribuidos"*** y ***"Redes y Telecomunicaciones"*** de la Licenciatura y el
Profesorado en Ciencias de la Computación, respectivamente, dictadas por el
[Departamento de Computación](https://dc.exa.unrc.edu.ar/) -
[FCEFQyN](https://www.exa.unrc.edu.ar/), de la [Universidad Nacional de Río
Cuarto](https://www.unrc.edu.ar/).

Estas notas contienen una guía principal de los temas estudiados en el curso y
no pretender contener todos los detalles de cada tema abordado. También se
incluyen las <a href="slides/index.html">presentaciones</a> correspondientes a
cada clase.

Estas notas incluyen los temas de la primera parte del curso y se recomienda el
libro en línea [Computer Networks: A System
Approach](https://book.systemsapproach.org/index.html), para profundizar en
algunos detalles.

En la segunda parte de estas notas se abordan temas de 
<a href="../ds/index.html#/">sistemas distribuidos</a>, es decir, algoritmos y
protocolos para implementar sistemas sin una coordinación central, permitiendo
lograr sistemas tolerantes a fallas y con mayor escalabilidad.

## Equipo docente

- [Marcelo Arroyo](https://marceloarroyo.gitlab.io/)
- [Gastón Scilingo](https://dc.exa.unrc.edu.ar/staff/gaston/)
