# Control de errores

Como los canales (medios) de comunicación generalmente no son 100% confiables,
es necesario utilizar técnicas de transmisión de datos que incluyan mecanismos
para detectar y eventualmente *corregir* errores en un receptor.

Estas técnicas se basan en las *teorías de la información y codificación*
desarrolladas por [Richard
Hamming](https://en.wikipedia.org/wiki/Richard_Hamming), [Claude
Shannon](https://en.wikipedia.org/wiki/Claude_Shannon) y otros.

Todos los esquemas de detección y corrección de errores requieren que se incluya
a los mensajes *información redundante* para que el receptor pueda realizar
alguna *verificación de consistencia*.

El diseño de un código corrector de errores requiere definir el conjunto de
símbolos válidos. Cada símbolo es de $n + m$ bits, donde los $n$ bits son de
datos y los $m$ bits son ***datos redundantes***.

> ***Bit de paridad***: Es el código de detección de errores simple. Se basa en
> el uso de 1 bit de redundancia a una palabra de *n* bits. En *paridad
> par (impar)*, cada palabra (codeword) ***válida*** contiene un número par
> (impar) de bits.

El desafío es diseñar un código (conjunto de símbolos válidos o ***codewords***)
que permita *detectar* un símbolo válido o no y en este último caso determinar
su *cercanía* a un código válido permitiendo su corrección.

> La ***distancia de Hamming entre dos codewords (hd)*** es el número de bits en
> que difieren.
>
> La ***distancia de Hamming de un código*** $HD(c)= \forall w_i, w_j \in c \mid
> min(hd(w_i,w_j))$.

Para *detectar* $d$ errores, se requiere $HD(c) = d + 1$ (así con $d$ errores no
hay forma de ir a un código válido a otro adyacente).

Para *corregir* $d$ errores, se requiere $HD(c) = 2d + 1$. Esto permite que un
código con $d$ errores se mantenga más cercano a un codeword (válido) que a otro.

> Ejemplo: Sea $c={000000000, 0000011111, 111110000, 1111111111}$.
>
> $HD(c) = 5$, por lo tanto puede corregir errores dobles (cambios en hasta 2
> bits).

## Códigos detectores

El uso de códigos simples como el bit de paridad, comúnmente se usa para
palabras cortas, comúnmente de longitud de 8 bits. Su uso es común en memorias
RAM y en comunicaciones orientadas a bytes, como en comunicaciones serie RS-232.

## Bit de paridad de dos dimensiones

Este esquema estructura la secuencia de datos en una matriz de $N \times M$.
Adiciona bits de paridad en cada fila y en cada columna. Así se adicionan
$m+n+1$ bits a los datos a transmitir. Este esquema permite detectar errores en
cadena o ráfagas (*bursts*).

Para detectar errores en secuencias de bits mas largas, se han desarrollado
otros códigos. A continuación se describen algunos de ellos.

### Checksum

Este algoritmo se usa generalmente en protocolos de red o en capas superiores.
Se utiliza en varios protocolos de internet (familia TCP/IP).
La idea es muy simple. A cada mensaje se le anexa el resultado de una suma de
las *palabras* (por ejemplo, de 16 bits).

La operación utilizada es generalmente la *suma en complemento a uno*.

En aritmética de complemento a uno, un número negativo $-x$ se representa por su
complemento, es decir cada uno de sus bits invertidos. La suma requiere que si
existe un acarreo en el bit más significativo, éste debe sumarse al resultado.
Por ejemplo: La suma de $-5 + -3 = 1010 + 1100 = 0111$.

El receptor recomputa el checksum del mensaje y lo compara con el campo que
contiene el checksum computado por el transmisor. Si son diferentes, ocurrió un
error.

Se debe notar que el método no es muy robusto. Un mensaje con palabras erróneas
que se *complementen*, es decir, una que incremente su valor y otra que lo
decremente, el checksum coincidirá y no se detectará el error. Sin embargo,
generalmente funciona en la práctica ya que se aplicación es independiente de la
longitud del mensaje.

### CRC :id=crc

El chequeo de redundancia cíclica (CRC) se basa en interpretar un mensaje de
$n+1$ bits como un *polinomio de grado n*.
Por ejemplo, el mensaje $x=10100101$, se interpreta $M(x)=x^7 + x^5 + x^2 + 1$.

Para calcular el CRC, ambos extremos (*Tx* y *Rx*) deben acordar un
***generador*** (divisor) $C(x)$ de grado $k$. Por ejemplo, el generador 1101 
(de grado 3) corresponde a $C(x)=x^3 + x^2 + 1$.

El mensaje a transmitir incluyendo el CRC se computa de la siguiente manera:

1) $T(x) = M(x) \times x^k$, es decir, anexar $k$ ceros al final del mensaje.
2) $R(x) = T(x) \: mod \: C(x)$ (obtener el resto)
3) $P(x) = T(x) - R(x)$ (es decir, $P(x) = T(x) \oplus R(x)$)

Es fácil ver que $P(x)$ es divisible por $C(x)$. Así el mensaje transmitido
($P(x)$) puede ser verificado por el receptor haciendo $P(x) \: mod \: C(x) = 0$.
Si el resto no da cero, ha ocurrido un error en la transmisión.

La siguiente figura muestra la operación del paso 2.

![crc](img/CRC.png ':class=imgcenter :size=50%')

> Notar que en la división módulo 2, la resta realizada en cada paso es 
> equivalente a una operación XOR.

Un generador de grado *k* puede detectar los siguientes tipos de errores:

- Errores simples: Si $x^k$ y $x^0$ son unos.
- Errores dobles: Si $G(x)$ tiene al menos tres términos (no cero).
- Número impar de errores: Si contiene $x + 1$
- Errores de ráfaga (*burst* o consecutivos): Para ráfagas de longitud de hasta
  *k* bits.

Es común que algunos protocolos a nivel de enlace usen CRC como mecanismo de
detección de errores. En particular, Ethernet usa *CRC-32* con el generador
$G(x) = x32 + x26 + x23 + x22 + x16 + x12 + x11 + x10 + x8 + x7 + x5 + x4 + x2 +
x + 1$ = 0x104C11DB7.

## Códigos correctores

Los códigos correctores se conocen como ***Forward Error Correction***. Como
vimos anteriormente, estos códigos requieren mayor redundancia.

Se pueden clasificar por la forma en cómo operan: por *bloques* o *lineales*
(sobre *streams o secuencias* de bits).

### Códigos de Hamming

En 1950, Hamming propuso el siguiente diseño de un código corrector de $m$
*check bits* ocupando las posiciones potencias de 2 y $m$ *data bits* en las
demás posiciones. Esto hace que califique como un *block code*. 
Las posiciones se cuentan de izquierda a derecha comenzando desde 1.

Cada *check bit* interviene en la paridad de las posiciones de bit cuya
representación binaria lo incluye como se muestra en la siguiente tabla
de ejemplo (hasta 15 posiciones). Las columnas $p$ corresponden a *bits de
paridad* y las $d$ corresponden a bits de datos.

Posición      | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15
------------: | - | - | - | - | - | - | - | - | - | -- | -- | -- | -- | -- | --
Codificación  | p | p | d | p | d | d | d | p | d | d  | d  | d  | d  | d  | d
  $p_1$       | X |   | X |   | X |   | X |   | X |    | X  |    | X  |    | X
  $p_2$       |   | X | X |   |   | X | X |   |   |  X | X  |    |    | X  | X
  $p_4$       |   |   |   | X | X | X | X |   |   |    |    | X  | X  | X  | X
  $p_8$       |   |   |   |   |   |   |   | X | X |  X |  X |  X |  X | X  | X

Por ejemplo, el bit de paridad $p_2$ se calcula en base a las posiciones 2
(10b), 3 (11b), 6 (110b), 7 (111b), 10 (1010b), 11 (1011b), 14 (1110b) y 15
(1111b), cuyos valores en que su representación binaria el bit en la posición 2
(contando de derecha a izquierda) es 1.

Si el *XOR* de todo el mensaje recibido da cero, el mensaje es correcto, sino
hay al menos un error.

En caso de error, el receptor *recalcula* los bits de chequeo. Si el error es en
un bit, el valor de la secuencia de *bits checks* como valor numérico (con $p_0$
como bit menos significativo), indica la posición del bit cambiado.
El receptor podrá *intercambiar (flip)* su valor para *corregir* el mensaje.

Esta codificación da una distancia de Hamming = 3.

> Ejemplo: Dado el mensaje 0110 se codificará de la siguiente manera: *--0-110*
> donde los guiones representan los bits de paridad. El transmisor calcula el
> mensaje a transmitir: *1100110*. El bit de paridad en la posición 1 (el de más a
> la izquierda) es la paridad de los bits 3,5 y 7. Suponiendo que el receptor
> recibe la secuencia *1100010* con errores (cambió el bit de la posición 5), al
> recalcular los bits de paridad determinará que los bits de paridad que no
> coinciden son $p_1$ (da 0) y $p_4$ (da 1). La suma de estas posiciones indica
> la posición errónea (1+4=5).

### Códigos convolucionales

Son códigos lineales y se usan ampliamente en comunicaciones por
radio-frecuencias, como en wi-fi, telefonía celular y comunicaciones
satelitales.

La [Figura 4.1](#convoltional) muestra un codificador desarrollado por la NASA
para la misión Voyager (1977) y actualmente usado en wi-fi 802.11.

![convolutional](img/convolutional-code.png ':id=convolutional :class=imgcenter')

#### Figura 4.1: Codificador convolucional.

La operación se basa en que por cada bit del flujo de entrada, se generan dos
bits de salida en base a la *suma (xor)* del bit de entrada con los *bits de
estado* almacenados en un *shift register* (en este caso de 6 bits).

Luego de cada *output*, el bit de entrada se *inserta por izquierda* en el
*shift register*.

Estos codificadores se caracterizan como $n/o,k$, donde $n$ es la tasa de bits de
entrada, $o$ es la tasa de bits de salida y $k$ es la *longitud de la
restricción*. El codificador de la figura 4.1 es $1/2,7$ ya que por cada bit de
entrada genera dos de salida y depende de 7 bits en la secuencia de entrada.

Estos códigos pueden formalizarse como una familia de funciones $c(i,s_k)=o$
donde $i$ es el *input bit*, $o$ son los *output bits* y $s_k$ es el *estado*
actual del *shift-register* (que se va actualizando).

La decodificación se basa en algoritmos que determinan la secuencia más cercana
a la transmitida tomando la secuencia de bits recibidos y *emulando* todos los
posibles estados del codificador. Para más información ver el [Viterbi
decoder](https://en.wikipedia.org/wiki/Viterbi_decoder). Estos algoritmos pueden
implementarse directamente en circuitos de hardware.

### Códigos Reed-Solomon

Se basan en la idea que un polinomio de grado $n$ está determinado por
$n+1$ puntos. Cualquier punto extra es *redundante* y puede ser usado para
corregir errores. Por ejemplo, una línea puede representarse por dos puntos. Si
se envían 4 puntos sobre la recta, al ocurrir un error, la recta puede
reconstruirse a partir de los otros 3.

Un código
[Reed-Solomon](https://en.wikipedia.org/wiki/Reed%E2%80%93Solomon_error_correction)
se basa en la teoría de campos finitos y opera sobre bloques de datos, agregando
*t* símbolos de chequeo a los bloques, puede *detectar t* errores y *corregir
t/2* errores.

También se conoce como *erasure codes* ya que permiten que un mensaje de
longitud $n=k+r$, donde $k$ es la longitud del mensaje original y $k$ son los
bits de redundancia pueda reconstruirse desde un subconjunto de $n$ símbolos.

Estos códigos se usan comúnmente en dispositivos de almacenamiento como
[Redundant Array of Inexpensive Discs
(RAID)](https://en.wikipedia.org/wiki/RAID), *CDs* y *DVDs*, *códigos QR*.
También lo usan algunas implementaciones de *Digital Subscriber Line (DSL)* para
la transmisión de datos digitales sobre líneas telefónicas.

Un código de redundancia cíclica califica como un código Reed-Salomon pero éstos
últimos incluye otros no cíclicos.

El esquema usado en RAID se basa en que un bloque se almacena en forma
redundante en dos discos, sean $A$ y $B$. En un tercer disco se puede almacenar
$C=A \oplus B$. Así, ante la falla de uno se puede reconstruir otro gracias a
las propiedades del operador XOR ($\oplus$) ta que $C \oplus A=B$ y $C \oplus
B=A$.