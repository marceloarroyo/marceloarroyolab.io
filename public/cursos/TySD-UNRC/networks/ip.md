# Internet Protocol

El protocolo
[IP](https://book.systemsapproach.org/internetworking/basic-ip.html#) permite la
interconexión de redes heterogéneas (*internet* o *redes de redes*) proveyendo
una abstracción independiente de la tecnología de comunicación subyacente.

Los paquetes generados en una capa se *encapsulan* en un paquete de la capa
inferior, tal como se muestra en la siguiente figura. Esto permite abstraer la
tecnología usada en la capa de enlace de datos y permite la inter-operabilidad
entre redes diferentes. Los *gateways (routers)* intermedios retransmiten
(*store-and-forward*) los paquetes IP encapsulados en los frames de cada
tecnología usada en cada enlace hasta llegar al destino.

Este último, analizando la dirección de destino lo entrega al módulo que
implementa el protocolo en la capa superior; en este ejemplo, TCP.

![ip-internetwoking](img/ip-internetwork.png ':class=imgcenter :size=60%')

Es un protocolo simple que permite *escalabilidad* y se basa en los siguientes
principios:

- Describe un esquema de *direccionamiento* de *hosts* y *gateways*.
- Provee un servicio de *datagramas* de *mejor esfuerzo* en la entrega de
  paquetes, es decir que no ofrece garantías (servicio *no confiable*).

El formato de un paquete IP (*IP datagram*) se muestra en la siguiente figura.

![ip-internetwoking](img/ip-datagram.png ':class=imgcenter :size=60%')

#### Figura 8.2: Formato de un datagrama IP (versión 4).

Un datagrama IP contiene las direcciones *origen* y *destino*. En la versión 4
(IPv4) las direcciones son de 32 bits. El campo `hlen` especifica la longitud
del *header* (de longitud variable) en palabras de 32 bits. El campo `TOS`
indica el *tipo de servicio* requerido por la aplicación. El campo `Length`
define el tamaño en bytes del datagrama, incluyendo el encabezado.

Los campos `Ident`, `Flags` y `Offset` se utilizan para la *fragmentación* y
*reensamble* de datagramas.

Esto ocurre cuando el protocolo de enlace usa frames de menor tamaño que un
datagrama IP. Por ejemplo, Ethernet tiene un tamaño máximo (*Maximum
Transmission Unit o MTU*) de su *payload* de 1500 bytes, sin embargo en un
datagrama IP puede ser de hasta $2^{16}=65536$ bytes.

> Cuando un datagrama tiene tamaño mayor que la MTU del protocolo de enlace, IP
> debe particionar y enviar el datagrama en *fragmentos*. Cada fragmento tiene
> el mismo `Ident`. El `Offset` indica el orden del fragmento en bloques de 8
> bytes. En cada fragmento, excepto el último, se setea el bit `M` en el campo
> `Flags` indicando al receptor que restan fragmentos.

La siguiente figura muestra la fragmentación de un datagrama IP con un tamaño de
1420 bytes (20 bytes del header + payload de 1400 bytes) en un enlace con una
MTU=532 bytes.

![fragments](img/ip-fragments.png ':size=30% :class=imgcenter')

#### Figura 8.3: a) Datagrama IP original. b) Fragmentos.

El receptor debe *reensamblar* el datagrama IP aunque los fragmentos arriben
fuera de orden, por lo que deberá memorizar los fragmentos para luego rearmar el paquete IP antes de entregarlo (*delivery*) al protocolo de la capa superior.

> ***MTU discovery***: Proceso para descubrir el menor MTU en un *camino* desde
> el emisor al receptor para evitar la fragmentación. 
> 
> Ejercicio: ¿Cómo se puede hacer con el comando `ping`?

## Direcciones IP

El direccionamiento IP se basa en un esquema jerárquico. Cada dirección se divide lógicamente en dos partes: El ***identificador de red*** y el ***identificador de host*** dentro de esa red. Las direcciones en IPv4 son de 32 bits y se denotan usando los valores decimales de cada uno de sus cuatro bytes separados por puntos. Por ejemplo: 192.168.0.15.

Una dirección IP se asigna a una interface (NIC) del host.

> Un ***router*** es un host que pertenece a más de una red. Cada una de sus
> *interfaces* se conecta a una de las redes y se les asignan direcciones con la
> parte de red de manera acorde.

Inicialmente, las direcciones IP se dividieron en *espacios (clases)*, como se
muestra en la siguiente figura, indicando el número de bits para el
identificador de red y host.

![ip-addresses](img/ip-classes.png ':size=50% :class=imgcenter :id=ip-classes')

#### Figura 8.4: Clases de direcciones IPv4. a) A, b) B y c) C.

En cada clase reserva un rango de *direcciones privadas* (class A:
10.0.0.0-10.255.255.255, B: 172.16.0.0-172.31.255.255 y C:
192.168.0.0-192.168.255.255), las cuales no son *ruteadas* y permiten *reusarse*
en diferentes subredes. Esto permite que no todos los hosts y routers internos
(en *intranets)* usen *direcciones públicas*, permitiendo mitigar la escasez
actual de direcciones IPV4.

> ***Masquerading***: Proceso que hace que un *gateway (router)* reenvíe 
> datagramas con destino a Internet, recibidos de hosts internos con 
> direcciones privadas re-escribiendo la dirección de origen con su dirección 
> pública, permitiendo su ruteo. Esto funciona a nivel de transporte ya que se 
> usa el número de puerto del paquete saliente para registrar (en una tabla) el 
> envío.
> 
> Al recibir la respuesta (con IP destino/port del gateway) éste busca en la 
> tabla par *(internal-ip, port)* registrado y reescribe la dirección de 
> destino con *internal-ip*, lo que hará que se retransmita al emisor original.
> 
> El *puerto* se encuentra en la cabecera del protocolo de transporte TCP o UDP,
> los cuales se analizan en el capítulo de [transporte](transport.md).
> 
> Este es un caso particular de un proceso más general denominado ***Network
> Address Translation (NAT)***.

En cada clase es posible configurar subredes, tal como se muestra en la
siguiente figura. Esto se realiza extendiendo el número de bits usados para
representar identificadores de *subredes*. En la [figura 8.5](#subnetting) se
muestran subredes dentro de la clase C, habiendo extendido 1 bit la parte de
identificación de red para dividir la red 128.96.34 en dos subredes
(128.96.34.0 y 128.96.34.128).

![subnets](img/subnets.png ':size=70% :class=imgcenter :id=subnetting')

#### Figura 8.5: Ejemplo de subnetting.

La máscara de subred se denota indicando los bits que participan en el
identificador de red en uno y los de host en cero. Por ejemplo la máscara de
subred 255.255.255.128 indica que se usan 25 bits para el identificador de red.

El esquema basado en clases ha mostrado ser demasiado rígido y no siempre
permite la asignación mas adecuada de direcciones IP, por lo que actualmente una
dirección IP se puede dividir en las dos partes de manera arbitraria,
permitiendo definir a los administradores de una red el número de bits a usar
para la identificación de sus subredes. Este esquema se conoce como ***classless
interdomain routing (CIDR)***.

En CIDR, las redes se denotan de la forma *network-id/x*, donde *x* es el número
de bits de la parte de red. Por ejemplo, la notación `192.168.34/24` denota la
red 192.168.34 de clase C y la notación `192.4.16/20` denota las redes desde
192.4.16 a 192.4.31.

Esto permite especificar en forma compacta un conjunto de redes y reduce el
número de entradas en las tablas de *ruteo*.

## Forwarding

Cada router debe determinar en el arribo de un datagrama si debe ser reenviado
por otra interface o entregado al módulo del protocolo de transporte local. En
el caso que deba reenviarse, el proceso se denomina *forwarding* y está
implementado tanto por routers como en cada host.

Desde la tabla de *ruteo* (configurada a nivel de red), se extraen las entradas
correspondientes a las interfaces del router en una *tabla de forwarding* para
poder determinar eficientemente el camino que debe seguir un paquete recibido.

La tabla de *forwarding* tiene entradas como se muestra a continuación.

| Net           |      Mask       | Gateway       | Ifce  |
| ------------- | --------------- | ------------- | ----- 
| 128.96.34.0   | 255.255.255.0   | 0.0.0.0       | eth0  |
| 128.96.34.128 | 255.255.255.128 | 0.0.0.0       | wlan0 |
| 0.0.0.0       | 0.0.0.0         | 210.134.17.5  | eth1  |

#### Tabla 8.1: Ejemplo de una tabla de forwarding.

La tabla muestra un ejemplo de un gateway que se conecta a un ISP por el enlace
`eth1` e interconecta dos subredes mediante las interfaces `eth0` y `wlan0`. La
última entrada se conoce como la ruta por omisión o *default gateway*.

A continuación se muestra un esquema del algoritmo.

<span id="forward"></span>

```python
def ip_forward(datagram):
   s = datagram.src_ddr
   d = datagram.dst_addr
   if datagram.ttl == 0:
      return icmp_send(s, TIME_EXCEDED)
   for (dst, mask, gw, ifce) in forwarding_table:
      m = d & mask
      if m == dst:
         if gw != 0:
            # send to gateway
            if not data_link_send(dev=ifce, dst=ARP(gw), data=datagram):
               # send ICMP error message to sender
               return icmp_send(s, NETWORK_UNREACHABLE)
         elif (ifce.ip & mask) != 0:
            # local delivery
            if not data_link_send(dev=ifce, dst=ARP(d), data=datagram):
               # send ICMP error message to sender
               return icmp_send(s, HOST_UNREACHABLE)
            else:
               return OK
   # no match, send ICMP error to sender
   return icmp_send(s, NO_ROUTE_TO_HOST)
```

#### Listado 8.1: Algoritmo simplificado de forwarding.

El algoritmo de forwarding busca una entrada en la tabla hasta encontrar una
entrada cuyo prefijo de la dirección de destino coincida con el valor de la
primer columna. Luego se solicita al protocolo de enlace de datos (L2)
correspondiente el envío del paquete. El ejemplo muestra el uso de un protocolo
auxiliar, el *Address Resolution Protocol (ARP)* para obtener la dirección MAC
asociada a la IP del gateway de destino del próximo salto (hop). Más abajo se
describe ARP más en detalle.

Si el campo `TTL` (*time to live*) alcanzó el valor cero, se descarta por haber
alcanzado el máximo número de saltos (re-envíos). Esto evita que algún datagrama
continúe reenviándose indefinidamente por no encontrar su destino.

Se debe notar que en el caso que si la tabla de forwarding incluye el *default
gateway* siempre se logrará un *matching* con esta entrada.

En el caso de *CIDR* el algoritmo debe modificarse (o mantener la tabla ordenada
apropiadamente) para que se seleccione la entrada de la tabla con matching del
prefijo más largo.

### Entrega local

En la recepción de un paquete un host o gateway determina si debe consumirlo o
reenviarlo.

En el caso que la dirección de destino del paquete coincida con la
dirección IP asignada a algunas de sus interfaces de red, se debe entregar al
módulo de transporte indicado en el campo `Protocol` del datagrama. 

En otro caso, si se debe enviar a otro host de su misma red local y en el caso
que ésta sea del tipo Ethernet (o similar) se debe encontrar la dirección MAC
correspondiente al host con dirección de destino en el datagrama IP.

Este proceso se delega en el ***Address Resolution Protocol (ARP)***, definido
en la [RFC 826](https://tools.ietf.org/html/rfc826) el cual puede enteenderse
como una implementación de la siguiente función:

$$ ARP(ip\_address) \rightarrow data\_link\_address $$

ARP implementa esta función enviando un mensaje ARP de tipo *"who has this
ip-address?"* a todos los hosts de la red local en un datagrama IP de
*broadcast* (dirección de destino 255.255.255.255). Esto hará que la capa de
enlace lo encapsule en un *broadcast frame* (destino
0xff:0xff:0xff,0xff,0xff,0xff) el cual será recibido por todos los hosts de la
red local. 

El host que tenga esa IP asociada a una interfaz, responde con un mensaje que
contiene la dirección de enlace (ej.: Ethernet MAC) correspondiente. Así,
ahora el emisor puede encapsular el datagrama IP en un frame de enlace de datos
con la dirección MAC de destino correspondiente.

ARP usa una tabla caché para *recordar* las resoluciones previas.

Este protocolo no se utiliza en el caso de enlaces del tipo PPP, ya que no hace
falta determinar la dirección de destino.

Otro protocolo auxiliar, el ***Reverse Address Resolution Protocol (RARP)***,
descripto en la [RFC 903](https://tools.ietf.org/html/rfc903) realiza la función
inversa, es decir $RARP(DataLinkAddress) \rightarrow IPAddress$. RARP se usa en
el protocolo [bootp](https://tools.ietf.org/html/rfc951) de *booting* remoto en
dispositivos sin disco. 

## Configuración de Hosts

Un host requiere una configuración de red para cada una de sus interfaces. Cada
interface se asocia con una dirección IP y una máscara de subred. Además se
requiere al menos una tabla de forwarding mínima (al menos conteniendo la
entrada de un *default gateway*).

Generalmente en un host el sistema operativo crea una *interface virtual* (por
software) llamado *localhost* o *loopback*. Esta interface se asocia con la
dirección IP 127.0.0.1 y se usa para la comunicación entre procesos locales del
host.

Para evitar recordar direcciones IP asociadas a cada host, se pueden asignar
*nombres*. En los sistemas tipo UNIX, este mapping puede definirse en el archivo
`/etc/hosts`.

En redes grandes como Internet, es necesario usar el *Domain Name Service (DNS)*
que implementa la función $DNS(domain) \rightarrow ip\_address$. Este protocolo
a nivel de aplicación se analiza mas adelante en estas notas.

Resumiendo, la configuración de una interfaz en un host consiste en:

1. Dirección IP.
2. Máscara de red.
3. La dirección IP de al menos un servidor de nombres (DNS). Generalmente se
   definen dos, uno como DNS primario y otro secundario (en Linux, ver el
   archivo `/etc/resolv.conf`).

Además hay que configurar la *tabla de ruteo* que describe cómo se alcanzan
otras redes asociadas a cada interfaz como se describe arriba.

Cada sistema operativo ofrece comandos para configurar la red.

- Configuración de interfaces: `ifconfig` en sistemas tipo UNIX, `ipconfig` en
  MS-Windows.
- Configuración de rutas: El comando `route` comúnmente se usa para agregar,
  modificar o eliminar rutas.
  
En sistemas GNU-Linux, el comando `ip` (hacer `man ip`) permite configurar
prácticamente todos los elementos de una red IP. Los archivos que contienen la
configuración de red son los siguientes:

Archivo                 | Descripción
----------------------- | --------------------------------------------------------------
/etc/hosts              | Mapping de direcciones IP a host names (dominios)
/etc/resolv.conf        | IPs de servidores de nombres (primario y secundario)
/etc/network/interfaces | Configuración estática de interfaces
/etc/network/if-*       | Scripts para manejo de interfaces (*up/down*, ...)

La configuración de un host puede ser manual, en archivos de configuración
dependientes del sistema operativo, o automática.

Para este último caso, existe el [Direct Host Configuration Protocol
(DHCP)](https://en.wikipedia.org/wiki/Dynamic_Host_Configuration_Protocol).
DHCP requiere un *servidor* y *clientes*. Comúnmente un *gateway* (router) implementa un
servidor DHCP. Cada host implementa un cliente DHCP.
Los switchs/routers basados en Ethernet/wi-fi usados comúnmente proveen
el servicio DHCP.

Un host en su inicio del subsistema de red envía un mensaje `DHCPDISCOVER`
broadcast (dst_ip=255.255.255.255, src_ip=0.0.0.0). El host/gateway que esté
corriendo el servidor DHCP responde con un mensaje `DHCPOFFER` el cual
comúnmente contiene la dirección IP ofrecida y la máscara de subred, la IP del
*gateway* e IPs de los DNS.

El cliente si acepta el mensaje responde al servidor con un mensaje
`DHCPREQUEST` indicando que aceptó la configuración recibida del server. Esto
permite seleccionar otros mensajes `DHCPOFFER` recibido por otros posibles DHCP
servers. Finalmente el servidor responde al cliente con un mensaje final
`DHCPPACK` que incluye el *lease time (tiempo de validez)* y alguna otra
información que el cliente pudiese haber requerido.

Un servidor DHCP usa una base de datos (archivo de configuración) que
describe los rangos de direcciones IP a asignar a los hosts y mantiene en su
estado las direcciones asignadas.

Es posible *fijar* la asignación de una dirección IP a un host determinado
usando su MAC.

## Notificaciones de errores

En todo protocolo pueden ocurrir errores y deben ser notificados. Por ejemplo,
en el [algoritmo de forwarding](#forward) se debe notificar un error su un
datagrama agotó su *tiempo de vida* o no se encuentra el destino o ruta de
reenvío.

Para esto se usa un protocolo auxiliar, el [Internet Control Message Protocol
(ICMP)](https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol)
definido en la [RFC 792](https://datatracker.ietf.org/doc/html/rfc792) el cual
define un conjunto de tipos de mensajes de error y control de ICMP.

Los mensajes de control más importantes son: 

- *Redirección*: Indica al host emisor que existe una *mejor ruta* por medio de
  otro router, indicando su dirección IP.
- *Echo*: Los mensajes *echo-request* y *echo-reply* se utilizan para verificar
  alcanzabilidad. Usados en las aplicaciones `ping` y `traceroute`.

Algunos de los mensajes de error son 

- *Destination unreachable*: Indica que el host, red o número de puerto es
  inalcanzable. 
- *Time exeeded*: El campo `TTL` (*time to live*) llegó a cero.
- *Packet too big*: El paquete tiene una longitud mayor que el que puede manejar
  la red por la que tiene que transitar. Esto permite al emisor determinar la
  MTU a utilizar.

## IPv6

La nueva versión de IP es la 6 y se conoce como IPv6. Este protocolo no es
compatible con IPv4 y pretende las siguientes mejoras:

1. ***Espacio de direcciones mayor***: Las direcciones son de 128 bits y se usa
   una notación en 8 grupos de 4 dígitos hexadecimales separados por `:`. Los
   grupos consecutivos en cero pueden omitirse usando como separador `::`. 
   Cada dispositivo puede ser alcanzado globalmente.

   Una dirección también se se separa lógicamente en dos partes. Al menos 64 bits mas significativos para el *identificador de red* y el resto representan el *identificador de host* en esa red.
2. Simplifica las comunicaciones *multicast* (a todos los hosts conectados al 
   enlace correspondiente de la LAN) utilizando la dirección `ff02::1` y utiliza *grupos* de hosts.
3. ***Configuración automática***: Cada host utiliza el protocolo [Neighbor
   Discovery Protocol](https://en.wikipedia.org/wiki/Neighbor_Discovery_Protocol)
   Este protocolo opera en la capa de *enlace de datos*.
   Define mensajes para obtener de un router local el *prefijo de red*.
   Cada host define el *host id* desde la dirección MAC de la interface u otros mecanismos.
4. ***Encabezado simplificado***: Provee mayor flexibilidad mediante las
   *extensiones* opcionales. La [figura 8.6](#ipv6-header) describe el
   *encabezado* de un paquete IPv6.
5. Mayor soporte para ***calidad de servicios***: Su cabecera contiene
   información sobre el *tipo de servicio requerido* en el campo `Traffic class`.
6. Soporte para ***flujos de paquetes***: Secuencias de paquetes relacionados
   que representan diferentes *flujos* como por ejemplo, sonido, video y datos en aplicaciones de streaming multimedia o videoconferencia.

![ipv6-header](img/ipv6-frame.png ':size=50% :class=imgcenter :id=ipv6-header')

#### Figura 8.6: Formato de la cabecera de un paquete IPv6.

Las siguiente tabla muestra los *prefijos* definidos para representar diferentes
tipos de direcciones:

| Prefijo   | Uso                              |
| --------- | -------------------------------- |
| ::        | No especificado                  |
| ::0001    | Loopback interface               |
| FFxx: ... | Direcciones multicast            |
| FE2x: ... | Multicast (en la red local)      |
| otra      | Direcciones de hosts (unicast)   |

Se debe notar que la cabecera no contiene un *checksum*, como IPv4. La
integridad de los datos se traslada a los protocolos en capas superiores.

El campo `next header` permite formar una lista enlazada de *extensiones*. Entre
las extensiones pre-definidas están las que brindan soporte para el uso de
protocolos de seguridad como [IPSec](https://en.wikipedia.org/wiki/IPsec),
extensiones para fragmentación, o *ruteo basado en el origen*, entre otras.

El ruteo es jerárquico, similar a IPv4 con CIDR. Los *registros* en el sistema
DNS para direcciones de hosts/routers se denotan con el tipo `AAAA` (a
diferencia de `A` para IPv4).

## Asignación de IPs y dominios

El [IANA](https://www.iana.org/numbers) es la organización encargada de asignar
rangos de direcciones IP en el mundo. La distribución se hace por regiones. Cada
ISP gestiona rangos de direcciones ante *Internet registry* (locales o
regionales). Cada usuario obtiene sus direcciones IP desde su ISP.

Un ISP puede asignar direcciones públicas a sus usuarios (a la interfaz externa de su modem/router), aunque generalmente las asignan dinámicamente (en base a las que están en desuso actualmente), por lo cual las direcciones pueden cambiar en el tiempo. Un usuario que desee una IP pública estática generalmente deberá adquirir un plan especial, comúnmente a un mayor costo.

Para simplificar el acceso a las aplicaciones (servicios) en red, en lugar de hacerlo utilizando su IP (la cual podría cambiar), se utilizan nombres. Un ***dominio***, determina la IP de un host o un servicio. El nombrado de hosts o servicios es jerárquico y se describe en detalle en el [capítulo de aplicaciones](apps.md). Por ejemplo, el dominio `dc.exa.unrc.edu.ar` determina la IP de un host del departamento de computación de la facultad de ciencias exactas de la Universidad Nacional de Río Cuarto, la cual es una organización educativa en Argentina.

Las aplicaciones usan el servicio ***Domain Name System (DNS)***, el cual es un sistema de base de datos distribuido global que dado un dominio resuelve a la IP correspondiente.

> *¿Cómo podría implementar un servidor en mi organización u hogar con 
> direcciones dinámicas?*
>
> Es posible utilizando un servicio de DNS dinámico (existen muchos gratuitos), 
> el cual permite asociar una dirección con un dominio, donde el mismo servidor 
> (mediante un simple script) del usuario *actualiza* ese par en el DNS ante un 
> cambio de direcciones.

## Network Address Translation (NAT)

NAT se usa ampliamente con IPv4 ya que las direcciones públicas ya se han
asignado en su totalidad y es común que una *intranet* (red interna hogareña o
de una organización) use direcciones *privadas*. El problema con el uso de
direcciones privadas es cómo hacer que un datagrama enviado desde un host
interno a traves de un gateway con destino a un servicio, logre recibir la
respuesta, ya que el servidor no podría enviar un datagrama a una IP privada
fuera de su red.

La solución se basa en que el gateway (router) de salida *reescriba* la
dirección origen (par *ip:port*) del host emisor con su propia dirección IP
externa (pública), posiblemente con un nuevo port generado. Se usa una *NAT
table* para recordar la traducción para que luego, al recibir una respuesta
desde el destino, el router pueda *reenviar* el datagrama al host interno, como
se muestra en la siguiente figura.

``` td2svg
                 
               2. tcp, dst=142.250.79.78:80,
                  src=170.210.132.2:5122          1. tcp, dst=142.250.79.78:80,
               +---------- ... ------------+         src=192.168.0.15:4532  +------+
  +--------+   |                           |   +-------+                    | host |
  | remote |<--+                           +---| local |<-------------------+------+
  |  host  |                                   |  gw   |--+                     ^
  +--------+-------------- ... --------------->+-------+  |                     |
               3. tcp, dst=170.210.132.2:5122,            +---------------------+
                  src=142.250.79.78:80                 4: tcp, dst:192.168.0.15:4532,
                                                          src=142.250.79.78:80 


                                 local gateway NAT table
            +------+------------------+--------------------+-------------------+
            | Prot | Destination      | Source             | Internal          |
            +------+------------------+--------------------+-------------------+
            | ...                                                              |
            +------+------------------+--------------------+-------------------+
            | tcp  | 142.250.79.78:80 | 170.210.132.2:5422 | 192.168.0.15:4532 |
            +------+------------------+--------------------+-------------------+
            | ...                                                              |
            +------------------------------------------------------------------+

```

Este tipo de NAT se conoce como *masquerading* ya que el gateway *enmascara* el
paquete enviado como si lo hubiese emitido él. Se hace *source nat (snat)* en el
envío de paquetes y *destination nat (dnat)* en cada respuesta.

En una respuesta, un router puede utilizar alguna de las siguientes
implementaciones. Sea *NAT* la tabla NAT del router. 

1. ***One-to-One NAT***: También conocida como *full cone NAT*. El router al
   recibir un paquete *pkt*, existe *i*, tal que: *pkt.protocol==NAT[i].prot*
   y *NAT[i].source==pkt.dst_ip:dst_port*. Luego, reescribe
   *pkt.dst_ip:dst_port=NAT[i].internal* reenviándolo al host interno.
2. ***Symmetric NAT***: El router reenvía al host interno un paquete *pkt*
   recibido sólo si existe *i*, tal que: *pkt.protocol==NAT[i].prot*,
   *pkt.dest_ip:dest_port==NAT[i].source* y
   *pkt.src_ip:src_port==NAT[i].destination*. Esto significa que el router debe
   crear una nueva entrada NAT que relaciona cada dirección interna con cada
   destino remoto diferente. El router reenvía un paquete de un host externo a
   un host interno sólo si éste le envió un mensaje previamente.

Esta funcionalidad comúnmente está habilitada en cualquier router provisto por
el ISP. *Full cone NAT* permite comunicaciones entre hosts (*peers* con
direcciones IP privadas) detrás de routers. *Symmetric NAT* no hace posible esta
comunicación entre *peers* detrás de routers con NAT. Al menos uno de ellos
deberá tener una dirección IP pública.

### Port forwarding

La técnica de *port forwarding* es una técnica de *destination nat* que permite
que una aplicación (servicio) interno (con IPs privadas) pueda alcanzarse desde
el exterior.

En éste caso se debe crear una entrada en una tabla en el gateway (router) local
para que *reenvíe* los paquetes entrantes (con destino *public-ip:out-port*) al
host interno correspondiente (*internal-ip:in-port*).

Comúnmente los routers provistos por los ISPs proveen una interfaz de
administración (comúnmente web) accesible con un navegador web a la IP interna
del router (comúnmente 192.168.0.1 o 192.168.1.1) como se muestra en la
siguiente figura.

![port forwading](img/port-forwarding.png ':size=85% :class=imgcenter :id=port-forwarding')

En la imagen se puede observar que es posible acceder a un servicio (web) desde
el exterior mediante la IP externa pública (WAN) al puerto 8080. El router
redirige cada paquete al host interno 192.168.1.13 (privada) al puerto 8000 en
el que la aplicación está escuchando.

### Aplicaciones

NAT es útil para resolver varios problemas como

- ***Masquerading***: Usado para mitigar el problema de la falta de direcciones
  IPv4 públicas.
- ***Balance de carga***: Un router puede redirigir paquetes de clientes a
  diferentes servidores permitiendo distribuir el procesamiento de los
  requerimientos.
- Establecer conexiones desde servidores a clientes detrás de routers NAT.
  Aplicaciones como [File Transfer Protocol
  (FTP)](https://en.wikipedia.org/wiki/File_Transfer_Protocol) y [Session
  Initiation Protocol
  (SIP)](https://en.wikipedia.org/wiki/Session_Initiation_Protocol) requiren
  esta capacidad.
- ***Port forwarding*** como se explica en la sección anterior.
- ***Packet filtering***: Filtrado de paquetes con destino a puertos específicos
  de servicios. Reglas de este tipo comúnmente se usan en *firewalls*.