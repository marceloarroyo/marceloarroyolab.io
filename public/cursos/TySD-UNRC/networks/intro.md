# Introducción

Este curso incluye el estudio de los siguientes temas:


* Conceptos básicos de telecomunicaciones
  - Dispositivos y medios físicos de comunicación
* Redes de computadoras
* Protocolos de comunicación
* Software (diseño, APIs y frameworks)
* Sistemas distribuidos vs centralizados


## Un poco de historia

Las computadoras de la década de 1950 generalmente conectaban dispositivos
remotos como las terminales o consolas. La comunicación digital se basaba en la
transmisión de caracteres (bytes). Para eso se desarrollaron dispositivos y
protocolos de comunicación en serie
[RS-232](https://es.wikipedia.org/wiki/RS-232) y paralelos como el [parallel
port](https://en.wikipedia.org/wiki/Parallel_port) de la IBM-PC.

En 1960 se comenzó a investigar y experimentar con la transmisión de datos en
grupos o ***paquetes***. La idea es que cada paquete podía transmitirse como una
unidad independiente y eventualmente seguir diferentes rutas para alcanzar su
destino. Esta técnica se conoce como ***packet switching*** y se diferencia con
la otra forma de transmitir datos como una secuencia de bits de longitud
desconocida a priori (***data stream***).

En 1972 se desarrolla [ARPANET](https://es.wikipedia.org/wiki/ARPANET), una red
en USA que interconectaba computadoras de entidades académicas y
gubernamentales. En 1983 migra sus protocolos a la suite ***TCP/IP***, conocida
como la familia de protocolos de Internet.

En 1972 se desarrolla [Ethernet](https://es.wikipedia.org/wiki/Ethernet), un
protocolo a nivel de enlace y físico para conectar computadoras en una red
local.

Con la aparición de ARPANET se desarrollan aplicaciones en red como

- Correo electrónico (SMTP, POP, IMAP, ...)
- Login remoto (telnet, ...)
- Transferencia de archivos (FTP, ...)
- otras...

En 1990 Tim Berners-Lee desarrolla la [World Wide
Web](https://es.wikipedia.org/wiki/Tim_Berners-Lee).

## Hardware y software de red

Los sistemas operativos modernos generalmente incluyen soporte (*device
drivers*) para dispositivos de comunicaciones y software de implementación de
protocolos de comunicación.

La siguiente figura muestra la arquitectura de un sistema.

![](img/system-arch.svg ':id=system-arch')

#### Figura 1: Arquitectura del sistema.

> Un ***Protocolo de comunicación*** describe:
> 1. Los *roles* de los nodos (*peers*)
> 2. El formato de los *mensajes*
> 3. La *coreografía* de la comunicación

Por ejemplo, un protocolo del tipo *request/reply* define roles de *clientes* ,
quienes inician la comunicación mediante el envío de un requerimiento al
*servidor*. El servidor es un proceso que está esperando pasivamente
requerimientos. Ante la recepción de un requermiento el servidor, lo procesa y
envía al cliente un mensaje de respuesta.

En este caso, la *coreografía* es simple: Una comunicación es iniciada por un
cliente y éste debe quedar a la espera de una respuesta del servidor. Luego
puede iniciar otro ciclo de comunicación.

> Los protocolos de comunicación se especifican en ***Comments for Requests
> (RFCs)***, los cuales son documentos de texto administrados por la
> [IEFT](https://www.ietf.org/). Cada documento tiene asociado su identificador
> (número).

### Diseño de protocolos

Los protocolos pueden clasificarse en dos grandes grupos:

- *Orientados a conexión*: Antes de intercambiar mensajes las partes deben
  establecer una conexión. Una *conexión* determina las direcciones de origen y
  destino. Conceptualmente es similar a una llamada telefónica. Cada mensaje se
  asocia a la conexión.
- *Orientado a datagramas*: Los mensajes se envían de manera independiente.
  Conceptualmente es similar al correo postal. Cada mensaje incluye su dirección
  de origen y destino.

Uno de los objetivos principales de un subsistema de redes es su
*heterogeneidad* de los participantes los cuales pueden tener diferente tipos de
software y hardware.

Esto motiva que el sistema debe diseñarse basado en *capas* de servicios. Esto
permite independizar subsistemas y *componer* un conjunto de protocolos para el
desarrollo de diferentes tipos de aplicaciones.

![layered-protocols](img/layered-protocols.png ':id=layered-prot :size=50% :class=imgcenter')

#### Figura 2: Diseño en capas.

El modelo ***OSI*** propone el siguiente diseño en 7 capas, como se muestra en
la [figura 3](#osi-model). Cada capa ofrece una API de servicios a las capas
superiores y puede usar las APIs de las inferiores. 

![osi-model](img/osi-model.png ':id=osi-model :class=imgcenter :size=70%')

#### Figura 3: Modelo OSI.

Los protocolos de Internet (familia TCP/IP) se basan en un modelo simplificado
del modelo OSI, como se muestra en la [figura 4](#tcpip-layers).

![tcpip-layers](img/tcp-ip-graph.png ':id=tcpip-layers :class=imgcenter :size=50%')

#### Figura 4: Modelo TCP/IP (4 capas).

## Protocolo de comunicación

Cada protocolo opera en alguna capa del modelo OSI. Un protocolo define los
siguientes elementos:

1. Identificación de las partes intervinientes en una comunicación.
2. Formato de mensajes.
3. Coreografía o reglas de la comunicación. Incluyendo los *roles* de los
   participantes.
4. Mecanismos de detección o corrección de errores de comunicación
5. Mapeo de direcciones con otros protocolos.
6. Mecanismos de ruteo o multiplexado.
7. Mecanismos de detección y prevención de congestión.

El formato de los mensajes puede estar basado en texto (strings) como en
[HTTP](https://en.wikipedia.org/wiki/HTTP), binario como en IP o TCP, o ambos
como en
[File Transfer Protocol(FTP)](https://en.wikipedia.org/wiki/File_Transfer_Protocol).

Cada protocolo debe diseñarse de tal forma para minimizar el acoplamiento con
los demás protocolos. Su implementación debe definir una interfaz de servicios a
las capas superiores y usar los servicios provistos por las capas inferiores.

Las reglas de la comunicación definen el orden en que deben ser enviados los
mensajes y los roles de las entidades que intervienen.

A modo de ejemplo, un protocolo del tipo *requerimiento-respuesta* (como *HTTP*)
define *roles*, un *cliente* y un *servidor*. El cliente envía un mensaje de
solicitud de un servicio y el servidor responde. El servidor inicia en forma
pasiva. La comunicación es siempre iniciada por el cliente.

### Encapsulamiento y multiplexado de mensajes

``` td2svg
                     application: bind(UDP, port 67587)
                               +---------+
                               | message | application (process)
                               +---------+
                                 | udp_sendto(ip:170.210.132.2, port:5000, message)
                                 v
                     +----------------------------------+---------+
                     | src port: 67578, dst port: 5000  | message | udp datagram
                     +----------------------------------+---------+
                                 | ip_sendto(ip: 170.210.132.2, type: udp, udp_datagram)
                                 v
            +--------------------------------------------+--------------+
            | ip src: 192.168.0.5, ip dst: 170.210.132.2 |  payload     | ip datagram
            +--------------------------------------------+--------------+
                                 | eth_sendto(mac: f1:32:ab:ff:5b:3c, type: ip, ip_datagram)
                                 v
  +--------------------------------------------------------+---------------+
  | eth src: ac:01:76:5d:0a:f1, eth dst: f1:32:ab:ff:5b:3c |  payload ...  | Ethernet frame
  +--------------------------------------------------------+---------------+
                                 |
                                 v
                  hardware (Ethernet device) send frame
```

#### Figura 5: Encapsulado en una transmisión.

La figura anterior muestra cómo se van añadiendo las cabeceras correspondientes
a cada protocolo que interviene en una transmisión de un mensaje. En éste caso
se trata de una aplicación que envía un mensaje a un destino determinado (host
con dirección IP 170.210.132.2 y a un proceso dentro de ese host asociado al
puerto 5000). Esto muestra que en un protocolo de red (como IP) sus entidades
son *hosts* (dispositivos) donde cada uno se identifica por su *dirección de
red* mientras que en un protocolo de transporte (como *UDP*) sus entidades son
*procesos* dentro de hosts, identificados por *puertos* (ńumeros de 16 bits).

A medida que una capa solicita un servicio de transmisión a una capa inferior,
ésta última adiciona su propio encabezado e incluye lo anterior como su
*payload* (datos).

Cuando el host destino del mensaje recibe el frame, lo recibirá por un
dispositivo de red (en éste caso por USB) y deberá realizar el proceso
inverso para entregarlo (*deliver*) al proceso (destinatario) correspondiente
como se muestra en la siguente figura.

``` td2svg
                               +---------+
                               | message | process (binded to port 5000)
                               +---------+
                                 ^ recv(src_ip: 192.168.0.5, src_port:67578, message)
                                 |
                     +----------------------------------+---------+
                     | src port: 67578, dst port: 5000  | message | udp datagram
                     +----------------------------------+---------+
                                 ^ udp_recv(src ip: 192.168.0.5, message)
                                 |
          +-------------------------------------------------------+---------------+
          | ip src: 192.168.0.5, ip dst: 170.210.132.2, type: udp |    payload    | ip datagram
          +-------------------------------------------------------+---------------+
                                 ^ ip_recv(payload)
                                 |
  +----------------------+----------------------------------------------------------+
  | usb header, type: ip |               payload (ip datagram)                      | usb frame
  +----------------------+----------------------------------------------------------+
                                 ^
                                 |
        hardware (ej: usb device with ip: 170.210.132.2) receive frame
```

#### Figura 6: Des-encapsulado en la recepción.

En la recepción, cada capa verifica su header, controla errores y determina a
qué capa superior se lo debe entregar. Cada header tiene un campo *protocol* que
identifica el *payload type* y determina a qué protocolo de la capa superior se
debe entregar.

Un protocolo de transporte, como UDP, deberá determinar a qué proceso
(aplicación) debe entregarle el mensaje usando el número de puerto al que está
asociado (*bind*). Esto se conoce como *multiplexado* ya que todos los paquetes
recibidos comúnmente encolados por el protocolo de red IP deberán *rutearse* al
proceso destinatario correspondiente dentro del host. Esto se implementa usando
una *tabla de bindings* con tuplas de la forma *(process_id, port)*.

## Métricas para comunicaciones

> ***Tasa de transmisión (data rate)***: Número de bits transmitidos por unidad
> de tiempo. 

Ejemplo: 10Mbps = 10000000 bits por segundo.

> ***Ancho de banda (bandwidth)***: Rango de frecuencias usado en un medio
> (cable, aire, ...)

A veces estos términos se usan de manera invertida. El *ancho de banda* es una
propiedad física del medio de transmisión, en cambio la *tasa de transmisión*
puede aumentarse/disminuirse con un mismo ancho de banda, usando diferentes
técnicas de modulación o cambiando la duración de un bit (*tiempo de bit*).

> ***Latencia***: Tiempo que demora un mensaje desde su transmisión hasta su
> arribo en el receptor.
> $Latencia = T_p + T_x + T_q$, donde
>
> $T_p = distance / speed$: Es el *tiempo de propagación de la señal*
>
> $T_x = size(msg) / bandwidth$: Tiempo de transmisión del mensaje
>
> $T_q$ es el tiempo de *encolado* (*store and forward*) por los *routers*
> intermedios.

Otra medida de la calidad de una comunicación es la ***variación de la
latencia de paquetes o jitter***, como se muestra en la siguiente figura.

![jitter](img/jitter.png ':id=jitter :class=imgcenter :size=70%')

#### Figura 5: Variación en el arribo de paquetes en el receptor.

## Software

Cada protocolo provee una interface de programación (API). Una de las primeras
APIs provistas que aún es ampliamente utilizada es [Berkeley
sockets](https://en.wikipedia.org/wiki/Berkeley_sockets), introducida como
*syscalls* en 4.2BSD Unix en 1983.

Soporta varios protocolos, incluyendo la familia de internet y soporta
protocolos orientados a conexión y orientado a datagramas.

![udpsockets](img/TCPsockets.jpg ':id=sockets :size=40%')
![tcpsockets](img/UDPsockets.jpg ':size=40% :class=imgsep')

#### Figura 6: a) Orientado a conexión. b) Orientado a datagramas.

A continuación se muestra código de ejemplo de dos programas (cliente y
servidor) usando la API *sockets*. Los programas usan el protocolo `TCP`
(orientado a conexión).

``` clike
/* file: tcp-client.c
 * compile: gcc -o tcp-client tcp-client.c 
 */
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>

#define SERVER_PORT 8000
#define MAX_LINE 256

int main(int argc, char * argv[])
{
  FILE *fp;
  struct hostent *hp;
  struct sockaddr_in sin; // server internet address
  char *host;
  char buf[MAX_LINE];
  int s;
  int len;

  if (argc==2) {
    host = argv[1];
  }
  else {
    fprintf(stderr, "usage: simple-talk host\n");
    exit(1);
  }

  /* translate host name into peer's IP address */
  hp = gethostbyname(host);
  if (!hp) {
    fprintf(stderr, "simple-talk: unknown host: %s\n", host);
    exit(1);
  }

  /* build server address data structure */
  bzero((char *)&sin, sizeof(sin));
  sin.sin_family = AF_INET;
  bcopy(hp->h_addr, (char *)&sin.sin_addr, hp->h_length);
  sin.sin_port = htons(SERVER_PORT);

  /* active open */
  if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
    perror("simple-talk: socket");
    exit(1);
  }
  /* connect to server */
  if (connect(s, (struct sockaddr *)&sin, sizeof(sin)) < 0)
  {
    perror("simple-talk: connect");
    close(s);
    exit(1);
  }
  /* main loop: get and send lines of text */
  while (fgets(buf, sizeof(buf), stdin)) {
    len = strlen(buf);
    buf[len] = 0;
    send(s, buf, len, 0);
    recv(s, buf, len, 0);
    printf("recv: %s", buf);
  }
}
```

#### Listado 1: Cliente TCP.

A continuación se muestra el código del servidor.

``` clike
/* file: tcp-server.c
 * compile: gcc -o tcp-server tcp-server.c 
 */
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdio.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#define SERVER_PORT  8000
#define MAX_PENDING  5
#define MAX_LINE     256

/* string to uppercase */
void toupperstr(char *str)
{
  while (*str != 0 && *str != '\n') {
    *str = toupper(*str);
    str++;
  }
}

int main()
{
  struct sockaddr_in sin;   // server internet address
  char buf[MAX_LINE];
  int buf_len, addr_len;
  int s, new_s;
  char addr_str[INET_ADDRSTRLEN];

  /* build server address data structure */
  bzero((char *)&sin, sizeof(sin));
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = INADDR_ANY;
  sin.sin_port = htons(SERVER_PORT);

  /* setup passive open */
  if ((s = socket(PF_INET, SOCK_STREAM, 0)) < 0) {
    perror("simple-talk: socket");
    exit(1);
  }
  if ((bind(s, (struct sockaddr *)&sin, sizeof(sin))) < 0) {
    perror("simple-talk: bind");
    exit(1);
  }
  listen(s, MAX_PENDING);

  /* wait for connection, then receive and print text */
  while(1) {
    if ((new_s = accept(s, (struct sockaddr *)&sin, &addr_len)) < 0) {
      perror("simple-talk: accept");
      exit(1);
    }
    inet_ntop(AF_INET, &sin.sin_addr, addr_str, sizeof(addr_str));
    printf("Connection: IP: %s port: %d\n", addr_str, ntohs(sin.sin_port));
    while (buf_len = recv(new_s, buf, sizeof(buf), 0)) {
      printf("server. Received: %s", buf)
      toupperstr(buf);
      send(new_s, buf, buf_len):
    }
    close(new_s);
  }
}
```

#### Listado 2: Servidor TCP iterativo.

En éste caso el *socket* `new_s` representa el *socket conectado a un cliente*,
mientras que el *socket* `s` está asociado (*binded*) sólo al extremo del
servidor (*half-end*).

Cada mensaje enviado incluye la información *de la conexión al que pertenece*.

Este servidor es *iterativo*, es decir que está en un ciclo (*infinito*) que:

1. Espera por una conexión de un cliente.
2. Espera mensajes, procesa y responde.
3. Cierra la conexión.

Para soportar requerimientos concurrentes (desde diferentes clientes), es
posible crear *procesos hijos* o *threads* asignándolos a cada nueva conexión o
requerimiento. Un servidor de este tipo se denomina *servidor concurrente*.

En UNIX un *socket* es una abstracción de un *archivo*. En el caso de una
comunicación orientada a conexión, pueden usarse las llamadas al sistema
`read(s,buf,count)` y `write(s,buf,count)` en lugar de `recv()` y `send()`.

Así es posible usar funciones que leen/escriben de/a archivos para que
lean/escriban de/a sockets.