# Ruteo

Los protocolos de *internetworking* atacan el problema de abtraer la
heterogeneidad de las tecnologías de cada red.

Otro problema a resolver es la *escalabilidad*. El algoritmo de *fordwarding*
usado en IP es muy simple y eficiente y se basa en una tabla dada. En pequeñas
redes esas tablas pueden definirse estáticamente (ver los comandos `ip route` en
GNU-Linux). En grandes redes, como Internet, es necesario que los *routers*
descubran automáticamente las rutas a ciertos destinos y sus características,
con el objetivo de lograr encontrar la *mejor ruta* para que los paquetes
alcancen su destino.

La diferencia entre una *tabla de forwarding* y una *tabla de ruteo* es que la
primera contiene información específica útil para realizar el *reenvío* de
paquetes, mientras que la segunda generalmente contiene una descripción
(parcial) de *alcanzabilidad* de redes y describe una *visión local* de una
parte de la red.

El problema de la construcción de *tablas de ruteo* dinámica y automáticamente,
se denomina ***ruteo*** y en éste capítulo se analizan diferentes algoritmos y
aplicaciones distribuidas (protocolos) para resolver este problema.

## Redes y grafos

Un *grafo* $G=<N,E>$, donde $N$ es un *conjunto de nodos (o vértices)* y *E* es
un *conjunto de arcos* se usa comúnmente para representar una red. Los *nodos*
representan *hosts* y *gateways* y los arcos representan *enlaces*. Los *arcos*
son *bidireccionales* y pueden tener atributos asociados como por ejemplo su
*costo*, *distancia* u otras propiedades.

Los algoritmos para *descubrir* la topología (parcial) de la red se basan en la construcción de grafos y pueden clasificarse en dos grandes grupos que se describen a continuación.

## Vector de distancias

La idea detrás de la estrategia conocida como *distance-vector algorithm* se
basa en que cada nodo conoce las distancias (inicialmente en 1) con sus vecinos
conectados directamente y con costo infinito a los demás nodos. Periódicamente,
cada nodo intercambia mensajes con sus vecinos, intercambiando sus tablas y
recalculando los caminos seleccionando los mas cortos a cada destino usando el
[algoritmo
Bellman-Ford](https://en.wikipedia.org/wiki/Bellman%E2%80%93Ford_algorithm).

Cada nodo mantiene una tabla (vector) de costos para alcanzar a los demás.

Cuando un nodo $n_i$ recibe el vector de costos de un vecino, actualiza su
vector $v[j]=min(c+1,v[j])$, para cada destino $j$.

Un nodo también puede enviar el mensaje a sus vecinos ante un evento de
detección de falla en la comunicación por alguno de sus enlaces estableciendo el
costo de llegar a su vecino es *infinito*.

Un problema de este algoritmo es que ante la caída de un nodo o un enlace, puede
darse el caso que otros nodos no actualizarán esta información ya que tienen un
camino de menor costo. 

Supongamos un conjunto de nodos conectados de la forma `A--B--C--D`. Luego de
algunas rondas de intercambios de tablas, el nodo `B` alcanza a `A` con costo 1,
`C` con costo 2 y `D` con costo 3. Aquí usamos como medida de costo el *número
de saltos*.

Ahora supongamos que `A` falla. `B` no recibe actualizaciones de `A` en un lapso
de tiempo preestablecido y actualiza el costo de alcazar `A` en $\infty$.
`B` recibe una actualización de `C`. El costo de `C` a `A` es $2 < \infty$, por
lo que `B` actualiza su costo para alcanzar `A` en 2+1=3. `B` no sabe que el
costo que recibió de `C` lo incluye en el camino.

Luego cuando `B` notifica a `C`, como `C` sabe que el costo de alcanzar a `A` se
computó desde `B`, decide actualizar su costo a `A` en 3+1. Este proceso se
repetirá y los costos crecerán, por lo que se conoce como *contando hasta el
infinito*.

Este problema puede solucionarse limitando el valor *infinito* a un valor
(pequeño) *n*, lo que representaría el *número máximo de saltos*.
La otra estrategia es enviar las rutas calculadas a un vecino cuyos resultados
no se han derivado de las rutas recibidas de él. Esta técnica se conoce como
*horizonte dividido*.

### Routing Information Protocol (RIP)

RIP se ha sido ampliamente usado en Internet. Los routers intercambian
información típicamente cada 30 segundos. Un paquete RIP incluye una tabla de
triplas $<address, mask, distance>$ y es prácticamente una implementación
directa del algoritmo descripto arriba.

## Link state

La principal diferencia de esta idea con la anterior es que la información de
cada nodo se envía a todos los demás. La técnica de *broadcast* utilizada es
la de *inundación (flooding)* de paquetes. Cada nodo actualiza su tabla de ruteo
con los caminos más cortos a partir de la información recibida de los demás.

La información se propaga ya que de cada nodo reenvía la tabla recibida de los
demás vecinos, excepto del que la recibió (*flooding*). Para que este proceso
sea confiable, es necesario que los mensajes incluyan:

- El *id* del nodo que creó el paquete.
- La lista de sus vecinos (enlaces con sus costos) directos.
- Un número de secuencia
- Un tiempo de vida (número máximo de saltos)

Los dos primeros items permiten hacer el cálculo de las rutas mínimas. El
tercero permite *filtrar* paquetes que son *copias* de otros recibidos por otros
vecinos. El tercero permite identificar paquetes con *nueva* información y descartar mensajes *viejos*. El *tiempo de vida* permite *eliminar* mensajes que pudieran aún estar circulando por la red y limitar el *alcance* de la red conocida por cada nodo, manteniendo las tablas con tamaños manejables.

### Open Shortest Path First Protocol (OSPF)

Es uno de los protocolos más usados en Internet y se basa principalmente en el
algoritmo de *link-state*.

Provee algunas características adicionales como:

- *Autenticación de mensajes*: Basado en diferentes técnicas criptográficas.
- *Particionado jerárquico* de la red: Permite la definición de *areas*
  (dominios).
- *Balance de carga*: Permite *distribuir* tráfico a destinos alcanzables por
  diferentes rutas (con el mismo costo).

## Métricas utilizadas

La noción de *costo* depende del tipo de servicio requerido. Inicialmente, en
ARPANET, una métrica utilizada fue la longitud de la cola de paquetes a ser
enviados en cada enlace. Posteriormente se introdujo como métrica el *delay*,
teniendo en cuenta la *latencia* y el *ancho de banda* del enlace.

$$ Delay = (ArrivalTime - DepartTime) + TransmissionTime + Latency $$

Los tiempos *ArrivalTime* y *DepartTime* son los tiempos de arribo y
retransmisión en el router, respectivamente. Los tiempos de transmisión y
latencia se extraen desde las propiedades del enlace utilizado.

El problema con ésta métrica es que no tiene en cuenta la *carga* o utilización
del enlace. Las métricas utilizadas actualmente incluyen la utilización,
tasa o capacidad de transmisión y tipo de enlace. Para mayores detalles, ver
[Metrics](https://book.systemsapproach.org/internetworking/routing.html#metrics).

## Ruteo interdominios

Para lograr escalabilidad en Internet, cada organismo (companía, instituciónes
educativas, gobierno, ISPs, etc) constituye un *dominio* o área y configura su
red (WAN) como un *sistema autónomo (AS)*, en el cual puede utilizar tecnologías
y sus propios protocolos de ruteo, como RIP u OSPF. Dentro de un AS el ruteo se
denomina *intradominio*.

Los ASs se conectan a un *backbone* regional, nacional o internacional. Esto
deriva en un sistema jerárquico de ruteo. Cada AS se conecta a otros ASs
mediante *external border gateways* utilizando el [Border Gateway Protocol
(BGP)](https://en.wikipedia.org/wiki/Border_Gateway_Protocol).

*BGP* califica como un protocolo de ruteo de *path vector* ya que los routers
intercambian mensajes de rutas (*advertisements*) y toman decisiones de ruteo en
base a *paths*, es decir secuencias de direcciones de ASs a redes de destino.

![bgp](img/bgp.png ':size=70% :class=imgcenter :id=bgp')

#### Figura 9.1: Ejemplo de ASs usando BGP.

BGP soporta métricas de costos en base a *políticas* definidas por cada AS, como
por ejemplo, factores económicos, tipo de tráfico o servicios, contratos con
otros ASs, etc.

BGP utiliza TCP y la versión actual se describe en la [RFC
4271](https://datatracker.ietf.org/doc/html/rfc4271).

Actualmente muchos ASs corren BGP también para el ruteo interno. Para esto se
utiliza una versión de BGP conocida como *interior BGP (iBGP)*, el cual
selecciona el subconjunto de las *mejores rutas* a los *routers exteriores*,
reduciendo el número de rutas almacenadas.

A modo de ejemplo, la [Red Interuniversitaria Nacional
(RIU)](https://riu.edu.ar/) es responsable del dominio `edu.ar` y consiste de
una red de ASs (Universidades y otros organismos) que utilizan BGP. Es posible
hacer consultas BGP en su servicio [looking
glass](https://riu.edu.ar/looking-glass/#page-content).

## Ruteo multicast

Los mecanismos de ruteo analizados hasta ahora se denominan *unicast* ya que los
paquetes tienen un único destino.

Un paquete *multicast* tiene como destino a un grupo de hosts. Un paquete
*broadcast* tiene destino a todos los nodos de la red y se utiliza
principalmente en redes locales.

Algunos protocolos a nivel de enlace, como Ethernet, tienen soporte para
*multicast* en hardware. Los protocolos a nivel de red generalmente también
deben brindar el servicio de *multicast* ya que muchas aplicaciones lo
requieren como por ejemplo las de video-conferencia.

Algunas aplicaciones usan el modelo *one-to-many*, como por ejemplo, IPTV, radio
por internet o video-broadcasting, como por ejemplo *youtube streaming*.

Otras aplicaciones, como las de video-conferencia, se basan en el modelo
*many-to-many*.

Para implementar envío *multicast* se utilizan direcciones reservadas para este
propósito. 

En redes locales de múltiple acceso, como Ethernet, su implementación es simple.
La interface *se configura* para aceptar una o más direcciones de diferentes
*grupos multicast* y el hardware *acepta* los paquetes conteniendo esas
direcciones de destino.

En redes IP es necesario implementar algoritmos en software. En el protocolo
IPv4 se utilizan direcciones *multicast* de clase *D*, en el rango 224.0.0.0 a
239.255.255.255. IPv6 también reserva un amplio rango de direcciones
*multicast*.

Una dirección *multicast* define un *grupo* de nodos. Cada nodo participante
del grupo envía un paquete IGMP con un mensaje *join* a su router inmediato. Los
protocolos [Internet Group Management Protocol
(IGMP)](https://en.wikipedia.org/wiki/Internet_Group_Management_Protocol) y
[Multicast Listener
Discovery](https://en.wikipedia.org/wiki/Multicast_Listener_Discovery), se usan
para el intercambio de mensajes entre *hosts* y *routers* para establecer
*grupos multicast* en IPv4 y IPv6, respectivamente.

La idea del ruteo multicast se basa en la construcción de *árboles de routers*,
los cuales definen sus propias *tablas de ruteo multicast*.

Analizaremos uno de los protocolos mas usados, el [Protocol Independent
Multicast (PIM)](https://en.wikipedia.org/wiki/Protocol_Independent_Multicast).

### PIM

PIM tiene varias versiones. La versión PIM-SM (*sparse mode*) es uno de los más
usados. PIM-SM asigna a cada *grupo* un router que asume el rol de *rendezvous
point (RP)*. Un *RP* actúa como la raíz del árbol de routers para ese grupo
particular. El *RP* puede configurarse estáticamente o usando el protocolo
*BootP*.

El árbol se va formando cuando un host envía un mensaje de *join G* a su router
inmediato y éste lo reenvía al *RP*, como se muestra en la [figura 9.2
a)](#pim). 

Este mensaje crea una entrada $(*,G)$ en la *tabla multicast* en cada router
intermedio, lo cual significa que recibe y debe reenviar paquetes del grupo *G*.

Cada router que participó del ruteo del mensaje *join* queda unido al árbol del
grupo, como lo muestran los arcos celestes en la figura 9.2 a) y 9.2 b).

![pim-sm](img/pim.png ':size=70% :class=imgcenter :id=pim')

#### Figura 9.2: Funcionamiento de PIM.

Supongamos que un host detrás del router *R1* envía un paquete a ese *grupo
multicast*. El router *R1 ancapsula* (*tunnels*) el paquete original en un
paquete *PIM Register*, con destino (*unicast*) al *RP*.

Cuando el *RP* recibe el paquete, lo *desencapsula* y al ver que es un paquete
*multicast* para el grupo, lo reenvía a los routers correspondientes,
(*R2*, en este caso) los cuales a su vez reenvían a los hosts del grupo.

Para evitar sucesivos encapsulamientos (lo cual afecta al rendimiento), el *RP*
envía un mensaje *join* al router *emisor* (*R1*, en nuestro ejemplo). Este
mensaje crea en cada router por donde pasó una entrada o *sender state* del tipo
$(S,G)$ el cual representa una ruta desde el *sender* al *RP*. 

A partir de aquí, los demás routers del grupo pueden advertir al *sender router*
un paquete *join (S,G)* indicando una posible ruta más corta que la ruta a
traves del *RP*.

Con estas nuevas entradas en las tablas multicast, en el ejemplo, *R1* y *R3*
podrán reenviar próximos paquetes directamente a los demás routers del grupo
vía *R2*.

El [Multicast Source Discovery Protocol
(MSDP)](https://en.wikipedia.org/wiki/Multicast_Source_Discovery_Protocol),
descripto en la [RFC 3618](https://datatracker.ietf.org/doc/html/rfc3618),
utiliza múltiples instancias de PIM-SM para diferentes dominios, conectando los
*RP* de cada dominio entre sí.

La versión *PIM-SSM (source specific multicast)* define un modelo *one-to-many*
introduciendo el concepto de *channel*. Un *host* (no un router) envía el par
$(Sender,Group)$ en un mensaje *IGMP Report Message* a su router local, el cual
lo reenvía en un mensaje *join* al *RP*. En este esquema sólo el *sender* puede
emitir paquetes al grupo.

La versión *BIDIR-PIM* permite armar árboles bidireccionales, proveyendo el
modelo *many-to-many*. Un router recibiendo un paquete de un host interno puede
reenviar hacia los routers superiores (como en PIM-SM) y por las otras ramas de
bajada en el árbol. Así, no es necesario el uso de *RP*s, sólo se requieren
*direcciones multicast* representando a los grupos. Un mensaje *join G* enviado
por un host sólo debe alcanzar al primer router que contenga una ruta al grupo
con dirección multicast *G*. 

![bidir-pim](img/BIDIR-PIM.png ':size=40% :class=imgcenter :id=bidir-pim')

#### Figura 9.3: PIM bidireccional.

En la [figura 9.3](#bidir-pim) a) se muestran los mensajes *join* enviados por
*R2* y *R3* hasta sus correspondientes routers con el grupo *G* (routers a los
que se les asignó la dirección multicast *G*), los routers
*R5* y *R6*, respectivamente. 
Estos últimos routers actúan como las *raíces* de los múltiples árboles
interconectados entre sí, tomando el rol de *RPs*.

La [figura 9.3](#bidir-pim) b) muestra cuando un host detrás de *R1* envía un
paquete multicast al grupo *G* y cómo se reenvían (flechas) a los demás routers
que participan en el grupo.

DIDIM-PIM incluye también en el árbol a los *senders*, así éstos pueden ser
receptores de otros.

BIDIR-PIM es utilizable dentro de un dominio ya que los routers *RPs* deben
configurarse apropiadamente, asignándoles las direcciones multicast asociadas a
cada grupo.

## Ruteo con dispositivos móviles

Actualmente es común que los dispositivos sean móviles, como laptops o
teléfonos con enlaces inalámbricos como wifi o 3/4/5G.

Un enfoque simple es manejar las redes con un protocolo del tipo *join/leave*,
es decir basado en operaciones de inicios/fin de sesión o conexión en cada red.
En este modelo, cada dispositivo al quedar fuera del alcance de una red el
router (su default gateway) correspondiente lo desconecta. Al entrar en el
alcance en otra red el dispositivo se *une* a la nueva red y es re-configurado
para ser un nodo de la nueva red.

En este enfoque un dispositivo móvil va cambiando su dirección de red y ante
cada movimiento desde una red a otra se produce una nueva asociación en la cual
se deben proveer las credenciales de acceso a la nueva red.

Cabe aclarar que en este escenario el dispositivo se reconfigura en cada nueva
asociación, obteniendo posiblemente una nueva dirección de red, default gateway,
DNS y otros.

En algunas aplicaciones, como telefonía celular, el identificador de cada
dispositivo de debe preservar independientemente de su localización.

Una estrategia comúnmente usada, por ejemplo en la red de telefonía móvil, es
que cada dispositivo (host) está asociado a su *home location* (localidad, dado
en el prefijo del número). Cada dispositivo está asociado a su *home host
agent*: Una especie de gateway fijo en la *home network* asociado al
dispositivo.

El dispositivo debe enviar su localización (en qué red está) al *host agent*
para que éste pueda reenviarle los paquetes cuando los reciba desde otro
dispositivo y re-establecer conexiones.