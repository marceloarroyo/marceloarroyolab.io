// functions used in web.md examples
function bg(color) {
    document.getElementById('dom-example').style.backgroundColor = color;
}

function fetchLogo() {
    fetch('img/HTML5-Logo.svg').
    then( (response) => {
        return response.ok ? response.text() : '';
    }).
    then( (text) => {
        document.getElementById('html5-logo').innerHTML = text;
    });
}

function incCounter() {
    if (!localStorage.viewsCounter)
        localStorage.viewsCounter = 0;

    localStorage.viewsCounter = parseInt(localStorage.viewsCounter) + 1;
    document.getElementById('views-counter').innerText = localStorage.viewsCounter;
}

function draw() {
    var canvas = document.getElementById('canvas-example');
    var ctx = canvas.getContext('2d');
    ctx.fillStyle = 'rgb(200, 0, 0)';
    ctx.fillRect(10, 10, 50, 50);
}