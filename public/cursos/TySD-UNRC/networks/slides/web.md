# Tecnologías web

> Las tecnologías implementadas en los navegadores y otros clientes web permiten
> el desarrollo de *aplicaciones ricas*. [HTML5](https://html.spec.whatwg.org/) 
> ofrece un *markup* evolucionado, mejor integración con 
> [CSS](https://www.w3.org/Style/CSS/specs.en.html) y 
> [Javascript](https://www.ecma-international.org/publications-and-standards/standards/ecma-262/).
> También ofrece APIs adicionales. 

- Representación y manipulación del *Document Object Model (DOM)*
- HTTP asincrónico ([AJAX](ajax) y fetch API)
- Soporte de multimedia (audio/video)
  - Acceso a cámara/micrófono y captura de pantalla
- Almacenamiento local
- SVG y Canvas (2D y 3D)
- Comunicaciones en tiempo real (websockets, ...)
- Otras: Geolocalización, notificaciones, ...

-----

## HTML5

- Marcas semánticas de estructura de la página:
  - `<header>`
  - `<nav>`
  - `<main>`
  - `<article>`
  - `<aside>`
  - `<section>`
  - `<footer>`
- Otras: `<address>`, `<time>`, `<audio>`, `<video>`, `<input>`, ...
- Diseño:
  - Documento *HTML*: Contenido y estructura
  - Hojas de estilo (*CSS*): Diseño (apariencia)
  - *Javascript*: Interactividad

-----

## Document Object Model

- Modelo de representación de un documento HTML orientado a objetos
- [API Javascript](https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model)
  que permite:
  - Obtener, crear y eliminar elementos del documento
  - Modificar propiedades y contenido de elementos
  - Interactividad mediante *eventos*

<!-- tabs:start -->

##### **HTML**

<div id="dom-example">
<button onclick="bg('gray')">Gray background</button>
<button onclick="bg('cyan')">Cyan backgound</button>
</div>

##### **Código**

```html
<div id="dom-example">
  <button onclick="bg('gray')">Gray background</button>
  <button onclick="bg('white')">Cyan backgound</button>
  <script>
    function bg(color) {
      document.getElementById('dom-example').style.backgroundColor = color;
    }
  </script>
</div>
```

<!-- tabs:end -->

-----

## Fetch API

> Permite el acceso a recursos (locales o remotos) de manera asíncrona.
> Ver [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/fetch).

<!-- tabs:start -->

##### **HTML**

<div>
  <button onclick="fetchLogo()">Fetch logo</button>
  <span id="html5-logo"></span>
</div>

##### **Código**

```html
<div>
  <button onclick="fetchLogo()">Fetch logo</button>
  <span id="html5-logo"></span>
</div>
<script>
  function fetchLogo() {
    fetch('img/HTML5-Logo.svg').
    then( (response) => {
      return response.ok ? response.text() : '';
    }).
    then( (text) => {
      document.getElementById('html5-logo').innerHTML = text;
    });
  }
</script>
```

<!-- tabs:end -->

-----

## Web storage

1. *Session storage*: El navegador almacena los datos sólo por la sesión con el 
   sitio. Al cerrar el navegador o pestaña la información se pierde.
2. *Local storage*: Los datos se preservan entre distintas conexiones.

<!-- tabs:start -->

##### **HTML**

<button onclick="incCounter()">Get counter</button>
<span> <code>counter</code> = <span id="views-counter"></span>

##### **Código**

```html
<button onclick="incCounter()">Get counter</button>
<span> <code>counter</code> = <span id="views-counter"></span>
<script>
  function incCounter() {
    if (!localStorage.viewsCounter)
      localStorage.viewsCounter = 0;
    localStorage.viewsCounter = parseInt(localStorage.viewsCounter) + 1;
    document.getElementById('views-counter').innerText = localStorage.viewsCounter;
  }
</script>
```

<!-- tabs:end -->

-----

## Almacenamiento local (cont.)

- [HTTP Cookies](https://en.wikipedia.org/wiki/HTTP_cookie):
  - Headers creadas por el servidor.
  - El cliente las almacena y retorna en los próximos accesos al sitio
  - Usado generalmente para (al igual que *WebStorage*)
    1. Manejo de sesiones (*sessionIds*)
    2. Personalización: Preferencias de usuarios, temas, ...
    3. Seguimiento (tracking) de usuarios y su comportamiento
- [IndexedDB
  API](https://developer.mozilla.org/en-US/docs/Web/API/IndexedDB_API/Using_IndexedDB)
  - Permite crear *bases de datos* de objetos (stores) con *índices* con claves
    de búsqueda.
  - Adecuado para almacenar volúmenes de datos mas grandes.
  - API
    - `let db = indexedDB.open(db, version)`
    - `db.createObjectStore(store)`
    - `let t = db.transaction([stores], mode)`
    - `t.objectStore(store).put(value, key)`, `let v =
      t.objectStore(store).get(key)`

-----

## Canvas

> El tag `canvas` permite *dibujar* por medio de una API Javascript.

- Contexto 2D
- [WebGL](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/Tutorial/Getting_started_with_WebGL)
  permite acceder a una API 3D basada en [OpenGL ES](https://www.khronos.org/opengles/).

<!-- tabs:start -->

##### **HTML**

<canvas id="canvas-example" width="150" height="150" style="border: 1px solid black"></canvas>
<button onclick="draw()">Draw</button>

##### **Código**

```html
<canvas id="canvas-example" width="150" height="150" style="border: 1px solid black"></canvas>
<button onclick="draw()">Draw</button>
<script>
  function draw() {
    var canvas = document.getElementById('canvas-example');
    var ctx = canvas.getContext('2d');
    ctx.fillStyle = 'rgb(200, 0, 0)';
    ctx.fillRect(10, 10, 50, 50);
  }
</script>
```

<!-- tabs:end -->

-----

## Web sockets

> Un [web
> socket](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API) 
> permite una conexión bidireccional entre un navegador y un servidor.

- API cliente: 
  - `let sw =new WebSocket(url)`: Crea una conexión con el server. 
    El URL debe usar el esquema `ws` o `wss` (segura).
  - `ws.send(msg)`: Envía un mensaje (string, array o blob) al server
  - Recepción de mensajes: `ws.onmessage = function(event) {
    process(event.data); }`
- Protocolo: Se inicia por HTTP con los headers `Connection: Upgrade`, `Upgrade:
  websocket` y `Sec-WebSocket-Key: <conn-key>`.

> ***Servers***: Existen bibliotecas para prácticamente todos los lenguajes de
> programación. Mas detalles en 
> [Writing WebSockets servers](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API/Writing_WebSocket_servers)

-----

## Web workers

- Scripts que corren como *threads de fondo*, sin interferir la interface de
  usuario.
  - Se crean con `let myWorker = new Worker('myworker.js')`;
- Generalmente pueden hacer entrada/salida usando la API *fetch* y operaciones
  que requieren altos tiempos de computación.
- Un *web worker* puede intercambiar mensajes con módulo Javascript que lo creó.
  1. Envío:
     - En el worker con `postMessage(msg)`
     - En el módulo js que lo creó con `myWorker.postMessage(msg)`
  2. Recepción con *event handlers* 
     - En el worker: `onmessage = function (e) {...}`
     - En el creador: `myWorker.onmessage = function (e) {...}` 

> Para más información y acceso a ejemplos usando *webworkers* visitar [Using
> Web Workers](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers)