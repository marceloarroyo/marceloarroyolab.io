# Protocolos de transporte

## Objetivos

- *End-to-end*: Comunicación entre *procesos* de hosts.
- Multiplexado/demultiplexado de paquetes desde/hacia aplicaciones o procesos.
  - Abstracción comúnmente usada: *ports* para asociar a un proceso usando el
    potocolo.
- Servicios:
  1. Orientado a *datagramas*
  2. Orientado a *conexión*

> Analizaremos los protocolos de transporte usados en TCP-IP

-----

##  User Datagram Protocol (UDP)

- Simple multiplexor sobre IP
- Orientado a datagramas:
    - Cada envío debe especificar el destino
- Servicio no confiable
  - No garantiza secuencialidad
  - No hay notificaciones sobre paquetes perdidos/rotos
- Eficiente
  - Comúnmente usado por otros protocolos como 
  - Protocolos del tipo *requerimiento/respuesta* como DNS y RPC
  - Flujos de tiempo real (VoIP, video-conferencias) como en RTP
  - Aplicaciones distribuidas: Mensajes entre *peers*

> En la API sockets, la función (syscall) `s = socket(AF_INET, SOCK_DGRAM, 0)`
> determina el uso de UDP para el socket `s`.

-----

## UDP: Demultiplexado

![udp-header](img/udp-multiplexing.png ':size=55% :class=imgcenter')

-----

## UDP datagram

![udp-header](img/udp-datagram.png ':size=60% :class=imgcenter')

- Puertos de origen y destino: Naturales de 16 bits
- *Length*: Longitud del datagrama

> El *checksum* se calcula desde la cabecera y el *pseudoheader* tomado desde la
> cabecera IP: *src/dst addresses*, *protocol* y *length*.

-----

## Asignación de puertos

En la API sockets, cada proceso (aplicación *end-point*) asigna la dirección IP
y el puerto en la `struct sockaddr`, segundo argumento de la función `bind(s,
sock_addr, sizeof(sock_addr))`.

Generalmente, un *server* asigna su *end-point*:

``` clike
sin.sin_family = AF_INET;
sin.sin_addr.s_addr = INADDR_ANY;
sin.sin_port = htons(SERVER_PORT);
```

El puerto usado dependerá de la aplicación y se conoce como *puertos bien
conocidos* (ver el archivo `/etc/services.txt`).

> También existe la aplicación *portmapper* (puerto 111) que puede usarse para *registrar* y
> *consultar* los puertos asignados a los servicios.

-----

## Transport Control Protocol (TCP)

- Orientado a conexión (ver funciones `accept/connect` de la API sockets)
- Es orientado a *secuencias de bytes*, los cuales se envían en *segmentos*
- Garantiza la entrega de datos al receptor el en mismo orden que fueron enviados
- Retransmite paquetes perdidos
- Tiene un mecanismo de *control de flujo* entre los extremos
- Incluye mecanismos de *control de congestión*

![tcp-segments](img/tcp-segments.png ':size=50% :class=imgcenter')

-----

## Segmentos TCP

![tcp-segments](img/tcp-header.png ':size=60% :class=imgcenter')

- El *SequenceNum* (en bytes) permite ordenar los segmentos en el receptor
- El campo *Acknowledgment* es válido para *reconocer* la recepción de los
  segmentos anteriores a su valor
- El campo *Flags* incluye los bits *SYN, FIN/RESET*, *ACK* y *URG*.

-----

## Conexión

![tcp-handshake](img/tcp-handshake.png ':size=60% :class=imgcenter')

#### Handshake de conexión

> Se requieren al menos tres mensajes para asegurar que los dos extremos arriban
> al mismo estado de conexión (pensar en que algunos de los mensajes podrían
> perderse).

-----

## Control de flujo: Ventana deslizante

![sliding-window](img/tcp-sliding-window.png ':size=60% :class=imgcenter')

#### Modelo de transmisión.

- El emisor planifica un *timer* por cada segmento enviado. Si éste expira
  (*timeout*), lo retransmite.
- El tamaño de la ventana representa la cantidad máxima a transmitir sin esperar
  por ACKs del receptor
- El receptor incluye en *AdvertisedWindow* el número máximo de bytes que puede
  recibir en el buffer
- El emisor bloquea al proceso si el tamaño de la ventana es cero y lo despierta
  cuando recibe un nuevo *AdvertisedWindow > 0*
- El receptor desliza a derecha *NextByteExpected* cuando recibe un segmento con
  número de secuencia contigua

-----

## Alternativas a TCP

> TCP no es muy adecuado para aplicaciones con protocolos del tipo
> *requerimiento/respuesta* como HTTP (web) o RPC ya que a veces no es necesario
> los *handshakes* de conexión y desconexión.

### QUIC

- Desarrollado por Google
- Implementado en navegadores web (browsers) modernos (Firefox, Chrome, ...)
- Caracerísticas:
  1. Simplifica el *handshake* de conexión con soporte de conexiones seguras
  2. Soporta *reconexiones* rápidas, comúnmente requeridas en dispositivos
     móviles (3/4G y wifi)
  3. Utiliza UDP por debajo

-----

## Transporte en tiempo real

- Aplicaciones multimedia en tiempo real:
  - Tasa de transferencia uniforme (tasa de reproducción)
  - Requieren secuencialidad. Los paquetes recibidos *fuera de orden*
    generalmente se descartan
  - No necesariamente confiabilidad. No es conveniente retransmitir paquetes
    perdidos.
  - Adecuación entre calidad y tasa de transferencia

    Generalmente, se adapta la *calidad* del lujo (por ejemplo: resolución de
    video) a la tasa de transferencia

> En esta categoría podemos mencionar aplicaciones de *streaming* de audio y
> video, VoIP (telefonía IP) y video-conferencias.

-----

## Real-time Transport Protocol (RTP)

- Generalmente se usa en conjunto con el *Real-time Control Transport Protocol
  (RCTP)*
- RCTP: Mensajes de control (parámetros) entre extremos
- Habitualmente RTP se usa sobre UDP

![rtp](img/rtp-header.png ':size=60% :class=imgcenter')

- Los *timestamps* sirven para la sincronización en la reproducción
- El *source identifier* representa un flujo (audio, video, ...)
- *Real Time Streaming Protocol (RTSP)*: Generalmente usa RTP

-----

## Otros protocolos de real time

- ***Real Time Streaming Protocol (RTSP)*** 
  - Permite el control del flujo con mensajes `PLAY`, `PAUSE`, `RECORD` (upload)
    y otros
  - Usado en cámaras IP y en circuitos cerrados de TV (CCTV)
  - URLs para el acceso a flujos de la forma `rtsp://domain/resource`
  - Operación:
    1. El cliente accede a un URL RTSP
    2. El servidor responde con metadatos del flujo (usando SDP)
    3. Comienza la transferencia usando RTP y RCTP

- ***Real Time Messaging Protocol (RTMP)***
  - Creado por Adobe. Liberado en 2012
  - Usa TCP y permite múltiples canales (ej: video, audio, texto)
  - Soporta seguridad y túneles HTTP

- ***HTTP Live Streaming (HLS)*** y ***DASH***
