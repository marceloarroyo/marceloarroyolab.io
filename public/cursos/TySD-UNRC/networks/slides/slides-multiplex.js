//=========================================================================
// Simple WebSockets multiplexer Docsify plugin
// Author: Marcelo Arroyo (marcelo.arroyo@gmail.com)
//=========================================================================
function slidesMultiplexPlugin(hook, vm) {
    let ws = undefined,
        myIP = '';

    // Get my external IP
    function getMyIP(cb) {
        fetch('https://api.ipify.org?format=json').
        then( (response) => response.ok ? response.text(): '').
        then( (text) => {
            let ip = JSON.parse(text);
            cb(ip.ip);
        });
    }

    // This function is called on presenter browser on click over some sidenav
    // anchor
    function go(ev) {
        let msg = { 
            cmd: "go",
            group: vm.config.multiplex.group, 
            url: ev.target.href
        };
        ws.send(JSON.stringify(msg));
        console.log('Presenter: going to: ' + msg.url);
    }

    // function called on message received by the multiplex server
    function onMessage(event) {
        let msg = JSON.parse(event.data);
        if (!msg.cmd || !msg.group || !msg.url)
            return;
        if (msg.cmd == 'go' && msg.group == vm.config.multiplex.group) {
            console.log('Received message: ' + msg.url);
            let links = document.querySelectorAll('.sidebar-nav a');
            for (let link of links) {
                if (msg.url == link.href) {
                    console.log('Going to: ' + msg.url);
                    link.click();
                    break;
                }
            }
        }
    }

    // connect to multiplex server
    hook.init( () => {
        console.log('On slidesMultiplexPlugin...');
        if (!vm.config.multiplex)
            return;
        if (vm.config.multiplex.wsserver) {
            ws = new WebSocket(vm.config.multiplex.wsserver);
            getMyIP( (ip) => {
                myIP = ip;
                if (vm.config.multiplex.presenterIp != myIP) {
                    ws.onmessage = onMessage;
                }
            });
            
            // send ping messages to maintain the connection alive
            setInterval(
                () => { ws.send(JSON.stringify({cmd: "ping"})) }, 
                30000
            );
        }
    });

    // After each markdown file is processed, set 'onclick' event in sidenav
    // anchors to go() function in presenter instance
    hook.doneEach( () => {
        if (vm.config.multiplex && vm.config.multiplex.presenterIp == myIP) {
            let slides = document.querySelectorAll('.sidebar-nav a');
            console.log('Slides: ' + slides.length);
            for (let ref of slides) {
                ref.onclick = go;
            }
        }
    });

    hook.ready( () => {
        // close sidebar nav to audience
        if (vm.config.multiplex && vm.config.multiplex.presenterIp != myIP) {
            document.querySelector('.sidebar-toggle').click();
        }
    });
}
