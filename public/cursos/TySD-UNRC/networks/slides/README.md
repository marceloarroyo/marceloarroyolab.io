# Presentaciones de clases

Estas slides son las utilizadas para cada clase dictada durante el curso. Cada
una se corresponde con los capítulos o secciones de las <a href="../index.html">notas del
curso</a>.