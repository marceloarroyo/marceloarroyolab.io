# Aplicaciones

- Una aplicación en red debe definir su propio protocolo
  1. Formato de los mensajes
  2. Nombrado de recursos
  3. Coreografía: Sistema de transición de estados que describe las secuencias
     de comunicaciones entre las partes
     - Cliente/servidor
     - Peer-to-peer
     - Híbrido

> ***Tipos de aplicaciones***
> - De soporte o *infraestructura* (DNS, SNMP, OpenConfig, ...)
> - Aplicaciones tradicionales
> - Aplicaciones multimedia
> - Sistemas distribuidos (sobre *overlay networks*)

-----

## Infraestructura

### Domain Name Service (DNS)

- Función: $ DNS: name \rightarrow value $

  (generalmente un valor es una dirección IP)
- Implementación: Base de datos distribuida (jerárquica)

![domains](img/domains.png ':size=75% :class=imgcenter')

#### Espacio de nombres jerárquico (*dominios*)

-----

## DNS (cont.)

- ***Name server***: Aplicación que responde a peticiones de DNS
  - El espacio de nombres se divide en *zonas*
  - Un *name server* contiene la información de su *zona*. Ejemplo: El DNS de la
    UNRC es responsable (*authority*) de la zona `unrc.edu.ar`
- ***DNS records***: La información dentro de una zona consiste en un conjuno de
  *registros* de la forma `(name, value, type, class, ttl)`
  
  Algunos tipos de registros:
  - `NS`: Nombre del host que está corriendo el servicio de DNS de la zona
  - `CNAME`: Nombre (canónico) de un host (alias)
  - `A`: Dirección IP de un host
  - `MX`: Host que corre el servicio de correo electrónico (*mail exchanger*)
    del dominio

-----

## Resolución de nombres

![name-resolution](img/name-resolution.png ':size=70% :class=imgcenter')

-----

## Administración y monitoreo

- ***Simple Network Management Protocol (SNMP)***
  - Protocolo de tipo *requerimiento/respuesta*. Comandos: `GET` y `SET`
  - Cada nodo SNMP mantiene un *estado*: Conjunto de *variables*, donde cada una
    tiene un *valor*
- ***OpenConfig***

  ![open-config](img/open-config.png ':size=50% :class=imgcenter')

-----

## Aplicaciones tradicionales

- Correo electrónico (e-mail)
  - *Simple Mail Transport Protocol (SMTP)*
  - Protocolos de acceso y gestión de *mailboxes* (bandejas)
    - ***Post Office Protocol (POP-3)***
    - ***Internet Message Access Protocol (IMAP)***
- Transferencia de archivos
  - ***File Transfer Protocol (FTP)***
- Login remoto
  - Telnet, rlogin
  - Secure Shell (ssh)
- ***World Wide Web (WWW)***
  - *HyperText Transport Protocol (HTTP)*
  - HTML, CSS, Javascript, multimedia, ...
  - Web services
- Otros: chat, aplicaciones multimedia, ...

-----

## Correo electrónico

![email](img/Email.svg ':size=80% :class=imgcenter')

-----

### Ejemplo de una sesión SMTP

```
HELO cs.princeton.edu
250 Hello daemon@mail.cs.princeton.edu [128.12.169.24]

MAIL FROM:<Bob@cs.princeton.edu>
250 OK

RCPT TO:<Alice@cisco.com>
250 OK

RCPT TO:<Tom@cisco.com>
550 No such user here

DATA
354 Start mail input; end with <CRLF>.<CRLF>
Blah blah blah...
...etc. etc. etc.
<CRLF>.<CRLF>
250 OK

QUIT
221 Closing connection
```

-----

## World Wide Web

![http](img/http_diagram.png ':size=60% :class=imgcenter')

- Protocolo de tipo *requerimiento/respuesta*
  - *Requests*: `GET`, `POST`, `PUT`, `DELETE`, ...
  - *Response codes*: 1xx (info), 2xx (success), 3xx (redirection), 4xx (client
    error), 5xx (server error)
- Comúnmente opera sobre TCP

-----

## URLs


![http](img/url-object.svg ':size=70% :class=imgcenter')

#### Uniform Resource Locator

- Recursos (objetos)
  - Documentos *HTML*: Incluyen contenido textual, *referencias (links)* a otros
    objetos, estilos de presentación (*CSS*), programas (Javascript), ...
  - Objectos multimedia (gráficos, sonido, video, ...)

-----

## Web services

- Acceso a recursos/información por medio de requerimientos HTTP
- Uso de HTTP para implementar RPC
- Tecnologías:
  - ***SOAP***: Protocolo del tipo *requerimiento/respuesta*
    - Usa XML como formato de mensajes
    - APIs definidas usando *WSDL*
  - ***REST***: Diseñado para usar la infraestructura de la web
    - Uso de los *verbos* HTTP para invocaciones remotas
    - URIs para identificar recursos/operaciones
    - JSON como formato de valores (argumentos y respuestas)
    - Sin estado
    - Ejemplo: `https://ipapi.co/201.252.199.204/json/`
- Muchos servicios requieren que el cliente adquiera una *API key*

-----

## Aplicaciones multimedia

- Sesiones y llamadas (audio/video)
  - ***Session Initiation Protocol (SIP)*** y ***H.323***
    - Localización de usuarios
    - Disponibilidad (estado) de usuarios
    - Establecimiento de una *sesión* (llamada)
- ***Session Description Protocol (SDP)***
  - Información sobre la sesión

```
v=0
o=larry 2890844526 2890842807 IN IP4 128.112.136.10
s=Networking 101
i=A class on computer networking
u=http://www.cs.princeton.edu/
e=larry@cs.princeton.edu
c=IN IP4 224.2.17.12/127
t=2873397496 2873404696
m=audio 49170 RTP/AVP 0
m=video 51372 RTP/AVP 31
m=application 32416 udp wb
```

Ejemplo de un mensaje SDP de una sesión.

-----

## WebRTC

- Comunicaciones en tiempo real por la web (browser a browser)
- Implementado en Browsers (WebRTC API): Acceso a micrófono y cámara. 
  Objetos para representar conexiones y streams (*channels*)

![webrtc](img/webRTC.png ':size=75% :class=imgcenter')
