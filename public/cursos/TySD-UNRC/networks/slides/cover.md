# Telecomunicaciones y Sistemas Distribuidos :id=covertitle

## Marcelo Arroyo

[Departamento de Computación](https://dc.exa.unrc.edu.ar/) - [FCEFQyN](https://www.exa.unrc.edu.ar/)

[Universidad Nacional de Río Cuarto](https://www.unrc.edu.ar/)
![](../img/escudo-unrc.png ':id=escudo-cover')

## Slides

![](../img/cover.jpg)