# Representación de datos

![encoding](img/presentation-encoding.png ':size=60% :class=imgcenter')

#### Codificación (marshalling o serialización) y decodificación.

-----

## Datos binarios

- Problema: *Endianness*

![endianness](img/endianness.png ':size=60% :class=imgcenter')

#### Representación en memoria del entero 34677374.

-----

## Marshalling o serialización de datos

1. Uso de *marcas* para identificar valores de un tipo determinado
   - Tipos primitivos: `(type_tag, value)`
   - Estructuras: `(struct_tag, fields_count, fields...)`
   - Arreglos: `(array_tag, base_type_tag, size, data...)`
   - Punteros: Uso de *ids* y *referencias* (ej: `#employee-5`)

2. Sin marcas: La aplicación *conoce* el formato de cada mensaje. 

> ***Remote Procedure Call (RPC)***: Invocación a funciones desde un cliente al
> servidor en forma transparente para el programador.
> Compilación de *stubs* a partir de la API definida en un ***Interface Definition
> Language (IDL)***

-----

## Remote Procedure Calls

![rpc](img/rpc-stubs.png ':size=50% :class=imgcenter')

#### Remote Procedure Call (RPC) stubs.

-----

## Algunos ejemplos

- ***External Data Representation (XDR)***: Formato usado por SunRPC
  - Soporta los tipos de datos de C
  - Formato binario
- ***ASN.1***: *Abstract Syntax Notation One* es un estándar ISO para datos a
  transmitir por red. 
  
  Cada dato se representa por una tripla `(tag, length, value)`
- ***Lenguajes de marcado (XML)***
  - Formato textual
  - Uso de *marcas* o *tags* de apertura y cierre
  - Cada tag puede tener *atributos*
  - Cada lenguaje basado en XML se define por medio de un *esquema* (ej: en
    *XSD*)
- ***Javascript Object Notation (JSON)***: Basado en literales (tipos básicos) y
  agregados (arrays, objects)

-----

## Datos multimedia

> Transmitir un cuadro de video de HDTV con 1080x1920 pixels, cada uno de 24
> bits (RGB) se requiere 50Mb. A una tasa de 24 cuadros por segundo demanda una
> tasa de transmisión de más de 1Gbps.

### Compresión

1. Sin pérdida de información: Transmisión de documentos, ...
   - Códigos *Huffman*: Los símbolos mas frecuentes se codifican en menos bits
   - Run length encoding (LRE): `AAABBCDDDD --> 3A2B1C4D`
   - Differencial Pulse Code Modulation: `AAABBCDDDD --> A0001123333`
   - Basados en diccionarios: Lempel-Ziv, ...

     Cada palabra o caracter se reemplaza por su índice en un *diccionario*
2. Con pérdida de información: Transmisión de audio, video, ...

-----

## Compresión de imágenes

- ***Graphical Interchange Format (GIF)***
  
  Cada pixel (RGB) de 24 bits se reemplaza por un *índice* de 8 bits de una
  *paleta* de 256 colores. Los colores de la *paleta* son aproximaciones a los
  colores originales.
- ***Joint Photographic Experts Group (JPEG)***
  1. RGB --> YUV 
  2. ***Discrete Cosine Transformation (DCT)***
  3. ***Cuantización***: Pérdida de bits menos signiicativos
  4. ***Codificación***: Usando un recorrido en *sig-zag* de la matriz aplicando
     RLE y Huffman

     ![jpeg](img/JPEG-compression.png ':size=50% :class=imgcenter')

-----

## Compresión de video (MPEG)


![mpeg](img/MPEG-compression.png ':size=50% :class=imgcenter')

- Cada frame se procesa dividiéndolo en *macroblocks* de 16x16 píxeles
- Frame *I* (intra-picture): Frame JPEG original
- Frame *P* (predicted): Diferencias con el I-frame anterior
- Frame *B* (bi-directional): Interpolación entre los frames I o P previo y
  anterior

> MPEG puede alcanzar un radio de compresión de 90:1. La transmisión de frames
> generalmente se intercalan: La secuencia de la figura se transmite en el orden
> `I P B B I B B`.
>
> Para pensar: ¿Qué sucede si se pierde un I-frame versus un P/B-frame?

-----

## Transmisión de video

- Un video se almacena en un *contenedor (archivo) multimedia* (.mp4, .ogv, ...)
- Streaming adaptivo:
  - Se almacenan diferentes archivos de un video con diferentes *calidades*
  - Los protocolos permiten *requerir* un cierto número de frames de cierta
    calidad
  - Si cambia la tasa de transmisión del enlace, se va *cambiando* de calidad
  - Es lo que hacen los protocolos sobre HTTP como DASH y HLS

### Otros estándares

- ***H series***: Definidos por el *Telecommunication Standarization Sector
  (ITU)*
  - H.261, H.263: Muy similares a MPEG
  - H.264/MPEG4: Estándar unificado

-----

## Compresión de audio

- Audio digital de calidad de CD:
  - Frecuencia de *muestreo* (*sampling*) de 44.1KHz. Cada muestra es un valor
    de 16 bits
  - En estéreo (2 canales) se requiere $2 \times 44.1 \times 1000 \times 16 =
    1.41$ Mbps
- El audio en telefonía se muestrea a 8KHz con muestras de 8 bits (64kbps)
- ***MP3***: Similar a los pasos de JPEG
  - Separación de *sub-bandas* (diferentes frecuencias)

    Las sub-bandas y el número de bits a usar para cada muestra se basan en
    modelos acústicos
  - Cada sub-banda se divide en bloques
  - Cada bloque se transforma (usando un DCT modificado), se cuantiza y codifica
  - Los bloques se intercalan con los de video en MPEG
