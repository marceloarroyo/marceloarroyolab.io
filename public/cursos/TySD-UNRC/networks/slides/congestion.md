# Control de congestión

- Las aplicaciones *compiten* por los recursos de la red
- Los hosts y routers deben *encolar* paquetes antes de ser transmitidos o
  reenviados
- Cuando esas colas alcanzan ciertas longitudes, se incrementa el *round trip
  time (RTT)* y puede afectar a la duración de los *timeouts* usados
- En este punto, se dice que se ha producido *congestión*

> El *control de congestión* tiene en cuenta los routers intermedios, mientras
> que en el *control de flujo* sólo interactúan los extremos o *end points*

### Técnicas

1. Detección de congestión y reducir la tasa de transmisión
2. Evitar la congestión, detectando el estado de routers
3. Clasificar paquetes en base al tipo de aplicación y usar colas de espera con
   diferentes prioridades. Técnicas conocidas como *QoS (calidad de servicio)*

-----

## Control de congestión en TCP

- TCP mantiene una variable de estado: `CongestionWindow`
- Cantidad máxima de bytes a transferir:

  $$ TxMax = MIN(CongestionWindow, AdvertisedWindow) $$

### Slow start

1. Inicio con incremento exponencial. Por cada ACK recibido se duplica el valor
2. Ante el primer *timeout* se reduce el valor a la mitad
3. Luego por cada ACK se incrementa linealmente (de a uno)

![slow-start](img/TCP-congestion-control.png ':size=60% :class=imgcenter')

-----

## Mejoras al algoritmo original

- Problema: Tiempos de inactividad mientras se espera por *timeouts*
- Algoritmo ***Fast retransmit***:
  1. El receptor al recibir un segmento *fuera de orden* retorna el último ACK
     enviado
  2. El emisor, luego de recibir 3 ACKs duplicados, reenvía los segmentos
     anteriores (sin esperar sus *timeouts*)
- ***CUBIC*** (Google, implementado en GNU-Linux 2.6.19 en 2006)
  
  Ajusta periódicamente la ventana de congestión $CW$:

    $ CW(t) = C \times (t-K)^3 + W_{max} $

    donde, $ K = \sqrt[3]{W_{max} \times (1 - \beta) / C} $ 

    *C* es el *factor de escala* y $\beta=0.7$ es el factor de decrecimiento. 
    $W_{max}$ es el máximo valor alcanzado por la ventana de congestión.

-----

## Otros algoritmos

- *DECbit*: Los routers congestionados setean el *congestion bit*.
  1. El receptor incluye el valor del bit en el ACK
  2. El emisor cuenta los ACKs con el bit en 1
  3. Si la cantidad es menor al 50% de los segmentos enviados, el emisor
     incrementa linealmente el valor de la ventana de congestión, sino se
     decrementa por 0.875.

- *Random Early Detection (RED)*: Los routers notifican implícitamente al emisor
  *descartando* paquetes. Así el emisor recibirá ACKs duplicados u ocurrirán
  timeouts

- *Notificaciones explícitas*: Similar a DECbit usando bits del campo *TOS* de
  IP y bits no usados en TCP para incluir información extra.

- *Source based*: El emisor mide las variaciones de RTT y varía el tamaño de la
  ventana de congestión. En esta categoría se pueden mencional algoritmos como
  *TCP Vegas*, *TCP BBR* y *CDTCP*
