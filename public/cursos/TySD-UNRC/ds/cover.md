# Telecomunicaciones y Sistemas Distribuidos / Redes y Telecomunicaciones :id=covertitle

## Notas del curso. Parte 2: Sistemas distribuidos

### Marcelo Arroyo

[Departamento de Computación](https://dc.exa.unrc.edu.ar/) - [FCEFQyN](https://www.exa.unrc.edu.ar/)

[Universidad Nacional de Río Cuarto](https://www.unrc.edu.ar/)
![](img/escudo-unrc.png ':id=escudo-cover')

## Notas del curso

[Comenzar](README.md)

![](img/cover.jpg)
