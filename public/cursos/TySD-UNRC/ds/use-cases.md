# Casos de estudio

## Google file system

Todos los servicios (apps) de Google que requieren gestión de almacenamiento de
archivos se basan en los servicios provistos por el ***GFS***.

![gfs](img/GoogleFileSystemGFS.svg ":size=70% :class=imgcenter")

#### Figura 5.1: Arquitectura de GFS.

El sistema se basa en un conjunto de _clusters_ interconectados. Los clientes
(apps) se conectan a alguno de ellos.

La figura anterior muestra un esquema de su arquitectura. Cada archivo se
particiona en _chunks_ de 64Mb. Cada _chunk_ se replica en al menos tres _chunk
servers_. Un nodo _master_ contiene los metadatos de cada archivo, lo cual
incluye la lista de _chunks_ y los servidores que los contienen (_chunk
mappings_). 

Una aplicación (cliente) contacta inicialmente al _master_ para obtener los
metadatos de un archivo. Luego la aplicación puede requerir _chunks_ desde
cualquier _chunk server_.

El nodo _master_ comúnmente tiene réplicas para distribuir la carga de trabajo y
proveer tolerancia a fallas.

[GFS
paper](https://static.googleusercontent.com/media/research.google.com/en//archive/gfs-sosp2003.pdf).

## Edición de documentos en tiempo real

La edición simultánea de documentos es un servicio provisto por varias
aplicaciones. Uno de los problemas a resolver es la edición en redes con alta
latencia como Internet.

Una de las técnicas mas utilizadas es la _transformación de operaciones_,
ilustrada en la siguiente figura.

![ot](img/ot.png ":size=60% :class=imgcenter")

#### Figura 5.2: Ejemplo de transformación de operaciones.

Las operaciones son secuencias de `insert(p,c)` y `delete(p,c)`, donde `c` es un
caracter y `p` es una posición o índice.

La idea consiste en que dada una operación ejecutada localmente se transmite a
los demás editores. En la recepción de operaciones, cada proceso secuencializa
las operaciones locales y remotas recibidas _modificando las posiciones_ de
algunas operaciones, tal como se muestra en la figura.

Dependiendo del ordenamiento usado, se puede lograr diferentes tipos de
consistencias. Cada operación puede incluir un _timestamp_ que determina el
orden basado en relojes lógicos. Si se usan relojes vectoriales puede lograrse
_consistencia secuencial_.

## Blockchain

Un _blockchain_ es una secuencia de bloques de datos extensible (sólo permite
operaciones _append_). Cada bloque comúnmente contiene _transacciones_, un _hash
criptográfico_ de los datos del bloque mas el hash del bloques anterior, y un
_nounce_ (valor usado por única vez). 

El _hash_ forma un _timestamp_ distribuido ya que prueba que el bloque es el
último.

![blockchain](img/blockchain.png ":class=imgcenter")

#### Figura 5.3: Secuencia de bloques (blockchain).

Esta secuencia de bloques comúnmente representa un _log de transacciones_ y se
usa en _criptomonedas_ como [bitcoin](https://bitcoin.org/) y
[ethereum](https://ethereum.org/) permitiendo replicar el _log_ en una red
_peer-to-peer_ abierta usando un protocolo que garantiza su _inviolabilidad
(integridad)_.

Cada bloque incluye _transacciones_. En el caso de criptomonedas, representan
transferencias entre dos usuarios o cuentas. Cada transacción generalmente está
firmada digitalmente por el _ejecutor_ de la transacción.

Cada usuario, quien utiliza un programa cliente conocido como su _billetera
(wallet) virtual_, tiene su par de claves pública y privada. La clave pública
actúa como su identificador.

Los clientes generan transacciones que son comunicadas a los demás nodos de la
red. Cada nodo recolecta las transacciones para ser incluidas en un nuevo
bloque. Un bloque tiene un límite de transacciones.

> Recordar que una _firma digital_ de un documento (transacción en éste caso)
> consiste en un _hash criptográfico_ del mismo cifrado con la _clave privada
> del firmante_. Esto permite que se pueda validar descifrando la firma con la
> _clave pública del firmante_ y comparando el hash obtenido con el cómputo del
> hash del documento. Si estos hashes coinciden, el documento es íntegro y ha
> sido emitido por el firmante.

En algunos frameworks de blockchain (Bitcoin, Ethereum, ...) las transacciones
se generan en base a la ejecución de _scripts_ definidas en la aplicación
distribuida, conocidas como _dapps_. Estos frameworks también permiten definir
_smart contracts_, los cuales se basan en eventos o tareas programadas que
disparan la ejecución de _scripts_ y permiten implementar las reglas
(dependientes de la aplicación) del sistema.

Un protocolo basado en blockchain usa algún algoritmo de consenso por mayoría
(quorum). Un nodo que ha recolectado un número dado de transacciones puede
incluirlas en un nuevo bloque y proponiéndolo (broadcast) a los demás. 

Cada nodo que recibe una propuesta, su aceptación se representa por la inclusión
del nuevo bloque en su copia local del blockchain, previa verificación del
bloque, lo cual incluye la validación de cada transacción y los metadatos (nro,
nounce y hashes) del bloque.

En el caso que dos o más nodos propongan concurrentemente nuevos bloques a los
demás _peers_, el bloque que posteriormente logre mayor aceptación gana. Un nodo
que recibe más de un bloque válido concurrentemente, los incluye (acepta
temporalmente) en diferentes _ramas (forks)_. Luego, al recibir un nuevo bloque,
éste se adiciona a la rama correspondiente (en base al hash del bloque
anterior). Con el tiempo, luego de pocas rondas, la rama más larga ganará y se
transforma en la *cadena canónica*, como se muestra en la siguiente figura.

![branches](img/blockchain-branches.png ":size=70% :class=imgcenter")

#### Figura 5.4: Diferentes ramas (temporales) en un blockchain.

Los nodos que proponen nuevos bloques se denominan _mineros_ ya que deben
lograr generar un bloque cuyo valor _hash_ tenga ciertas propiedades,
típicamente una cierta cantidad de ceros al comienzo. Esto hace que un _minero_
tenga que encontrar un _nonce_ que permita lograr el hash requerido. Esta
computación se logra mediante un algoritmo de _fuerza bruta_ (prueba y error),
por lo que esto se conoce como _prueba de trabajo_, dado que el nodo que propone
un nuevo bloque válido ha invertido un considerable poder de cómputo para
lograrlo.

En este modelo la aceptación de un nuevo bloque por el sistema no se logra por
un sistema de votación, sino que se logra implícitamente por la adición del
bloque en cada copia local de cada participante, logrando así mantener las
réplicas sincronizadas.

La integridad del blockchain se logra ya que si un atacante quiere modificar
algún bloque intermedio del blockchain, deberá modificar los subsiguientes, y
deberá lograr su validación de la mayoría. En el caso de _proof of work_ esto
significa que deberá tener un poder de cómputo superior a la mayoría de los
mineros. El modelo hace que no convenga ser un nodo malicioso, ya que si tiene
suficiente poder de cómputo, es conveniente ser un minero lícito.

Cada minero al lograr que su bloque propuesto sea aceptado por el sistema,
obtiene una recompensa, mediante la creación de nuevas monedas (si el sistema
está aún en un estado inflacionario) y/o por el cobro de _comisiones_ a los
usuarios que participan en las transacciones incluidas en el bloque.

La técnica _blockchain_ puede usarse en cualquier escenario que requiera un
sistema de seguimiento de transacciones distribuidas en logs replicados
inviolables como por ejemplo, sistemas de expedientes, trazabilidad de productos
y otros.

Para lograr comprender mejor el funcionamiento de un _blockchain_ ver los videos
en [Blockchain demo](https://andersbrownworth.com/blockchain) y experimentar en
las diferentes secciones del sitio (hash, block, blockchain, ...).

### Algoritmos de consenso

El algoritmo descripto (conocido como _proof of work_) requiere grandes consumos
de energía dado que se basa en una competencia de poder de cómputo entre los
mineros. Existen alternativas de consenso basados en otros conceptos.

- _Proof of stake/importance (prueba de apuesta/importancia)_: Se basa en que un
  nodo tiene una mayor participación (confianza) basado en las cantidad de
  _tokens_ que posee. En las cripto-monedas tiene la desventaja que induce a una
  estrategia de ahorro más que en operaciones de gastos.
- _Proof of Authority_: Se basa en que un conjunto de nodos son _validadores_
  que ganan su rol por medio de la _confianza_ de los demás nodos. Se basa en
  una idea similar a _web o trust_ usada en PGP aunque requiere una organización
  algo más centralizada.
- _Basados en Byzantine Agreement_: Algoritmos basados en consenso de mayoría
  (votación) como los estudiados previamente, como por ejemplo Paxos o algunas
  de sus versiones.

> Se debe hacer notar que estos protocolos proveen _consistencia eventual
> temporal_ ya que en un momento dado puede haber varias ramas diferentes en las
> réplicas. Si se toma en cuenta el blockchain descartando las posibles ramas
> temporales (vistas como un estado interno temporal) y asumiendo que la
> operación _write (append)_ conceptualmente se realiza luego de la selección de
> la rama más larga, es posible concluir que ofrece _consistencia secuencial
> fuerte_.

## Referencias

- [Concurrence Control in Groupware
  Systems](https://www.lri.fr/~mbl/ENS/CSCW/2012/papers/Ellis-SIGMOD89.pdf).
  Ellis, Gibbs. ACM 1989.
- [Bitcoin](https://bitcoin.org/bitcoin.pdf). Satoshi Nakamoto. Bitcoin
  whitepaper.
- [A survey of Consortium Blockchain Consensus
  Mechanisms](https://arxiv.org/pdf/2102.12058.pdf). Wei Yao, et al. 2021.