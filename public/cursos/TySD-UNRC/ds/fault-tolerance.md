# Tolerancia a fallas

La tolerancia a fallas es una propiedad indispensable para lograr ***sistemas
confiables (dependable systems)***, los cuales deben garantizar:

1. _Disponibilidad_
2. _Confiabilidad_
3. _Seguridad_
4. _Mantenibilidad_

Una falla en el sistema se debe por el malfuncionamiento tanto en el software
como en el hardware de algún nodo o enlaces de la red. Hay diferentes tipos de
fallas como se muestran en la siguiente tabla.

Tipo de falla | Descripción                                        
------------- | --------------------------------------------------
Caída (crash) | Caída del sistema con funcionamiento previo normal
Omisión       | Fallas en recepciones/envíos de mensajes
Timeouts      | No responde dentro de los límites establecidos
Respuesta     | Responde incorrectamente
Arbitraria    | El nodo falla en intermitentemente

#### Tabla 4.1: Tipos de fallas.

Las fallas de tipo *respuesta* se conocen como *fallas bizantinas*. El término
se debe al paper de Leslie Lamport quien fue un pionero en el estudio de este
tema. Un nodo que tiene *fallas de respuesta* puede representar un *atacante* en
la red ya que no sigue las reglas del protocolo o algoritmo y se dice que el
nodo es *malicioso* o *traidor*. Este modelo de fallas se usa en análisis de
seguridad.

## Enmascarado de fallas

Una forma de ocultar (_enmascarar_) fallas es por medio de redundancia. Es muy
usado en hardware y común en sistemas distribuidos para soportar fallas en nodos
o enlaces.

Una forma de replicación de software común en sistemas distribuidos se conoce
como _process resilience (resistencia)_ y se basa en la replicación de procesos
formando grupos. Cada grupo puede organizarse según alguna de las topologías
virtuales ya descriptas.

## Consenso

En un grupo de procesos replicados, si existen nodos con fallas la respuesta del
sistema debería lograr un _consenso_ sobre la operación ejecutada
concurrentemente en las réplicas. Este consenso puede lograrse mediante un
algoritmo de _inundación (flooding)_ o mediante mensajes desde un _coordinador_,
aunque es difícil de lograr en presencia de fallas arbitrarias.

En presencia de *fallas bizantinas* comúnmente se deberá lograr consistencia en
coincidir en algún valor replicado o en una decisión en un sistema sin
coordinación central mediante algún algoritmo distribuido de consenso.

Para ilustrar el problema Lamport planteó el *problema de los generales
bizantinos*. En este problema hay al menos tres generales que deben sitiar una
ciudad protegida. Sólo puede garantizarse la victoria si al menos una mayoría de
generales coincide en atacar en forma simultánea. El problema es que algunos
generales pueden ser *traidores* o estar bajo el control del enemigo.

El objetivo es que los generales *honestos* coincidan en la decisión de atacar o
no.

> En 1988, Dwork y oros demostraron que para lograr _consenso por mayoría_ en
> presencia de hasta $k$ fallas arbitrarias (_Bizantinas_) se requieren al menos $3k+1$ nodos.

### Paxos

Leslie Lamport en 1989, publicó el artículo [The Part-Time
Parliament](https://lamport.azurewebsites.net/pubs/lamport-paxos.pdf), donde
describe el algoritmo conocido como _Paxos_ (por la isla Griega). 

Los nodos en el sistema pueden tener los siguientes roles:

- _Client_: Realiza requerimientos de operaciones a un server que se
  convertirá en el _proposer_.

- _Proposer_: Servidor que acepta el requerimiento del cliente y será el _líder_
  de inicio del consenso.

- _Acceptor_: Servidores que aceptan (o _votan_ por) el requerimiento recibido.

- _Learner_: Proceso que ejecutará el requerimiento al recibir mensajes desde
  una mayoría de _acceptors_.

Es común que los roles _proposer_, _acceptor_ y _learner_ se implementen en el
mismo proceso.

![paxos](img/paxos.png ":size=60% :class=imgcenter")

#### Figura 4.1: Arquitectura de Paxos.

> *Definición*: Un _quorum_ es un conjunto de una _mayoría_ de nodos del sistema. 

Paxos asegura las propiedades de _conditional liveness_ si existen suficientes
procesos sin fallas. También garantiza _safety_ en el sentido que la misma
operación será ejecutada (_learned_) por los procesos que no fallan.

El algoritmo se puede describir en los siguientes pasos.

***Fase 1***:

1. Un cliente envía a $P$ una propuesta (u operación) $p$.

2. _Preparación_: $P$ asume el rol de _proposer_ y envía a un _quorum_  
   de  _acceptors_ `PROPOSAL(p,n)` donde $n$ es el _reloj lógico (escalar)_ de $P$.

3. _Promesa_: Un _acceptor_ puede recibir múltiples propuestas (de diferentes 
   _proposers_). Sea `PROPOSAL(p,n)` recibida de $P$:
   
   1. $n$ es el mayor de todos los recibidos previamente: Envía `PROMISE(p,n)`
      a $P$, indicando que ignorará `PROPOSAL(p',n')` con $n' < n$.
   
   2. Si ya había aceptado una `PROMISE(p',n')` con $n' < n$ responde a $P$ `PROMISE(p',n')`.

   3. Aún no aceptó $(p,n)$ pero recibió previamente `PROMISE(p',n')` con
      $n<n'$, responde con `NACK` (negative ack).

***Fase 2***:

1. *Aceptar*: Si $P$ recibe `PROMISE(q,m)` de un _quorum_ de _acceptors_, envía 
   `ACCEPT(q,m)` a los _acceptors_. En otro caso, no hay concenso y se aborta el intento.

2. *Aceptada*: Cuando un _acceptor_ recibe `ACCEPT(q,m)` de $P$ y no había
   enviado `PROMISE(q',m')` con $m' < m$, envía `LEARN(q)` a los _learners_ y
   `ACCEPTED(q,m)` a $P$. Sino, no hace nada, no participando del quorum de
   _acceptors_.

3. Un _learner_ al recibir `LEARN(q)` desde los _acceptors_, procesa (o acepta) $q$ y 
   notifica al cliente.

### Análisis de Paxos

El protocolo dado asume que hay un único _leader_. En el caso que existan más de
un _leader_ puede suceder que _compitan_ por un mismo `PROPOSAL(p,n)`, en cuyo
caso podrán fallar los intentos de lograr concenso no garantizando _progreso_
pero aún garantizando _safety_.

De hecho, es posible usar _Paxos_ para _elección de líder_ (donde $p$ es
_elect_), así permitiendo elegir un único _proposer_.

Es _tolerante a fallas_ tanto en el _proposer_, _acceptors_ y _learners_. Se
deja como ejercicio, analizar diferentes escenarios de fallas.

### Raft

Una alternativa a Paxos es el algoritmo propuesto en 2014 por Diego Ongaro y
Ousterhout conocido como _Raft_ (por *Reliable, Replicated, Redundant and
Fault-tolerant*).

Se basa (como Paxos) en lograr concenso basado en máquina de estado replicadas.
Cada proceso contiene una máquina de transición de estados y un _log_. La
máquina de estados representa el componente del sistema que deseamos que sea
tolerante a fallas y aparece ante los clientes como una única máquina de estados
aún en presencia de fallas minoritarias de nodos. El log actúa como una cola
de comandos a ejecutar por la máquina de transición de estados.

El algoritmo se asegura que si una máquina ejecutó la operación *n-ésima* del
log ninguna otra aplicará otro *n-ésimo* comando.

Un cluster Raft contiene un conjunto de servers. Cada server contiene su copia
del log, que es una secuencia de pares `(cmd,term)`. El tiempo del sistema
avanza por intervalos denominados `terms` (una especie de reloj lógico escalar).

Cada *server* en un momento dado puede estar en uno de los siguientes estados: 

- *leader*: Maneja los requermientos de clientes. Cualquier *server* que recibe
  un mensaje de un cliente, lo redirige al *leader*. El líder es quien inicia
  una transacción distribuida (*commit*) a los *followers*.
- *follower*: Responden a requerimientos del *líder* y de los *candidates*.
- *candidate*: Un server que detectó la falla del líder e inició una elección de
  líder.

Raft divide el tiempo en *terms*, numerados con enteros consecutivos. Cada
*term* inicia una *elección de líder* y también actúa como un reloj lógico.

1. ***Inicialización***: Los servers comienzan en el estado *follower*.

2. ***Elección***: El líder envía periódicamente un mensaje `heartbeat(term)` a
   los *followers*. Cada *follower* luego de recibir un *heartbeat* reinicia su
   *timeout* con un valor aleatorio. Cuando un *follower* $p_i$ no recibe un
   *heartbeat* antes de su *heartbeat timeout*, inicia una *elección*, cambiando
   su estado a *candidate*, incrementa su *term* y envía un mensaje
   `leader(term)` a los demás junto con su log para que algún follower no
   sicronizado actualice su copia. 
   
   Si recibe una mayoría de votos positivos se
   convierte en el nuevo *lider*, enviando periódicamente el *heartbeat*. Un
   *follower* vota positivamente si el *term* recibido del *candidate* es mayor
   al suyo y no ha respondido a otro *candidate* con ese *term*. En el caso que
   no reciba una mayoría de votos positivos, incrementa el *term*, espera un
   tiempo aleatorio y luego inicia una nueva elección. Si recibe un mensaje
   `append(entries)` con *term* mayor que el de él, pasa a estado *follower*,
   asumiendo que otro líder ganó una elección iniciada concurrentemente.

3. ***Replicación del log***: El líder acepta un comando *cmd* de un cliente y
   envía un mensaje `append(entries)` a cada *follower*. El líder mantiene una
   variable *lastIndex* aceptado (*commited*) por cada follower. *entries*
   contiene la secuencia de entradas del log desde el *lastIndex*. Si el líder
   recibe una mayoría de mensajes `ok`, les envía `commit cmd` y actualiza el
   *lastIndex* de los *followers* que respondieron. Luego todos hacen persistente
   las entradas en el log.

La aceptación de un *commit* garantiza la siguiente propiedad global:

>  Si dos entradas en la posición *i* en diferentes logs tienen el mismo *term*,
   entonces almacenan el mismo *cmd* y todas las entradas precedentes coinciden.

En Raft, cada *follower* actualiza su log para satisfacer la propiedad anterior.

Si un *follower* falla, cuando se vuelva operativo, recibirá del líder las
entradas desde su último *commit* aceptado (desde el punto de vista de este
líder) y actualiza su log quedando en un estado consistente con el del líder.

Ante una falla del líder, la próxima elección seleccionará a un *candidate* con
el máximo *term* y éste impondrá a los demás servers a sincronizarse con su log
en el próximo mensaje `append(entries)`.

Para más detalles del algoritmo Raft, ver el artículo original disponible en
https://raft.github.io/raft.pdf.

Es posible ver una explicación animada en http://thesecretlivesofdata.com/raft/.

### Otros algoritmos de consenso

Los algoritmos de consenso permiten implementar la *replicación de máquinas de
estado*. Otros algoritmos similares se usan en aplicaciones distribuidas como
las de *criptomonedas* como [Bitcoin](https://bitcoin.org/) y
[Ethereum](https://ethereum.org/). Estos sistemas mantiene réplicas de un log de
operacionesen un ambiente _abierto_, es decir que los nodos no son
necesariamente *autenticados*. 

La consistencia se basa en un algoritmo de consenso que trata de impedir
transacciones (*smart contracts*) inválidas (como el pago doble).

El log es una secuencia de bloques $b_0, b_1, \ldots, b_{n-1}$ donde
cada $b_i$, con $0 > i < n$, tiene la forma $<header,transactions>$.

El header comúnmente contiene los siguientes campos:

- ***Versión***: Número de versión del protocolo
- ***Previous***: Identificador (hash) del bloque previo
- ***Time***: Fecha y hora de creación (timestamp)
- ***Hash***: Hash del bloque

El *hash* del bloque comúnmente se calcula desde los restantes campos del header
y de las transacciones. El nombre *cadena de bloques* proviene de que el hash de
cada bloque depende del anterior y provee la *intangibilidad* del log. Si un
intruso modifica un bloque, deberá modificar todos los siguientes, sino el log
será inválido.

Un cliente (billetera o *wallet* en cripto-monedas) genera transacciones y se
las envía a un grupo de nodos de la red (*mineros* o *validadores*). Un nodo
recolecta un grupo de transacciones y propone un nuevo *bloque* a los demás para
ser incorporado al log.

Cada réplica que recibe un bloque, si éste es válido, lo acepta agregándolo al
log y cancela la recolección de transacciones y la construcción del bloque a
proponer. Es común que se incluya una transacción de *recompensa*, el cual puede
ser una *nueva criptomoneda* o *cargos* en las transacciones recolectadas.

Ante la recepción de bloques válidos en forma concurrente desde dos o más
*mineros* (*proposers*) se crea una rama por cada bloque recibido. Luego, al
recibir un siguiente bloque, éste pertenecerá a una una de las ramas. La rama
que eventualmente se convierta en la mas larga y se convertirá en el log
*definitivo* (*commit*). En la práctica, en un corto tiempo (dos o tres rondas
de minado) se logra el consenso sobre la cadena mas larga.

La diferencia entre las cadenas de bloques con los algoritmos clásicos es que el
líder (*minero* o *validador*) debe *demostrar alguna capacidad* para proponer o
validar un bloque ya que éstos no son *autenticados* (es una red *abierta*).

La *capacidad* de un minero/validador puede basarse en diferentes ideas como:

- *Proof of work*: Debe construirse un bloque de transacciones cuyo *hash* debe
  ser un valor menor que un valor de *dificultad*. Esto requiere encontrar un
  valor en el campo *nounce* de la cabecera del bloque para que el hash cumpla
  con lo requerido. Encontrar el *nounce* adecuado es un problema NP-hard,
  básicamente prueba y error (fuerza bruta). Este método requiere de un gran
  consumo de energía y se usa en [Bitcoin](https://bitcoin.org/).
- *Proof of stake*: Los validadores deben *invertir* una cierta cantidad para
  calificar como tal. Un validador se selecciona aleatoriamente como el *block
  proposer* en cada período o *término* (*time slot*). En el caso que un
  validador opere incorrectamente (ataque o falla) se penaliza quitándole
  monedas desde su *stake*. Con cada validación obtiene una pequeña recompensa.
  Este tipo de consenso se usa en Ethereum. Los validadores *votan* agregando el
  nuevo bloque al log. Comúnmente se requiere lograr más de 2/3 de los votos
  para aceptar un nuevo bloque. Este algoritmo se usa en
  [Ethereum](https://ethereum.org/).

El paper [Bitcoin: A Peer-to-peer Electronic Cash
System](https://bitcoin.org/bitcoin.pdf), por un autor anónimo bajo el pseudónimo de
Satoshi Nakamoto, describe el protocolo propuesto y usado en *bitcoin*.

En el próximo capítulo se describe en mayor detalle las técnicas conocidas como
*blockchain* o *cadenas de bloques*.

## Consistencia, disponibilidad y particionado

En un sistema confiable sería deseable lograr consistencia, una propiedad de
_safety_, es decir que nada malo sucederá y disponibilidad, una propiedad de
_liveness_, es decir que eventualmente el sistema responderá.

Cuando un grupo de procesos no puede comunicarse entre sí, se dice que la red
está _partida_. Algunos clientes pueden seguir operando sobre una partición y
otros sobre las demás.

> ***Teorema CAP***: Un sistema distribuido con posibles fallas sólo puede
> garantizar dos de las tres siguientes propiedades:
> 1. *Consistencia*
> 2. *Disponibilidad (Availability)*
> 3. *Tolerante al Particionado (Partitioning tolerance)*

Esto significa que en una red sujeta a fallas de comunicación arbitrarias deberá
resignar una de las tres propiedades.

Los algoritmos como Paxos o Raft garantizan consistencia y disponibilidad pero
no toleran particionado.

El modelo *BASE* relaja los requisitos *CAP* y es un acrónimo de:

- *Basically-Available*: El sistema debería siempre responder con algún
  reconocimiento.
- *Soft-state*: Puede cambiar de estados al recibir nueva información.
- *Eventually-consistent*: Se toleran inconsistencias temporales pero garantiza
  consistencia eventual.

## Transacciones distribuidas

Un conjunto de operaciones que deben realizarse de manera atómica se denomina
una _transacción_ y tiene las siguientes propiedades (_ACID_):

1. _Atomicidad_: La secuencia de transacciones se consideran una operación
   indivisible, es decir se ejecutan todas o ninguna.
2. _Consistencia_: El sistema transiciona de un estado válido a otro.
3. _Aislamiento (Isolation)_: Los efectos de una transacción no deben afectar a
   otra. Esto requiere que si se realizan en un ambiente concurrente, debe haber
   control de la concurrencia.
4. _Durabilidad_: Cuando una transacción se completa, los cambios producidos se
   vuelcan en el próximo estado del sistema. A modo de ejemplo, en un sistema de
   bases de datos, en un _commit_ los cambios pasan de la memoria temporal a la
   persistente.

Es común el uso de transacciones en operaciones de bases de datos para la
gestión de operaciones concurrentes en un ambiente cliente-servidor.

### Two phase commit

Este algoritmo fue propuesto por Gray en 1978 y asume que el sistema es libre de
fallas. Se basa en un nodo con el rol de _coordinador_ y el resto son los
_participantes_.

Suponiendo que algún cliente envió una secuencia de operaciones (como por
ejemplo delimitadas por `begin transaction` y `end transaction` en SQL) al
_coordinador_ y éste replica las operaciones en cada _participante_.
Posiblemente cada participante reciba parte de la transacción.

Cada participante almacena los resultados de las operaciones en una memoria
temporaria, conocido como el _log de transacciones_. El paso final es el
_commit_, el cual dispara el siguiente algoritmo:

1. El _coordinador_ envía a los _participantes_ el mensaje `VOTE-REQUEST`.
2. Cada _participante_ que recibe `VOTE-REQUEST`, responde con `VOTE-COMMIT` en
   caso que esté listo para realizar su _commit_ local o `VOTE-ABORT` en otro
   caso.
3. El _coordinador_ si recibe `VOTE-COMMIT` de todos los participantes les envía
   `COMMIT`, sino `ABORT`.
4. Cada participante que recibe `COMMIT` realiza su _commit_ local, sino se
   eliminan las operaciones en la memoria transaccional (log).

## Recuperación

Cuando un sistema ha sufrido una falla es deseable que se pueda _recuperar_, es
decir continuar funcionando desde un _estado anterior_ consistente o tratar de
realizar alguna transición a un _nuevo estado consistente_.

Es común usar un *log* de las operaciones (y datos) de la transacción. Al final
se escribe un registro especial de *commit*.

Luego se aplica cada operación del log en el almacenamiento permanente
finalmente el log se elimina.

El mecanismo de transacciones se utiliza comúnmente para implementar el primer
enfoque tanto en bases de datos como en sistemas de archivos.

Puede ocurrir una falla en el sistema mientras se están aplicando las
operaciones o antes de eliminar el log.

En el reinicio del sistema, si el log contiene un registro de *commit* al final,
se aplica la transacción nuevamente. Si el log está incompleto, se ignora y se
elimina, dejando al sistema en un estado consistente.
Esto se corresponde a una operación *rollback*, ya que queda en un estado
anterior consistente.

A modo de ejemplo, en un sistema de archivos, el borrar un archivo requiere
liberar los bloques de datos (apagar los bits correspondientes en el _bitmap_),
luego eliminar los metadatos del archivos (ej: los inodes) y finalmente eliminar
la entrada en el directorio correspondiente. Si ocurre una falla entre esas
operaciones el sistema, en el reinicio, el sistema de archivos quedaría en un
estado inconsistente.

En sistemas distribuidos, el _roolback_ puede requerir que cada _log_ local
contribuya a la consistencia global. Una técnica es utilizar _checkpoints
consistentes_ basado en _tomas de instantáneas_ como el descripto.

## Detección de fallas

Un nodo puede detectar que otro falla ya sea porque no recibe respuestas o
reconocimientos (_acks_) en un cierto intervalo de tiempo (*timeout*) o porque
un nodo responde con valores incorrectos.

Comúnmente las aplicaciones se implementan haciendo que los nodos pueden
periódicamente enviar _pruebas de vida_ a otros nodos, conocidas como
_heartbeat_, enviando una _prueba de vida_. La perioricidad de estos mensajes es
importante para no sobrecargar la red con estos mensajes de control o que la
detección no sea tardía.

La notificación al resto de los nodos de una detección de una falla puede hacerse utilizando _broadcasting_, o protocolos de _flooding_ o _gossip_.