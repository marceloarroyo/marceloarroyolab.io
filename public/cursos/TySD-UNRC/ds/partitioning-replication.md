# Particionado y Replicación

La _replicación_ de datos y computadores permite que un sistema escale
permitiendo distribuir los requerimientos en diferentes nodos de la red.

Por otra parte, cuando el sistema debe manejar grandes volúmenes de datos, a
veces es imposible almacenar la totalidad de esos datos en un nodo, por lo que
los datos deberán _dividirse_ en partes y ser almacenados en un conjunto de
nodos determinados.

Ambas técnicas permiten la _escalabilidad_ de los sistemas y también la
tolerancia a fallas. La replicación permite que si un nodo de la red se torna
inaccesible, otros puedan seguir brindando el servicio. El particionado de los
datos permite la distribución de las computaciones sobre ellos y asegura que al
menos algunos datos serán accesibles, aún en presencia de fallas. Por supuesto,
ambos enfoques generan problemas adicionales.

> La _replicación_ genera el problema de mantener las réplicas _consistentes_
> (actualización de las copias).
>
> El _particionado_ de datos genera el problema que las operaciones deben
> aplicarse a un conjunto de nodos (los que contienen los datos involucrados)-
> Otro problema es lograr un _particionado uniforme_ (balance de carga) entre
> los nodos.

## Distribución de datos

Uno de los principales problemas es decidir cómo dividir los datos para lograr
una fragmentación uniforme. Analizaremos tres estrategias comúnmente usadas,
asumiendo que los items de datos se identifican unívocamente por alguna clave.

1. ***Particionado de claves***: El conjunto de claves se particiona
   en grupos entre el conjunto de nodos. El particionado puede ser basado en
   conjuntos o rangos de claves o usar un esquema jerárquico como por ejemplo,
   en el sistema DNS.

   La ventaja de este enfoque es que la búsqueda de un item puede dirigirse
   al nodo en el cual está almacenado, por ejemplo, mediante la consulta de un
   índice de particionado.
   
   La principal desventaja es que algunos nodos pueden quedar sobrecargados de
   items o que algunos items son mas frecuentemente accedidos o actualizados que
   otros, en cuyo caso no se logrará un buen balance de carga, creando
   _hotspots (puntos calientes)_ en el sistema. La detección de _hotspots_
   permite una re-distribución de los datos logrando un mejor balance de carga.
2. ***Hashing de claves***: El utilizar una función de la forma 
   $hash(key) \: mod \: N$ para mapear el nodo que contiene el item provee una
   mayor uniformidad en la distribución de los items.

   Desventajas: Items lógicamente contiguos podrán estar en diferentes nodos.
   Además si se cambia la cantidad de nodos hay que redistribuir completamente
   los datos.
3. ***Hashing consistente***: En este esquema, a cada nodo se le asocia un
   identificador aleatorio de _m_ bits que determina su posición en una topología
   virtual de anillo. Cada item de datos tiene una clave correspondiente a un
   $k=hash(value)$ de _m_ bits y se almacena en el nodo $n = successor(k)$, es
   decir, en el nodo identificado como $k$ o (si no existe) en el nodo $k'$
   próximo en el anillo tal que $hash(k) < k'$. De esa forma, cada nodo es
   responsable de los items con claves mayores (módulo _m_) que su predecesor.

   El uso de funciones _hash_ provee una dispersión uniforme. La inserción (o
   remoción) de un nuevo nodo en una posición determinada del anillo requiere
   que se redistribuyan las claves de su sucesor y que se actualicen las
   referencias de _sucesor_ y _predecesor_ en los nodos involucrados.

   Cuando un nodo recibe desde un cliente una operación `read(k)` o `write(k,v)`
   la debe reenviar al nodo $successor(k)$.

   Esta estrategia también se conoce como _distributes hash tables (DHT)_ y hay
   varios protocolos basados en esta idea como por ejemplo
   [Chord](https://en.wikipedia.org/wiki/Chord_(peer-to-peer)). Este protocolo
   incluye detalles de implementación para encontrar $successor/predecessor(k)$
   en $O(log(N))$ en un anillo de N nodos. Usa como función hash SHA-1 que
   genera claves de 160 bits.

   También este esquema sugiere una estrategia simple de replicación. Por
   ejemplo, un nodo podría replicar los datos de los _k_ predecesores o
   sucesores.

   ![dht](img/dht-chord-new_node.png ":size=50% :class=imgcenter")

   Figura 3.1: Inserción de un nodo en el anillo.

   Un ejemplo de uso de esta técnica de distribución de datos es Amazon Dynamo
   el cual da soporte a los servicios como AWS S3.

## Replicación

La replicación de datos y servicios (computaciones) permite la escalabilidad y
hace a los sistemas mas tolerantes a fallas. Hay varios problemas a resolver con
la replicación. Uno de ellos es el número de réplicas y su ubicación en la red.
También se debe determinar cómo replicar contenidos. Esto tiene como
consecuencia el problema de _consistencia_ de las réplicas, esto es, cómo
hacer que el sistema como un todo funcione transparentemente como un almacén
único o centralizado ante los clientes.

La ubicación de las réplicas puede hacerse en base a la geografía, muy común en
aplicaciones en Internet, muchas veces luego de un estudio de las demandas. 
Esto habilita a que los clientes se conectan con un server o réplica más
próxima, teniendo en cuenta la topología de interconexión.
Esta técnica se aplica en _redes overlay_ usadas por los gigantes de servicios
en Internet como Google, YouTube, AWS, Netflix, y otros.

En cuanto a la replicación de contenidos pueden aplicarse varias estrategias:

1. _Notificación de una actualización_: También se conoce como _protocolos de
   invalidación_, donde un nodo notifica a los demás que cierto contenido ha
   cambiado, por lo que las réplicas se tornan inválidas.
   Esta técnica se usa comúnmente en sistemas multiprocesadores para _invalidar_
   entradas en las cachés locales.
2. _Propagación del dato o de la operación del update_: En este caso el nodo que
   procesó un _update (write)_ sobre un item, propaga el nuevo valor o la
   operación para que sea ejecutada en las demás réplicas. Cuando se propagan
   _operaciones_ (en vez de los datos), se dice que se usa _replicación activa_
   ya que cada nodo es un proceso capaz de ejecutar las operaciones. Este
   mecanismo es común en los sistemas de bases de datos distribuidos.

El manejo de las réplicas puede hacerse de diferentes formas:

1. _Leader-replica_: En este caso existe un nodo _leader_ o coordinador recibe
   los _writes_ desde los clientes y luego propagará la actualización a las
   réplicas. Las lecturas pueden hacerse sobre cualquier réplica. Un dispositvo
   de almacenamiento [RAID](https://en.wikipedia.org/wiki/RAID) tiene una
   arquitectura de este tipo.
   
2. _Multi-master_: Los clientes pueden realizar _writes_ en cualquier réplica y
   ésta propagará los cambios a las demás réplicas.
   Esta arquitectura genera el problema de _consistencia_ ya que pueden
   realizarse escrituras sobre el mismo dato en forma concurrente en diferentes
   réplicas.

3. _Basadas en caché_: En este caso, los clientes o servidores que han resuelto
   una consulta previa de un item, lo almacenan temporalmente en su memoria
   local conformando una caché de datos. Los items en la caché pueden
   invalidarse por su _tiempo de vida_ o por notificaciones.

## Consistencia

En sistemas distribuidos la arquitectura _leader-replica_ conduce a un sistema
centralizado (al menos para procesar los _writes_), siendo aplicable a sistemas
con pocas actualizaciones. Además tienen un *único punto de falla*.

Los sistemas _multi-master_ (o _peer-to-peer_) deben resolver el problema de
consistencia generado por las posibles actualizaciones concurrentes. Hay varios
tipos o variantes de la noción de consistencia, dependiendo del tipo de servicio
requerido por la aplicación.

- ***Secuencial***: Una secuencia de operaciones (_read/write_) es válida si se
  corresponde a una traza posible de cada cliente. Esta noción es fuerte ya que
  requiere que todos los procesos deberían seleccionar esa traza, requiriendo
  un sistema determinístico. Requiere que el sistema preserve el orden de
  operaciones de cada cliente. Puede lograrse con un protocolo de *replicaciçon
  activa* basado en comunicación *multicast confiable con orden total*.

- ***Linearización***: Similar a la consistencia secuencial pero centrada en el
  tiempo en que ocurren las comunicaciones. Puede lograse con una arquitectura
  de *replicación pasiva* (*backup primario* o *replicación basa en líder*).

- ***Causal***: Garantiza que se fuerza la secuencialidad sólo sobre las
  operaciones sobre el mismo dato. Es decir, si un proceso $p_i$ ejecuta
  $write(x,v)$ y luego $p_j$ ejecuta $x=read(x)$ (antes de un nuevo $write(x,?)$
  en cualquier otro proceso), entonces $x=v$. Dos operaciones $write(x,v_1)$ y
  $write(y,v_2)$ son _concurrentes_ y pueden hacerse en cualquier orden.

- ***Eventual***: Tolera inconsistencias temporales, pero que luego de un tiempo
  sin _updates_ todas las réplicas se tornarán consistentes. Ejemplos de
  aplicaciones que usan este modelo de consistencia son los _content delivery
  networks (CDN)_ que contienen archivos de sólo lectura y el sistema DNS, los cuales
  usan replicación basada en _caché_ (_pull based_).

## Replicación pasiva (leader-replica)

Un sistema de replicación pasiva o *master-replicas* se basa en la arquitectura
de la siguiente figura.

``` td2svg
  +--------+  write(x,v1)                         +-----------+
  | client |---------+                      +-----+ secondary |
  +--------+         |       +---------+    |     +-----------+
                     +-------+ primary +----+
                             |         |
                     +-------+ replica +----+
  +--------+         |       +---------+    |     +-----------+
  | client |---------+                      +-----+ secondary |
  +--------+  write(x,v2)                         +-----------+
```

Los clientes realizan las operaciones `write` a la réplica primaria, éste
ejecuta la operación y en caso de éxito reenvía (broadcast) la operación a las
réplicas secundarias (que deberían tener éxito ya que ejecutan el mismo
programa). Esto es equivalente a *1 phase commit*.

La réplica primaria o *master* aplica las operaciones en el orden en que los
recibe y se los reenvía a las réplicas en el mismo orden. Esto garantiza
*linearización*.

## Arquitectura multi-master

En una arquitectura multi-master, los clientes envían las operaciones a todas
las réplicas.

> En un sistema _multi-master_ pueden ocurrir _conflictos_. Un conflicto
> _write/write_ ocurre cuando se realizan concurrentemente escrituras del mismo
> dato en diferentes nodos. Un conflicto _read/write_ es cuando ocurre una
> lectura en un nodo y concurrentemente una escritura del mismo item en otro,
> antes que se inicie su propagación.

Una solución puede basarse mediante comunicaciones *multicast confiable
totalmente ordenada* que garantiza que los mensajes serán entregados en cada
réplica en el mismo orden.
Este tipo de comunicación garantiza *consistencia secuencial* ya que preserva el
orden de las operaciones en cada programa cliente.

Otras posibles soluciones incluyen _regiones críticas_, _transacciones
distribuidas_ o usando protocolos basados en _quorums_ que se analizan en el
próximo capítulo.

> En un sistema basado en _quorum_, cada item de datos tiene asociado su
> _versión_ o _timestamp_, un valor numérico creciente en cada $write$. El
> *quorum* $N$ es el conjunto mínimo de nodos que deben responder a un
> requerimiento de un cliente. Un cliente debe contactar a un grupo de
> servidores ($N_R$ para leer y $N_W$ para escribir) y debe cumplir con las
> siguientes condiciones (con quorum $N$):
>
> 1. $N_R + N_W > N$
> 2. $N_W > N/2$
> 
> La primera condición soluciona conflictos _read/write_. La segunda soluciona
> conflictos _write/write_.

## Registro compartido

Un dato replicado en un sistema distribuido puede verse como un registro
compartido en un sistema de memoria compartido.

Las operaciones de lectura y escritura deben ser consistentes (consistencia
secuencial). Attiya, Bar-Noy y Dolev en 1990 propusieron el siguiente algoritmo
tolerante hasta $f$ fallas.

```
write(v):                  read():
  t++                         broadcast(read_msg)
  broadcast(v,t)              wait for n-f responses (v,t)
  wait for n-f acks           return v with max t
  register = v
```

Este algoritmo requiere al menos $n > 2f$ procesos.

Una implementación es *linearizable* si:

1. Si las operaciones se ejecutan secuencialmente en el orden de la
   serialización, deberían arrojar el mismo resultado que en la ejecución
   concurrente.
2. Si $op_1$ finaliza antes que $op_2$, entonces $op_1$ precede a $op_2$ en la
   linearización.

Se puede demostrar que esta implementación es *linearizable*.

El orden de operaciones es: ordenar los `writes` en el orden que ocurren e
insertar los `reads` luego de los `writes` del valor que cada `read` retorna.

En el caso que $op_1=write$ y $op_2=read$ y $op_2$ sigue inmediatamente a
$op_1$ puede demostrarse por contradicción. Supongamos que el `read` no ve al
`write` previo. Entonces, por la implementación, deben haber dos conjuntos
disjuntos de procesos de tamaño $n-f$. Así, $2*(n-f) \leq n$, entonces $n \leq
2f$ lo que contradice la hipótesis que $n > 2f$. 