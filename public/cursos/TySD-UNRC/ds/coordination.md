# Coordinación

En aplicaciones distribuidas es común que un proceso deba esperar a que otro
alcance cierto estado antes de poder continuar. En otros casos los procesos
deben acceder a recursos compartidos y para garantizar consistencia deben
acceder de manera exclusiva. Este último caso se conoce como _sincronización en
el acceso a datos_. _Coordinación_ es un término más general para lograr una
interacción controlada entre los procesos.

Como cada proceso en un sistema distribuido tiene su propio reloj físico, lograr
una sincronización de estos tipos de relojes con un alto nivel de precisión es
difícil de lograr. Algunos protocolos como [Network Time Protocol
(NTP)](http://www.ntp.org/) permiten la sincronización de los relojes físicos de
las computadoras conectadas en red y ampliamente usado en Internet. El
[NIST](https://www.nist.gov/) soporta un conjunto de servidores con relojes
atómicos de máxima precisión y ofrece el servicio de hora y fecha basado en NTP
y otros. Nuestras computadoras personales generalmente ejecutan un cliente NTP
(utilidad del sistema) que mantiene el reloj de hardware actualizado. 

Hay que considerar que la sincronización no es perfecta, ya que se deben tomar
en cuenta las demoras en las transmisiones de los mensajes, por lo que los
protocolos utilizan algoritmos de ajustes aproximados. Para más información, ver
la sección 5.1 de [DS-4ed] y
[NTP](https://www.eecis.udel.edu/~mills/ntp/html/index.html).


## Relojes lógicos

Para conseguir operar en forma coordinada, alcanza con el uso de un modelo
simplificado de tiempo. El objetivo generalmente es lograr capturar la relación
de _causalidad en la ocurrencia de los eventos_ en el sistema.

> Denotaremos $a \rightarrow b$ si $a$ ocurre antes que $b$. Esta relación
> implica:
> 1. Si $a$ y $b$ ocurren en el mismo proceso y $a$ ocurre antes que $b$,
>    entonces $a \rightarrow b$.
> 2. Si $a$ es el evento de enviar un mensaje desde un proceso y $b$ es el
>    evento de recibir ese mensaje en otro proceso, entonces $a \rightarrow b$.

Esta relación (_sucede antes que_) es transitiva.

Leslie Lamport, en 1978, en [Time, Clocks and the Ordering of Events in a
Distriibuted System](http://lamport.azurewebsites.net/pubs/time-clocks.pdf)
propuso el siguiente algoritmo conocido como _relojes escalares_.

1. Cada proceso $p_i$ tiene su _reloj local_ $c_i$.
2. En cada cambio de estado interno de $p_i$: $c_i \leftarrow c_i + 1$.
3. En $p_i$: Antes de enviar un mensaje, $c_i \leftarrow c_i + 1$. El mensaje
   toma la forma $(msg,c_i)$, donde $c_i$ se conoce como el *timestamp*.
4. En cada mensaje recibido de $p_j$: $(msg,c_j)$, $p_i$ ajusta su reloj:
   $c_i = max(c_i+1, c_j)$

Es posible definir una función que captura la noción de un reloj global del
sistema: $C(e) = c_i$ si el evento $e$ ocurrió en el proceso $p_i$.

Es fácil demostrar que $C()$ captura la noción de paso del tiempo discreto
interna de cada proceso y de los eventos de emisión y recepción de mensajes:

1. Si $a$ y $b$ son eventos que ocurren en el mismo proceso y $a \rightarrow b$,
   entonces, $C(a) < C(b)$.
2. Si $a$ es el evento de _enviar un mensaje_ a otro proceso y $b$ es el evento
   de recepción de ese mismo mensaje, $C(a) < C(b)$.

La implementación de incluir los _timestamps_ en los mensajes generalmente se
implementa en el _middleware_ utilizado para la comunicación entre procesos.

Cabe notar que $C(a) < C(b)$ no necesariamente implica que $a \rightarrow b$
como en el caso que $a$ y $b$ sean eventos que ocurren en dos procesos
independientes (no se comunican). En este caso esos eventos se consideran
_concurrentes_.

> Los relojes escalares no capturan la relación de ***causalidad***: $C()$ es un
> *orden parcial*.

Es posible capturar ***causalidad*** si cada proceso tiene en cuenta el _estado
interno_ de cada otro proceso en el sistema. Ya que un reloj escalar abstrae los
cambios de estado en un proceso, es posible capturar esta información si cada
proceso incluye su último valor conocido del estado de los demás.

Los _relojes vectoriales_ son una implementación de esta idea.

1. Cada proceso (sea $p_i$) mantiene un _vector_ $vc_i[N]$, con $vc_i[i]$ como su
   reloj lógico local. $N$ es el número de procesos en el sistema.
2. En cada evento interno (o cambio de estado), $p_i$ hace $vc_i[i] = vc_i[i] + 1$.
3. En cada mensaje enviado, se incluye el _timestamp_: $send(p_j, msg ++ vc_i)$.
4. En cada mensaje recibido desde $p_j$, $p_i$ hace $vc_i[k] = max(vc_i[k],
   vc_j[k])$ para todo $k$ con $1 \leq k \leq N$.

La principal desventaja de los _relojes vectoriales_ es que el _timestamp_ de
cada mensaje puede tener un tamaño considerable para un $N$ grande.

## Exclusión mutua

El acceso exclusivo en el uso de recursos compartidos es común en sistemas
concurrentes y distribuidos. En sistemas basados en memoria compartida es común
el uso de primitivas de sincronización como los _spinlocks_, _variables de
condición_, _semáforos_ o _monitores_. En un sistema distribuido se requiere
lograr un protocolo que garantice que los procesos accederán al recurso sólo
cuando los demás no lo estén usando y además se debería garantizar _progreso_,
es decir, que un proceso que requiera el recurso, eventualmente lo logrará.

Una solución inmediata es designar un proceso (nodo) del sistema como
coordinador de los recursos, centralizando el estado del sistema. Cada nodo que
requiere un recurso le envía al coordinador un mensaje `REQUEST` al coordinador
y queda a la espera por la respuesta.
El coordinador responde cuando el recurso esté libre. Luego el proceso podrá
utilizar el recurso y notificar al coordinador cuando lo libere.

La elección de un coordinador puede ser definida estática o dinámicamente
mediante un algoritmo de _elección de líder_ que se analiza más adelante.

Obviamente el coordinador es un punto de falla crítico por lo que si se quiere
lograr un sistema tolerante a fallas el coordinador debería ser *elegible* ante
la detección de su caída.

![tokenring](img/token-ring.svg ":size=50% :class=imgcenter")

#### Figura 2.1: Topología token-ring.

En esta topología es fácil lograr la exclusión mutua en el acceso a un recurso.
Simplemente el protocolo define un _token_ (o *capacidad*) por recurso. El
mecanismo de circulación del token en forma secuencial garantiza el acceso
exclusivo al recurso. Cada proceso que desea acceder a un recurso, debe esperar
por el token, acceder al recurso y luego reenviar el token al próximo proceso en
el anillo.

La configuración del anillo puede realizarse a-priori (estáticamente) o
dinámicamente, asumiendo que cada nodo puede determinar su sucesor, por ejemplo
mediante un esquema de identificación numerable de los nodos.

Las desventajas de tal enfoque es que es necesario construir a priori la
topología virtual y si el número de procesos es grande, pueden ocurrir demoras
considerables. Otra desventaja es que para que sea tolerante a fallas hay que
asumir que el anillo es dinámico y que cada nodo es capaz de detectar una falla
e iniciar una _reconfiguración_ del anillo en forma automática.

En 1981, Ricart y Agrawala propusieron un algoritmo distribuido y requiere el
uso de mensajes con _timestamps_ (*reloj escalar*) y cada proceso tiene una cola
de mensajes ordenados por *timestamps* por cada recurso. Sean $n$ procesos:

- El proceso $p_i$ desea utilizar el recurso $r$: 
  1. $broadcast(enter\{p_i, r, ts_i\})$
  2. $p_i.state \leftarrow waiting(r, ts_i)$
  3. Espera por $n$ mensajes $(p_j, OK_r)$, luego
     1. $p_i.state \leftarrow using(r)$ y usa $r$
     2. Al salir de la región crítica hace $send(k, OK_r)$,
        $\forall (k \mid \{k, ts_k\} \leftarrow r\_dequeue())$
- Un proceso $p_j$ recibe un mensaje $enter(p_i, r, ts_i)$:
  - Si $p_j.state \neq using(r)$: $send(p_i, OK_r)$
  - Si $p_j.state = waiting(r, ts_j)$ y $ts_i < ts_j$: $send(p_i, OK_r)$
  - Si $p_j.state = using(r)$: $r\_queue.insert(\{p_i, ts_i\})$

## Broadcasting/multicasting de mensajes

Muchas aplicaciones y algoritmos distribuidos requieren que un nodo envíe
mensajes a un grupo de nodos en la red. Este tipo de comunicación se conoce como
*multicast*. Se denomina *broadcast* cuando el grupo contiene la totalidad de
los nodos.

Es común que en redes locales un protocolo de enlace, como por ejemplo los
basados en Ethernet, provean un mecanismo eficaz. En general en redes como
Internet se use IP como protocolo punto a punto.

Se requiere que esta operación sea *confiable*, es decir que debe cumplir con
algunas de las siguientes propiedades.

- ***FIFO broadcast***: Si $m_1$ y $m_2$ son enviados por el mismo nodo y
  $broadcast(m1) \rightarrow broadcast(m2)$, entonces en cada receptor $m_1$
  debe *entregarse (deliver)* antes que $m_2$.

- ***Broadcast causal***: Si $broadcast(m1) \rightarrow broadcast(m2)$ ,
  entonces en cada receptor $m_1$ debe *entregarse (deliver)* antes que $m_2$.

- ***Broadcast con orden total***: Si $m_1$ se entrega antes que $m_2$ en un
  nodo, entonces también se deberán entregar en ese orden en todos los nodos.

- ***Broadcast FIFO-orden total***: Combinación de *FIFO* y *orden total*.

El último tipo de *broadcast* es la mas fuerte y se conoce también como
*broadcast atómico* mientras que *FIFO broadcast* es el más débil ya que permite
que los mensajes sean entregados *fuera de orden*.

### Algoritmos

Realizar broadcasting en un protocolo del tipo *token ring* sobre una topología
de anillo es natural, ya que el mensaje de broadcast puede implementarse como el
envío a sí mismo. El mensaje pasará por cada nodo hasta retornar al emisor, el
cual al recibirlo lo retira de la red.

Este algoritmo no es muy usado en sistemas grandes, escalables y tolerantes a
fallas ya que cada enlace y cada nodo representa un punto de falla crítico.

La mayoría de los algoritmos se basan en la siguiente idea:

1. El nodo emisor envía a todos sus *vecinos* por sus *enlaces directos*.
2. Cada nodo, cuando recibe un mensaje lo reenvía por sus enlaces, excepto por
   el que lo recibió.

Es fácil ve der que el mensaje se diseminará por toda la red.

Este algoritmo es muy mejorable ya que un nodo podría recibir varias copias del
mensaje (reenvíos de otros vecinos). Esto se puede solucionar con un *timestamp*
en el mensaje que permita detectar y descartar mensajes duplicados. Este
algoritmo se conoce comúnmente como *inundación (flooding)*.

Otra modificación es que en lugar de enviar el mensaje por los enlaces directos,
cada nodo reenvíe el mensaje a $n$ nodos elegidos aleatoriamente. Esto cambia la
noción de *vecino*. Estos protocolos reciben el nombre de ***gossip***
(*pandemia* o *rumor*) ya que se basan en modelos de propagación de enfermedades
o *chismes*.

## Elección

Cualquier algoritmo o protocolo que requiera un coordinador central, deberá
realizar una selección del mismo. En un sistema tolerante a fallas, si falla un
coordinador, algún otro proceso deberá detectar esta anomalía y disparar una
elección de otro líder o coordinador.

En este caso un modelo centralizado no corresponde ya que justamente este
algoritmo se debe disparar ante la falta de tal coordinador.

En una topología virtual de anillo nuevamente el algoritmo es simple, ya que el
proceso $p_i$ que detectó la falla en el coordinador, luego de reconfigurar el
anillo, genera un token $(ELECTION, p_i)$ y lo envía a su sucesor. Cada proceso
$p_j$ que reciba un token $(ELECTION, p_k)$, reenvía $(ELECTION, max(p_j,
p_k))$. El proceso iniciador ($p_i$) cuando reciba el token $(ELECTION, p_w)$,
lo consume y envía $(ELECTED, p_w)$, el cual será reenviado por cada proceso
intermedio definiendo en su estado $coordinator = p_w$. Finalmente $p_i$, al
recibir $(ELECTED,p_w)$ lo consume. El proceso elegido es $p_w$.

García y Molina en 1982 propusieron un algoritmo distribuido que se conoce como
_bully_ (matón) que selecciona el proceso con mayor identificador.
El algoritmo se describe en los siguientes pasos:

- Sea $p_i$ el proceso _iniciador_: Envía $send(p_k, ELECTION)$ a todos los
  procesos con $k > i$
  - Si no obtiene respuestas (o no conoce $p_k$ con $k > i$), cambia su estado
    a $coordinator=p_i$ y $broadcast((ELECTED, p_i))$. 
  - Si recibe una respuesta de $p_j$, $p_j$ es el nuevo _iniciador_.
- Un proceso $p_j$ que recibe $ELECTION$ desde $p_i$ responde sólo si $j > i$.
- Cada proceso que recibe $(ELECTED, p_i)$, actualiza su estado a
  $coordinator=p_i$.

Es un algoritmo básicamente de _flooding (inundación)_ en un _grafo acíclico
dirigido (DAG)_. Se debe notar que en un momento dado puede haber varios
_iniciadores_ simultáneos. Ejercicio: Mostrar que aún con múltiples
iniciadores, se logrará una elección.

Uno de los problemas de este algoritmo es que es costoso ya que requiere la
transmisión de $O(n^2)$ mensajes en un grupo de $n$ nodos.

No es tolerante a fallas, ya que queda esperando por todas las respuestas. En la
sección [tolerancia a fallas](fault-tolerance.md) se analizan algoritmos de
*consenso* que pueden usarse para elegir un coordinador en presencia de fallas.

## Protocolos Publish/Subscribe

Estos protocolos permiten implementar el conocido patrón arquitectural
_publish/subscribe_. En este patrón un _publisher_ envía _notificaciones_ por
un canal al que están conectados los _subscribers_.

Estos protocolos son aplicables en una gran variedad de escenarios como la
adquisición del estado de contactos en aplicaciones de chat y bases de datos
distribuidas, donde los subscriptores se corresponden a las réplicas de un
conjunto de datos que reciben (y posiblemente envíen) notificaciones de
_actualizaciones de datos_.

Una topología descentralizada requiere la formación de una _red overlay_ de
peers interconectados. El principal problema es la comunicación de las
notificaciones a los nodos _subscriptores_, lo cual puede hacerse mediante
algunos protocolos de diseminación como los ya mencionados.

Una técnica comúnmente usada es el _ruteo selectivo_, lo cual es similar a los
protocolos de [ruteo
multicast](https://marceloarroyo.gitlab.io/cursos/TySD-UNRC/networks/#/routing?id=ruteo-multicast)
vistos previamente.

En este modelo, cada router mantiene una tabla (generalmente parcial) de los
nodos (vecinos) que se han subscripto a cada evento. En la recepción de un
mensaje de _suscripción_ se lo incluye en la tabla.

Al ocurrir un evento (ejemplo: un usuario se conecta a ese nodo) o recibir una
_notificación_ de un evento, se reenvía el mensaje con la notificación a los
nodos de la tabla cuya entrada _coincide (match)_ con el evento registrado.

> Ejercicio: Comparara esta estrategia con el algoritmo de *ruteo multicast*
> visto en el curso de redes.

## Estado global y toma de instantáneas

Una _instantánea (snapshot)_ es un *estado consistente* del sistema que puede
ser usado en una recuperación. Otras aplicaciones incluyen recolección de basura
distribuida, detección de deadlock y terminación y monitoreo o depuración.

Sean *N* procesos, la *historia* de un proceso $h_i=<e_i^0, e_i^1, e_i^2
\ldots>$ es la secuencia de eventos ocurridos en $p_i$. Cada evento es un cambio
de estado interno o el envío y recepción de mensajes. La *historia global del
sistema* de define como $H=\cup_{i=0}^{n-1} p_i$.

> ***Definición***: Un *corte (cut) C* es un subconjunto de $H$, el cual está
> formado por la unión de *prefijos* de $h_i$, $0 \leq i < N$.

Cualquier subconjunto podría ser *inconsistente* ya que podría incluir el evento
de recepción de un $m$ por $p_j$ (desde $p_i$) pero no el evento de envío de $m$
por $p_i$, como en la figura (b).

![cuts](img/cuts.jpg ":size=70% :class=imgcenter")


> ***Definición***: Un *corte C es consistente* si $\forall e \in C, f
> \rightarrow e \implies f \in C$. 

Un corte puede incluir mensajes enviados aún no recibidos.

> Una ***linearización*** es un orden de eventos de *H* que respeta la relación
> $\rightarrow$.

En una linearización es una secuencia de eventos que *recorre cortes
consistentes*.

### Toma de instantáneas (snapshot)

A continuación se describe el algoritmo propuesto por Chandy y Lamport en 1985
útil para determinar el estado global de un sistema distribuido.

El algoritmo se basa en registrar los estados locales en cada proceso. Cada
proceso podría enviar su estado a un monitor de estados central el cual podría
verificar propiedades globales.

El algoritmo se basa en el envío de un mensaje especial (la *marca M*) que
actúa como un *iniciador* del proceso de registrar el estado en cada proceso.

Cada proceso $p$ tiene un conjunto de variables $vars_$, *canales de entrada
(ic_p) y salida (oc_p)*.

El estado de un proceso es el conjunto de valores de sus variables y de los
mensajes recibidos por cada canal de entrada.

- Un proceso *iniciador* $p$ realiza los siguientes pasos:
  1. $state_p=(vars_p, messages(ic_p))$.
  2. $send(M, oc_p)$ (envía *M* por los canales de salida)
  3. Comienza a registrar los mensajes de sus canales de entrada.
- Cuando un proceso $p$ recibe la marca sobre un canal de entrada $c \in ic_p$: 
  - Es la primera vez que ve (conoce) la marca:
    1. $state_p=(vars_p, messages(ic_p))$
    2. $c=\emptyset$
    3. $send(M, oc_p)$ (colabora en la redistribución de la marca)
    3. comienza a registrar mensajes de los demás canales de entrada.
  - En otro caso:
    1. $messages(c)={rcvd\_msgs(c)}$ (mensajes recibidos por *c* desde el último
       estado registrado)
    2. Deja de registrar mensajes nuevos recibidos por $c$

El algoritmo descripto permite registrar estados que permiten construir *cortes
consistentes*.

Ejemplo:
<>
Sean dos procesos $p_1$ y $p_2$, conectados por dos canales unidireccionales
$c_{12}$ (desde $p_1$ a $p_2$) y $c_{21}$ (desde $p_2$ a $p_1$) donde $p_2$ es
un vendedor de items.

1. Estado $S_0$: $p_1=(account=\$1000, items=0)$, $p_2=(account=\$50,
   items=50)$, $c_{12}=c_{21}=\emptyset$
2. $p_1$ registra su estado y envía un requerimiento de compra
   
   Estado $S_1$: $p_1=(account=\$900, items=0)$, $p_2=(account=\$50,
   items=50)$, $c_{12}=\{order(10,\$100), M\}, c_{21}=\emptyset$
3. $p_2$ recibe $order(10,\$100)$ antes que la marca, así responde con el
   mensaje $items(5)$.
   
   Estado $S_2$: $p_1=(account=\$900, items=0)$, $p_2=(account=\$150,items=45)$,
   $c_{12}=\emptyset$, $c_{21}={items(5)}$
4. $p_1$ recibe el mensaje $items(5)$ por $c_{21}$
   
   Estado: $S_3$: $p_1=(account=\$900,items=5)$, $p_2=(account=\$150,items=45)$,
   $c_{12}=c_{21}=\emptyset$

Cada estado es un *corte consistente*.

## Referencias

1. [Distributed Systems. 4th edition. Maarten Van Steen and Andrew S. Tanenbaum](https://www.distributed-systems.net)
2. *Distributed Systems: Concepts and Design*. G. Coulouris et al. Fifth
   Edition. 2012.
3. [Distributed Systems: Course notes at University of Cambridge 2021/22](https://www.cl.cam.ac.uk/teaching/2122/ConcDisSys/dist-sys-notes.pdf)