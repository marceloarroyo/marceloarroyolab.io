# Telecomunicaciones y Sistemas Distribuidos

Estas son las notas de la segunda parte del curso ***"Telecomunicaciones y Sistemas
Distribuidos"*** de la Licenciatura, dictado por el
[Departamento de Computación](https://dc.exa.unrc.edu.ar/) -
[FCEFQyN](https://www.exa.unrc.edu.ar/), de la [Universidad Nacional de Río
Cuarto](https://www.unrc.edu.ar/).

En estas notas se abordan temas de *sistemas distribuidos*, es decir,
algoritmos y protocolos para implementar sistemas sin una coordinación central,
permitiendo lograr sistemas escalables y tolerantes a fallas.

Las notas de la primera parte del curso, incluyen temas de [redes de
computadoras](https://marceloarroyo.gitlab.io/cursos/TySD-UNRC/networks/index.html).

## Equipo docente

- [Marcelo Arroyo](https://marceloarroyo.gitlab.io/)
- [Gastón Scilingo](https://dc.exa.unrc.edu.ar/staff/gaston/)

## Bibliografía recomendada

1. *Distributed Systems*. 4th edition. Maarten Van Steen and Andrew S.
   Tanenbaum. 2013. URL: https://www.distributed-systems.net/.
   Puede descargar su copia libre personalizada.

2. *Distributed Systems*. Course notes of University of Cambridge 2021/22. URL:
   https://www.cl.cam.ac.uk/teaching/2122/ConcDisSys/dist-sys-notes.pdf
