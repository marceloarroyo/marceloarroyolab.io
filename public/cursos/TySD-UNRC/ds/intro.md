# Sistemas distribuidos

Actualmente existe la necesidad de desarrollar sistemas que brinden servicios a
escala global. Algunos ejemplos de este tipo de aplicaciones on Google Docs,
redes sociales (Facebook, Instagram, ...). Otras organizaciones proveen acceso a
servicios de infraestructura de computación y almacén de datos, como [Amazon Web
Services (AWS)](https://aws.amazon.com), [Google
cloud](https://cloud.google.com/) y otros.

Estas aplicaciones requieren de alta _disponibilidad_, _escalabilidad_ y de
fácil _mantenibilidad_ o evolución.

El requerimiento de _disponibilidad_ implica que no es posible que esos servicios
o aplicaciones tengan un _único punto de acceso_ como en el caso de una
aplicación web tradicional ya que ante una caída de una parte de la red podría
dejar a millones de usuarios sin acceso. Además es punto de acceso podría
requerir un ancho de banda prácticamente imposible de lograr.

Los requermientos de _escalabilidad_ para satisfacer a millones de clientes
interactuando concurrentemente con el sistema presenta no sólo desde el punto de
vista del poder de cómputo necesario, sino también de las capacidades de la red
interna para soportar una gigantesca cantidad de tráfico y con una bajísima
latencia para alcanzar tiempos de respuestas razonables.

Un sistema de estas características debe ser fácilmente _mantenible_ y
configurable ya que no puede soportar interrupciones por tareas de mantenimiento
y/o reconfiguración. En lo posible estos sistemas deberían ser
auto-configurables.

El diseño de estos sistemas prácticamente obliga a abandonar la idea de tener
control, estado y procesamiento centralizado como en una arquitectura básica
cliente-servidor y se debe ir hacia un diseño de un _sistema distribuido_.

> Un ***sistema distribuido*** es una colección de elementos de computación
> (nodos) interconectados que cooperan para brindar un conjunto de servicios
> formando un sistema que se observa como único y coherente.

Es importante para que un sistema califique como distribuido que tenga la
_transparencia_ necesaria para que para los usuarios aparezca como un sistema
ejecutándose en una única computadora o como si fuera en apariencia un sistema
cliente-servidor tradicional.

Esto significa que los recursos que gestiona deben ser accesibles por medio de un
sistema de identificación (_naming_) que sea independiente de su ubicación
física.

En las notas del curso de redes y telecomunicaciones se analizan algunos
ejemplos de sistemas distribuidos. Uno de ellos es el sistema DNS, el cual
brinda el servicio de resolución de nombres a sus respectivas direcciones IP,
formando un conjunto de nodos que _particionan_ la base de datos de dominios de
manera jerárquica. Otro ejemplo que puede mencionarse dentro de los protocolos
de infraestructura de Internet son el sistema de ruteo que puede verse como un
sistema que contiene las rutas (utilizando algoritmos de descubrimiento de
nuevas rutas) lo que constituye una gran base de datos distribuida.

Un sistema distribuido generalmente organiza como una _overlay network_, es
decir, una red _lógica_ o _virtual_ sobre una red actual, típicamente Internet.
Algunas veces el sistema define una _topología virtual_ de esta red en base a
los algoritmos utilizados. Mas adelante se verá que algunas aplicaciones se
basan en topologías virtuales como _anillos_, _árboles_ o _grillas_.

Para lograr alta _disponibilidad_ generalmente los recursos se deben _replicar_
para que en el caso de fallas de algunos nodos, el resto del sistema pueda
seguir funcionando.

Para lograr _escalar_, es decir que el sistema soporte inmensos volúmenes de
datos o pueda ejecutar cientos de miles de operaciones por segundo es necesario
_particionar_ los datos y/o computaciones. Esto se conoce como _escalado
horizontal_, es decir, agregar elementos (computadoras) con aplicaciones que
operen en forma cooperativa usando un modelo de _pasaje de mensajes_.

Agregar capacidad  de cómputo (memoria, CPUs, interfaces de red, etc) a una
computadora existente se denomina _escalabilidad vertical_ lográndose una
escalabilidad limitada.

El particionado genera problemas de _disponibilidad_ ya que si un nodo que
contenía ciertos valores se cae, éstos datos se tornan inaccesibles. Una
solución al problema de disponibilidad y tolerancia a fallas es la _replicación_
de los recursos del sistema.

Si los recursos replicados pueden evolucionar, es decir son objetos mutables,
surge el problema de mantener su _coherencia_ en las réplicas.

En estas notas se analizan los principales problemas a resolver en los sistemas
distribuidos y sus alternativas de solución.

## Tipos de sistemas distribuidos

Hay diferentes tipos de sistemas distribuidos para diferentes propósitos. Cuando
el objetivo es lograr _computación de alto desempeño (high performance
computing - HPC)_ es posible clasificar en dos tipos de sistemas:

1. ***Clusters***: Red de computadoras interconectadas por enlaces de alta
   velocidad como por ejemplo Gigabyte Ethernet. Generalmente cada nodo corre el
   mismo sistema operativo como GNU-Linux. El uso de clusters se conoce como
   _computación paralela_.

   Un ejemplo de software utilizado en clusters es
   [Beowulf](https://beowulf.org/).

   Se utiliza un _planificador de tareas (jobs)_ en base a sus requerimientos de
   recursos, tiempo estimado de computación y otros parámetros. Los usuarios
   _encolan_ sus _jobs_ y luego el sistema los notifica con los resultados
   obtenidos generalmente por correo electrónico.
2. ***Grid computing***: Sistemas que comprenden un conjunto de computadoras
   generalmente de alto rendimiento interconectados que se ofrecen al usuario
   como un gran supercomputador. La principal diferencia con la computación
   paralela en clusters es que los nodos pueden ser heterogéneos tanto en
   hardware como en software y generalmente están dispersos geográficamente e
   inclusive pertenecer a diferentes propietarios y/o administrados por
   diferentes dominios.

El término _computación en la nube_ o _cloud computing_ se aplica a ofrecer los
recursos de hardware (generalmente virtualizados o contenerizados) y servicios
de computación (software) en Internet. Ejemplos de estos servicios son los
provistos por [Amazon We Services (AWS)](https://aws.amazon.com/) y [Google
cloud](https://cloud.google.com/).

Generalmente una _nube_ se diseña en capas, así una de las capas mas bajas
ofrece los servicios de recursos de computación (computadoras reales o máquinas
virtuales) y almacenamiento. Ejemplos de servicios en este nivel son como
[Amazon EC2](https://aws.amazon.com/ec2) y [Amazon
S3](https://aws.amazon.com/s3). El siguiente nivel ofrece _frameworks de
software_ y APIs para el desarrollo de aplicaciones distribuidas, como [MS
Azure](https://azure.microsoft.com/) y [Google Apps](https://play.google.com).

Otro tipo de sistemas distribuidos son los _sistemas de información_ que
generalmente tienen los datos distribuidos y posiblemente replicados. Estos
sistemas generalmente ejecutan _transacciones sobre bases de datos
distribuidas_.

Finalmente cabe nombrar los sistemas conocidos como _pervasive (penetrantes)
systems_ en los cuales intervienen dispositivos móviles formando sistemas con
poca _estabilidad_ en cuanto a que sus nodos aparecen (se unen), desaparecen y
cambian su ubicación geográfica. Algunos ejemplos de estos tipos de sistemas son
las redes de telefonía celular, redes de sensores y lo que se conoce como
_Internet de las cosas_.

## Arquitecturas

Para que un sistema califique como un sistema distribuido, tolerante a fallas y
con un desempeño aceptable, generalmente no es posible un diseño basado en una
arquitectura tradicional del tipo cliente-servidor ya que tiene un único punto
de falla y congestionamiento de computación y/o comunicaciones.

Una extensión inmediata es la arquitectura _multitier_ o _multicapa_.

![multitier](img/multitier.svg ":size=40% :class=imgcenter")

#### Figura 1.1: Arquitectura multitier.

La arquitectura que permite la mejor escalabilidad y tolerancia a fallas son
aquellas basadas en una estructura descentralizada. En una red _peer to peer
(P2P)_, cada nodo ejecuta algoritmos distribuidos generalmente sin un 
coordinador central fijo.

![peer-to-peer](img/peer-to-peer.svg ":size=40% :class=imgcenter")

#### Figura 1.2: Arquitectura peer-to-peer.

Algunos problemas pueden solucionarse de manera mas eficiente o simple
utilizando un coordinador central, pero éste constituye un único punto de falla
por lo que es necesario seleccionarlo de manera colaborativa entre los peers.
Por eso analizaremos los algoritmos de _elección_ generalmente usados para
selecciona un coordinador o líder temporal.

Un problema a resolver en una red peer-to-peer es cómo cada nodo conoce a los
demás. Generalmente esto se resuelve iniciando el sistema con un conjunto base
de nodos los cuales conocen de su existencia o usando un conjunto de servidores
_conocidos_ al que pueden consultar por la existencia de los demás. En este
último caso cada peer tiene que _suscribirse_ en uno de estos servidores.

Una arquitectura muy usada es de varios niveles (o capas).

![peers-superpeers](img/peers-and-superpeers.svg ":size=50% :class=imgcenter")

#### Figura 1.3: Arquitectura peer-to-peer de dos niveles.

En esta arquitectura, un peer se debe conectar a un _super peer_, generalmente
seleccionado por proximidad o de menor costo (utilización, ancho de banda, etc).
Los super-peers forman la _overlay network_ conformando el sistema distribuido.

Varias aplicaciones distribuidas, como por ejemplo Skype, se basan en esta
arquitectura. Un cliente toma la forma de una aplicación de usuario o una
aplicación web conectada a un _peer_. Un super-peer puede ser un simple nodo o
un cluster de computadoras (_data center_) operando como una gran computadora
paralela.

Un _peer_ (también conocido como _edge point_) puede ofrecer servicios de
_caching_ de contenidos formando parte de la red de entrega de contenidos o
_Content Delivery Contents (CDN)_, permitiendo entregar contenidos en base a la
proximidad del usuario, como por ejemplo videos de YouTube (Google Cloud) o
Netflix. En el caso de Google Cloud es posible ver a los _puntos de presencia_
como los _peers_ y los _data centers_ como los _super peers_.

Otras arquitecturas comúnmente usadas en aplicaciones distribuidos,
principalmente en nodos con grandes requerimientos de procesamiento son:

### Workers Farm

Esta arquitectura permite distribuir los requerimientos entre un conjunto de
_workers_. El distribuidor o _splitter_ (_s_) generalmente actúa como
_balanceador de carga_ y determina la mejor estrategia de _splitting_,
generalmente dividiendo la tarea en sub-tareas independientes. Esto generalmente
requiere algún análisis de _dependencia de datos_. El nodo _merge_ recolecta las
soluciones parciales y las _combina_ para entregar la solución final.

![workers-farm](img/Worker-Farm.jpg ":size=50% :class=imgcenter")

#### Figura 1.4: Arquitectura Workers-Farm.

### Map-Reduce

El patrón map-reduce se popularizó con su aplicación por Google en su motor de
búsqueda y otras aplicaciones.

Se basa en las siguientes funciones:

- $map(k1,v1) \rightarrow \[(k2,v2)\]$: Toma un par $(key, value)$ de tipo
  $(k:k1,v:k2)$ y genera una lista de pares de tipo $(k2,v2)$ 
- $reduce(k2,\[v2\]) \rightarrow \[(k3,v3)\]$: Toma una clave de tipo $k2$ y una
  lista de valores de tipo $v2$ y genera una lista de pares de tipo $(k3,v3)$.

La salida de cada _mapper_ se _combina_ y _particiona_ (_shuffled_) formando
listas de valores asociadas a cada _key_ de tipo $k2$ para asociar a cada
_reducer_, como se muestra en la siguiente figura.

![map-reduce](img/map-reduce.png ":size=50% :class=imgcenter")

#### Figura 1.5: Arquitectura Map-Reduce.

Frameworks como el de Google Map-Reduce (descripto en
[GoogleMapReduce](https://static.googleusercontent.com/media/research.google.com/en//archive/mapreduce-osdi04.pdf))
o [Apache Hadoop](https://hadoop.apache.org/) ofrecen una API para el desarrollo
de aplicaciones distribuidas basadas en este patrón.

Estas operaciones generalmente se implementan sobre una arquitectura del tipo
_farm workers_.

El siguiente pseudo-código muestra una aplicación map-reduce de recuento de
palabras en un conjunto de documentos.

```
function map(String name, String document):
  // name: document name
  // document: document contents
  for each word w in document:
    emit (w, 1)

function reduce(String word, Iterator partialCounts):
  // word: a word
  // partialCounts: a list of aggregated partial counts
  sum = 0
  for each pc in partialCounts:
    sum += pc
  emit (word, sum)
```

Los frameworks ofrecen las operaciones de _spliting_ de los items de entrada a
los _mappers_ y las operaciones _combine_, _sort_ y _partition_ (_shuffling_)
distribuyendo sus salidas entre los _reducers_. 
Estas operaciones pueden ser redefinidas por el usuario.

## Topologías virtuales

Independientemente de la arquitectura física, es común que los nodos se
organicen lógicamente formando determinadas formas o _topologías_ de
comunicación. En lugar de favorecer una comunicación todos con todos, se sigue
un patrón de comunicaciones siguiendo alguna estrategia sugerida naturalmente
por la topología lógica seleccionada.

Las topologías comúnmente usados son _pipelines_, _árboles_, _anillos_ y
_grillas_, como se muestra en la siguiente figura. Una topología menos
estructurada puede tener formas de grafos arbitrarios.

![topologies](img/network-topology.jpg ":size=60% :class=imgcenter")

#### Figura 1.3: Algunas topologías.

## Comunicaciones

Los sistemas distribuidos se basan en la interacción entre los nodos
participantes mediante la comunicación de mensajes. Si bien es posible
implementar sistemas distribuidos usando las APIs provistas por una capa de
transporte de algún protocolo de comunicaciones generalmente se utilizan
bibliotecas o frameworks de comunicación, conocidos como _middleware_ que
brindan un mayor nivel de abstracción.

Uno de los frameworks muy utilizados son los basados en _Remote Procedure Call
(RPC)_, los cuales proveen una abstracción basada en interfaces de módulos u
objetos donde el programador sigue la técnica familiar de invocación a funciones
o métodos. Cada objeto remoto tiene un _proxy_ local el cual convierte cada
invocación a alguna de sus funciones en un mensaje (_serialización_), lo envía
al host remoto y éste convierte el mensaje en una invocación local
(_dispatching_). Luego de la ejecución de la función, el resultado se retorna al
host invocante. RPC generalmente usa comunicación sincrónica, aunque varios
frameworks y bibliotecas modernas usan comunicación asincrónica retornando
_futuros_, es decir, objetos que quedarán a la espera del resultado y que podrá
obtenerse posteriormente por la aplicación. Esto permite que la aplicación
solape comunicaciones con computaciones concurrentemente. Para más detalles
sobre RPC ver
[RPC](https://marceloarroyo.gitlab.io/cursos/TySD-UNRC/networks/index.html#/presentation?id=rpc)
en las notas del curso de Redes y Telecomunicaciones.

### Frameworks de pasaje de mensajes

Otras abstracciones posibles son aquellas basadas en _patrones de comunicación_
convenientes entre _grupos de nodos_.

[ZeroMQ](https://zeromq.org/) es una biblioteca de abstracciones de patrones de
comunicación basado en mensajes sobre sockets. Se basa en comunicaciones
asincrónicas, permitiendo así explotar al máximo el paralelismo entre
comunicación y uso de CPU. Soporta varios patrones útiles de comunicación como

- _Request/Reply_: Provee mecanismos similares a RPC.
- _Publish/Subscribe_: Conecta un conjunto de _publishers_ con un conjunto de
  _subscribers_. En este patrón cada mensaje enviado por un _publisher_ es
  enviado (multicast) a sus _subscribers_.
- _Pipeline_: Permite configurar un conjunto de nodos en una topología virtual
  de _etapas_, donde cada etapa procesa datos de la anterior y los pasa a la
  siguiente. Este patrón es común para la división de tareas paralelas y su
  sincronización.

Otros frameworks y bibliotecas permiten otras abstracciones y patrones de
comunicación de uso común como por ejemplo, MPI.

El estándar [Message Passing Interface
(MPI)](https://www.mcs.anl.gov/research/projects/mpi/index.htm) provee la
descripción de una API de funciones que ejecutan patrones de comunicación
comúnmente usados para la implementación de aplicaciones paralelas de alto
rendimiento (HPC). MPI se utiliza generalmente en _clusters_ de computadoras y
brinda las abstracciones para ver un cluster como un sistema multi-procesador
basado en el modelo de programación _Single Program Multiple Data_. En este
modelo existe un único programa que se ejecuta en cada nodo participante. La API
permite que cada nodo tome diferentes roles en cada comunicación grupal.

1. Comunicaciones punto a punto
   - `send(data, dst)`: El nodo invocante envía `data` al nodo de destino dado.
   - `recv(data, src)`: El nodo invocante recibe datos del origen dado. Si la
     comunicación es sincrónica, el invocante se bloquea hasta la recepción del
     mensaje.
2. Comunicaciones grupales
   - `broadcast(data, src, grp)`: Envía copias de `data` desde `src` a todos los
     demás nodos del grupo. Esta operación se conoce también como `map`.
   - `scatter(data, src, grp)`: El nodo origen envía partes disjuntas de `data`
     a los demás nodos del grupo.

     ![scatter](img/scatter.png ':size=50% :class=imgcenter')

     #### Figura 1.4: Scatter con src=0.

   - `gather(data, dst, grp)`: El nodo `dst` recibe partes disjuntas de `data`
     desde el resto del grupo.

     ![gather](img/gather.png ':size=50% :class=imgcenter')

     #### Figura 1.5: Gather con dst=0 (de la suma de los valores de cada nodo).

   - `reduce(data, op, dst, grp)`: El nodo `dst` actúa como el receptor de datos
     enviados por los demás y aplica la operación `op` para retornar el valor
     final. El parámetro `data` es el valor transmitido por cada nodo diferente
     del destino. La operación `op` (+, -, *, min, max, avg, ...) generalmente
     debe ser asociativa y conmutativa para permitir su aplicación en cualquier
     orden (a medida que los datos arriban). Es posible implementar `reduce`
     usando `scatter` y `gather`.

En MPI, cada nodo se identifica por su _rank_ (identificador de proceso
independiente del sistema operativo). La función `rank()` ejecutada por un nodo
retorna su identificador. También MPI permite formar topologías lógicas en forma
de grillas o grafos, realizando el _mapping_ de posiciones lógicas a
identificadores de procesos, facilitando al programador coordinar las
comunicaciones.

> Un ***cluster*** es un conjunto de computadoras personales inter-conectadas
> por interfaces de red de alta velocidad usado como un sistema multi-procesador
> (_multi-computadoras_)

Comúnmente los sistemas basados en mensajes soportan comunicaciones asíncronas 
por lo que se requieren que los mensajes sean (temporalmente) encolados para su 
posterior procesamiento.

Los frameworks mencionados se implementan en forma de biblioteca, comúnmente en
C/C++ con _bindings_ a un gran número de lenguajes de programación.
