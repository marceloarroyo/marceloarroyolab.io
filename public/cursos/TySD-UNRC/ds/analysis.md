# Especificación y Análisis

En este capítulo se describen algunos métodos y formalismos para la
especificación y análisis de sistemas distribuidos. Estos formalismos comúnmente
se utilizan generalmente para la especificación de sistemas concurrentes,
independientemente si se usan modelos de memoria compartida o pasaje de
mensajes.

Existen numerosos enfoques y aquí se describen algunos de los mas utilizados
como son los basados en _finite state machines (FSM)_, _lógicas temporales_,
_redes de Petri_ y _session types_.

## Máquinas de estados finitos

Los autómatas finitos (FSMs) se utilizan comúnmente para describir programas o
máquinas de estados secuenciales y concurrentes. Una de las herramientas
utilizadas en el ámbito académico es [Labeled Transition System Analyzer
(LTSA)](https://www.doc.ic.ac.uk/~jnm/book/firstbook/ltsa/LTSA.html) que soporta
modelos en Java. La herramienta se complementa con el libro[[5]](#referencias) de
los mismos autores.

Para modelar sistemas distribuidos se han propuesto varias extensiones. Una de
las más utilizadas son el modelado con _máquinas de estado finitos comunicantes
(CFSM)_. La idea del formalismo es describir un conjunto de sistemas de
transición de estado que interactúan por medio de eventos/mensajes. Cada
autómata representa un proceso del sistema.

Formalmente, un _CFSM_ representa un _protocolo_ en el que intervienen _N_
procesos $P_1, \ldots, P_N$ y se define como una tupla $(S, I, M, T, F)$, donde

- $S=\{S_1, \ldots, S_n\}$ donde cada $S_i$ ($1 \leq i \leq N$) es el conjunto
  de estados del proceso $i$.
- $I=\{I_1, \ldots, I_n\}$ donde cada $I_i \in S_i$ ($1 \leq i \leq N$) es el
  _estado inicial_ de $P_i$.
- $M=\{M_1, \ldots, M_n\}$ donde cada $M_i$ ($1 \leq i \leq N$) es el conjunto
  de _tipos de mensajes_ que puede enviar $P_i$.
- $T=\{T1, \ldots, T_N\}$ donde cada 
  $T_i: S_i \times \cup_{j=1}^N (M_{j,i} \cup M_{i,j}) \rightarrow S_i$,
  para $1 \leq i \leq N$. El conjunto denotado po $M_{j,i}$ representa
  los mensajes que pueden ser enviados de $P_j$ a $P_i$.
- $F=\{F_1, \ldots, F_N\}$ donde cada $F_i$ ($1 \leq i \leq N$) es el _conjunto
  de estados finales_ de $P_i$.

La siguiente figura muestra una posible especificación del _two phase handshake
protocol_ simplificado en forma gráfica (similar al usados en autómatas
finitos), donde los rótulos de los arcos con las formas $!msg$ y $?msg$
representan un _envío_ o _recepción_ de un mensaje, respectivamente. Cuando hay
más de dos procesos involucrados las operaciones de envío y recepción pueden
especificar el o los procesos de destino u origen de la forma $P!msg$ y $Q?msg$,
respectivamente.

![cfsm](img/cfsm-protocol.svg ":size=70% :class=imgcenter")

#### Figura 6.1: Ejemplo de CFSM del _two-phase handshake protocol_.

En este formalismo es posible analizar algunas propiedades automáticamente, como
por ejemplo, _alcanzabilidad_, es decir, determinar si un estado es alcanzable
desde otro y _deadlock_. También es posible determinar algunos tipos de
_invariantes_.

## Redes de Petri

Este formalismo, propuesto 1n 1939 por Carl Adam Petri, forma parte de la clase
de modelos de eventos discretos en sistemas dinámicos. Una _red de Petri_ es un
grafo _bipartito_ con dos clases de nodos, _places (lugares)_ y _transiciones_.
Un _place_ puede contener un número arbitrario de _tokens_. Una transición está
_habilitada_ si todos los _places_ conectadas como _inputs_ tienen al menos un
token. Una transición al ser _disparada_. Un _disparo_ consume _tokens_ de cada
_places_ de entrada y genera _tokens_ en su salida. Cada _arco_ determina el
número de tokens extraidos y generados, lo que se conoce como la _multiplicidad
del arco_ y en caso de omisión generalmente es 1.

Existen numerosas extensiones. Una de las mas utilizadas para modelar sistemas
distribuidas son las _redes de Petri coloreadas (CPN)_ que permiten que los
tokens sean _rotulados_ o _tipados_ como se muestra en la siguiente figura.

![cfsm](img/petri-net.svg ":size=50% :class=imgcenter")

#### Figura 6.2: Two-phase handshake protocol modelado con Petri-net.

Formalmente una red de Petri es una tupla $(P, T, F, M_0, W)$ donde:

- $P$ es el conjunto de _places_.
- $T$ es el conjunto de _transiciones_.
- $P \cap T = \emptyset$
- $F \subseteq (P \times T) \cup (T \times P)$: Es el conjunto de _arcos_.
- $M_0: P \rightarrow \mathbb{Z}$ es la función de _marcado inicial_.
- $W: F \rightarrow \mathbb{Z}$ es la función de asignación de _pesos_
  (_multiplicidad_) a arcos.

En la figura 6.2 se muestra el marcado inicial con un conjunto de tokens
representando el número de mensajes a transmitir en el _place msgs_ y un token
en el _place start_ que _habilita_ la transmisión del primer mensaje.

### Ejecución

La semántica de ejecución de una _Petri net N_ se define por medio de una
relación de transición entre estados entre dos _marcados_.

$M \xrightarrow{N,t} M'$ donde $t$ es una _transición habilitada_ por $M$
y $M' = M - W(p_s,t) + W(t,p_d)$ para cada $p_s$ y $p_d$ correspondientes a los
_places_ de origen y destino de $t$, respectivamente. Las operaciones
(sobrecargadas) representan el efecto de la transición $t$, lo cual quita (-)
$W(p_s,t)$ tokens del _place_ $p_s$ y agrega (+) $W(t,p_d)$ tokens al _place_
$p_d$.

La relación $M \xrightarrow{N} M'$ representa un paso (disparo) de
cualquier transición habilitada. Se debe notar que una red de Petri es un modelo
_no determinístico_ ya que en un estado puede haber múltiples transiciones
habilitadas.

Se denota como $M \xrightarrow{N^*} M'$ la _clausura reflexo-transitiva_
(cero o más pasos) de $\xrightarrow{N}$.

Una _secuencia de disparos_ de una red de Petri $N$ es una secuencia de
transiciones $<t_1 \ldots t_n>$ tal que 
$M_0 \xrightarrow{N,t_1} \ldots \xrightarrow{N,t_n} M_n$.
El conjunto de todas las secuencias de disparo se denota como $L(N)$.

$R(N)=\{ M \mid M_0 \xrightarrow{N^*} M\}$ es el conjunto de _markings
alcanzados_.

### Análisis de Petri-nets

Las redes de Petri permiten construir un modelo ejecutable con el cual se puede
experimentar (simular y probar) su comportamiento. Generalmente también se desea
analizar propiedades generales y sus algoritmos de decisión como:

1. ***Alcanzabilidad***: Determinar si un marking $M' \in R(N)$ desde un marking
   dado. Recientemente (2021) se demostró que este problema es no decidible en
   general. En la práctica es mas común tratar de mostrar que no es posible
   alcanzar estados erróneos, lo cual puede realizarse utilizando _lógica
   temporal lineal_ conjuntamente con el método _tableau_.
2. ***Liveness (progreso)***: Es posible demostrar diferentes niveles:
   - *dead*: No puede disparar desde un marking dado.
   - $L_1-live$: Existe al menos un disparo en alguna secuencia de $L(N)$
   - $L_2-live$: Puede disparar eventualmente.
   - $L_3-live$: Puede disparar eventualmente infinitamente.
   - $L_4-live$ (_live_): Puede disparar siempre en cada estado posible.
3. ***Boundeness (acotada)***: Una Petri net se dice _k bounded_ si no contiene
   markings posibles con más de _k_ tokens. Esto es útil para analizar el uso y
   gestión de recursos.

Existen numerosas herramientas para la simulación y análisis de redes de Petri y
es uno de los modelos más usados en la industria. Muchos lenguajes de
especificación de sistemas concurrentes y distribuidos definen su semántica en
términos de redes de Petri.

Entre las numerosas extensiones cabe mencionar las _timed Petri nets_ que
permiten que las transiciones se disparen en base a _eventos temporales_, útiles
para modelar _timeouts_ y otros escenarios. Las _Petri Nets Jerárquicas_
permiten modelar sistemas modulares, haciendo que un _place_ represente una
_Petri net_ o subsistema.

## Lógica Temporal

La lógica temporal contiene _modalidades_ para predicar sobre estados en el
tiempo (pasos discretos). La fórmula $\square F$ denota que $F$ es verdadera
siempre (en cada estado) y la fórmula $\diamond F$ especifica que $F$ es
_eventualmente_ verdadera (en algún estado futuro). [Temporal Logic of Actions
(TLA+)](https://lamport.azurewebsites.net/tla/tla.html) es un lenguaje
desarrollado por Leslie Lamport para la especificación de sistemas distribuidos.
Además del lenguaje, el cual se basa en lógica temporal enriquecido con la
definición de tipos de datos y especificación de propiedades a verificar.

Las lógicas temporales permiten una técnica para determinar semi-automáticamente
si una fórmula dada es _satisfactible_ conocida como _model ckecking_, el cual se
basa en la generación de [autómatas de
Büchi](https://en.wikipedia.org/wiki/B%C3%BCchi_automaton), los cuales son una
versión de autómatas finitos sobre _entradas infinitas_, aunque muchas
herramientas actuales de model-checking utilizan SAT solvers proposicionales.

TLA+ _toolset_ es un conjunto de herramientas de soporte. Existen lenguajes de
alto nivel (como PlusCal) que permite definir algoritmos (especialmente
concurrentes y distribuidos) y es más adecuado para desarrolladores de software.
Un algoritmo PlusCal se traduce a un modelo TLA+ sobre el cual se pueden realizar
análisis de propiedades.

El siguiente listado muestra una especificación al estilo _TLA+_ (con pequeños
cambios tipográficos) del protocolo de ejemplo.

```
EXTENDS Naturals
CONSTANT Data
VARIABLES val, rdy, ack
TypeInvariant = val in Data and rdy in {0,1} and ack in {0,1}
Init = val in Data and rdy in {0,1} and ack == rdy
Send = rdy == ack and val' in Data and rdy'=(1-rdy) and UNCHANGED ack
Recv = rdy != ack and ack' = (1-ack) and UNCHANGED <val,rdy>
Next = Send or Recv
Spec = Init and [][Next]<val,rdy,ack>

THEOREM Spec ==> []TypeInvariant
```

La especificación define los _estados_ `Init`, `Send`, `Recv` y `Next` del
sistema. 
La notación `v'=v` denota que `v'` es el _nuevo valor_ de 'v' (o su valor en el
próximo estado).

La especificación `Spec` define que partiendo del estado inicial,
siempre vale `Next`, lo cual representa que el sistema puede transicionar a los
estados `Send` y `Recv` infinitamente. La notación `[Next]<val,rdy,ack>` es una
abreviación de `Next or (val'=val and rdy'=rdy and ack'=ack)`, es decir que es
válida una transición $epsilon$ (dejando los valores sin cambiar).

## Session types

Otro enfoque para la especificación e implementación de sistemas distribuidos es
el desarrollo de sistemas de tipos que permitan describir programas verificables
por un compilador o intérprete de un lenguaje de programación.

El sistema de tipos conocido como _session types_ permite definir un protocolo
global y la lógica de cada uno de los procesos participantes (o roles). El
sistema es verificable (_type-checked_) automáticamente.

El sistema de tipos define los siguientes constructores de tipos:

- $send_{p,q} \tau$: Envío de un mensaje de tipo $\tau$ del _rol_ $p$ a $q$.
- $recv_{q,p} \tau$: Recepción (en $q$) de un mensaje de tipo $\tau$ enviado por
  $p$.
- $offer_{p,q} \{t_1, \ldots , t_n\}$: El rol $p$ selecciona (no
  determinísticamente) un sub-protocolo (tipo) y se lo comunica a $q$.
- $choose_{q,p} \{l_1: t_1, \ldots, l_n: t_n\}$: El rol $q$ selecciona la rama
  rotulada $l_i$ en base al _offer_ $t_i$ recibido por $p$.
- $\epsilon$: End protocol.

En los casos de protocolos con sólo dos roles, es posible omitir el emisor y
receptor.

A continuación se muestra el protocolo global usado como ejemplo con _session
types_ donde `S` y `R` son los _roles_ de los procesos _Sender_ y _Receiver_.

> TPP: $send_{S,R}$ (data,seq); $recv_{R,S}$ (data,seq); $send_{R,S}$ ack(seq);
$recv_{S,R}$ ack(seq); TPP

En este ejemplo el protocolo _TPP_ es recursivo, lo cual permite especificar
ciclos o comunicación _infinita_.

El protocolo global se puede _proyectar_ (automáticamente) en los tipos de cada
participante (rol).

```
S: send (data,seq); recv ack(seq); S
R: recv (data,seq); send ack(seq); R;
```

Estas expresiones sobre tipos pueden ser verificadas por un _type checker_. La
verificación consiste en que los procesos siguen una coreografía que no lleva a
_deadlock_ y que los mensajes enviados y recibidos tienen los tipos esperados.

A continuación se muestran dos programas que implementan los tipos definidos.

```
Sender: S {                           Receiver: R {
  while true {                          while true {
    send (data, seq);                     recv (data, seq);
    recv ack(n);                          send ack(seq);
    seq = 1 - seq;                      }
  }                                   }
}
```

El _type checker_ debe verificar que un proceso es _DUAL_ del otro, usando las
siguientes reglas:

1. $dual(\epsilon) = \epsilon$
2. $dual(send_{tx,rx} T; \alpha) = recv_{rx,tx} T; dual(\alpha)$
3. $dual(recv_{rx,tx} T; \alpha) = send_{tx,rx} T; dual(\alpha)$
4. $dual(offer_{p,q} \{l_i: t_i\}); \alpha = choice_{q,p} \{l_i: dual(t_i)\};
   dual(\alpha)$ con $(1 \leq i \leq n)$
5. $dual(choice_{q,p} \{l_i: t_i\}); \alpha = offer_{p,q} \{l_i: dual(t_i)\};
   dual(\alpha)$, con $(1 \leq i \leq n)$

Es fácil ver que cuando dos procesos son _duales_ (o _co-tipos_) siguen los
patones de comunicación adecuadamente, es decir que cuando uno envía un tipo de
mensaje, el otro lo espera y viceversa, pero no describe la _lógica_ del
protocolo, es decir cómo computan o procesan los datos transmitidos y recibidos.
Por ejemplo, no es posible especificar cómo transicional los números de secuencia 
entre los valores 0 y 1.

Es fácil de demostrar que el tipo `S = dual(R)`:

1. `dual(R) = dual(recv (data,seq); send ack(seq); R;) = `
2. `send (data,seq); dual(send ack(seq); R); = `
3. `send (data,seq); recv ack(seq); dual(R); = `
3. `send (data,seq); recv ack(seq); S = S`

A continuación se muestra un ejemplo mas interesante que contempla el caso de
transmisiones con errores, en cuyo caso el receptor en lugar de responder con
$ack(n)$ responde con _error_. Esta especificación requiere el uso de _offer_ y
_choice_ .

```
S: send (data,seq); choice {ack(n): S, error: end}
R: recv (data,seq); offer {send ack(seq); R, send error; end}
```

***Ejercicio:*** Probar $S=dual(R)$ (o viceversa).

Existen lenguajes y herramientas para especificar, verificar y generar código
para diferentes lenguajes de programación basados en esta idea. Uno de ellas es
[Scribble](http://www.scribble.org/) basado en session types y _Pi calculus_.
Este proyecto tiene herramientas para generar código (interfaces o esqueletos)
para varios lenguajes de programación y puede _proyectar_ el código de cada
participante (rol) a partir de la _especificación global_ del protocolo.

También se han implementado bibliotecas en algunos lenguajes de programación
aprovechando las características de sus sistemas de tipos y mecanismos de
meta-programación, como para Haskell, Ocaml, Scala y Rust. En algunos de estos
lenguajes es necesario realizar algunas verificaciones en tiempo de ejecución
(_runtime_) ya que en general se requiere un sistema de tipos _lineal_ (en donde
una variable puede modificarse una única vez). El sistema de tipos de Rust
(_affine_) es muy adecuado para implementar este sistema de tipos y se han
desarrollado varias bibliotecas.

## Referencias

1. Leslie Lamport. _Specifying Systems. The TLA+ Language and Tools for Hardware
   and Software Engineers_. 2003, Pearson Education.
   [Online](https://lamport.azurewebsites.net/tla/book-21-07-04.pdf) update
   (July, 2021).
2. N. Yoshida at al. _The Scribble Protocol Language_. [Scribble
   tutorial](https://www.dmi.unict.it/barba/FOND-LING-PROG-DISTR/PROGRAMMI-TESTI/READING-MATERIAL/ScribbleTutorial.pdf).
3. Kurt Jensen, Lars Cristensen. _Coloured Petri Nets. Modeling and Validation
   of Concurrent Systems_. Springer-Verlag. 2009. ISBN 978-3-642-00283-0. e-ISBN
   978-3-642-00284-7.
4. N. Yoshida et al. _Communicating Finite State Machines and an Extensible
   Toolchain for Multiparty Session Types_.
   [Download](http://mrg.doc.ic.ac.uk/publications/communicating-finite-state-machines-and-an-extensible-toolchain-for-multiparty-session-types/main.pdf).
5. Jeff Magee, Jeff Kramer. _Concurrency. State Models & Java Programs_. Second
   Edition. Wiley.