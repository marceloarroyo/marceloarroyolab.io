# Marcelo Arroyo

## About me
        
I'm full time professor at 
[Computer science department](https://dc.exa.unrc.edu.ar/) -
FCEFQyN - Río Cuarto National University (***UNRC***), Argentina.

Also I have a position as visiting professor at 
[Engineering Faculty](https://www.ing.unlpam.edu.ar/) of La Pampa
National University (UNLPam).

------------------------------------------------------------------

## Teaching

Below there are links to some of my current course notes (in spanish).

### At UNRC: *Bachelor in computer science*

- <a href="cursos/SO/index.html">Operating Systems</a>
- <a href="cursos/TySD-UNRC/networks/">Networking and Telecommunications</a>
- <a href="cursos/TySD-UNRC/ds/">Telecommunications and Distributed Systems</a>

### UNLPam: *Computer Engineering*

- <a href="/cursos/seguridad-UNLPam/">Computer Security</a>
- Creator and coordinator of the *Diplomacy on computer security*

--------------------------------------------------------------------

## Research
        
I'm member of [MFIS](https://mfis.dc.exa.unrc.edu.ar/): *Formal Methods
and Software Engineering* research group at UNRC. You can get more
information about staff members, research projects, papers and tools
used and developed by our research team.

#### My main research areas of interest

- Programming languages (type systems, compilers, ...)
- Program static analysis (bugs, vulnerabilities, ...)
- Network protocols analysis and distributed systems.
- Operating systems design and implementation.

and others...
