<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<meta charset="utf-8"/>
	<title>Introducción a make y makefiles</title>
	<meta name="author" content="Marcelo Arroyo"/>
	<meta name="date" content="2021"/>
<link rel="stylesheet" href="https://unpkg.com/@highlightjs/cdn-assets@10.7.2/styles/a11y-dark.min.css"> <script src="https://unpkg.com/@highlightjs/cdn-assets@10.7.2/highlight.min.js"> </script> <script>hljs.initHighlightingOnLoad();</script>
	<link type="text/css" rel="stylesheet" href="tutorial.css"/>
</head>
<body>

<blockquote>
<h1 id="introduccióna_make_">Introducción a <em>Make</em></h1>

<p>Marcelo Arroyo</p>

<p>2021</p>
</blockquote>

<hr />

<p>La herramienta <a href="https://www.gnu.org/software/make/manual/make.html">make</a> es ampliamente usada para la compilación de grandes
proyectos de software, permitiendo la <em>recompilación</em> de sólo aquellos módulos
que han sido modificados.</p>

<p>Estas herramientas, conocidas como <em>build tools</em>, detectan los módulos
modificados (analizando la fecha y hora) y sus dependencias para determinar el
conjunto de módulos a recompilar, la secuencia requerida y ejecutando los
comandos necesarios para hacerlo.</p>

<h2 id="makefiles">Makefiles</h2>

<p>El usuario (programador) debe definir un <em>Makefile</em>, el cual se basa en reglas
de la siguiente forma:</p>

<pre><code class="Makefile">target: dependencies
&lt;tab&gt; commands
</code></pre>

<p>El comando <code>make</code> asume que las reglas están en un archivo llamado <code>Makefile</code> o
<code>makefile</code>. Es posible que tenga otro nombre, en cuyo caso hay que indicárselo
de la forma <code>make -f mymakefile ...</code>.</p>

<p>A modo de ejemplo, supongamos que tenemos un pequeño proyecto en C con tres
módulos (archivos fuente): <code>main.c</code>, <code>m1.c</code> y <code>m2.c</code>, donde desde <code>main.c</code> se
invocan funciones definidas en <code>m1.c</code> y <code>m2.c</code>. A su vez desde <code>m1.c</code> se invocan
a funciones de <code>m2.c</code>.</p>

<figure>
<img src="deps.svg" alt="Grafo de dependencias de los módulos." />
<figcaption>Grafo de dependencias de los módulos.</figcaption>
</figure>

<p>Un <code>Makefile</code> correspondiente para este ejemplo, el cual produciría el
ejecutable <code>myprog</code>, sería el siguiente.</p>

<pre><code class="Makefile">myprog: main.o m1.o m2.o
    gcc -o myprog main.o m1.o m2.o

main.o: main.c
    gcc -c main.c

m1.o: m1.c
    gcc -c m1.c

m2.o: m2.c
    gcc -c m2.c

clean:
    rm *.o
</code></pre>

<p>Ejecutando el comando <code>make</code> (sin ningún argumento) se dispara la evaluación de
la primera regla.</p>

<p>El comando <code>make clean</code> dispara la regla <code>clean</code>, la cual borra los archivos
intermedios <code>*.o</code>.</p>

<p>Es posible ejecutar mútiples <em>objetivos</em> desde un mismo comando.</p>

<h2 id="variables">Variables</h2>

<p>En un makefile podemos definir variables para luego referenciarlas en las
reglas.</p>

<p>Una <em>variable</em> se puede definir de dos formas:</p>

<ul>
<li><code>var = expression</code>: En éste caso <code>var</code> se dice que es una variable de <em>uso
recursivo</em>. Su valor se expandirá en su <em>uso</em>, no en su definición.</li>
<li><code>var := expression</code>: Su valor se determina en el contexto de la evaluación
(expansión) de <code>expression</code>. Esta forma de definción se denomina <em>expansión
simple</em>.</li>
</ul>

<p>Ejemplo:</p>

<pre><code class="Makefile">v1 = hello ${other}
v2 := what a ${other}
other = world
...
    $(v1)   # expands to &quot;hello world&quot;
    $(v2)   # expands to &quot;what a&quot;
</code></pre>

<p>También <code>make</code> define para cada regla las siguientes (<em>automatic</em>) variables,
entre otras:</p>

<ul>
<li><code>$@</code>: Referencia el <code>target</code></li>
<li><code>$^</code>: Referencia los <em>prerequisitos</em> o <em>dependencias</em></li>
<li><code>$&lt;</code>: Referencia al <em>primer prerequisito</em></li>
</ul>

<p>El <code>Makefile</code> anterior podría ser modificado usando variables.</p>

<pre><code class="Makefile">objs = main.o m1.o m2.o

myprog: $(objs)
    gcc -o $@ $(objs)

main.o: main.c
    gcc -c main.c

m1.o: m1.c
    gcc -c m1.c

m2.o: m2.c
    gcc -c m2.c
</code></pre>

<h2 id="comodineswilcards">Comodines (wilcards)</h2>

<p>Es posible usar comodines como <code>*</code> y <code>?</code> tanto en la definición de variables
como en las reglas.</p>

<pre><code class="Makefile">objs: *.o

...
</code></pre>

<h2 id="patronesyreglasimplícitas">Patrones y reglas implícitas</h2>

<p>Para simplificar la escritura de Makefiles en grandes proyectos es posible
definir patrones que representan muchas otras.</p>

<p>Por ejemplo, las reglas que compilar cada archivo fuente <code>.c</code> en el <code>Makefile</code>
del ejemplo anterior, podrían colapsarse en una única regla como la siguiente:</p>

<pre><code class="Makefile">objs: main.o m1.o m2.o

myprog: $(objs)
    gcc -o $@ $(objs)

%.o: %.c
    gcc -c $&lt; -o $@
</code></pre>

<p>La compilación de programas C es muy común tener reglas de la forma como
vimos en el <code>Makefile</code> de ejemplo. Por eso <code>make</code> soporta <em>reglas implícitas</em>:</p>

<ul>
<li>Una regla de la forma <code>target.o: target.c</code> ejecuta automáticamente el comando
<code>$(CC) -c $(CPPFLAGS) $(CFLAGS)</code></li>
<li>Una regla de la forma <code>target: m1.o m2.o</code> ejecuta automáticamente el
comando <code>$(CC) $(LDFLAGS) $^ $(LOADLIBES) $(LDLIBS)</code> si existe un archivo
<code>target.c</code>. Es decir, realiza los siguientes pasos:</li>
</ul>

<ol>
<li> <code>cc -c target.c -o target.o</code></li>
<li> <code>cc -c m1.c -o m1.o</code></li>
<li> <code>cc -c m2.c -o m2.o</code></li>
<li> <code>cc -o target target.o m1.o m2.o</code></li>
<li> <code>rm target.o m1.o m2.o</code></li>
</ol>

<p>Se debe notar que las reglas implícitas asumen ciertas <em>variables</em> como <code>CC</code>,
<code>CPPFLAGS</code>, <code>CFLAGS</code>, <code>LDFLAGS</code>, <code>LDLIBS</code> y otras, que corresponden al
<em>compilador C</em>, <em>flags del pre-procesador</em>, <em>flags de compilación</em>, <em>flags de
linking</em> y la lista de <em>bibliotecas</em> las cuales el <em>linker</em> debe enlazar,
respectivamente.</p>

<p>Para otros lenguajes, como <em>C++</em>, <em>Fortran</em>, <em>Pascal</em>, <em>lex</em>, <em>yacc</em> y otros,
existen reglas similares con las variables correspondientes.</p>

<h2 id="condicionales">Condicionales</h2>

<p>Es muy común tener que realizar acciones dependiendo de determinadas
condiciones (plataforma, tipos de archivos, etc), por lo que <code>make</code> soporta
<em>cláusulas condicionales</em> como se muestra a continuación:</p>

<pre><code class="Makefile">ifeq ($(PLATFORM),UNIX)
    ...
else
    ...
endif
</code></pre>

<p>Los condiciones <code>ifdef</code> <em>variable</em> y <code>ifndef</code> <em>variable</em> permiten ejecutar
acciones basadas en base a si una determinada variable está definida o no.</p>

<h2 id="funciones">Funciones</h2>

<p>Make incluye un conjunto importante de <em>funciones de manejo de texto</em> y también
permite que el usuario defina sus propias funciones.</p>

<p>Una función se invoca según la siguiente sintaxis: <code>$(function args)</code> o
<code>${function args}</code>.</p>

<p>Para una referencia de las <em>funciones built-in</em> de <code>make</code>, referirse a
<a href="https://www.gnu.org/software/make/manual/html_node/Text-Functions.html#Text-Functions">funciones de texto</a>.</p>

<p>Es posible <em>crear funciones</em> por medio de definición de variables. En la
expresión la variable <code>$(n)</code> refiere al parámetro <em>n</em>.</p>

<pre><code class="Makefile">pair = \( $(0) , $(1) \)
</code></pre>

<p>Se invoca usando la función (built-in): <code>@echo $(call pair, hello, world)</code>, lo
que genera la salida <code>(hello,world)</code>.</p>

<h2 id="inclusióndeotros_makefiles_">Inclusión de otros <em>makefiles</em></h2>

<p>La directiva <code>include filename ...</code> permite incluir contenido de otros
makefiles. Esto es muy útil cuando un proyecto incluye muchos subproductos.</p>

</body>
</html>

