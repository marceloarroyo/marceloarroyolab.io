use std::sync::{Arc, Mutex};
use std::thread;

fn main() {
    let counter = Arc::new(Mutex::new(0));
    let mut threads = vec![];

    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let t = thread::spawn(move || {
            let mut num = counter.lock().unwrap();
            *num += 1;
        });
        threads.push(t);
     }

     for t in threads {
        t.join().unwrap();
     }

     println!("Result: {}", *counter.lock().unwrap());
}
