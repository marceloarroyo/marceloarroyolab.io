fn main() {
    let s1 = String::from("Hello");
    let s2 = s1 + " world";
    println!("s2 = {}", s2);
    println!("s1 = {}", s1);
}
