fn f(s1: &mut String) {
    s1.push_str(" world");
    println!("s1 = {}", s1);
}

fn main() {
    let mut s = String::from("Hello");
    f(&mut s);
    println!("s = {}", s);
}
