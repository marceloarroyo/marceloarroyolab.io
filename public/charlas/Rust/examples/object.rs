struct A {}

impl A {
    fn m(&self) {
        println!("Hi from A::m()");
    }
}

fn main() {
    let a = A{};
    a.m();
}
