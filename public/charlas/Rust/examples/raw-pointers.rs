fn main() {
    let mut x = 5;
    let cptr = &x as *const i32;
    let mptr = &mut x as *mut i32;
    // x += 1; // error: x is inmutable
    unsafe {
        // *cptr += 1; // again
        *mptr += 1;
        println!("x={}, *cptr={}, *mptr={}", x, *cptr, *mptr);
    }
}
