
#[derive(Clone, Copy, Debug)]
struct Point {x: i32, y: i32}

fn main() {
    let x = 42; let y = x;
    println!("x={}, y={}", x, y);
    let p1 = Point {x: 100, y:200}; let mut p2 = p1;
    p2.y += 50;
    println!("p1={:?}, p2={:?}", p1, p2);
}
