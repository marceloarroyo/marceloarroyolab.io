# Introducción al lenguaje de programación Rust

Marcelo Arroyo [Dpto. de Computación - FCEFQyN - UNRC](https://dc.exa.unrc.edu.ar/)

------------------------------------------------------------------------

## Introducción al lenguaje

Marcelo Arroyo - Dpto. de Computación - FCEFQyN - UNRC</p>

<figure><img alt="Rust logo" class="cover" src="img/rust-logo.webp"></figure>

------------------------------------------------------------------------

## ¿Porqué Rust?

Necesidad de un lenguaje moderno como alternativa a C/C++

- Apto para programación a nivel de sistema (operativo). Ej: componenyes del
  Linux kenel
- *Runtime* simple: A lo C/C++ (no garbage collector). Mínimas verificaciones
  dinámicas
- Programación *segura*
  1. Sin errores de gestión de memoria
  2. Sin *race conditions* en concurrencia

------------------------------------------------------------------------

## Características

- Sistema de tipos avanzado: *Generecidad + traits*
- Variables ***inmutables*** por omisión
- Asignación: *Move semantics* de objetos *no copiables*
- Restricción de *aliasing*: Múltiples referencias al mismo valor
- Sintaxis clara
- Estilos procedural/funcional + mecanismos orientados a objetos

------------------------------------------------------------------------

### Ejemplo

``` rust
// file: hello.rs
fn main() {
    println!("Hello, world!"); // call to macro printf
}
```

------------------------------------------------------------------------

### Compilación

<div id="hello" class="asciinema-play"></div>

------------------------------------------------------------------------

## Estructura de un programa/biblioteca

**Programa/módulo**: Secuencia de *declaraciones*:

  - Definición de módulos y uso de *crates (libraries)*
  - Definición de tipos de datos
  - Variables globales
  - Funciones

------------------------------------------------------------------------

## Herramientas de desarrollo

- `rustc`: El compilador Rust. Genera código [LLVM](https://llvm.org/)
- `cargo`: Build system, genera documentación, ejecuta aplicaciones y gestiona
  dependencias.

#### Terminología:

- *Paquete*: Conjunto de módulos que integran una aplicación o biblioteca
- *Crate*: Biblioteca

------------------------------------------------------------------------

## Tipos de datos *escalares*

- Enteros (signed/unsigned): `i8/u8`, `i16/u16`, ... `i128/u128` 
- Reales (*floating point*): `f32`, `f64`
- Boolean (`bool`): Constantes `true`, `false`
- Char (`char`): Códigos Unicode (4 bytes)
- Referencias y punteros

------------------------------------------------------------------------

## Tipos de datos *compuestos*

- Arreglos
- Tuplas
- Uniones disjuntas
- Estructuras
- Slices
- String literals (vectors of chars): `"I'm a string"`

------------------------------------------------------------------------

### Arreglos: Secuencia *homogénea* de elementos *contiguos*

``` rust
...
let a = [1, 2, 3, 4, 5];
...
assert_eq!(a[3], 4);
println!("a[5]={}", a[5]); // panic! (runtime error)
```

------------------------------------------------------------------------

### Tuplas: grupo de datos heterogéneo

``` rust
let t: (i32, i32, f32) = (100, 120, 60.5);
let (x, y, angle) = t;
let x2 = t.0;
let y2 = t.1;
println!("x={}, y={}, angle={}", x2, y, angle);
```

------------------------------------------------------------------------

### Uniones disjuntas (enums): *diferentes valores* o *representaciones*

``` rust
enum Option<T> {None, Some(T)};
let v : Option<int> = get_integer();
match (v) {
    Some(n) => println!("Value: {}", n),
    None    =>  println!("Invalid input"),
}
```

------------------------------------------------------------------------

### Estructuras: *grupo de valores nombrados (campos)*

``` rust
struct Person {id: i32; name: String};
let me = Person {
    id: 1000, 
    name: String::from("It's me")
};
println!("Me: {}", me.name);
```

------------------------------------------------------------------------

## Containers (in standard library)

- Vector: Secuencia ***extensible*** de valores (homogéneo)

  ``` rust
  let mut v : Vec<i32> = Vec::new(); 
  v.push(1);
  let x : Option<i32> = v.get(0); // or v[0]
  let n = x.unwrap(); // get value from Some(value)
  ```

------------------------------------------------------------------------

## Containers: String (extensible char arrays)

``` rust
let s1 = String::from("Hello, ");
let s2 = String::from("world!");
let s3 = s1 + &s2; // op + = add(self, other:&String)
println!("s3={}", s3);
println!("s2={}", s2);
```

------------------------------------------------------------------------

### Containers: HashMap<K,V> (dictionary)

``` rust
    use std::collections::HashMap;
    let mut scores = HashMap::new();
    scores.insert(String::from("Blue"), 10); // ...
    for (key, value) in &scores {
        println!("{key}: {value}");
    }
```

------------------------------------------------------------------------

### Variables: shadowing

``` rust
let x = 5;
let x = x + 1; // first x life ends here
{
    let x = x + 1; // another in inner scope
    println!("inner x; {}", x); // x == 7
}
println!("outer x; {}", x); // x == 6
```

------------------------------------------------------------------------

### Variables y mutabilidad

- Variables: ***inmutables*** por omisión

``` rust
let v = 5;
let mut m = 10;
m += 1;
v += 1; // error here
println!("v={}, m={}", v, m);
```

------------------------------------------------------------------------

### Asignación de valores *copiables*

- Los tipos básicos escalares son *copiables*
- En estos casos, la asignación *copia* valores (como en otros lenguajes de
  programación

``` rust
let mut x = 5; let y = 1;
x = y;
println!("x={}, y={}", x, y);
```

------------------------------------------------------------------------

### Asignación de valores *no copiables*: *move*

``` rust
let s1 = String::from("Hello");
let s2 = s1;    // s1 moved into s2, s1 becomes invalid
println!("s3 = {}", s3);
println!("s1 = {}", s1); // error
```

> Un *move* cambia el *owner*. La variable anterior se invalida

------------------------------------------------------------------------

### Referencias

- Alias de variables.
- Una referencia toma prestado el *ownership* durante su vida
- Invariante: `vida(referencia) < vida(valor_referenciado)`

``` rust
let v = 5;                    //  -+
let r = &v;   // r "borrows" v     | v life
println!("v={}, r={}", v, r); //  -+
```

------------------------------------------------------------------------

### Control de aliasing

- No se puede tener aliasing de valores ***mutables*** en un mismo *scope*

``` rust
fn main() {
    let mut v = 5;
    let r = &mut v;     // r borrows v
    println!("v={}, r={}", v, r); // error
}
```

------------------------------------------------------------------------

### Error de compilación

<div id="aliasing" class="asciinema-play"></div>

------------------------------------------------------------------------

### Ownership: ejemplo

``` rust
let s1 = String.from("Hello"); // String is not Copy
let s2 = s1; // s1 "moved" to s2
println!("s1: {}", s1);
println!("s2: {}", s2); // error here
```

Si comentamos la última línea, no hay error: (`s2` *muere* antes de reusar `s1`)

------------------------------------------------------------------------

### Ownership error

<div id="string-example" class="asciinema-play"></div>

------------------------------------------------------------------------

### Moving: Asignación eficiente (copia de punteros)

I) Antes de la línea 2:

```
s1 -> +----------+   +->+---+---+---+---+---+
      | length=5 |   |  |'h'|'e'|'l'|'l'|'o'|
      +----------+   |  +---+---+---+---+---+
      |   ptr ---+---+
      +----------+
```

------------------------------------------------------------------------

### Moving: Asignación eficiente (copia de punteros)

II) Luego de la línea 2:

```
s1 -> null

s  -> +----------+   +->+---+---+---+---+---+
      | length=5 |   |  |'h'|'e'|'l'|'l'|'o'|
      +----------+   |  +---+---+---+---+---+
      |   ptr ---+---+
      +----------+
```

------------------------------------------------------------------------

### Borrowing (prestar ownership) a referencias

``` rust
let mut v = vec![1,2,3];
let mut r = &v[1]; // v borrows ownership to r
r = 5;
println!("r={}", r);
// here r is dead (no more uses), v recover ownership
println!("v[1]={}", v[1]);
```

------------------------------------------------------------------------

### Pasaje de parámetros no copiables: *move*

``` rust
fn f(s: String) { 
    println!("s={}", s); 
} // s died (and free) here
...
let s1 = String.from("Hello");
f(s1); // s1 moved into f here
println!("s1={}", s1); // error: s1 memory dropped
```

------------------------------------------------------------------------

### Error de compilación (de nuevo...)

<div id="move-args" class="asciinema-play"></div>

------------------------------------------------------------------------

### Solución 1

``` rust
fn f(s: String) { 
    println!("s={}", s); 
    s // or return s;
}
...
let s1 = f(s1); // recovering (new) s1
println!("s1={}", s1); // ok
```

------------------------------------------------------------------------

### Pasar por referencia (prestar ownership)

- Retornar parámetros para recuperar *valores* es tedioso y a veces imposible
- Una ***referencia*** toma *prestado* el valor temporalmente

``` rust
fn f(s: &String) { println!("s={}", s); }
...
let s1 = String::from("Hello");
f(&s1);                 // borrows s1 to s
println!("s1={}", s1);  // we owns s1 again
```

------------------------------------------------------------------------

### Slices

- Un *slice* representa una *secuencia de objetos contiguos en memoria*
- Semánticamente son *referencias*: Toman prestado *ownership*
- Ej: El tipo `&str` es un *slice de un string*: <br>

``` rust
let s  = String::from("hello world");
let s1 = &s[0..5]; // s1 == "hello"
```

------------------------------------------------------------------------

### Traits: Typeclasses (shared behavior)

- `Copy`: Los tipos escalares son (implementan) este trait. La asignación es
  *copia* (equivalente a un `memmov(dst, src, size)` de C)
- Una estructura puede ser `Copy` si todos sus campos son `Copy`
- `Clone`: Tipos clonables. Implementan el método `clone` (copia su
  representación). Ej: `let v2 = v1.clone();`
- El compilador puede *generar* implementaciones (*default*)
- `Deref` usado por algunos *smart pointers* para desreferenciar implícitamente

------------------------------------------------------------------------

### Implementación de Traits. Ejemplos

``` rust
struct MyVec = {...}
impl Clone for myVec {
    fn clone(&self)  -> Self { return self.duplicate(); }
}
// drop (destructor) is called when value life ends
impl Drop for MyVec {
    fn drop(&mut self) { // free MyVec memory }
}
```

------------------------------------------------------------------------

``` rust
#[derive(Clone, Copy, Debug)]
struct Point {x: i32, y: i32}
fn main() {
    let x = 42; let y = x; // copyable
    println!("x={}, y={}", x, y);
    let p1 = Point {x: 100, y:200};
    let mut p2 = p1;  // copied! (not noved)
    p2.y += 50;
    println!("p1={:?}, p2={:?}", p1, p2);
}
```

------------------------------------------------------------------------

### Ejecución del ejemplo anterior

<div id="copyable-types" class="asciinema-play"></div>

------------------------------------------------------------------------

### Los traits son más que Java interfaces

- Comúnmente se usan para *limitar (bound)* tipos
- Ej: `fn sort(x: &Vec<PartialOrd>) -> Vec<PartialOrd>)`
- Múltiples *bounds*: <br>
  Ej: `fn sort(x: &Vec<PartialOrd + Copy>) -> Vec<PartialOrd + Copy>)`
- *Tipos asociados*: `trait Value { item A; fn get(&self) -> A}`

------------------------------------------------------------------------

### Reglas de ownership

1. Pueden haber múltiples referencias ***inmutables*** a un valor
2. Sólo una variable (o referencia) puede mutar un valor

### Ventajas

1. Previene *efectos colaterales*
2. Permite *recuperar memoria* en forma segura al morir el *dueño*
3. En programas *multithreaded* previene *race conditions*

------------------------------------------------------------------------

### Limitaciones de *ownership*

- Implementación de estructuras de datos con *aliasing*

  - Listas doblemente enlazadas: `node == node.prev.next`
  - Árboles: `node == node.left.parent`
  - Grafos, ...

<span style="color:red;">Solución???</span>

------------------------------------------------------------------------

### Punteros (*raw*) y código *inseguro*

- Rust también tiene *punteros*:
- Diferencias con *referencias*:
  1. Un puntero debe ser *desreferenciado explícitamente* (`*ptr`)
  2. La *des-referenciación* es *insegura* (*unsafe*)

> **Código unsafe**: Usado para des-referenciar punteros, e invocar a 
> **funciones unsafe** (ej: externas, de bibliotecas C)

------------------------------------------------------------------------

### Ejemplo de punteros y código unsafe

``` rust
fn main() {
    let mut x = 5;
    let cptr = &x as *const i32;
    let mptr = &mut x as *mut i32;
    unsafe { *mptr += 1; }
    println!("x={}", x); // output: x=6
}
```

------------------------------------------------------------------------

### ¿Porqué des-referenciar punteros es inseguro?

1. En el ejemplo anterior: `cptr` apunta a un valor inmutable (`&x`), pero en
   realidad el valor de `x` cambia.
2. El código anterior no cumple con el *invariante* de aliasing.
3. Es posible que apunten a datos inválidos, cuya memoria fue liberada
   (*dangling references*)

> Permiten implementar estructuras de datos con aliasing. El código
> inseguro debe **verificarse** ya que el compilador desactiva el **borrow
> chequer**

------------------------------------------------------------------------

### Código unsafe: Recomendaciones

1. Evitarlo en lo posible
2. Ver si es posible lograr algo equivalente usando funciones de la biblioteca
   estándar
3. Ocultarlo en APIs seguras
4. Fragmentos de código unsafe cortos y simples
5. ***Verificar*** que no *rompe* los invariantes del *borrow checker*

------------------------------------------------------------------------

### Otros errores capturados por el compilador

``` rust
fn gt(x:&str, y:&str) -> &str {
  x.len() > y.len() ? x : y
}
...
let s1 = String(...); let v1 = &s1; let r;
{ let s2= ..., let v2 = &s2; r = gt(v1, v2); }
println!("{}",r); // compilation error here!
```

------------------------------------------------------------------------

### ¿Porqué el ejemplo anterior no compila?

1. El tiempo de vida de `v1` es mayor que el de `v2`
2. Si `v1 <= v2`, `r == v2`
3. El valor `v2` ya no vive más en la última línea: ***dangling reference!***

> Recordemos: Una **referencia** es un *alias* de un valor (válido)

- Veremos cómo Rust detecta ésto con *type checking*

------------------------------------------------------------------------

### Anotaciones de tiempo de vida

- Internamente la función `gt` se codifica como <br>
  `fn gt<a'>(x:&a'str, y:&b'str) -> &a'str`
- La notación `a'` es un *rótulo (label)* un tiempo de vida dado
- En este caso necesitamos indicarle a Rust que `r` tiene al menos el tiempo de
  vida que los valores de `x` e `y`
- Si anotamos explícitamente `gt`: <br>
  `fn gt<a'>(x:&a'str, y:&a'str) -> &a'str {...}` <br>
  compila si movemos el `println!(...)` dentro del bloque

------------------------------------------------------------------------

### Referencias en campos de estructuras

``` rust
struct Person<'a> {
    // Rust assume name's references to a
    // value which must live at least as Person value
    name: &'a str; 
    ...
}
```

------------------------------------------------------------------------

### Smart pointers

- Algunos tipos (ej: `String`) desconocen su tamaño a priori
- Sus valores deben almacenarse dinámicamente en el *heap*

``` rust
{
    let s = String.from("Hello");
    // s is an structure in the stack
    // s.ptr points to characters array in the heap
} // s goes out of scope. s.drop() called
```

------------------------------------------------------------------------

### Smart pointers (cont.)

- `Box<T>`: Crea un valor con *único dueño* en el heap
- `Rc<T>`: *Reference counter* que permite aliasing

``` Rust
enum List { Cons(i32, Rc<List>), Nil, }
...
let a = Box::new(5); // a owns value 5 on heap
let b = Rc::new(Cons(1, Rc::new(Nil));
let c = Rc::new(Cons(2, Rc::clone(&b)));
```

------------------------------------------------------------------------

## Reference counters

```
rc1 +-----+  +--> +---------+ <--+   +-----+ rc2
    | ptr |--+    |   data  +    +---| ptr |
    +-----+       +---------+        +-----+
                  | counter |
                  +---------+
```

- `rc1` *smart pointer* a v=(dato + un contador de referencias)
- `counter` se incrementa en  `rc2 = Rc::clone(&rc1)`
- `counter` se decrementa en `Rc::drop(&rc)`. Si es cero libera v.

------------------------------------------------------------------------

## Reference counter

- *Garbage collector* liviano
- No es útil en estructuras de datos con referencias cíclicas
- No garantiza *atomicidad* en el incremento/decremento del contador
- Por eso no es aplicable en programas concurrentes

> Rust provee contadores de referencias atómicos: Ver `Arc<T>`

------------------------------------------------------------------------

## Programación orientada a objetos

- ***Métodos***: Funciones asociadas a una `struct` o `trait`
- *Polimorfismo* en Rust: ***Generecidad***. Ej: `let x : Vec<Person> = ...` 
- Rust no tiene herencia. En su lugar usa ***traits***
- ***Traits***: Definen *comportamiento común* (similares a *Haskell
  typeclasses*)

  ``` rust
  pub trait Draw { fn draw(&self); }
  ```

------------------------------------------------------------------------

### Un contenedor de componentes *drawable*

``` rust
pub struct Screen<T: Draw> {
    // components: vector of smart pointers to
    // Draw objects with dynabic binding
    pub components: Vec<Box<dyn Draw>>,
}
```

- Veremos cómo definir métodos (que *implementan*)  `Screen`

------------------------------------------------------------------------

### Definición (implementación) de métodos

``` rust
impl<T> Screen<T> where T: Draw,
{
    pub fn run(&self) {
        for component in self.components.iter() {
            component.draw();
        }
    }
}
```

------------------------------------------------------------------------

### Definición de un tipo concreto *drawable*

``` rust
struct Button { pub pos: (u32, u32), ...}
// Button implements the Draw trait
impl Draw for Button {
    fn draw(&self) { // self: &Button
        // code to actually draw a button
    }
}
```

------------------------------------------------------------------------

### Otro tipo concreto *drawable*

``` rust
struct Input { pub type: ...}
// Input implements the Draw trait
impl Draw for Input {
    fn draw(&self) { // self: &Input
        // code to actually draw an input field
    }
}
```

------------------------------------------------------------------------

### Ejemplo de polimorfismo

``` rust
let screen: Screen { 
    components: vec![new_button(), new_input(),...];
}
screen.run(); // equivalent to run(&screen)
```

- Cuando `run()` invoque a `component.draw()` invocará a la implementación
  correspondiente del objeto (`Button`, `Input`, ...)

------------------------------------------------------------------------

### Algunos *traits* predefinidos

- `Copy`: Valores *copiables* (como los *escalares*)
- `Clone`: Copiables *explícitamente*. Ej: `let v2 = v1.clone()`
- `Iterator`: Define el método `next(&self) -> Option<Self::Item>`
- `Drop`: Define el método `fn drop<T>(_: T) {}`. Se invoca automáticamente
  cuando la variable se va de su *scope*. <br>
  Todos los tipos son `Drop` (para los escalares no hace nada)

------------------------------------------------------------------------

### Ejemplo: *Destructor* en una lista

``` rust
...
impl Drop for ListNode {
  fn drop(self) { // note self, to take ownership
     let _next = self.next();
  } // drop(_next) called here
}
```

------------------------------------------------------------------------

### Concurrencia: problemas con *shared memory*

> Mantener invariantes en datos manipulados por múltiple threads (*race
> conditions*)

- Solución: Uso de *locks*
- Nuevo problemas: Disminución de concurrencia, *deadlock*, ...

------------------------------------------------------------------------

### Concurrencia en Rust

- Soporta programas con múltiples *threads*
- *Shared memory*: Requiere el uso de `Mutex<T>` y `Arc<T>`
- *Message passing* entre threads

> Rust propone **message passing** sobre **shared memory** aunque soporta ambos
> modelos

------------------------------------------------------------------------

``` rust
let counter = Rc::new(Mutex::new(0));
let threads = vec![];
for _ in 0..10 {
    let counter = Rc::clone(&counter);
    let t = thread::spawn(move || {
        let mut n = counter.lock().unwrap();
        *n += 1;
    });
    threads.push(t);
}
...
```

------------------------------------------------------------------------

``` rust
...
for t in threads {
    t.join().unwrap(); // wait for finish
}
println!("counter: {}", *counter.lock().unwrap());
```

***Compiling error***: `Rc<Mutex<i32>>` cannot be sent between threads safely

> Trait `Send`: Indica que un tipo que lo implementa es seguro moverlo entre
> threads. `Rc` no es `Send` (implementa `!Send`).

------------------------------------------------------------------------

### Análisis del error

- `Rc.clone(&value)` incrementa el contador de referencias a `value`
- Pero *no garantiza* ***atomicidad***. Por eso no implementa el trait `Send`
- `Arc<T>` (*atomic reference counter*) es `Send`.

Reemplazando `Rc` por `Arc`, el programa anterior compila.

> `Mutex<T>`, `Rc<T>` y `Arc<T>` también son **smart pointers**

------------------------------------------------------------------------

### Concurrencia por *pasaje de mensajes*

> “Do not communicate by sharing memory; instead, share memory by
> communicating.”
> -- Go language slogan

- Uso de *canales de comunicación* con extremos de envío (`tx`) y recepción
  (`rc`)
- `tx.send(v)` *consume* `v` (no puede usarse más en ese thread)
- `v=rx.recv()` *owns* `v`

------------------------------------------------------------------------

``` rust
let (tx, rx) = mpsc::channel();
thread::spawn(move || {
    let val = String::from("hi");
    tx.send(val).unwrap();
    // we can not use val anymore (it was moved)
});
// on main thread
let received = rx.recv().unwrap();
println!("Got: {}", received);
```

------------------------------------------------------------------------

## Conclusiones

- ***Programas seguros = programación disciplinada***
- Rust ***impone*** disciplinas por medio de su sistema de tipos
- Pros: 
  1. Rendimiento: Sin *garbage collector*, código resultante simple, con pocas
     verificaciones en *runtime*
  2. Seguridad: Manejo de memoria segura, sin *races*
- Contras: Curva de aprendizaje algo más alta

------------------------------------------------------------------------

<div class="center">

<h2 style="margin-top: 10rem">Gracias<h2>

### ¿Preguntas?

</div>
