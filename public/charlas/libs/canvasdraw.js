let fg_canvas = null
let fg_context = null
let fg_height = 0
let fg_width = 0
let fg_mouseClicked = false, fg_mouseReleased = true
let fg_drawing = false

function fg_setDrawingCanvas(canvasId) {
    fg_canvas = document.getElementById(canvasId)
    fg_context = fg_canvas.getContext("2d")
    fg_height = fg_canvas.height
    fg_width = fg_canvas.width
}

function fg_canvasToggleDrawing() {
    console.log('toogle fg canvas')
    if (!fg_drawing) {
        fg_canvas.addEventListener("click", fg_onMouseClick, true)
        fg_canvas.addEventListener("mousemove", fg_onMouseMove, true)
        fg_drawing = true
    } else {
        fg_canvas.removeEventListener("click", fg_onMouseClick, true)
        fg_canvas.removeEventListener("mousemove", fg_onMouseMove, true)
        fg_drawing = false
    }
}

function fg_onMouseClick(e) {
    fg_mouseClicked = !fg_mouseClicked
}

function fg_onMouseMove(e) {
    if (fg_mouseClicked) {
        fg_context.beginPath()
        context.arc(e.clientX, e.clientY, 1, 0, Math.PI * 2, false);
        fg_context.lineWidth = 1
        fg_context.strokeStyle = "#f00"
        fg_context.stroke()
    }
}