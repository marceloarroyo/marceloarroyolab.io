/**
 * Minified by jsDelivr using Terser v5.19.2.
 * Original file: /npm/marked-footnote@1.1.2/dist/index.umd.js
 *
 * Do NOT use SRI with dynamically generated files! More information: https://www.jsdelivr.com/using-sri-with-dynamic-files
 */
!(function (e, t) {
    "object" == typeof exports && typeof module < "u" ? (module.exports = t()) : "function" == typeof define && define.amd ? define(t) : ((e = typeof globalThis < "u" ? globalThis : e || self).markedFootnote = t());
})(this, function () {
    "use strict";
    function e({ prefixId: e, lexer: t, description: n }) {
        const o = { type: "footnotes", raw: n, items: [] };
        return {
            name: "footnote",
            level: "block",
            childTokens: ["content"],
            tokenizer(n) {
                t.hasFootnotes || (this.lexer.tokens.push(o), (t.tokens = this.lexer.tokens), (t.hasFootnotes = !0), (o.items = []));
                const r = /^\[\^([^\]\n]+)\]:(?:[ \t]+|[\n]*?|$)([^\[\n]*?(?:\n|$)(?:[\n]*?[ ]{4,}[^\[\n]*)*)/.exec(n);
                if (r) {
                    const [t, n, s = ""] = r;
                    let i = s.split("\n").reduce((e, t) => e + "\n" + t.replace(/^(?:[ ]{4}|[\t])/, ""), "");
                    const l = i.trimEnd().split("\n").pop();
                    (i += l && /^[ \t]*?[>\-\*][ ]|[`]{3,}$|^[ \t]*?[\|].+[\|]$/.test(l) ? "\n\n" : ""), (i += `<a href="#${e}ref-${encodeURIComponent(n)}" data-${e}backref aria-label="Back to reference ${n}">↩</a>`);
                    const a = { type: "footnote", raw: t, label: n, content: this.lexer.blockTokens(i) };
                    return o.items.push(a), a;
                }
            },
            renderer: () => "",
        };
    }
    function t(e, t = !1) {
        return {
            name: "footnoteRef",
            level: "inline",
            tokenizer(e) {
                const t = /^\[\^([^\]\n]+)\]/.exec(e);
                if (t) {
                    const [e, n] = t;
                    return { type: "footnoteRef", raw: e, label: n };
                }
            },
            renderer({ label: n }) {
                const o = encodeURIComponent(n);
                return `<sup><a id="${e}ref-${o}" href="#${e + o}" data-${e}ref aria-describedby="${e}label">${t ? `[${n}]` : n}</a></sup>`;
            },
        };
    }
    function n(e) {
        return {
            name: "footnotes",
            renderer({ raw: t, items: n = [] }) {
                if (0 === n.length) return "";
                const o = n.reduce((t, { label: n, content: o }) => {
                    let r = `<li id="${e + encodeURIComponent(n)}">\n`;
                    return (r += this.parser.parse(o)), (r += "</li>\n"), t + r;
                }, "");
                let r = '<section class="footnotes" data-footnotes>\n';
                return (r += `<h2 id="${e}label" class="sr-only">${t.trimEnd()}</h2>\n`), (r += `<ol>\n${o}\n</ol>\n`), (r += "</section>\n"), r;
            },
        };
    }
    return function (o = {}) {
        const { prefixId: r = "footnote-", description: s = "Footnotes", refMarkers: i } = o,
            l = { hasFootnotes: !1, tokens: [] };
        return {
            extensions: [e({ prefixId: r, lexer: l, description: s }), t(r, i), n(r)],
            walkTokens(e) {
                0 === l.tokens.indexOf(e) && "footnotes" === e.type && (l.tokens.push(l.tokens.shift()), (l.hasFootnotes = !1));
            },
        };
    };
});
//# sourceMappingURL=/sm/f0215bdde9a06e0a10890354fc00ab03cc445686defb16a1cf0285816c01c9c6.map
