function inline(text) {
    return text
    .replaceAll(/\$([^\$]+)\$/gm, (_, latex) => 
        katex.renderToString(latex)
    )
    .replaceAll(/\*\*([^*]+)\*\*/gm, '<b>$1</b>')
    .replaceAll(/[_*]([^_*]+)[_*]/gm, '<em>$1</em>')
    .replaceAll(/\*\*([^*]+)\*\*/gm, '<b>$1</b>') 
    .replaceAll(/!\[([^\]]*)\]\(([^ ]+) ?([^\)]*)\)/gm,
                '<img alt="$1" src="$2" $3>')
}

function md2html(text) {
    const blocks = text.trim().split(/(\n\s*){2,}/)
    const headerRE = /^(#{1,6}) (.*)$/
    const itemsRE = /^\s*([-\*]|\d+[\.\)]) /m
    let html = ''
    let match = null
    for (let b of blocks) {
        b = b.trim()
        if (b.length == 0) continue
        if (match = b.match(headerRE)) {
            const l = match[1].length
            const inner = inline(match[2])
            html += `<h${l}>${inner}</h${l}>\n`
            continue
        }
        if (match = b.match(itemsRE)) {
            const tag = (match[1] == '-' || match[1] == '*') ? 'ul' : 'ol'
            const next = match[1] == '*' || match[1].endsWith(')') ? 
                            'iclass="next"' : ''
            const items = b.split('\n')
            html += `<${tag}>\n`
            for (let i=0; i<items.length; i++) {
                const m = items[i].match(itemsRE)
                if (!m) {
                    html += inline(items[i])
                } else {
                    const inner = inline(items[i].substring(m[0].length))
                    const n = i > 0 ? next : ''
                    html += `<li${n}>${inner}</li>\n`
                }
            }
            html += `</${tag}>\n`
            continue
        }
        if (b.startsWith('> ')) {
            const inner = inline(b.replaceAll(/\n?\s*> /gm, ''))
            html += `<blockquote>${inner}</blockquote>\n`
            continue
        }
        if (b.startsWith('<') && b.endsWith('>')) {
            html += b + '\n'
            continue
        }
        html += `<p>${inline(b)}</p>\n`
    }
    return html
}

async function loadSlidesMarkdownBefore(file, selector) {
    let element = document.querySelector(selector)
    if (element) {
        const response = await fetch(file)
        const text = await response.text()
        const slides = text.split(/^-{5,} *$/m)
        for (const slide of slides) {
            const html = md2html(slide.trim())
            const section = '<section class="slide">' + html + '</section>'
            element.insertAdjacentHTML('beforebegin', section)
        }
    }
}

function insertMarkdownBefore(markdown, selector) {
    let element = document.querySelector(selector)
    const slides = markdown.split(/^-{5,} *$/m)
    for (const slide of slides) {
        const html = md2html(slide.trim())
        const section = '<section class="slide">' + html + '</section>'
        element.insertAdjacentHTML('beforebegin', section)
    }
}