var markdown=`
<div id="cover" class="center">

## Inteligencia Artificial

## ¿Qué es y cómo funciona?

<figure>
    <img class="cover" src="img/ai-icon.svg" alt="IA">
</figure>
<style>
  #cover > h2 {
    color: blue;
  } 
  #cover > figure {
    z-index: -1;
    opacity: 0.3;
  }
</style>
</div>

---------------------------------------------------------------

## Objetivos de esta charla

1. Comprender qué entendemos por IA
2. Describir las diferencias tipos de IA
3. Tratar de describir cómo funcionan estos programas
4. Ver algunos ejemplos
5. Comprender sus ventajas y posibles peligros

---------------------------------------------------------------

## Inteligencia artificial

> Programas que *razonan* y *aprenden* como los humanos

- ***Razonar***: *Inferir* información desde datos básicos
- ***Aprender***: El programa *resuelve* el problema luego de ***entrenarse***
  (o *estudiar*) desde *muuuuchos* datos sin saber de antemano *cómo* se
  resuelve el problema.

---------------------------------------------------------------

## Conceptos comúnmente erróneos

1. IA es sinónimo de robots
2. Puede sobrepasar la inteligencia de los humanos
3. Comprende cosas que los humanos no. La IA *no comprende* los datos
4. Puede reemplazar trabajos de humanos
5. No tiene prejuicios. En realidad puede tener *sesgo* (*preferencias*),
   dependientes de los datos usados

---------------------------------------------------------------

## Aplicaciones: Inferencia lógica

- Programas que intentan *generar nueva información* a partir de un conjunto de
  datos inicial
- Son *demostradores de teoremas* (comúnmente semi-automáticos)
- Aplican *reglas de inferencia lógicas* como

<div class="center">

*Modus Ponens*: $\\frac{P \\rightarrow Q \\: \\: P}{Q}$,
*Resolución*: $\\frac{P \\lor Q \\: \\: \\neg P \\lor R}{Q \\lor R}\\hspace{1cm}$ ...

</div>

---------------------------------------------------------------

## Aplicaciones: Reconocimiento de patrones

<div class="columns two">

- Imágenes
- Sonido (búsqueda de canciones de Google)
- Seguimiento de objetos
- Reconocimiento de caracteres

<div class="center">

![pattern recognition](img/pattern-recognition.jpg style="width: 100%;")

</div>

</div>

---------------------------------------------------------------

### Aplicaciones: Reconocimiento óptico de caracteres (OCR)

<div class="columns two">

- Escritura a mano
- Texto en fotos
- ...

<div class="center">

![ocr](img/ocr.jpg style="width: 100%;")

</div>

</div>

---------------------------------------------------------------

## Aplicaciones: Automatización

<div class="columns two">

- Robótica
- Líneas de producción
- IA permite *reconfigurar automáticamente* ante cambios
- ...

<div class="center">

![automation](img/automation.webp style="width: 100%;")

</div>

</div>

---------------------------------------------------------------

## Aplicaciones: Generación a partir de

<div class="columns two">

- Texto
- Sonido
- Imágenes
- ...

<div class="center">

![automation](img/generative-AI.webp style="width: 100%;")

</div>

</div>

> Ej: *Chat-GPT* genera nuevos contenidos desde datos dados

---------------------------------------------------------------

## Redes neuronales artificiales

- Programas que simulan un modelado *simplificado* del cerebro
- Estos programas ***aprenden*** desde *muchos* datos dados
- El proceso de aprendizaje se conoce como ***entrenamiento***
- Sus orígenes son desde los 1940's
- Simulan una *máquina conexionista*: Neuronas conextadas por *sinapsis*

---------------------------------------------------------------

## Redes neuronales artificiales

<div class="columns two">

<div>

![neural network](img/artificial-neural-network.jpg style="width:80%")

Estructura general:

- Círculos: *neuronas*
- Flechas: *sinapsis*

</div>

<div class="center">

![activation function](img/activation-function.jpg style="width:100%")

Activación de una neurona

</div>
</div>

---------------------------------------------------------------

## Aprendizaje (entrenamiento)

- Algoritmos que ajustan los valores $w_t^i$ de las sinapsis (flechas) para
  obtener los resultados esperados en base a los datos de entrenamiento dados
- Ejemplo: Datos de entrenamiento en un corrector de texto:
  $('suseción', 'sucesión')$, $('bertical','vertical')$, ...
- Mientras más datos de entrenamiento se usen, mejor

<p class="next">
Veamos un ejemplo simple de reconocimiento de caracteres...
</p>

---------------------------------------------------------------

<iframe src="example/HopfieldNetworks.html" width="90%" height="90%">

---------------------------------------------------------------

## ¿Qué podemos inferir?

1. Resuelven problemas muy difíciles de resolver mediante otros algoritmos
2. Son programas muy rápidos (luego que aprendieron)
3. Necesitaremos *muchísimos datos* de entrenamiento para que sean efectivos
4. Los resultados dependen totalmente de los datos de entrenamiento
5. El entrenamiento puede llevar mucho tiempo (y poder de cómputo)

---------------------------------------------------------------

## La revolución: ChatGPT, Google Gemini, ...

- Usa redes neuronales complejas (muchas capas con muchas neuronas)
- Cada capa aprende una *característica* de los datos (*deep learning*)
- Se conocen como *Large Language Models (LLMs)*
- ***Generan contenido*** desde un *contexto* dado desde lo aprendido
- Continúan aprendiendo con los nuevos datos dados mientras se usa

---------------------------------------------------------------

## Jugando con ChatGPT

<div class="center">

![chatGPT1](img/ChatGptExample.png style="width: 100%")

<div>

---------------------------------------------------------------

## Problemas en su aplicación

- Los ***resultados son probabilísticos***: Podemos obtener resultados no
  deseados
- No siempre es la mejor tecnología para resolver un problema
- Existen problemas para los cuales ya se conocen excelentes algoritmos para
  solucionarlos

---------------------------------------------------------------

## Conclusiones

1. Permiten resolver eficientemente problemas muy complejos
2. No es magia! Es matemáticas y computación
3. Pueden darnos respuestas no esperadas!
4. Para crear aplicaciones de IA debemos contar con muchos datos

<div class="next center">

Muchas gracias!

¿Preguntas?

</div>`;

