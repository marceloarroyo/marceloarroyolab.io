const trainPatterns = [
    [
    "    OO    ",
    "    OO    ",
    "   OOOO   ",
    "   O  O   ",
    "  OO  OO  ",
    "  O    O  ",
    " OOOOOOOO ",
    " OOOOOOOO ",
    "OO      OO",
    "OO      OO"
    ],
    [
    "OOOOOO    ",
    "OOOOOOO   ",
    "OO   OO   ",
    "OOOOOOO   ",
    "OOOOOOO   ",
    "OO   OOO  ",
    "OO    OO  ",
    "OO   OOO  ",
    "OOOOOOO   ",
    "OOOOOO    "
    ],
    [
    "OOOOOOOOOO",
    "OOOOOOOOOO",
    "OO      OO",
    "OO        ",
    "OO        ",
    "OO        ",
    "OO        ",
    "OO      OO",
    "OOOOOOOOOO",
    "OOOOOOOOOO"
    ],
    [
    "OO      OO",
    "OO      OO",
    "OO      OO",
    "OO      OO",
    "OOOOOOOOOO",
    "OOOOOOOOOO",
    "OO      OO",
    "OO      OO",
    "OO      OO",
    "OO      OO"
    ]
]

const testPatterns = [
    ["    OO    ",
	 "    OO    ",
	 "   OOOO   ",
	 "   O  OO  ",
	 "  OO  OOO ",
	 "  O    OO ",
	 " OOOOOOOO ",
	 " OOOOOOOO ",
	 "OO      OO",
	 "OO      OO"],

	["OOOOOOO   ",
	 "OOOOOOOOO ",
	 "OO   OOOO ",
	 "OOOOOOOOO ",
	 "OOOOOOO   ",
	 "OO   OOO  ",
	 "OO    OO  ",
	 "OO   OOO  ",
	 "OOOOOOO   ",
	 "OOOOOOOO  "],

	["OOOOOOOOOO",
	 "OOOOOOOOOO",
	 "OO      OO",
	 "OO        ",
	 "OOOOOO    ",
	 "OO    OOO ",
	 "OO        ",
	 "OO      OO",
	 "OOOOOOOOOO",
	 "OOOOOOOOOO"],

	["OO      OO",
	 "OO      OO",
	 "OO OOOO OO",
	 "OO      OO",
	 "OOOOOOOOOO",
	 "OOOOOOOOOO",
	 "OO      OO",
	 "OO OOOO OO",
	 "OO      OO",
	 "OO      OO"],
    [
    "          ",
    "    OO    ",
    "   O  O   ",
    "  O    O  ",
    " O  O   O ",
    "  O    O  ",
    "  OO  O   ",
    "    OO    ",
    "     O    ",
    "          "
    ]
]

// load a pattern p into a textarea element
function loadPattern(p, id) {
    console.log("loading pattern " + id)
    let textarea = document.getElementById(id)
    let text = ''
    for (const str of p) {
        text += str + '\n'
    }
    text = text.substring(0, text.length - 1) // drop last '\n'
    textarea.value = text
}

// load train patterns into textarea elements
function loadTrainPatterns() {
    for (let n = 0; n < trainPatterns.length; n++) {
        loadPattern(trainPatterns[n], 'p' + n)
    }
}

// text to pattern: An array of strings
function textToPattern(text) {
    const lines = text.split('\n')
    let p = []
    for (let i=0; i<10; i++) {
        let line = lines[i].substring(0, 10)
        p.push(line)
    }
    return p
}

// return a binary (-1, 1) vector from pattern p
function toVector(p) {
    let v = Array(p.length * p[0].length)
    let n = 0
    for (let i=0; i<p.length; i++)
        for (let j=0; j<p[i].length; j++)
            v[n++] = p[i][j] != ' ' ? 1 : -1
    return v
}

// from binary vector to pattern
function toPattern(vector, rows, cols) {
    let p = Array(rows)
    let k = 0
    for (let i=0; i<rows; i++) {
        let str = ''
        for (let j=0; j<cols; j++) {
            str += vector[k] == -1 ? ' ' : 'O'
            k++
        }
        p[i] = str
    }
    return p
}

// get patterns from textarea
function getPatternsFrom(ids) {
    let patterns = []
    for (let id of ids) {
        let text = document.getElementById(id).value
        patterns.push(textToPattern(text))
    }
    return patterns
}
