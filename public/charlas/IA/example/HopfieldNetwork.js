// Simple Hopfield Network

class HopfieldNetwork {
    
    constructor(trainset) {
        let neurons = trainset[0].length
        this.weights = Array(neurons)
        for (let i=0; i<neurons; i++) {
            this.weights[i] = Array(neurons).fill(0)
        }
        
        // calculate weights (training)
        let w
        for (let i=0; i<neurons; i++) {
            for (let j=0; j<neurons; j++) {
                w = 0
                if (i != j) {
                    for (let n=0; n<trainset.length; n++) {
                        w += trainset[n][i] * trainset[n][j]
                    }
                }
                this.weights[i][j] = w / trainset.length
            }
        }
    }

    predict(vector) {
        let changed = false
        let iteration = 0, last_change = 0
        let x = vector.map((x) => x)   // clone pattern

        console.log('predict: x.length=' + x.length)

        do {
            iteration++;

            // select a random neuron i
            let i = Math.floor(Math.random() * vector.length)
            let changed = false
            

            // update neuron i
            let sum = 0
            for (let j=0; j<x.length; j++) {
                sum += this.weights[i][j] * x[j]
            }
            if (sum != 0) {
                let state = sum > 0 ? 1 : -1
                if (state != x[i]) {
                    changed = true
                    x[i] = state
                }
            }

            if (changed)
                last_change = iteration;
        } while (iteration - last_change < 20 * x.length);
        return x
    }
}