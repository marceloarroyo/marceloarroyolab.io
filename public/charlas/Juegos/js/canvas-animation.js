var animation = {
    objects: [],
    canvas: null,
    ctx: null,
    paused: true,
    gravity: 0.2,
    setCanvas: canvasId => {
        animation.canvas = document.getElementById(canvasId)
        animation.ctx = animation.canvas.getContext("2d")
    },
    addObject: obj => { animation.objects.push(obj) },
    start: () => { 
        animation.paused = false
        animate() 
    },
    stop: () => { animation.paused = true },
    clear: () => { animation.objects = [] }
}

function animate() {
    let a = animation
    
    if (!a.canvas) return
    
    a.ctx.clearRect(0, 0, a.canvas.width, a.canvas.height)
    
    if (!a.paused)
      requestAnimationFrame(animate)

    for (let o of a.objects)
        o.isColliding = false

    for (let i=0; i<a.objects.length; i++) {
        let o = a.objects[i]
        if (o.limits) {
            // canvas horizontal limits
            if (o.x + o.radius >= a.canvas.width) {
                o.vx = -(o.vx) * o.dumping
                o.x = a.canvas.width - o.radius
            } else if (o.x - o.radius <= 0) {
                o.vx = -(o.vx) * o.dumping
                o.x = o.radius
            }

            // canvas vertical limits
            if (o.y + o.radius >= a.canvas.height) {
                o.vy = -(o.vy) * o.dumping
                o.y = a.canvas.height - o.radius
                o.vx *= o.traction     // <-- traction here
            } else if (o.y - o.radius <= 0) {
                o.vy = -(o.vy) * o.dumping
                o.y = o.radius
            }
        }

        o.handleCollisions(a.objects, i+1)

        o.vy += a.gravity // <--- this is it
    
        o.x += o.vx
        o.y += o.vy

        o.draw(a.ctx)
    }
}

// return a circle with initial position, velocity, forces, radius, color,
// dumping and traction
class Circle {
    constructor(x,y, vx,vy, radius, color, dumping, traction) {
        this.x = x
        this.y = y
        this.vx = vx
        this.vy = vy
        this.radius = radius
        this.color = color
        this.dumping = dumping || 0.9
        this.traction = traction || 0.8
        this.limits = true
        this.mass = 4 * Math.PI * this.radius * this.radius
        // this.mass = radius
        this.isColliding = false
        this.collisionResponse = false
    }

    draw(ctx) {
        ctx.beginPath()
        ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true)
        ctx.closePath()
        const color = (this.isColliding) ? 'cyan' : this.color
        ctx.fillStyle = color
        ctx.fill()
    }

    collideWith(c) {
        let o1 = this
        let o2 = c
        const a = o1.x - o2.x
        const b = o1.y - o2.y
        const d = Math.sqrt(a*a + b*b)
        return d <= o1.radius + o2.radius
    }

    handleCollisions(objects, from) {
        for (let i=from; i<objects.length; i++) {
            let c = objects[i]
            if (this.collideWith(c)) {
                this.isColliding = true
                c.isColliding = true

                if (this.collisionResponse && c.collisionResponse) {
                    const m1 = this.mass
                    const m2 = c.mass
                    const md1 = m1 - m2
                    const md2 = m2 - m1
                    const ms = m1 + m2

                    // calculate velocity vector for this object
                    const vx = (this.vx * md1 + (2 * m2 * c.vx)) / ms
                    const vy = (this.vy * md1 + (2 * m2 * c.vy)) / ms

                    // calculate velocity vector for other (c)
                    c.vx = (c.vx * md2 + (2 * m1 * this.vx)) / ms
                    c.vy = (c.vy * md2 + (2 * m1 * this.vy)) / ms

                    this.vx = vx
                    this.vy = vy
                }
            }
        }
    }
}
