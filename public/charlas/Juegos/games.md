<div id="cover" class="center">

## Programación de juegos

## ¿Qué hay que saber?

<figure>
    <img class="cover" src="assets/gaming.jpg" alt="Juegos">
    <figcaption class="copyright right">
        <a id="freecodecamp" href="https://freecodecamp.org">© FreeCodeCamp</a>
    </figcaption>
</figure>
<style>
  #cover > h1, #cover > h2 {color: white;}
</style>
</div>

---------------------------------------------------------------

## Objetivos de esta charla

- Diseño de juegos: sus componentes
- Animaciones
- Conocimientos básicos requeridos
- Resolución de problemas
- Relacionar computación (programación), matemáticas, física y arte

---------------------------------------------------------------

## ¿Qué es un juego de computadora?

- Un programa que **simula** escenarios
- El escenario **evoluciona en el tiempo**
- Objetos móviles (personajes, vehículos, etc)
- Tiene un objetivo (vencer al rival, volar sin accidentarse, ...)
- Puede tener jugadores virtuales. Puede usar inteligencia artificial

Desafío: Simular con gran realismo (gráficos 3D, sonido, tiempo real, ...)

---------------------------------------------------------------

## Animación por computadora

Cada cierto intervalo de tiempo, hacer:

1. Limpiar la pantalla
2. Calcular velocidades
3. Calcular posiciones de objetos
4. Dibujar los objetos en la pantalla (*frame*)

- Estos pasos se ejecutan entre 25 y 60 veces por segundo (*fps*)

---------------------------------------------------------------

## Objeto en movimiento. En un instante tiene:

<div class="columns two">

- *Posición*: coordenada $p=(x,y)$
- Una *velocidad*
- Una *dirección*

<div>

![sistema de coordenadas](assets/coordinates.png style="width: 350px;")

</div></div>

---------------------------------------------------------------

## Velocidad: Vector $v=(x,y)$

<div class="columns two">

- **Magnitud**: $m = \sqrt{v.x^2 + v.y^2}$
- **Ángulo**: $\theta = \arcsin(\frac{v.y}{m})$

<div>

![vector](assets/vector.png style="width: 250px")

</div></div>

---------------------------------------------------------------

## Cálculo de posiciones de objetos

<div class="columns two">
<div>

$frame_i \rightarrow frame_{i+1} \rightarrow \ldots$

En cada frame, para cada objeto, calcular

1. Cambios de su *velocidad* $v$ (por posibles aceleraciones) 
2. Su posición $p$

</div>
<div class="next">

$p_{i+1} = p_i + v * dt$

- $p_i$: Posición en frame actual
- $p_{i+1}$: Próxima posición
- $v$: Velocidad
- $dt$: Tiempo entre frames

(operaciones sobre *vectores*)

</div>
</div>

---------------------------------------------------------------

### Demo 1: $v_{azul}=(3,0)$, $v_{rojo}=(-1,0.5)$

<div class="center">
    <canvas id="anim1" width="500" height="300"></canvas> <br>
    <input type="button" value="Iniciar" onclick="circle1('anim1')">
    <input type="button" value="pausar/cont." onclick="toogleAnim()">
</div>

---------------------------------------------------------------

## Rebotes en los bordes

<div class="columns two">
<div>

- Si $p.x \geq ancho$ o $p.x \leq 0$, <br> $v.x = -v.x$
- Si $p.y \geq alto$ o $p.y \leq 0$,  <br> $v.y = -v.y$

<br>(inversión de direcciones)

</div>

<div class="center">
  <canvas id="anim2" width="300" height="250"></canvas> <br>
  <input type="button" value="start" onclick="circle2('anim2')">
  <input type="button" value="pausar/cont." onclick="toogleAnim()">
</div>
</div>

---------------------------------------------------------------

### Aceleración: Cambio de velocidad en el tiempo

<div class="columns two">

  <div>

  $f = masa \times a$

  - Una *fuerza (f)* produce una *aceleración (a)*
  - Afectará a la *velocidad* del objeto
  - Ejemplo: *gravedad*, $v_{0}=(3,0)$

  </div>

  <div class="center">
    <canvas id="anim3" width="300" height="250"></canvas> <br>
    <input type="button" value="start" onclick="circle3('anim3', true)">
    <input type="button" value="pausar/cont." onclick="toogleAnim()">
  </div>

</div>

---------------------------------------------------------------

## Fricción y elasticidad

<div class="columns two">

- *Fricción o tracción*: Disminuye velocidad (horizontal)
- *Elasticidad*: Pérdida de velocidad en una colisión

<div class="center">
    <canvas id="anim4" width="300" height="250"></canvas> <br>
    <input type="button" value="start" onclick="circle4('anim4', true)">
    <input type="button" value="pausar/cont." onclick="toogleAnim()">
</div>

</div>

---------------------------------------------------------------

## Colisiones (de círculos)

<div class="columns two">
  <!-- column 1 -->
  <div>

  ![colisión](assets/circles-collision.png style="width:300px;")

  * ¿Cuándo $c1$ y $c2$ colisionan?
  * Si $dist(c1,c2) \leq c1.r + c2.r$

  </div>

  <!-- column 2 -->
  <div class="center">
  <canvas id="anim5" width="300" height="250"></canvas> <br>
  <input type="button" value="start" onclick="circles5('anim5', true)">
  <input type="button" value="pausar/cont." onclick="toogleAnim()">
  </div>
</div>

---------------------------------------------------------------

## Reacción en colisiones

<div class="columns two">

 <!-- first column -->
<div>

![colisiones](assets/collision.gif style="width:250px")

- Calcular vectores (<span style="color:blue;">&rarr;</span>) de velocidad para ***cada par de objetos***
- Teniendo el cuenta sus *masas*

</div>

<!-- column 2 -->
<div class="center">
<canvas id="anim6" width="300" height="250"></canvas> <br>
<input type="button" value="start" onclick="collidingCircles('anim6')">
<input type="button" value="pausar/cont." onclick="toogleAnim()">
</div>

</div>

---------------------------------------------------------------

## Gráficos 3D

***Rendering***: Transformación de un modelo de objetos 3D en una imagen
con perspectiva, iluminación y texturas (requiere muchísimo cómputo)

<div class="columns two">

![teapot-wireframe](assets/teapot-Wireframe.png style="width:300px;")

![teapot-rendered](assets/teapot-rendered.jpg style="width:300px;")

</div>

---------------------------------------------------------------

### Animaciones de personajes (sprites)

<div class="columns two">

<div>

![super mario spritesheet](assets/sprite-sheet.png style="width:280px")

Sprite sheet (así funciona un *.gif*)

</div>

<div>

![super mario anim](assets/super-mario-sprite-anim.gif style="width:300px")

</div>
</div>

---------------------------------------------------------------

## Objetos articulados (algoritmos)

<div class="center">

![body animation](assets/3d-body-animation.gif style="width:600px")

</div>

---------------------------------------------------------------

## Colisiones entre objectos complejos

<div class="columns two">

- Cada parte en una *caja de límites*
- Se detectan colisiones entre rectángulos y/o círculos
- El número de cajas en una escena puede ser muy grande
- Recordar que ***hay que comparar todos con todos!***

<div>

![bounding box](assets/boundig-boxes.png style="width:350px;")

Cajas de límites del personaje

</div>
</div>

---------------------------------------------------------------
<div class="center">
  <video width="600" height="600" controls>
    <source src="assets/hit-team-fortress.mp4" type="video/mp4">
    Team fortress game video
  </video>
</div>

---------------------------------------------------------------

## Y aún hay más...

- Gráficar escena completa
- Reaccionar ante eventos externos (jugadores, ...)
- Contabilizar puntajes
- Inteligencia Artificial (IA) para jugadores virtuales
- Comunicaciones (juegos en red)

<div class="center" style="border:1px solid blue;color:red;">
Hay que hacer estos pasos unas 60 veces por segundo!
</div>

---------------------------------------------------------------

## Hardware (requisitos)

- Los juegos modernos 3D requieren mucho poder de cómputo
- Cientos o miles de objetos colisionables
- Personajes complejos (muchas partes para animar)
- Escenarios complejos (ciudades, terrenos, texturas, iluminación, etc)
- Rendering con gran resolución
* <span style="color: red">Computadoras para gaming!</span>

---------------------------------------------------------------

## CPU y GPU (graphics processing unit)

<div class="columns two">

- CPU: Algunos procesadores (*cores*)
- GPU: Miles de ALUs
- CPU: Cada *core* opera en secuencia
- GPU: Los *cores* operan en paralelo
- GPU: Acelera operaciones vectoriales (física y rendering)

<!-- column 2 -->
<div class="center">

![cpu vs gpu](assets/cpu-vs-gpu.jpeg)

![cpu vs gpu arquitecturas](assets/cpu-gpu-arch.png)

</div>
</div>

---------------------------------------------------------------

## Conclusiones

- Los juegos son programas muy interesantes y complejos
- Combinan programación, matemáticas, física y arte
- Se pueden usar en educación y entrenamiento (ej: simuladores de vuelo)

<div class="center">

Si quieren programar juegos, hay que *dominar estos temas*

</div>

---------------------------------------------------------------

<h2 class="shout grow" style="font-size: 48px;">

¿Preguntas?

<p class="next">Muchas gracias</p>

<h2>

