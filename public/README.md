# Marcelo Arroyo

## Sobre mí

- Profesor con dedicación exclusiva en el
[Departamento de Computación](https://dc.exa.unrc.edu.ar/) de la
FCEFQyN - Universidad Nacional de Río Cuarto (UNRC), Argentina.
- Profesor a tiempo parcial en la 
[Facultad de Ingeniería](https://www.ing.unlpam.edu.ar/) de la
Universidad Nacional de La Pampa.

------------------------------------------------------------------

## Docencia

Abajo están algunos enlaces a notas de cursos que estoy dictando actualmente.

### UNRC: Licenciatura en Ciencias de la Computación

- [Sistemas Operativos](cursos/SO/index.html ':ignore')
- ***Telecomunicaciones y sistemas distribuidos***
  - Parte 1: [Networking](cursos/TySD-UNRC/networks/ ':ignore')
  - Parte 2: [Sistemas Distribuidos](cursos/TySD-UNRC/ds/ ':ignore')

### UNLPam: Ingeniería en Computación

- [Seguridad Informática](cursos/seguridad-UNLPam/ ':ignore')
- Coordinador y docente de la *Diplomatura en Seguridad Informática*.
  Dictada en 2022-2023.

---------------------------------------------------------------------------

## Investigación
        
Soy miembro del grupo de investigación
[MFIS](https://mfis.dc.exa.unrc.edu.ar/): *Formal Methods and Software
Engineering* del Departamento de Computación de la UNRC. En el sitio del
grupo se puede acceder a artículos publicados y herramientas y técnicas
desarrolladas.

#### Mis principales áreas de interés son:

- Lenguajes de programación (parsing, semántica, ...)
- Análisis estático de programas (errores, vulnerabilidades, etc).
- Análisis de protocolos de red y sistemas distribuidos.
- Diseño e implementación de sistemas operativos.

... entre otros.

### Charlas

- [Introducción a Rust](charlas/Rust/rust-intro.html ':ignore')
- [Juegos: ¿Qué hay detrás de la pantalla?](charlas/Juegos/games.html ':ignore')
- [Inteligencia Artificial: ¿Qué es y cómo funciona?](charlas/IA/IA.html ':ignore')
- [Introducción a la Seguridad informática](slides/seguridad-intro/seguridad-intro.html ':ignore')

-----------------------------------------------------------------------

## Enlaces

- [Río 2025](rio2025/index.html ':ignore')

## Contacto

Departamento de Computación - FCEFQyN - Universidad Nacional de Río Cuarto. Ruta
36, km. 601. Río Cuarto, provincia de Córdoba, Argentina.

[Marcelo Arroyo](mailto:marcelo.arroyo@dc.exa.unrc.edu.ar)
