---
marp: true
html: true
theme: gaia
class: invert
size: 16:9
paginate: true
---

<!-- _class: lead invert -->

# ***Introducción a la Seguridad informática***

Marcelo Arroyo

![w:50px](img/escudo-unrc.png)

---

## ¿Qué significa esto?

<!-- _class: lead invert -->

![width:500px](img/secure-site.jpg)

---

![bg left:40% w:80%](img/sec-triangle.jpg)

* ***Confidencialidad***: Acceso a *recursos* por *sujetos* autorizados
* ***Integridad***: Modificación de *recursos controlado* por *sujetos autorizados*
  - ***No repudio***: Imposibilidad de negar una acción/autoría
* ***Disponibilidad***: Los *sujetos* deben poder acceder a los *recursos* cuando lo requieran

---

### Terminología

- ***Vulnerabilidad***: Error de diseño o implementación en un sistema de computación
- ***Amenaza***: Acción (deliberada o accidental) que puede causar una violación de seguridad
- ***Ataque***: Acción deliberada, generalmente mal intencionada
- ***Malware***: Software que implementa algún ataque
  - ***Virus***: Replica su código en otros programas
  - ***Gusano***: Se propaga por los nodos de una red
  - ***Troyano***: Se oculta en un *programa de uso común*

---

## Confidencialidad: Mecanismos

* ***Control de acceso***: Relación entre *(sujetos,objetos,permisos)*
   - Ejemplo: Modelos de permisos en sistemas de archivos
   - ***Autenticación***: Verificación de la *identidad* de un sujeto
     1. Debe presentar algo que *conoce* (contraseña, ...)
     2. Debe presentar algo que *posee* (tokens, huella dactilar, ...)

* ***En redes: Control de tráfico y contenidos***: Firewalls, antivirus, ...

* ***Criptografía***: *Cifrado* de recursos/objetos

---

### Criptografía

> Del griego (romanizado) *kriptós* **oculto** y *graphein* **escritura** 

- Principalmente usada para *confidencialidad*

##### Criptología

- Estudio de técnicas de comunicaciones seguras en presencia de adversarios
- Análisis y desarrollo de ***protocolos*** de comunicación seguros
- Disciplinas involucradas: computación, matemáticas, física, ...

---

## Resúmenes (hash) criptográficos

> Generan una salida de longitud fija desde una entrada arbitraria

Es muy difícil, dado $x \mid hash(x)=h$, encontrar $y \mid hash(y)=h$

<div style="margin-top: 1rem; text-align: center;">
<textarea id="hashtext" cols="120" rows="4">
El aplicar un HASH a esto genera una cadena (número) de una cierta cantidad de dígitos fija.
Abajo aparecen los resultados de aplicar el hash a este texto y luego aplicado a cambios mínimos (un bit por vez). Notar cómo difieren los valores generados
</textarea> <br>
<button onclick="calcHash()">Calcular hash (sha-256)</button> <br>
<textarea id="hashresult" cols="120" rows="3">
</textarea>
</div>

---

## Aplicaciones de los resúmenes criptográficos

1) ***No es un mecanismo de cifrado/descrifrado***
2) Autenticación por contraseña
   1. Almacenar los pares $(user_i, p_i=hash(password_i))$
   2. Autenticación: $(user, hash(password)) = (user_i, p_i)$

3) ***Integridad*** (de archivos o mensajes)
   1. Calcular $h1=hash(file)$ de un archivo (y guardarlo)
   2. Verificación (a posteriori)

      Si $hash(file) = h1$, el archivo no ha cambiado

---

### Criptografía simétrica

* Uso de una *clave secreta*
* Algoritmos basados en:
  - *Transposición* de símbolos
  - *Sustitución* de símbolos
* Ventajas: Muy eficientes
* Desventajas: Se debe *compartir* el *secreto*. No escala
* Algoritmos: AES, 3-DES, Salsa20, ...

![bg right:45% width:95%](img/cripto-simetrica.svg)

---

### Ejemplo: Cifrado César

- Alfabeto de entrada y salida: $\{a,b,\ldots,z\}$
- Codificación: $a=0, b=1, \ldots, z=25$
- Cifrado: $E(p_1p_2 \ldots p_n,k)=c_1c_2 \ldots c_n$, donde
  $c_i=(p_i+k) \: mod \: 26$, con $1 \leq i \leq n$
  - *E("hola",3)="krod"*
- Descifrado: Ejercicio
- No es seguro: Un ataque por *fuerza bruta* requiere sólo 26 pruebas

---

### Otro ejemplo: xor (cifrado aditivo)

- Clave $k$ de la misma longitud (en bits) que el *texto plano*
- $E(p,k))=P \oplus k$, $D(c,k)=c \oplus k$
- $E(101011, 110001)=011010$
- $D(011010,110001)=101011$
- No es seguro si no se usa como un *one-time pad*, en el cual
  1. Las claves son aleatorias, *uniformemente distribuida*
  2. Cada clave es usada una *única vez* (se denomina *nounce*)
  3. *One-time pad*: Teóricamente inquebrable

---

![bg left:40% w:98%](img/aes.jpg)

#### Algoritmos (ej: AES)

- Secuencias (rondas) de operaciones de sustitución y transposición
- Opera sobre bloques de la longitud de la clave
  -  Modo de operación sobre secuencias de bloques

![w:98%](img/cipher-modes.svg)

---

### Criptografía simétrica (cont.)

<div style="margin-top: 1rem; text-align: center;">
<textarea id="plaintext" cols="120" rows="4">
Este es el texto plano que queremos almacenar o transmitir de manera confidencial.
En este caso lo cifraremos con AES con la clave (secreta) de abajo.
</textarea>

Cifrar/Descifrar con los botones de abajo.

Key:

<textarea id="key" cols="80" rows="1">
1bf8be421b201adf247297140aa76cc8211620976125db505d348cd60bee4198
</textarea>

<button onclick="AES_CBC_encrypt()">Cifrar (AES-CBC)</button> <button onclick="AES_CBC_decrypt()">Descifrar (AES-CBC)</button>

<p id="ciphertext" style="font-size:1rem; border: solid 1px">
Result:
</p>
</div>

---

## Cómputo seguro de un secreto

1) ***Alice*** y ***Bob*** *acuerdan* en usar $p=$<input id="dhp" type="number" value="23"> y $g=$ <input id="dhg" type="number" value="5">
2) ***Alice*** *elige* un secreto $a$, $(0 > a < p)$
   Sea $a=$ <input type="number" id="a" min="1" max="22" value="6"> y envía a ***Bob*** $A=g^a \: mod \: p$ <button onclick="dh('dhg','a','A')">Calcular</button> <input type="number" id="A" min="0" max="22">
3) ***Bob*** *elige* un secreto $b$, $(0 > b < p)$
   Sea $b=$ <input type="number" id="b" min="1" max="22" value="15"> y envía a ***Alice*** $B=g^b \: mod \: p$ <button onclick="dh('dhg','b','B')">Calcular</button> <input type="number" id="B" min="0" max="22">
4) A recibir los mensajes, *A* y *B* calculan el mismo secreto:
   - ***Alice secret***: $B^a \: mod \: p$ <button onclick="dh('B','a','sa')">Calcular</button> <input type="number" id="sa" min="0" max="22">
   - ***Bob secret***: $A^b \: mod \: p$ <button onclick="dh('A','b','sb')">Calcular</button><input type="number" id="sb" min="0" max="22">

---

## Diffie-Hellman: (algoritmo anterior)

- Notar que $A^b \: mod \: p = g^{ab} \: mod \: p = g^{ab} \: mod \: p = B^a \: mod \: p$
- Los parámetros $p$ y $g$ ***no son secretos***
- El atacante puede conocer $\{p, g, A, B\}$
- Necesita *calcular* $a$ o $b$ desde $A^b \: mod \: p$ o $B^a \: mod \: p$

  > Es necesario calcular un ***logaritmo discreto***: $O(2^{n^k})$

---

## Criptografía asimétrica

- En Diffie-Hellman podemos ver que 
  - El par $(g,p)$ es la ***clave pública***
  - $a$ y $b$ son las ***claves privadas*** de *Alice* y *Bob*, respectivamente
- Sentó las bases de la ***criptografía de clave pública y privada***
  - [Rivest-Shamir-Adleman (RSA)](https://en.wikipedia.org/wiki/RSA_(cryptosystem))
  - [El Gamal](https://en.wikipedia.org/wiki/ElGamal_encryption)
  - ...

---

![bg right:45% w:95%](img/cripto-asimetrica.svg)

### Criptotrafía asimétrica

- Generación de claves: $keygen(s) \rightarrow (s_{sk},s_{pk})$
- Cifrado: $E(key_1,P)=C$
- Descifrado: $D(key_2,C)=P$

Donde $key_1=s_{sk}$ y $key_2=s_{pk}$ o viceversa 

---

## RSA: Rivest-Shamir-Adleman, 1977

* Generación de claves:
  1) Elegir $p$ y $q$, primos. Sea $n = p \times q$
  2) Computar $\Theta(n) = lcm(p-1, q-1)$. Esto es, $\Theta(n)=m$ tal que $a^m
     \equiv 1 \: mod \: n$, $\forall a$ *comprimo* con $n$
  3) Elegir $1 < e < n$, con $gdc(e,\Theta(n))=1$. Ej: $e=65537$
  4) Calcular $d=e^{-1} \: mod \: \Theta(n)$. $d$ es *inverso multiplicatico* de
     $e$
  5) $(e,n)$ es la ***clave pública*** ($pk$), $d$ es la ***clave privada***
     ($sk$)

* Cifrado/Descifrado: $E(P,k) = P^k \: mod \: n$
* Fortaleza: Para calcular $d$ se debe factorizar $n$ (alg. exponencial)

---

## Ejemplo de RSA (Wikipedia)

1) Sean $p=61$, $q53$, $n=p \times q = 3233$
2) $\Theta(n)=780$
3) Elegir $e$, coprimo a 780. Sea $e=17$
4) $d=413$, ya que $(17 \times 413 \: mod \: 780)=1$
5) Cifrado de $m=65$ con clave pública: $m^e \: mod \: n = 2790=c$
6) Descifrado de $c=2790$: $2790^d \: mod \: n = 65$

---

#### Criptografía simétrica

- Eficiente (operaciones sobre bits: *shift*, $\oplus$, ...)
- Opera sobre bloques de bits de la logitud de la clave
- Modos de operación sobre secuencias de bloques (CBC, ...)
- ***Problema***: Requiere distribuir secretos

#### Criptografía de clave pública/privada

- Ventaja: No requiere distribuir secretos (sólo las claves públicas)
- Poco eficiente: Cómputos con números muy grandes (ej: 2048 bits)

---

## Aplicaciones de la criptografía asimétrica

* *A* desea enviar un mensaje $m$ ***confidencial*** a *B*:
  - *A* envía $m$ cifrado con la clave pública de *B*: $E(m,Bob_{pk})$
* ***No repudio (firma digital)***: 
  - *A* envía a *B* $m$ ***firmado***: $(m,ds=E(hash(m), A_{sk}))$
  - *B* verifica que $hash(m)=D(ds, A_{pk})$, lo que garantiza
  - ***Integridad***: $m$ no fue modificado
  - ***No repudio (de origen)***: El mensaje fue enviado por $A$

---

## Más aplicaciones...

* ***Autenticación***:
  1) *A* envía $E(m,B_{pk})$ a *B* (un *reto* o *challenge*)
  2) *B* descifra el mensaje con su clave privada
  3) *B* envía $E(m+1,B_{sk})$ a *A*
  4) *A* descifra y verifica el valor esperado
* Nota: Si $m$ es un mensaje *viejo* enviado por *A*, capturado por *E*, es
  vulnerable a un *reply attack*: *E* envía *m* a *B* y éste creerá que es
  de *A*. *E* puede interceptar y descifrar el *challenge*.

---

## Autenticación y confidencialidad

1) *A* y *B* se autentican
2) Intercambian una clave secreta (DH, RSA, ...) de *sesión*
3) Usan esa clave secreta para cifrar los mensajes con criptografía simétrica (eficiente)
4) Al terminar la sesión esa clave secreta no debe usarse nunca más (evita
   *reply attacks*)

---

###### Distribución de claves públicas

![bg left w:95%](img/cripto-asimetrica-eve.svg)

- Este escenario muestra el problema de distribución de claves públicas

> Es necesario garantizar la asociación entre la *identidad de un sujeto* y su
> *clave pública*

---

## Public Key Infraestructure (PKI)

##### Uso de terceros de confianza

1) La clave pública de un sujeto está *firmada* por otros conocidos
   (*introducers*). Esto comúnmente se usa en ***Pretty Good Privacy (PGP)***
   para *confidencialidad* en *correo electrónico*

2) Una ***Autoridad de Certificación (CA)*** emite un *certificado firmado*
   Un certificado es un documento que básicamente contiene:
   - La *identidad del sujeto* y su *clave pública*
   - Los algoritmos y parámetros (públicos) usados
   - La firma digital de la CA del documento

---

#### PKI

- En lugar de distribuir claves públicas, se distribuyen *certificados*
- Cuando *Alice* recibe el certificado de *B*, lo puede *verificar*

###### Verificación de un certificado (sea $cert$)

1) Verificar que $cert.subjectId \equiv B$
2) Verificar que $D(cert.ds_{CA}, CA_{pk})=hash(cert)$
   * Requiere obtener (y verificar) el certificado de la CA, el cual está
     firmado por una CA superior. Una CA *raíz* su certificado está
     *auto-firmado*

---

![bg left w:95%](img/csr.svg)

###### Gestión de un certificado

1. *Alice* genera sus claves
2. Envía el *CSR* a la CA
3. La CA *verifica su identidad*: Le pide pruebas (DNI, ...)
4. La CA *firma* el *CSR*
5. Envía el certificado firmado a *Alice*
6. Alice puede publicar o distribuir su certificado

---

### Protocolos de comunicaciones seguros

1. Autenticación basada en criptografía de clave pública/privada
2. Intercambian secretamente una *clave de sesión sk*
3. Cifran los mensajes usando criptografía simétrica con clave *sk*

Algunos ejemplos:

- *Secure Sockets Layer (SSL)* y *Transport Layer for Security (TLS)*
- *HTTPS*: *HTTP* (web) sobre *SSL/TLS*
- *FTPS*: *File Transport Protocol (FTP)* sobre *SSL/TLS*

---

###### Volviendo a nuestra pregunta inicial

1. Visitemos https://www.gogle.com
1. El navegador recibió un certificado del sitio
2. El certificado es de *google.com*
   - Es auténtico, pero...
   - La *identidad no coincide*: $google.com \neq gogle.com$
3. El navegador nos advierte: *SSL_ERROR_BAD_CERT_DOMAIN*

![bg right:40% w:95%](img/insecure-site.png)

---

### Desarrollo de aplicaciones seguras

#### Cómo evitar *vulnerabilidades*
  
* No crashes. Afecta a la *disponbibilidad*
* Filtrar (sanitizar) inputs
* Uso de componentes sin vulnerabilidades
* Incorporar seguridad en el proceso de desarrollo
  - Diseño seguro (control de acceso, no filtrar secretos, ...)
  - Usar herramientas: Detectores de vulnerabilidades en componentes, análisis
    estático, revisión de código, etc
  - Testing de seguridad

---

### Desarrollo de aplicaciones seguras

#### Algunas herramientas

- Github [CodeQL](https://codeql.github.com/)
- Web apps: [OWASP ASST](https://github.com/OWASP/ASST) y [ZAP](https://www.zaproxy.org/)
- Análisis estático: [SonarCube](https://www.sonarsource.com/), ...
- Análisis de dependencias vulnerables: [OWASP Dependency
  Check](https://owasp.org/www-project-dependency-check/)
- ... y muchas más

---

## Otros aspectos de la seguridad

* Aspectos legales
  - Ley de delitos informáticos (26388) del código penal
  - Ley de protección de datos personales (25326)

* Aspectos éticos
  - Testing de seguridad y *penetration testing* (*white hat testing*)
  - Debe hacerse bajo el *consentimiento firmado* del usuario
  - Se debe garantizar no producir daños y revelación de datos confidenciales

---

### Algunas herramientas (libres) y enlaces útiles

- [OpenSSL](https://www.openssl.org/): *CLI* y *biblioteca* con implementaciones de algoritmos criptográficos, PKI y protocolos (TLS, ...) seguros
- [OWASP](https://owasp.org/): Open web Application Security Project
  - Aplicaciones, tutoriales, documentación sobre vulnerabilidades, ataques, herramientas, técnicas, etc
- *Bases de datos de vulnerabilidades (CVE database)*:
  - [US National Vulnerability Database](https://nvd.nist.gov/)
  - [Mitre CVE](https://cve.mitre.org/)
- [Firma digital en Argentina](https://www.argentina.gob.ar/jefatura/innovacion-publica/innovacion-administrativa/firma-digital)

---

<!-- _class: lead invert-->

# Gracias!

## ¿Preguntas?

Esta presentación está disponible en [seguridad intro](https://marceloarroyo.gitlal.io/slides/seguridad-intro/seguridad-intro.html ':ignore')

<!-- Custom style and scripts -->
<style>
figure {background-color: white !important;}
blockquote {
    border: solid 1px black;
    border-radius: 15px;
    padding: 0.5em;
    color: white;
    background-color: #333;
}
input {
    font-size:1rem;
    width: 3rem;
}
</style>
<script type="text/javascript" src="crypto.js"></script>