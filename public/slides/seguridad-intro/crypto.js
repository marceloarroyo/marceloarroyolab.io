// ============================================================================
// utils
// ============================================================================
function hexStringToUint8Array(hexString)
{
    if (hexString.length % 2 != 0)
        throw "Invalid hexString";
    var arrayBuffer = new Uint8Array(hexString.length / 2);

    for (var i = 0; i < hexString.length; i += 2) {
        var byteValue = parseInt(hexString.substr(i, 2), 16);
        if (byteValue == NaN)
            throw "Invalid hexString";
        arrayBuffer[i/2] = byteValue;
    }

    return arrayBuffer;
}
function bytesToHexString(bytes)
{
    if (!bytes)
        return null;

    bytes = new Uint8Array(bytes);
    var hexBytes = [];

    for (var i = 0; i < bytes.length; ++i) {
        var byteString = bytes[i].toString(16);
        if (byteString.length < 2)
            byteString = "0" + byteString;
        hexBytes.push(byteString);
    }

    return hexBytes.join("");
}
function asciiToUint8Array(str)
{
    var chars = [];
    for (var i = 0; i < str.length; ++i)
        chars.push(str.charCodeAt(i));
    return new Uint8Array(chars);
}
function bytesToASCIIString(bytes)
{
    return String.fromCharCode.apply(null, new Uint8Array(bytes));
}
function failAndLog(error)
{
    console.log(error);
}

// ============================================================================
// hash
// ============================================================================
async function hash(string) {
    const utf8 = new TextEncoder().encode(string);
    const hashBuffer = await crypto.subtle.digest('SHA-256', utf8);
    const hashArray = Array.from(new Uint8Array(hashBuffer));
    function toHex(bytes) {
      return bytes.toString(16).padStart(2, '0');
    }
    const hashHex = hashArray.map(toHex).join('');
    return hashHex;
}

function calcHash() {
    hash(document.getElementById('hashtext').value)
    .then(function(result) {
        document.getElementById('hashresult').value += 'Hash: ' + result + '\n';
    });
}

// ============================================================================
// Diffie-Hellman
// ============================================================================
function dh(baseId, exponentId, resultId) {
    let b = document.getElementById(baseId).value
    let p = document.getElementById('dhp').value
    let e = document.getElementById(exponentId).value
    let value = (b ** e) % p
    document.getElementById(resultId).value = value
    console.log(`dh(${b},${e},${p})=${value}`)
}

// ============================================================================
// symetric criptography
// ============================================================================
var iv = crypto.getRandomValues(new Uint8Array(16));

function AES_CBC_encrypt()
{
    let keyData = hexStringToUint8Array(document.getElementById('key').value.trim())
    crypto.subtle.importKey("raw", keyData, "aes-cbc", false, ["encrypt"]).then(function(key) {
        let plainText = document.getElementById("plaintext").value;
        return crypto.subtle.encrypt({name: "aes-cbc", iv: iv}, key, asciiToUint8Array(plainText));
    }, failAndLog).then(function(cipherText) {
        document.getElementById("plaintext").value = '';
        document.getElementById("ciphertext").innerText = bytesToHexString(cipherText);
    }, failAndLog);
}

function AES_CBC_decrypt()
{
    let keyData = hexStringToUint8Array(document.getElementById('key').value.trim())
    crypto.subtle.importKey("raw", keyData, "aes-cbc", false, ["decrypt"]).then(function(key) {
        var cipherText = document.getElementById("ciphertext").innerText;
        return crypto.subtle.decrypt({name: "aes-cbc", iv: iv}, key, hexStringToUint8Array(cipherText));
    }, failAndLog).then(function(plainText) {
        document.getElementById("plaintext").value = bytesToASCIIString(plainText);
    }, failAndLog);
}