// toogle (start/stop) animation
function toggleAnim() {
    if (animation.paused) {
        animation.paused = false
        animation.start()
    }
    else
        animation.paused = true
}

// animate a circle (no gravity, no restitution, no limits)
function circle1(canvasId) {
    let c1 = new Circle(15,100, 3,0, 10, 'blue')
    let c2 = new Circle(485,50, -1,0.2, 10, 'red')
    c1.dumping = 1
    c1.traction = 1
    c1.limits = false
    c2.dumping = 1
    c2.traction = 1
    c2.limits = false
    animation.clear()
    animation.gravity = 0
    animation.addObject(c1)
    animation.addObject(c2)
    animation.setCanvas(canvasId)
    animation.start()
}

// set limits
function circle2(canvasId) {
    let c = new Circle(15,150, 3,1, 10, 'red')
    c.dumping = 1
    c.traction = 1
    c.limits = true
    animation.clear()
    animation.gravity = 0
    animation.addObject(c) 
    animation.setCanvas(canvasId)
    animation.start()
}

// gravity without friction and energy loose
function circle3(canvasId) {
    let c = new Circle(15,150, 3,0, 10, 'red')
    c.dumping = 1
    c.traction = 1
    c.limits = true
    animation.clear()
    animation.addObject(c)
    animation.gravity = 0.2
    animation.setCanvas(canvasId)
    animation.start()
}

// gravity with traction and energy loose on collission
function circle4(canvasId) {
    let c = new Circle(15,150, 3,0, 10, 'red')
    c.limits = true
    animation.clear()
    animation.addObject(c)
    animation.gravity = 0.2
    animation.setCanvas(canvasId)
    animation.start()
}

// circles collision detection
function circles5(canvasId) {
    let c1 = new Circle(15,150, 2,0, 10, 'red')
    let c2 = new Circle(280,150, -2,0, 15, 'blue')
    c1.limits = true
    c2.limits = true
    c1.dumping = 1; c1.traction = 1
    c2.dumping = 1; c2.traction = 1
    animation.clear()
    animation.addObject(c1)
    animation.addObject(c2)
    animation.setCanvas(canvasId)
    animation.gravity = 0
    animation.start()
}

// colliding circles
function collidingCircles(canvasId) {
    let c1 = new Circle(15,150, 2,1, 10, 'red')
    let c2 = new Circle(280,20, -2,0, 15, 'blue')
    let c3 = new Circle(20,20, 2,4, 5, 'green')
    c1.limits = true
    c2.limits = true
    c3.limits = true
    c1.dumping = 1; c1.traction = 1
    c2.dumping = 1; c2.traction = 1
    c3.dumping = 1; c3.traction = 1
    c1.collisionResponse = true
    c2.collisionResponse = true
    c3.collisionResponse = true
    animation.clear()
    animation.addObject(c1)
    animation.addObject(c2)
    animation.addObject(c3)
    animation.setCanvas(canvasId)
    animation.gravity = 0
    animation.start()
}
