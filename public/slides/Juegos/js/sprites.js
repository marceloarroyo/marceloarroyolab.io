let spritesPaused = true

let ssheet = new Image()
ssheet.src = 'assets/sprite-sheet.png'

let canvas = null
let ctx = null
const rows = 2
const cols = 4
const sw = 2048 / cols 
const sh = 512 / rows
let sx = 0, sy = 0
let tout = null

function animateSpriteSheet() {
    const cw = canvas.width, ch = canvas.height
    if (!spritesPaused) {
        tout = setTimeout(animateSpriteSheet, 1000/20)
        ctx.drawImage(ssheet, sx, sy, sw, sh, 0, 0, cw, ch)
        if (sx >= sw * cols) {
            sx = 0
            sy = sy == 0 ? sh : 0
        } else
            sx += sw
    }
}

// toogle sprite animation
function toggleSprites() {
    canvas = document.getElementById('anim0')
    ctx = canvas.getContext('2d')
    if (spritesPaused) {
        spritesPaused = false
        animateSpriteSheet()
    } else {
        spritesPaused = true
        clearTimeout(tout)
    }
}
