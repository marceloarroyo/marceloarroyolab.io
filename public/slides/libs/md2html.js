// simple markdown to html for slides presentations

function inline(text) {
    return text.
    replaceAll(/`([^`]+)`/gm, (_,code) => `<code>${code}</code>`).
    replaceAll(/\$([^\$]+)\$/gm, (fullMatch, latex) =>
        katex ? katex.renderToString(latex) : fullMatch
    ).
    replaceAll(/\*\*([^*]+)\*\*/gm, '<b>$1</b>').
    replaceAll(/[_*]([^_*]+)[_*]/gm, '<em>$1</em>').
    replaceAll(/!\[([^\]]*)\]\(([^ ]+) ?([^\)]*)\)/gm,
               '<img alt="$1" src="$2" $3>')
}

function md2html(text) {
    const blocks = text.trim().split(/(\n\s*){2,}/)
    const headerRE = /^\s*(#{1,6}) (.*)$/
    const itemsRE = /^\s*([-\*]|\d+[\.\)]) /m
    const bqRE = /^\s*> /gm
    const codeRE = /^\s*[`~]{3,} ?(.*)$/
    let html = ''
    let match = null
    let bn = 0
    while (bn < blocks.length) {
        let b = blocks[bn]
        
        // skip empty (blank) paragraphs
        if (b.trim().length == 0) { bn++; continue }
        
        // header
        if (match = b.match(headerRE)) {
            const l = match[1].length
            const inner = inline(match[2])
            html += `<h${l}>${inner}</h${l}>\n`
            bn++; continue
        }
        
        // items
        if (match = b.match(itemsRE)) {
            const tag = (match[1] == '-' || match[1] == '*') ? 'ul' : 'ol'
            const lines = b.split('\n')
            html += `<${tag}>\n`
            for (const line of lines) {
                let next = ''
                match = line.match(itemsRE)
                if (match[1] == '*' || match[1].endsWith(')'))
                    next = ' class="next"'
                if (!match) {
                    html += inline(line)
                } else {
                    const inner = inline(line.substring(match[0].length))
                    html += `<li${next}>${inner}</li>\n`
                }
            }
            html += `</${tag}>\n`
            bn++; continue
        }
        
        // code block
        if (match = b.match(codeRE)) {
            const lines = b.split('\n')
            const lang = match[1] && match[1].length > 0 ? match[1] : null
            const cls = lang ? ` class="language-${lang}"` : ''
            let   lastLine = lines[lines.length-1]
            html += `<pre><code${cls}>\n`
            while (!lastLine.match(codeRE)) {
                html += b
                b = blocks[++bn];
                lines = b.split('\n')
                lastLine = lines[lines.length-1]
            }
            html += lines.slice(1, lines.length-1).join('\n')
            html += '\n</code></pre>\n'
            bn++; continue
        }
        
        // blockquote
        if (b.match(bqRE)) {
            const inner = inline(b.replaceAll(bqRE, ''))
            html += `<blockquote>${md2html(inner)}</blockquote>\n`
            bn++; continue
        }
        
        // table
        const lines = b.split('\n')
        const justRow = lines.length > 1 ? lines[1].split(' | ') : []
        if (justRow.length >= 2 && justRow[0].match(/^\s*\:?\-+\:?/)) {
            html += '<table>\n'
            const just = []
            justRow.foreach((col) => {
                col = col.trim()
                if (col.startsWith(':') && col.endsWith(':')) {
                    just.push('center')
                } else if (col.endsWith(':')) {
                    just.push('right')
                } else {
                    just.push('left')
                }
            })
            for (let i=0; i<lines.length; i++) {
                if (i == 1) continue
                const cols = lines[i].split(' | ')
                html += (i == 0) ? '<th>\n' : '<tr>\n'
                for (let j = 0; j < cols.length; j++) {
                    const style = `style="text-justify: ${just[j]}"`
                    const text = inline(cols[j])
                    html += `<td ${style}>${text}</td>\n`
                }
                html += (i == 0) ? '</th>\n' : '</tr>\n'
            }
            html += '</table>\n'
            bn++; continue
        }

        b = b.trim()

        // html or paragraph
        if (b.startsWith('<') && b.endsWith('>'))
            html += b + '\n'
        else
            html += `<p>${inline(b)}</p>\n`
        bn++
    }
    return html
}

// load markdown slides file and put before given element selector
async function loadMarkdown(args) {
    if (!args.url && (!args.before || !args.container)) return
    const response = await fetch(args.url)
    const text = await response.text()
    if (args.slides) {
        const slides = text.split(/^-{5,} *$/m)
        for (const slide of slides) {
            const html = md2html(slide.trim())
            const section = '<section class="slide">' + html + '</section>'
            insertHTML(section, args)
        }
        showerInit()
    } else {
        insertHTML(md2html(text.trim()), args)
    }
}

// put html in given element selector (before or inside)
function insertHTML(html, where) {
    let element = document.querySelector(where.before || where.container)
    if (where.before) {
        element.insertAdjacentHTML('beforebegin', html)
    }
    else if (where.container) {
        element.append(html)
    }
}
